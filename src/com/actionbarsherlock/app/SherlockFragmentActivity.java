package com.actionbarsherlock.app;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app._ActionBarSherlockTrojanHorse;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import com.actionbarsherlock.ActionBarSherlock;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import static com.actionbarsherlock.ActionBarSherlock.OnActionModeFinishedListener;
import static com.actionbarsherlock.ActionBarSherlock.OnActionModeStartedListener;

// TODO: Auto-generated Javadoc
/**
 * The Class SherlockFragmentActivity.
 *
 * @see {@link _ActionBarSherlockTrojanHorse}
 */
public class SherlockFragmentActivity extends _ActionBarSherlockTrojanHorse implements OnActionModeStartedListener, OnActionModeFinishedListener {
    
    /** The Constant DEBUG. */
    private static final boolean DEBUG = false;
    
    /** The Constant TAG. */
    private static final String TAG = "SherlockFragmentActivity";

    /** The m sherlock. */
    private ActionBarSherlock mSherlock;
    
    /** The m ignore native create. */
    private boolean mIgnoreNativeCreate = false;
    
    /** The m ignore native prepare. */
    private boolean mIgnoreNativePrepare = false;
    
    /** The m ignore native selected. */
    private boolean mIgnoreNativeSelected = false;

    /**
     * Gets the sherlock.
     *
     * @return the sherlock
     */
    protected final ActionBarSherlock getSherlock() {
        if (mSherlock == null) {
            mSherlock = ActionBarSherlock.wrap(this, ActionBarSherlock.FLAG_DELEGATE);
        }
        return mSherlock;
    }


    ///////////////////////////////////////////////////////////////////////////
    // Action bar and mode
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Gets the support action bar.
     *
     * @return the support action bar
     */
    public ActionBar getSupportActionBar() {
        return getSherlock().getActionBar();
    }

    /**
     * Start action mode.
     *
     * @param callback the callback
     * @return the action mode
     * @see android.app.Activity#startActionMode(android.view.ActionMode.Callback)
     */
    public ActionMode startActionMode(ActionMode.Callback callback) {
        return getSherlock().startActionMode(callback);
    }

    /**
     * On action mode started.
     *
     * @param mode the mode
     * @see android.app.Activity#onActionModeStarted(android.view.ActionMode)
     */
    @Override
    public void onActionModeStarted(ActionMode mode) {}

    /**
     * On action mode finished.
     *
     * @param mode the mode
     * @see android.app.Activity#onActionModeFinished(android.view.ActionMode)
     */
    @Override
    public void onActionModeFinished(ActionMode mode) {}


    ///////////////////////////////////////////////////////////////////////////
    // General lifecycle/callback dispatching
    ///////////////////////////////////////////////////////////////////////////

    /**
     * On configuration changed.
     *
     * @param newConfig the new config
     * @see android.support.v4.app.FragmentActivity#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getSherlock().dispatchConfigurationChanged(newConfig);
    }

    /**
     * On post resume.
     *
     * @see android.support.v4.app.FragmentActivity#onPostResume()
     */
    @Override
    protected void onPostResume() {
        super.onPostResume();
        getSherlock().dispatchPostResume();
    }

    /**
     * On pause.
     *
     * @see android.support.v4.app.FragmentActivity#onPause()
     */
    @Override
    protected void onPause() {
        getSherlock().dispatchPause();
        super.onPause();
    }

    /**
     * On stop.
     *
     * @see android.support.v4.app.FragmentActivity#onStop()
     */
    @Override
    protected void onStop() {
        getSherlock().dispatchStop();
        super.onStop();
    }

    /**
     * On destroy.
     *
     * @see android.support.v4.app.FragmentActivity#onDestroy()
     */
    @Override
    protected void onDestroy() {
        getSherlock().dispatchDestroy();
        super.onDestroy();
    }

    /**
     * On post create.
     *
     * @param savedInstanceState the saved instance state
     * @see android.app.Activity#onPostCreate(android.os.Bundle)
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        getSherlock().dispatchPostCreate(savedInstanceState);
        super.onPostCreate(savedInstanceState);
    }

    /**
     * On title changed.
     *
     * @param title the title
     * @param color the color
     * @see android.app.Activity#onTitleChanged(java.lang.CharSequence, int)
     */
    @Override
    protected void onTitleChanged(CharSequence title, int color) {
        getSherlock().dispatchTitleChanged(title, color);
        super.onTitleChanged(title, color);
    }

    /**
     * On menu opened.
     *
     * @param featureId the feature id
     * @param menu the menu
     * @return true, if successful
     * @see android.app.Activity#onMenuOpened(int, android.view.Menu)
     */
    @Override
    public final boolean onMenuOpened(int featureId, android.view.Menu menu) {
        if (getSherlock().dispatchMenuOpened(featureId, menu)) {
            return true;
        }
        return super.onMenuOpened(featureId, menu);
    }

    /**
     * On panel closed.
     *
     * @param featureId the feature id
     * @param menu the menu
     * @see android.support.v4.app.FragmentActivity#onPanelClosed(int, android.view.Menu)
     */
    @Override
    public void onPanelClosed(int featureId, android.view.Menu menu) {
        getSherlock().dispatchPanelClosed(featureId, menu);
        super.onPanelClosed(featureId, menu);
    }

    /**
     * Dispatch key event.
     *
     * @param event the event
     * @return true, if successful
     * @see android.app.Activity#dispatchKeyEvent(android.view.KeyEvent)
     */
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (getSherlock().dispatchKeyEvent(event)) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }


    ///////////////////////////////////////////////////////////////////////////
    // Native menu handling
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Gets the support menu inflater.
     *
     * @return the support menu inflater
     * @see android.support.v4.app._ActionBarSherlockTrojanHorse#getSupportMenuInflater()
     */
    public MenuInflater getSupportMenuInflater() {
        if (DEBUG) Log.d(TAG, "[getSupportMenuInflater]");

        return getSherlock().getMenuInflater();
    }

    /**
     * Invalidate options menu.
     *
     * @see android.app.Activity#invalidateOptionsMenu()
     */
    public void invalidateOptionsMenu() {
        if (DEBUG) Log.d(TAG, "[invalidateOptionsMenu]");

        getSherlock().dispatchInvalidateOptionsMenu();
    }

    /**
     * Support invalidate options menu.
     *
     * @see android.support.v4.app.FragmentActivity#supportInvalidateOptionsMenu()
     */
    public void supportInvalidateOptionsMenu() {
        if (DEBUG) Log.d(TAG, "[supportInvalidateOptionsMenu]");

        invalidateOptionsMenu();
    }

    /**
     * On create panel menu.
     *
     * @param featureId the feature id
     * @param menu the menu
     * @return true, if successful
     * @see android.support.v4.app._ActionBarSherlockTrojanHorse#onCreatePanelMenu(int, com.actionbarsherlock.view.Menu)
     */
    @Override
    public final boolean onCreatePanelMenu(int featureId, android.view.Menu menu) {
        if (DEBUG) Log.d(TAG, "[onCreatePanelMenu] featureId: " + featureId + ", menu: " + menu);

        if (featureId == Window.FEATURE_OPTIONS_PANEL && !mIgnoreNativeCreate) {
            mIgnoreNativeCreate = true;
            boolean result = getSherlock().dispatchCreateOptionsMenu(menu);
            mIgnoreNativeCreate = false;

            if (DEBUG) Log.d(TAG, "[onCreatePanelMenu] returning " + result);
            return result;
        }
        return super.onCreatePanelMenu(featureId, menu);
    }

    /**
     * On create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     * @see android.support.v4.app._ActionBarSherlockTrojanHorse#onCreateOptionsMenu(com.actionbarsherlock.view.Menu)
     */
    @Override
    public final boolean onCreateOptionsMenu(android.view.Menu menu) {
        return true;
    }

    /**
     * On prepare panel.
     *
     * @param featureId the feature id
     * @param view the view
     * @param menu the menu
     * @return true, if successful
     * @see android.support.v4.app._ActionBarSherlockTrojanHorse#onPreparePanel(int, android.view.View, com.actionbarsherlock.view.Menu)
     */
    @Override
    public final boolean onPreparePanel(int featureId, View view, android.view.Menu menu) {
        if (DEBUG) Log.d(TAG, "[onPreparePanel] featureId: " + featureId + ", view: " + view + ", menu: " + menu);

        if (featureId == Window.FEATURE_OPTIONS_PANEL && !mIgnoreNativePrepare) {
            mIgnoreNativePrepare = true;
            boolean result = getSherlock().dispatchPrepareOptionsMenu(menu);
            mIgnoreNativePrepare = false;

            if (DEBUG) Log.d(TAG, "[onPreparePanel] returning " + result);
            return result;
        }
        return super.onPreparePanel(featureId, view, menu);
    }

    /**
     * On prepare options menu.
     *
     * @param menu the menu
     * @return true, if successful
     * @see android.support.v4.app._ActionBarSherlockTrojanHorse#onPrepareOptionsMenu(com.actionbarsherlock.view.Menu)
     */
    @Override
    public final boolean onPrepareOptionsMenu(android.view.Menu menu) {
        return true;
    }

    /**
     * On menu item selected.
     *
     * @param featureId the feature id
     * @param item the item
     * @return true, if successful
     * @see android.support.v4.app._ActionBarSherlockTrojanHorse#onMenuItemSelected(int, com.actionbarsherlock.view.MenuItem)
     */
    @Override
    public final boolean onMenuItemSelected(int featureId, android.view.MenuItem item) {
        if (DEBUG) Log.d(TAG, "[onMenuItemSelected] featureId: " + featureId + ", item: " + item);

        if (featureId == Window.FEATURE_OPTIONS_PANEL && !mIgnoreNativeSelected) {
            mIgnoreNativeSelected = true;
            boolean result = getSherlock().dispatchOptionsItemSelected(item);
            mIgnoreNativeSelected = false;

            if (DEBUG) Log.d(TAG, "[onMenuItemSelected] returning " + result);
            return result;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    /**
     * On options item selected.
     *
     * @param item the item
     * @return true, if successful
     * @see android.support.v4.app._ActionBarSherlockTrojanHorse#onOptionsItemSelected(com.actionbarsherlock.view.MenuItem)
     */
    @Override
    public final boolean onOptionsItemSelected(android.view.MenuItem item) {
        return false;
    }

    /**
     * Open options menu.
     *
     * @see android.app.Activity#openOptionsMenu()
     */
    @Override
    public void openOptionsMenu() {
        if (!getSherlock().dispatchOpenOptionsMenu()) {
            super.openOptionsMenu();
        }
    }

    /**
     * Close options menu.
     *
     * @see android.app.Activity#closeOptionsMenu()
     */
    @Override
    public void closeOptionsMenu() {
        if (!getSherlock().dispatchCloseOptionsMenu()) {
            super.closeOptionsMenu();
        }
    }


    ///////////////////////////////////////////////////////////////////////////
    // Sherlock menu handling
    ///////////////////////////////////////////////////////////////////////////

    /**
     * On create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     * @see android.support.v4.app._ActionBarSherlockTrojanHorse#onCreateOptionsMenu(com.actionbarsherlock.view.Menu)
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /**
     * On prepare options menu.
     *
     * @param menu the menu
     * @return true, if successful
     * @see android.support.v4.app._ActionBarSherlockTrojanHorse#onPrepareOptionsMenu(com.actionbarsherlock.view.Menu)
     */
    public boolean onPrepareOptionsMenu(Menu menu) {
        return true;
    }

    /**
     * On options item selected.
     *
     * @param item the item
     * @return true, if successful
     * @see android.support.v4.app._ActionBarSherlockTrojanHorse#onOptionsItemSelected(com.actionbarsherlock.view.MenuItem)
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }


    ///////////////////////////////////////////////////////////////////////////
    // Content
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Adds the content view.
     *
     * @param view the view
     * @param params the params
     * @see android.app.Activity#addContentView(android.view.View, android.view.ViewGroup.LayoutParams)
     */
    @Override
    public void addContentView(View view, LayoutParams params) {
        getSherlock().addContentView(view, params);
    }

    /**
     * Sets the content view.
     *
     * @param layoutResId the new content view
     * @see android.app.Activity#setContentView(int)
     */
    @Override
    public void setContentView(int layoutResId) {
        getSherlock().setContentView(layoutResId);
    }

    /**
     * Sets the content view.
     *
     * @param view the view
     * @param params the params
     * @see android.app.Activity#setContentView(android.view.View, android.view.ViewGroup.LayoutParams)
     */
    @Override
    public void setContentView(View view, LayoutParams params) {
        getSherlock().setContentView(view, params);
    }

    /**
     * Sets the content view.
     *
     * @param view the new content view
     * @see android.app.Activity#setContentView(android.view.View)
     */
    @Override
    public void setContentView(View view) {
        getSherlock().setContentView(view);
    }

    /**
     * Request window feature.
     *
     * @param featureId the feature id
     */
    public void requestWindowFeature(long featureId) {
        getSherlock().requestFeature((int)featureId);
    }


    ///////////////////////////////////////////////////////////////////////////
    // Progress Indication
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Sets the support progress.
     *
     * @param progress the new support progress
     */
    public void setSupportProgress(int progress) {
        getSherlock().setProgress(progress);
    }

    /**
     * Sets the support progress bar indeterminate.
     *
     * @param indeterminate the new support progress bar indeterminate
     */
    public void setSupportProgressBarIndeterminate(boolean indeterminate) {
        getSherlock().setProgressBarIndeterminate(indeterminate);
    }

    /**
     * Sets the support progress bar indeterminate visibility.
     *
     * @param visible the new support progress bar indeterminate visibility
     */
    public void setSupportProgressBarIndeterminateVisibility(boolean visible) {
        getSherlock().setProgressBarIndeterminateVisibility(visible);
    }

    /**
     * Sets the support progress bar visibility.
     *
     * @param visible the new support progress bar visibility
     */
    public void setSupportProgressBarVisibility(boolean visible) {
        getSherlock().setProgressBarVisibility(visible);
    }

    /**
     * Sets the support secondary progress.
     *
     * @param secondaryProgress the new support secondary progress
     */
    public void setSupportSecondaryProgress(int secondaryProgress) {
        getSherlock().setSecondaryProgress(secondaryProgress);
    }
}
