package com.actionbarsherlock.app;

import android.app.ListActivity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import com.actionbarsherlock.ActionBarSherlock;
import com.actionbarsherlock.ActionBarSherlock.OnActionModeFinishedListener;
import com.actionbarsherlock.ActionBarSherlock.OnActionModeStartedListener;
import com.actionbarsherlock.ActionBarSherlock.OnCreatePanelMenuListener;
import com.actionbarsherlock.ActionBarSherlock.OnMenuItemSelectedListener;
import com.actionbarsherlock.ActionBarSherlock.OnPreparePanelListener;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

// TODO: Auto-generated Javadoc
/**
 * The Class SherlockListActivity.
 */
public abstract class SherlockListActivity extends ListActivity implements OnCreatePanelMenuListener, OnPreparePanelListener, OnMenuItemSelectedListener, OnActionModeStartedListener, OnActionModeFinishedListener {
    
    /** The m sherlock. */
    private ActionBarSherlock mSherlock;

    /**
     * Gets the sherlock.
     *
     * @return the sherlock
     */
    protected final ActionBarSherlock getSherlock() {
        if (mSherlock == null) {
            mSherlock = ActionBarSherlock.wrap(this, ActionBarSherlock.FLAG_DELEGATE);
        }
        return mSherlock;
    }


    ///////////////////////////////////////////////////////////////////////////
    // Action bar and mode
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Gets the support action bar.
     *
     * @return the support action bar
     */
    public ActionBar getSupportActionBar() {
        return getSherlock().getActionBar();
    }

    /**
     * Start action mode.
     *
     * @param callback the callback
     * @return the action mode
     * @see android.app.Activity#startActionMode(android.view.ActionMode.Callback)
     */
    public ActionMode startActionMode(ActionMode.Callback callback) {
        return getSherlock().startActionMode(callback);
    }

    /**
     * On action mode started.
     *
     * @param mode the mode
     * @see android.app.Activity#onActionModeStarted(android.view.ActionMode)
     */
    @Override
    public void onActionModeStarted(ActionMode mode) {}

    /**
     * On action mode finished.
     *
     * @param mode the mode
     * @see android.app.Activity#onActionModeFinished(android.view.ActionMode)
     */
    @Override
    public void onActionModeFinished(ActionMode mode) {}


    ///////////////////////////////////////////////////////////////////////////
    // General lifecycle/callback dispatching
    ///////////////////////////////////////////////////////////////////////////

    /**
     * On configuration changed.
     *
     * @param newConfig the new config
     * @see android.app.Activity#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getSherlock().dispatchConfigurationChanged(newConfig);
    }

    /**
     * On post resume.
     *
     * @see android.app.Activity#onPostResume()
     */
    @Override
    protected void onPostResume() {
        super.onPostResume();
        getSherlock().dispatchPostResume();
    }

    /**
     * On pause.
     *
     * @see android.app.Activity#onPause()
     */
    @Override
    protected void onPause() {
        getSherlock().dispatchPause();
        super.onPause();
    }

    /**
     * On stop.
     *
     * @see android.app.Activity#onStop()
     */
    @Override
    protected void onStop() {
        getSherlock().dispatchStop();
        super.onStop();
    }

    /**
     * On destroy.
     *
     * @see android.app.ListActivity#onDestroy()
     */
    @Override
    protected void onDestroy() {
        getSherlock().dispatchDestroy();
        super.onDestroy();
    }

    /**
     * On post create.
     *
     * @param savedInstanceState the saved instance state
     * @see android.app.Activity#onPostCreate(android.os.Bundle)
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        getSherlock().dispatchPostCreate(savedInstanceState);
        super.onPostCreate(savedInstanceState);
    }

    /**
     * On title changed.
     *
     * @param title the title
     * @param color the color
     * @see android.app.Activity#onTitleChanged(java.lang.CharSequence, int)
     */
    @Override
    protected void onTitleChanged(CharSequence title, int color) {
        getSherlock().dispatchTitleChanged(title, color);
        super.onTitleChanged(title, color);
    }

    /**
     * On menu opened.
     *
     * @param featureId the feature id
     * @param menu the menu
     * @return true, if successful
     * @see android.app.Activity#onMenuOpened(int, android.view.Menu)
     */
    @Override
    public final boolean onMenuOpened(int featureId, android.view.Menu menu) {
        if (getSherlock().dispatchMenuOpened(featureId, menu)) {
            return true;
        }
        return super.onMenuOpened(featureId, menu);
    }

    /**
     * On panel closed.
     *
     * @param featureId the feature id
     * @param menu the menu
     * @see android.app.Activity#onPanelClosed(int, android.view.Menu)
     */
    @Override
    public void onPanelClosed(int featureId, android.view.Menu menu) {
        getSherlock().dispatchPanelClosed(featureId, menu);
        super.onPanelClosed(featureId, menu);
    }

    /**
     * Dispatch key event.
     *
     * @param event the event
     * @return true, if successful
     * @see android.app.Activity#dispatchKeyEvent(android.view.KeyEvent)
     */
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (getSherlock().dispatchKeyEvent(event)) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }


    ///////////////////////////////////////////////////////////////////////////
    // Native menu handling
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Gets the support menu inflater.
     *
     * @return the support menu inflater
     */
    public MenuInflater getSupportMenuInflater() {
        return getSherlock().getMenuInflater();
    }

    /**
     * Invalidate options menu.
     *
     * @see android.app.Activity#invalidateOptionsMenu()
     */
    public void invalidateOptionsMenu() {
        getSherlock().dispatchInvalidateOptionsMenu();
    }

    /**
     * Support invalidate options menu.
     */
    public void supportInvalidateOptionsMenu() {
        invalidateOptionsMenu();
    }

    /**
     * On create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public final boolean onCreateOptionsMenu(android.view.Menu menu) {
        return getSherlock().dispatchCreateOptionsMenu(menu);
    }

    /**
     * On prepare options menu.
     *
     * @param menu the menu
     * @return true, if successful
     * @see android.app.Activity#onPrepareOptionsMenu(android.view.Menu)
     */
    @Override
    public final boolean onPrepareOptionsMenu(android.view.Menu menu) {
        return getSherlock().dispatchPrepareOptionsMenu(menu);
    }

    /**
     * On options item selected.
     *
     * @param item the item
     * @return true, if successful
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public final boolean onOptionsItemSelected(android.view.MenuItem item) {
        return getSherlock().dispatchOptionsItemSelected(item);
    }

    /**
     * Open options menu.
     *
     * @see android.app.Activity#openOptionsMenu()
     */
    @Override
    public void openOptionsMenu() {
        if (!getSherlock().dispatchOpenOptionsMenu()) {
            super.openOptionsMenu();
        }
    }

    /**
     * Close options menu.
     *
     * @see android.app.Activity#closeOptionsMenu()
     */
    @Override
    public void closeOptionsMenu() {
        if (!getSherlock().dispatchCloseOptionsMenu()) {
            super.closeOptionsMenu();
        }
    }


    ///////////////////////////////////////////////////////////////////////////
    // Sherlock menu handling
    ///////////////////////////////////////////////////////////////////////////

    /**
     * On create panel menu.
     *
     * @param featureId the feature id
     * @param menu the menu
     * @return true, if successful
     * @see android.app.Activity#onCreatePanelMenu(int, android.view.Menu)
     */
    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        if (featureId == Window.FEATURE_OPTIONS_PANEL) {
            return onCreateOptionsMenu(menu);
        }
        return false;
    }

    /**
     * On create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /**
     * On prepare panel.
     *
     * @param featureId the feature id
     * @param view the view
     * @param menu the menu
     * @return true, if successful
     * @see android.app.Activity#onPreparePanel(int, android.view.View, android.view.Menu)
     */
    @Override
    public boolean onPreparePanel(int featureId, View view, Menu menu) {
        if (featureId == Window.FEATURE_OPTIONS_PANEL) {
            return onPrepareOptionsMenu(menu);
        }
        return false;
    }

    /**
     * On prepare options menu.
     *
     * @param menu the menu
     * @return true, if successful
     * @see android.app.Activity#onPrepareOptionsMenu(android.view.Menu)
     */
    public boolean onPrepareOptionsMenu(Menu menu) {
        return true;
    }

    /**
     * On menu item selected.
     *
     * @param featureId the feature id
     * @param item the item
     * @return true, if successful
     * @see android.app.Activity#onMenuItemSelected(int, android.view.MenuItem)
     */
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        if (featureId == Window.FEATURE_OPTIONS_PANEL) {
            return onOptionsItemSelected(item);
        }
        return false;
    }

    /**
     * On options item selected.
     *
     * @param item the item
     * @return true, if successful
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }


    ///////////////////////////////////////////////////////////////////////////
    // Content
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Adds the content view.
     *
     * @param view the view
     * @param params the params
     * @see android.app.Activity#addContentView(android.view.View, android.view.ViewGroup.LayoutParams)
     */
    @Override
    public void addContentView(View view, LayoutParams params) {
        getSherlock().addContentView(view, params);
    }

    /**
     * Sets the content view.
     *
     * @param layoutResId the new content view
     * @see android.app.Activity#setContentView(int)
     */
    @Override
    public void setContentView(int layoutResId) {
        getSherlock().setContentView(layoutResId);
    }

    /**
     * Sets the content view.
     *
     * @param view the view
     * @param params the params
     * @see android.app.Activity#setContentView(android.view.View, android.view.ViewGroup.LayoutParams)
     */
    @Override
    public void setContentView(View view, LayoutParams params) {
        getSherlock().setContentView(view, params);
    }

    /**
     * Sets the content view.
     *
     * @param view the new content view
     * @see android.app.Activity#setContentView(android.view.View)
     */
    @Override
    public void setContentView(View view) {
        getSherlock().setContentView(view);
    }

    /**
     * Request window feature.
     *
     * @param featureId the feature id
     */
    public void requestWindowFeature(long featureId) {
        getSherlock().requestFeature((int)featureId);
    }


    ///////////////////////////////////////////////////////////////////////////
    // Progress Indication
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Sets the support progress.
     *
     * @param progress the new support progress
     */
    public void setSupportProgress(int progress) {
        getSherlock().setProgress(progress);
    }

    /**
     * Sets the support progress bar indeterminate.
     *
     * @param indeterminate the new support progress bar indeterminate
     */
    public void setSupportProgressBarIndeterminate(boolean indeterminate) {
        getSherlock().setProgressBarIndeterminate(indeterminate);
    }

    /**
     * Sets the support progress bar indeterminate visibility.
     *
     * @param visible the new support progress bar indeterminate visibility
     */
    public void setSupportProgressBarIndeterminateVisibility(boolean visible) {
        getSherlock().setProgressBarIndeterminateVisibility(visible);
    }

    /**
     * Sets the support progress bar visibility.
     *
     * @param visible the new support progress bar visibility
     */
    public void setSupportProgressBarVisibility(boolean visible) {
        getSherlock().setProgressBarVisibility(visible);
    }

    /**
     * Sets the support secondary progress.
     *
     * @param secondaryProgress the new support secondary progress
     */
    public void setSupportSecondaryProgress(int secondaryProgress) {
        getSherlock().setSecondaryProgress(secondaryProgress);
    }
}
