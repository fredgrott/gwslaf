package com.actionbarsherlock.internal.widget;

import java.util.Locale;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

// TODO: Auto-generated Javadoc
/**
 * The Class CapitalizingTextView.
 */
public class CapitalizingTextView extends TextView {
    
    /** The Constant SANS_ICE_CREAM. */
    private static final boolean SANS_ICE_CREAM = Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    
    /** The Constant IS_GINGERBREAD. */
    private static final boolean IS_GINGERBREAD = Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;

    /** The Constant R_styleable_TextView. */
    private static final int[] R_styleable_TextView = new int[] {
        android.R.attr.textAllCaps
    };
    
    /** The Constant R_styleable_TextView_textAllCaps. */
    private static final int R_styleable_TextView_textAllCaps = 0;

    /** The m all caps. */
    private boolean mAllCaps;

    /**
     * Instantiates a new capitalizing text view.
     *
     * @param context the context
     * @param attrs the attrs
     */
    public CapitalizingTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * Instantiates a new capitalizing text view.
     *
     * @param context the context
     * @param attrs the attrs
     * @param defStyle the def style
     */
    public CapitalizingTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = context.obtainStyledAttributes(attrs, R_styleable_TextView, defStyle, 0);
        mAllCaps = a.getBoolean(R_styleable_TextView_textAllCaps, true);
        a.recycle();
    }

    /**
     * Sets the text compat.
     *
     * @param text the new text compat
     */
    public void setTextCompat(CharSequence text) {
        if (SANS_ICE_CREAM && mAllCaps && text != null) {
            if (IS_GINGERBREAD) {
                setText(text.toString().toUpperCase(Locale.ROOT));
            } else {
                setText(text.toString().toUpperCase());
            }
        } else {
            setText(text);
        }
    }
}
