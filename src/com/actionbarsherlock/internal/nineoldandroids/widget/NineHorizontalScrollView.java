package com.actionbarsherlock.internal.nineoldandroids.widget;

import android.content.Context;
import android.widget.HorizontalScrollView;
import com.actionbarsherlock.internal.nineoldandroids.view.animation.AnimatorProxy;

// TODO: Auto-generated Javadoc
/**
 * The Class NineHorizontalScrollView.
 */
public class NineHorizontalScrollView extends HorizontalScrollView {
    
    /** The m proxy. */
    private final AnimatorProxy mProxy;

    /**
     * Instantiates a new nine horizontal scroll view.
     *
     * @param context the context
     */
    public NineHorizontalScrollView(Context context) {
        super(context);
        mProxy = AnimatorProxy.NEEDS_PROXY ? AnimatorProxy.wrap(this) : null;
    }

    /**
     * Sets the visibility.
     *
     * @param visibility the new visibility
     * @see android.view.View#setVisibility(int)
     */
    @Override
    public void setVisibility(int visibility) {
        if (mProxy != null) {
            if (visibility == GONE) {
                clearAnimation();
            } else if (visibility == VISIBLE) {
                setAnimation(mProxy);
            }
        }
        super.setVisibility(visibility);
    }

    /**
     * Gets the alpha.
     *
     * @return the alpha
     * @see android.view.View#getAlpha()
     */
    public float getAlpha() {
        if (AnimatorProxy.NEEDS_PROXY) {
            return mProxy.getAlpha();
        } else {
            return super.getAlpha();
        }
    }
    
    /**
     * Sets the alpha.
     *
     * @param alpha the new alpha
     * @see android.view.View#setAlpha(float)
     */
    public void setAlpha(float alpha) {
        if (AnimatorProxy.NEEDS_PROXY) {
            mProxy.setAlpha(alpha);
        } else {
            super.setAlpha(alpha);
        }
    }
}
