package com.actionbarsherlock.internal.nineoldandroids.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.actionbarsherlock.internal.nineoldandroids.view.animation.AnimatorProxy;

// TODO: Auto-generated Javadoc
/**
 * The Class NineLinearLayout.
 */
public class NineLinearLayout extends LinearLayout {
    
    /** The m proxy. */
    private final AnimatorProxy mProxy;

    /**
     * Instantiates a new nine linear layout.
     *
     * @param context the context
     */
    public NineLinearLayout(Context context) {
        super(context);
        mProxy = AnimatorProxy.NEEDS_PROXY ? AnimatorProxy.wrap(this) : null;
    }
    
    /**
     * Instantiates a new nine linear layout.
     *
     * @param context the context
     * @param attrs the attrs
     */
    public NineLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mProxy = AnimatorProxy.NEEDS_PROXY ? AnimatorProxy.wrap(this) : null;
    }
    
    /**
     * Instantiates a new nine linear layout.
     *
     * @param context the context
     * @param attrs the attrs
     * @param defStyle the def style
     */
    public NineLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mProxy = AnimatorProxy.NEEDS_PROXY ? AnimatorProxy.wrap(this) : null;
    }

    /**
     * Sets the visibility.
     *
     * @param visibility the new visibility
     * @see android.view.View#setVisibility(int)
     */
    @Override
    public void setVisibility(int visibility) {
        if (mProxy != null) {
            if (visibility == GONE) {
                clearAnimation();
            } else if (visibility == VISIBLE) {
                setAnimation(mProxy);
            }
        }
        super.setVisibility(visibility);
    }

    /**
     * Gets the alpha.
     *
     * @return the alpha
     * @see android.view.View#getAlpha()
     */
    public float getAlpha() {
        if (AnimatorProxy.NEEDS_PROXY) {
            return mProxy.getAlpha();
        } else {
            return super.getAlpha();
        }
    }
    
    /**
     * Sets the alpha.
     *
     * @param alpha the new alpha
     * @see android.view.View#setAlpha(float)
     */
    public void setAlpha(float alpha) {
        if (AnimatorProxy.NEEDS_PROXY) {
            mProxy.setAlpha(alpha);
        } else {
            super.setAlpha(alpha);
        }
    }
    
    /**
     * Gets the translation x.
     *
     * @return the translation x
     * @see android.view.View#getTranslationX()
     */
    public float getTranslationX() {
        if (AnimatorProxy.NEEDS_PROXY) {
            return mProxy.getTranslationX();
        } else {
            return super.getTranslationX();
        }
    }
    
    /**
     * Sets the translation x.
     *
     * @param translationX the new translation x
     * @see android.view.View#setTranslationX(float)
     */
    public void setTranslationX(float translationX) {
        if (AnimatorProxy.NEEDS_PROXY) {
            mProxy.setTranslationX(translationX);
        } else {
            super.setTranslationX(translationX);
        }
    }
}
