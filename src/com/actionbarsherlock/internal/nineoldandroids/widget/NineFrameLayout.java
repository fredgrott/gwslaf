package com.actionbarsherlock.internal.nineoldandroids.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.actionbarsherlock.internal.nineoldandroids.view.animation.AnimatorProxy;

// TODO: Auto-generated Javadoc
/**
 * The Class NineFrameLayout.
 */
public class NineFrameLayout extends FrameLayout {
    
    /** The m proxy. */
    private final AnimatorProxy mProxy;

    /**
     * Instantiates a new nine frame layout.
     *
     * @param context the context
     */
    public NineFrameLayout(Context context) {
        super(context);
        mProxy = AnimatorProxy.NEEDS_PROXY ? AnimatorProxy.wrap(this) : null;
    }
    
    /**
     * Instantiates a new nine frame layout.
     *
     * @param context the context
     * @param attrs the attrs
     */
    public NineFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mProxy = AnimatorProxy.NEEDS_PROXY ? AnimatorProxy.wrap(this) : null;
    }
    
    /**
     * Instantiates a new nine frame layout.
     *
     * @param context the context
     * @param attrs the attrs
     * @param defStyle the def style
     */
    public NineFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mProxy = AnimatorProxy.NEEDS_PROXY ? AnimatorProxy.wrap(this) : null;
    }

    /**
     * Sets the visibility.
     *
     * @param visibility the new visibility
     * @see android.view.View#setVisibility(int)
     */
    @Override
    public void setVisibility(int visibility) {
        if (mProxy != null) {
            if (visibility == GONE) {
                clearAnimation();
            } else if (visibility == VISIBLE) {
                setAnimation(mProxy);
            }
        }
        super.setVisibility(visibility);
    }

    /**
     * Gets the alpha.
     *
     * @return the alpha
     * @see android.view.View#getAlpha()
     */
    public float getAlpha() {
        if (AnimatorProxy.NEEDS_PROXY) {
            return mProxy.getAlpha();
        } else {
            return super.getAlpha();
        }
    }
    
    /**
     * Sets the alpha.
     *
     * @param alpha the new alpha
     * @see android.view.View#setAlpha(float)
     */
    public void setAlpha(float alpha) {
        if (AnimatorProxy.NEEDS_PROXY) {
            mProxy.setAlpha(alpha);
        } else {
            super.setAlpha(alpha);
        }
    }
    
    /**
     * Gets the translation y.
     *
     * @return the translation y
     * @see android.view.View#getTranslationY()
     */
    public float getTranslationY() {
        if (AnimatorProxy.NEEDS_PROXY) {
            return mProxy.getTranslationY();
        } else {
            return super.getTranslationY();
        }
    }
    
    /**
     * Sets the translation y.
     *
     * @param translationY the new translation y
     * @see android.view.View#setTranslationY(float)
     */
    public void setTranslationY(float translationY) {
        if (AnimatorProxy.NEEDS_PROXY) {
            mProxy.setTranslationY(translationY);
        } else {
            super.setTranslationY(translationY);
        }
    }
}
