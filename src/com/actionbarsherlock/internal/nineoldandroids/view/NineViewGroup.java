package com.actionbarsherlock.internal.nineoldandroids.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.actionbarsherlock.internal.nineoldandroids.view.animation.AnimatorProxy;

// TODO: Auto-generated Javadoc
/**
 * The Class NineViewGroup.
 */
public abstract class NineViewGroup extends ViewGroup {
    
    /** The m proxy. */
    private final AnimatorProxy mProxy;

    /**
     * Instantiates a new nine view group.
     *
     * @param context the context
     */
    public NineViewGroup(Context context) {
        super(context);
        mProxy = AnimatorProxy.NEEDS_PROXY ? AnimatorProxy.wrap(this) : null;
    }
    
    /**
     * Instantiates a new nine view group.
     *
     * @param context the context
     * @param attrs the attrs
     */
    public NineViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        mProxy = AnimatorProxy.NEEDS_PROXY ? AnimatorProxy.wrap(this) : null;
    }
    
    /**
     * Instantiates a new nine view group.
     *
     * @param context the context
     * @param attrs the attrs
     * @param defStyle the def style
     */
    public NineViewGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mProxy = AnimatorProxy.NEEDS_PROXY ? AnimatorProxy.wrap(this) : null;
    }

    /**
     * Sets the visibility.
     *
     * @param visibility the new visibility
     * @see android.view.View#setVisibility(int)
     */
    @Override
    public void setVisibility(int visibility) {
        if (mProxy != null) {
            if (visibility == GONE) {
                clearAnimation();
            } else if (visibility == VISIBLE) {
                setAnimation(mProxy);
            }
        }
        super.setVisibility(visibility);
    }

    /**
     * Gets the alpha.
     *
     * @return the alpha
     * @see android.view.View#getAlpha()
     */
    public float getAlpha() {
        if (AnimatorProxy.NEEDS_PROXY) {
            return mProxy.getAlpha();
        } else {
            return super.getAlpha();
        }
    }
    
    /**
     * Sets the alpha.
     *
     * @param alpha the new alpha
     * @see android.view.View#setAlpha(float)
     */
    public void setAlpha(float alpha) {
        if (AnimatorProxy.NEEDS_PROXY) {
            mProxy.setAlpha(alpha);
        } else {
            super.setAlpha(alpha);
        }
    }
    
    /**
     * Gets the translation x.
     *
     * @return the translation x
     * @see android.view.View#getTranslationX()
     */
    public float getTranslationX() {
        if (AnimatorProxy.NEEDS_PROXY) {
            return mProxy.getTranslationX();
        } else {
            return super.getTranslationX();
        }
    }
    
    /**
     * Sets the translation x.
     *
     * @param translationX the new translation x
     * @see android.view.View#setTranslationX(float)
     */
    public void setTranslationX(float translationX) {
        if (AnimatorProxy.NEEDS_PROXY) {
            mProxy.setTranslationX(translationX);
        } else {
            super.setTranslationX(translationX);
        }
    }
    
    /**
     * Gets the translation y.
     *
     * @return the translation y
     * @see android.view.View#getTranslationY()
     */
    public float getTranslationY() {
        if (AnimatorProxy.NEEDS_PROXY) {
            return mProxy.getTranslationY();
        } else {
            return super.getTranslationY();
        }
    }
    
    /**
     * Sets the translation y.
     *
     * @param translationY the new translation y
     * @see android.view.View#setTranslationY(float)
     */
    public void setTranslationY(float translationY) {
        if (AnimatorProxy.NEEDS_PROXY) {
            mProxy.setTranslationY(translationY);
        } else {
            super.setTranslationY(translationY);
        }
    }
}
