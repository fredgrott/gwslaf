package com.actionbarsherlock.internal.view;

import android.view.View;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving view_OnAttachStateChange events.
 * The class that is interested in processing a view_OnAttachStateChange
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addView_OnAttachStateChangeListener<code> method. When
 * the view_OnAttachStateChange event occurs, that object's appropriate
 * method is invoked.
 *
 * @see View_OnAttachStateChangeEvent
 */
public interface View_OnAttachStateChangeListener {
    
    /**
     * On view attached to window.
     *
     * @param v the v
     */
    void onViewAttachedToWindow(View v);
    
    /**
     * On view detached from window.
     *
     * @param v the v
     */
    void onViewDetachedFromWindow(View v);
}
