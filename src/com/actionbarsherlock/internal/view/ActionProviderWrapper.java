package com.actionbarsherlock.internal.view;

import com.actionbarsherlock.internal.view.menu.SubMenuWrapper;
import com.actionbarsherlock.view.ActionProvider;
import android.view.View;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionProviderWrapper.
 */
public class ActionProviderWrapper extends android.view.ActionProvider {
    
    /** The m provider. */
    private final ActionProvider mProvider;


    /**
     * Instantiates a new action provider wrapper.
     *
     * @param provider the provider
     */
    public ActionProviderWrapper(ActionProvider provider) {
        super(null/*TODO*/); //XXX this *should* be unused
        mProvider = provider;
    }


    /**
     * Unwrap.
     *
     * @return the action provider
     */
    public ActionProvider unwrap() {
        return mProvider;
    }

    /**
     * On create action view.
     *
     * @return the view
     * @see android.view.ActionProvider#onCreateActionView()
     */
    @Override
    public View onCreateActionView() {
        return mProvider.onCreateActionView();
    }

    /**
     * Checks for sub menu.
     *
     * @return true, if successful
     * @see android.view.ActionProvider#hasSubMenu()
     */
    @Override
    public boolean hasSubMenu() {
        return mProvider.hasSubMenu();
    }

    /**
     * On perform default action.
     *
     * @return true, if successful
     * @see android.view.ActionProvider#onPerformDefaultAction()
     */
    @Override
    public boolean onPerformDefaultAction() {
        return mProvider.onPerformDefaultAction();
    }

    /**
     * On prepare sub menu.
     *
     * @param subMenu the sub menu
     * @see android.view.ActionProvider#onPrepareSubMenu(android.view.SubMenu)
     */
    @Override
    public void onPrepareSubMenu(android.view.SubMenu subMenu) {
        mProvider.onPrepareSubMenu(new SubMenuWrapper(subMenu));
    }
}
