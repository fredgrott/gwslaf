/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.actionbarsherlock.internal.view;

import android.content.Context;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

import java.lang.ref.WeakReference;

import com.actionbarsherlock.internal.view.menu.MenuBuilder;
import com.actionbarsherlock.internal.view.menu.MenuPopupHelper;
import com.actionbarsherlock.internal.view.menu.SubMenuBuilder;
import com.actionbarsherlock.internal.widget.ActionBarContextView;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

// TODO: Auto-generated Javadoc
/**
 * The Class StandaloneActionMode.
 */
public class StandaloneActionMode extends ActionMode implements MenuBuilder.Callback {
    
    /** The m context. */
    private Context mContext;
    
    /** The m context view. */
    private ActionBarContextView mContextView;
    
    /** The m callback. */
    private ActionMode.Callback mCallback;
    
    /** The m custom view. */
    private WeakReference<View> mCustomView;
    
    /** The m finished. */
    private boolean mFinished;
    
    /** The m focusable. */
    private boolean mFocusable;

    /** The m menu. */
    private MenuBuilder mMenu;

    /**
     * Instantiates a new standalone action mode.
     *
     * @param context the context
     * @param view the view
     * @param callback the callback
     * @param isFocusable the is focusable
     */
    public StandaloneActionMode(Context context, ActionBarContextView view,
            ActionMode.Callback callback, boolean isFocusable) {
        mContext = context;
        mContextView = view;
        mCallback = callback;

        mMenu = new MenuBuilder(context).setDefaultShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        mMenu.setCallback(this);
        mFocusable = isFocusable;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     * @see com.actionbarsherlock.view.ActionMode#setTitle(java.lang.CharSequence)
     */
    @Override
    public void setTitle(CharSequence title) {
        mContextView.setTitle(title);
    }

    /**
     * Sets the subtitle.
     *
     * @param subtitle the new subtitle
     * @see com.actionbarsherlock.view.ActionMode#setSubtitle(java.lang.CharSequence)
     */
    @Override
    public void setSubtitle(CharSequence subtitle) {
        mContextView.setSubtitle(subtitle);
    }

    /**
     * Sets the title.
     *
     * @param resId the new title
     * @see com.actionbarsherlock.view.ActionMode#setTitle(int)
     */
    @Override
    public void setTitle(int resId) {
        setTitle(mContext.getString(resId));
    }

    /**
     * Sets the subtitle.
     *
     * @param resId the new subtitle
     * @see com.actionbarsherlock.view.ActionMode#setSubtitle(int)
     */
    @Override
    public void setSubtitle(int resId) {
        setSubtitle(mContext.getString(resId));
    }

    /**
     * Sets the custom view.
     *
     * @param view the new custom view
     * @see com.actionbarsherlock.view.ActionMode#setCustomView(android.view.View)
     */
    @Override
    public void setCustomView(View view) {
        mContextView.setCustomView(view);
        mCustomView = view != null ? new WeakReference<View>(view) : null;
    }

    /**
     * Invalidate.
     *
     * @see com.actionbarsherlock.view.ActionMode#invalidate()
     */
    @Override
    public void invalidate() {
        mCallback.onPrepareActionMode(this, mMenu);
    }

    /**
     * Finish.
     *
     * @see com.actionbarsherlock.view.ActionMode#finish()
     */
    @Override
    public void finish() {
        if (mFinished) {
            return;
        }
        mFinished = true;

        mContextView.sendAccessibilityEvent(AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED);
        mCallback.onDestroyActionMode(this);
    }

    /**
     * Gets the menu.
     *
     * @return the menu
     * @see com.actionbarsherlock.view.ActionMode#getMenu()
     */
    @Override
    public Menu getMenu() {
        return mMenu;
    }

    /**
     * Gets the title.
     *
     * @return the title
     * @see com.actionbarsherlock.view.ActionMode#getTitle()
     */
    @Override
    public CharSequence getTitle() {
        return mContextView.getTitle();
    }

    /**
     * Gets the subtitle.
     *
     * @return the subtitle
     * @see com.actionbarsherlock.view.ActionMode#getSubtitle()
     */
    @Override
    public CharSequence getSubtitle() {
        return mContextView.getSubtitle();
    }

    /**
     * Gets the custom view.
     *
     * @return the custom view
     * @see com.actionbarsherlock.view.ActionMode#getCustomView()
     */
    @Override
    public View getCustomView() {
        return mCustomView != null ? mCustomView.get() : null;
    }

    /**
     * Gets the menu inflater.
     *
     * @return the menu inflater
     * @see com.actionbarsherlock.view.ActionMode#getMenuInflater()
     */
    @Override
    public MenuInflater getMenuInflater() {
        return new MenuInflater(mContext);
    }

    /**
     * On menu item selected.
     *
     * @param menu the menu
     * @param item the item
     * @return true, if successful
     * @see com.actionbarsherlock.internal.view.menu.MenuBuilder.Callback#onMenuItemSelected(com.actionbarsherlock.internal.view.menu.MenuBuilder, com.actionbarsherlock.view.MenuItem)
     */
    public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
        return mCallback.onActionItemClicked(this, item);
    }

    /**
     * On close menu.
     *
     * @param menu the menu
     * @param allMenusAreClosing the all menus are closing
     */
    public void onCloseMenu(MenuBuilder menu, boolean allMenusAreClosing) {
    }

    /**
     * On sub menu selected.
     *
     * @param subMenu the sub menu
     * @return true, if successful
     */
    public boolean onSubMenuSelected(SubMenuBuilder subMenu) {
        if (!subMenu.hasVisibleItems()) {
            return true;
        }

        new MenuPopupHelper(mContext, subMenu).show();
        return true;
    }

    /**
     * On close sub menu.
     *
     * @param menu the menu
     */
    public void onCloseSubMenu(SubMenuBuilder menu) {
    }

    /**
     * On menu mode change.
     *
     * @param menu the menu
     * @see com.actionbarsherlock.internal.view.menu.MenuBuilder.Callback#onMenuModeChange(com.actionbarsherlock.internal.view.menu.MenuBuilder)
     */
    public void onMenuModeChange(MenuBuilder menu) {
        invalidate();
        mContextView.showOverflowMenu();
    }

    /**
     * Checks if is ui focusable.
     *
     * @return true, if is ui focusable
     * @see com.actionbarsherlock.view.ActionMode#isUiFocusable()
     */
    public boolean isUiFocusable() {
        return mFocusable;
    }
}
