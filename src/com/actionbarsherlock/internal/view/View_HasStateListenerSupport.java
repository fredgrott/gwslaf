package com.actionbarsherlock.internal.view;

// TODO: Auto-generated Javadoc
/**
 * The Interface View_HasStateListenerSupport.
 */
public interface View_HasStateListenerSupport {
    
    /**
     * Adds the on attach state change listener.
     *
     * @param listener the listener
     */
    void addOnAttachStateChangeListener(View_OnAttachStateChangeListener listener);
    
    /**
     * Removes the on attach state change listener.
     *
     * @param listener the listener
     */
    void removeOnAttachStateChangeListener(View_OnAttachStateChangeListener listener);
}
