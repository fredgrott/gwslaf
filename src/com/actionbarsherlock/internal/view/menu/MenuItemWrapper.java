package com.actionbarsherlock.internal.view.menu;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import com.actionbarsherlock.internal.view.ActionProviderWrapper;
import com.actionbarsherlock.view.ActionProvider;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;

// TODO: Auto-generated Javadoc
/**
 * The Class MenuItemWrapper.
 */
public class MenuItemWrapper implements MenuItem, android.view.MenuItem.OnMenuItemClickListener {
    
    /** The m native item. */
    private final android.view.MenuItem mNativeItem;
    
    /** The m sub menu. */
    private SubMenu mSubMenu = null;
    
    /** The m menu item click listener. */
    private OnMenuItemClickListener mMenuItemClickListener = null;
    
    /** The m action expand listener. */
    private OnActionExpandListener mActionExpandListener = null;
    
    /** The m native action expand listener. */
    private android.view.MenuItem.OnActionExpandListener mNativeActionExpandListener = null;


    /**
     * Instantiates a new menu item wrapper.
     *
     * @param nativeItem the native item
     */
    public MenuItemWrapper(android.view.MenuItem nativeItem) {
        if (nativeItem == null) {
            throw new IllegalStateException("Wrapped menu item cannot be null.");
        }
        mNativeItem = nativeItem;
    }


    /**
     * Gets the item id.
     *
     * @return the item id
     * @see com.actionbarsherlock.view.MenuItem#getItemId()
     */
    @Override
    public int getItemId() {
        return mNativeItem.getItemId();
    }

    /**
     * Gets the group id.
     *
     * @return the group id
     * @see com.actionbarsherlock.view.MenuItem#getGroupId()
     */
    @Override
    public int getGroupId() {
        return mNativeItem.getGroupId();
    }

    /**
     * Gets the order.
     *
     * @return the order
     * @see com.actionbarsherlock.view.MenuItem#getOrder()
     */
    @Override
    public int getOrder() {
        return mNativeItem.getOrder();
    }

    /**
     * Sets the title.
     *
     * @param title the title
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setTitle(java.lang.CharSequence)
     */
    @Override
    public MenuItem setTitle(CharSequence title) {
        mNativeItem.setTitle(title);
        return this;
    }

    /**
     * Sets the title.
     *
     * @param title the title
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setTitle(int)
     */
    @Override
    public MenuItem setTitle(int title) {
        mNativeItem.setTitle(title);
        return this;
    }

    /**
     * Gets the title.
     *
     * @return the title
     * @see com.actionbarsherlock.view.MenuItem#getTitle()
     */
    @Override
    public CharSequence getTitle() {
        return mNativeItem.getTitle();
    }

    /**
     * Sets the title condensed.
     *
     * @param title the title
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setTitleCondensed(java.lang.CharSequence)
     */
    @Override
    public MenuItem setTitleCondensed(CharSequence title) {
        mNativeItem.setTitleCondensed(title);
        return this;
    }

    /**
     * Gets the title condensed.
     *
     * @return the title condensed
     * @see com.actionbarsherlock.view.MenuItem#getTitleCondensed()
     */
    @Override
    public CharSequence getTitleCondensed() {
        return mNativeItem.getTitleCondensed();
    }

    /**
     * Sets the icon.
     *
     * @param icon the icon
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setIcon(android.graphics.drawable.Drawable)
     */
    @Override
    public MenuItem setIcon(Drawable icon) {
        mNativeItem.setIcon(icon);
        return this;
    }

    /**
     * Sets the icon.
     *
     * @param iconRes the icon res
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setIcon(int)
     */
    @Override
    public MenuItem setIcon(int iconRes) {
        mNativeItem.setIcon(iconRes);
        return this;
    }

    /**
     * Gets the icon.
     *
     * @return the icon
     * @see com.actionbarsherlock.view.MenuItem#getIcon()
     */
    @Override
    public Drawable getIcon() {
        return mNativeItem.getIcon();
    }

    /**
     * Sets the intent.
     *
     * @param intent the intent
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setIntent(android.content.Intent)
     */
    @Override
    public MenuItem setIntent(Intent intent) {
        mNativeItem.setIntent(intent);
        return this;
    }

    /**
     * Gets the intent.
     *
     * @return the intent
     * @see com.actionbarsherlock.view.MenuItem#getIntent()
     */
    @Override
    public Intent getIntent() {
        return mNativeItem.getIntent();
    }

    /**
     * Sets the shortcut.
     *
     * @param numericChar the numeric char
     * @param alphaChar the alpha char
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setShortcut(char, char)
     */
    @Override
    public MenuItem setShortcut(char numericChar, char alphaChar) {
        mNativeItem.setShortcut(numericChar, alphaChar);
        return this;
    }

    /**
     * Sets the numeric shortcut.
     *
     * @param numericChar the numeric char
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setNumericShortcut(char)
     */
    @Override
    public MenuItem setNumericShortcut(char numericChar) {
        mNativeItem.setNumericShortcut(numericChar);
        return this;
    }

    /**
     * Gets the numeric shortcut.
     *
     * @return the numeric shortcut
     * @see com.actionbarsherlock.view.MenuItem#getNumericShortcut()
     */
    @Override
    public char getNumericShortcut() {
        return mNativeItem.getNumericShortcut();
    }

    /**
     * Sets the alphabetic shortcut.
     *
     * @param alphaChar the alpha char
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setAlphabeticShortcut(char)
     */
    @Override
    public MenuItem setAlphabeticShortcut(char alphaChar) {
        mNativeItem.setAlphabeticShortcut(alphaChar);
        return this;
    }

    /**
     * Gets the alphabetic shortcut.
     *
     * @return the alphabetic shortcut
     * @see com.actionbarsherlock.view.MenuItem#getAlphabeticShortcut()
     */
    @Override
    public char getAlphabeticShortcut() {
        return mNativeItem.getAlphabeticShortcut();
    }

    /**
     * Sets the checkable.
     *
     * @param checkable the checkable
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setCheckable(boolean)
     */
    @Override
    public MenuItem setCheckable(boolean checkable) {
        mNativeItem.setCheckable(checkable);
        return this;
    }

    /**
     * Checks if is checkable.
     *
     * @return true, if is checkable
     * @see com.actionbarsherlock.view.MenuItem#isCheckable()
     */
    @Override
    public boolean isCheckable() {
        return mNativeItem.isCheckable();
    }

    /**
     * Sets the checked.
     *
     * @param checked the checked
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setChecked(boolean)
     */
    @Override
    public MenuItem setChecked(boolean checked) {
        mNativeItem.setChecked(checked);
        return this;
    }

    /**
     * Checks if is checked.
     *
     * @return true, if is checked
     * @see com.actionbarsherlock.view.MenuItem#isChecked()
     */
    @Override
    public boolean isChecked() {
        return mNativeItem.isChecked();
    }

    /**
     * Sets the visible.
     *
     * @param visible the visible
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setVisible(boolean)
     */
    @Override
    public MenuItem setVisible(boolean visible) {
        mNativeItem.setVisible(visible);
        return this;
    }

    /**
     * Checks if is visible.
     *
     * @return true, if is visible
     * @see com.actionbarsherlock.view.MenuItem#isVisible()
     */
    @Override
    public boolean isVisible() {
        return mNativeItem.isVisible();
    }

    /**
     * Sets the enabled.
     *
     * @param enabled the enabled
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setEnabled(boolean)
     */
    @Override
    public MenuItem setEnabled(boolean enabled) {
        mNativeItem.setEnabled(enabled);
        return this;
    }

    /**
     * Checks if is enabled.
     *
     * @return true, if is enabled
     * @see com.actionbarsherlock.view.MenuItem#isEnabled()
     */
    @Override
    public boolean isEnabled() {
        return mNativeItem.isEnabled();
    }

    /**
     * Checks for sub menu.
     *
     * @return true, if successful
     * @see com.actionbarsherlock.view.MenuItem#hasSubMenu()
     */
    @Override
    public boolean hasSubMenu() {
        return mNativeItem.hasSubMenu();
    }

    /**
     * Gets the sub menu.
     *
     * @return the sub menu
     * @see com.actionbarsherlock.view.MenuItem#getSubMenu()
     */
    @Override
    public SubMenu getSubMenu() {
        if (hasSubMenu() && (mSubMenu == null)) {
            mSubMenu = new SubMenuWrapper(mNativeItem.getSubMenu());
        }
        return mSubMenu;
    }

    /**
     * Sets the on menu item click listener.
     *
     * @param menuItemClickListener the menu item click listener
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setOnMenuItemClickListener(com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener)
     */
    @Override
    public MenuItem setOnMenuItemClickListener(OnMenuItemClickListener menuItemClickListener) {
        mMenuItemClickListener = menuItemClickListener;
        //Register ourselves as the listener to proxy
        mNativeItem.setOnMenuItemClickListener(this);
        return this;
    }

    /**
     * On menu item click.
     *
     * @param item the item
     * @return true, if successful
     * @see android.view.MenuItem.OnMenuItemClickListener#onMenuItemClick(android.view.MenuItem)
     */
    @Override
    public boolean onMenuItemClick(android.view.MenuItem item) {
        if (mMenuItemClickListener != null) {
            return mMenuItemClickListener.onMenuItemClick(this);
        }
        return false;
    }

    /**
     * Gets the menu info.
     *
     * @return the menu info
     * @see com.actionbarsherlock.view.MenuItem#getMenuInfo()
     */
    @Override
    public ContextMenuInfo getMenuInfo() {
        return mNativeItem.getMenuInfo();
    }

    /**
     * Sets the show as action.
     *
     * @param actionEnum the new show as action
     * @see com.actionbarsherlock.view.MenuItem#setShowAsAction(int)
     */
    @Override
    public void setShowAsAction(int actionEnum) {
        mNativeItem.setShowAsAction(actionEnum);
    }

    /**
     * Sets the show as action flags.
     *
     * @param actionEnum the action enum
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setShowAsActionFlags(int)
     */
    @Override
    public MenuItem setShowAsActionFlags(int actionEnum) {
        mNativeItem.setShowAsActionFlags(actionEnum);
        return this;
    }

    /**
     * Sets the action view.
     *
     * @param view the view
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setActionView(android.view.View)
     */
    @Override
    public MenuItem setActionView(View view) {
        mNativeItem.setActionView(view);
        return this;
    }

    /**
     * Sets the action view.
     *
     * @param resId the res id
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setActionView(int)
     */
    @Override
    public MenuItem setActionView(int resId) {
        mNativeItem.setActionView(resId);
        return this;
    }

    /**
     * Gets the action view.
     *
     * @return the action view
     * @see com.actionbarsherlock.view.MenuItem#getActionView()
     */
    @Override
    public View getActionView() {
        return mNativeItem.getActionView();
    }

    /**
     * Sets the action provider.
     *
     * @param actionProvider the action provider
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setActionProvider(com.actionbarsherlock.view.ActionProvider)
     */
    @Override
    public MenuItem setActionProvider(ActionProvider actionProvider) {
        mNativeItem.setActionProvider(new ActionProviderWrapper(actionProvider));
        return this;
    }

    /**
     * Gets the action provider.
     *
     * @return the action provider
     * @see com.actionbarsherlock.view.MenuItem#getActionProvider()
     */
    @Override
    public ActionProvider getActionProvider() {
        android.view.ActionProvider nativeProvider = mNativeItem.getActionProvider();
        if (nativeProvider != null && nativeProvider instanceof ActionProviderWrapper) {
            return ((ActionProviderWrapper)nativeProvider).unwrap();
        }
        return null;
    }

    /**
     * Expand action view.
     *
     * @return true, if successful
     * @see com.actionbarsherlock.view.MenuItem#expandActionView()
     */
    @Override
    public boolean expandActionView() {
        return mNativeItem.expandActionView();
    }

    /**
     * Collapse action view.
     *
     * @return true, if successful
     * @see com.actionbarsherlock.view.MenuItem#collapseActionView()
     */
    @Override
    public boolean collapseActionView() {
        return mNativeItem.collapseActionView();
    }

    /**
     * Checks if is action view expanded.
     *
     * @return true, if is action view expanded
     * @see com.actionbarsherlock.view.MenuItem#isActionViewExpanded()
     */
    @Override
    public boolean isActionViewExpanded() {
        return mNativeItem.isActionViewExpanded();
    }

    /**
     * Sets the on action expand listener.
     *
     * @param listener the listener
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setOnActionExpandListener(com.actionbarsherlock.view.MenuItem.OnActionExpandListener)
     */
    @Override
    public MenuItem setOnActionExpandListener(OnActionExpandListener listener) {
        mActionExpandListener = listener;

        if (mNativeActionExpandListener == null) {
            mNativeActionExpandListener = new android.view.MenuItem.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(android.view.MenuItem menuItem) {
                    if (mActionExpandListener != null) {
                        return mActionExpandListener.onMenuItemActionExpand(MenuItemWrapper.this);
                    }
                    return false;
                }

                @Override
                public boolean onMenuItemActionCollapse(android.view.MenuItem menuItem) {
                    if (mActionExpandListener != null) {
                        return mActionExpandListener.onMenuItemActionCollapse(MenuItemWrapper.this);
                    }
                    return false;
                }
            };

            //Register our inner-class as the listener to proxy method calls
            mNativeItem.setOnActionExpandListener(mNativeActionExpandListener);
        }

        return this;
    }
}
