/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.actionbarsherlock.internal.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;

// TODO: Auto-generated Javadoc
/**
 * The model for a sub menu, which is an extension of the menu.  Most methods are proxied to
 * the parent menu.
 */
public class SubMenuBuilder extends MenuBuilder implements SubMenu {
    
    /** The m parent menu. */
    private MenuBuilder mParentMenu;
    
    /** The m item. */
    private MenuItemImpl mItem;

    /**
     * Instantiates a new sub menu builder.
     *
     * @param context the context
     * @param parentMenu the parent menu
     * @param item the item
     */
    public SubMenuBuilder(Context context, MenuBuilder parentMenu, MenuItemImpl item) {
        super(context);

        mParentMenu = parentMenu;
        mItem = item;
    }

    /**
     * Sets the qwerty mode.
     *
     * @param isQwerty the new qwerty mode
     * @see com.actionbarsherlock.internal.view.menu.MenuBuilder#setQwertyMode(boolean)
     */
    @Override
    public void setQwertyMode(boolean isQwerty) {
        mParentMenu.setQwertyMode(isQwerty);
    }

    /**
     * Checks if is qwerty mode.
     *
     * @return true, if is qwerty mode
     * @see com.actionbarsherlock.internal.view.menu.MenuBuilder#isQwertyMode()
     */
    @Override
    public boolean isQwertyMode() {
        return mParentMenu.isQwertyMode();
    }

    /**
     * Sets the shortcuts visible.
     *
     * @param shortcutsVisible the new shortcuts visible
     * @see com.actionbarsherlock.internal.view.menu.MenuBuilder#setShortcutsVisible(boolean)
     */
    @Override
    public void setShortcutsVisible(boolean shortcutsVisible) {
        mParentMenu.setShortcutsVisible(shortcutsVisible);
    }

    /**
     * Checks if is shortcuts visible.
     *
     * @return true, if is shortcuts visible
     * @see com.actionbarsherlock.internal.view.menu.MenuBuilder#isShortcutsVisible()
     */
    @Override
    public boolean isShortcutsVisible() {
        return mParentMenu.isShortcutsVisible();
    }

    /**
     * Gets the parent menu.
     *
     * @return the parent menu
     */
    public Menu getParentMenu() {
        return mParentMenu;
    }

    /**
     * Gets the item.
     *
     * @return the item
     * @see com.actionbarsherlock.view.SubMenu#getItem()
     */
    public MenuItem getItem() {
        return mItem;
    }

    /**
     * Sets the callback.
     *
     * @param callback the new callback
     * @see com.actionbarsherlock.internal.view.menu.MenuBuilder#setCallback(com.actionbarsherlock.internal.view.menu.MenuBuilder.Callback)
     */
    @Override
    public void setCallback(Callback callback) {
        mParentMenu.setCallback(callback);
    }

    /**
     * Gets the root menu.
     *
     * @return the root menu
     * @see com.actionbarsherlock.internal.view.menu.MenuBuilder#getRootMenu()
     */
    @Override
    public MenuBuilder getRootMenu() {
        return mParentMenu;
    }

    /**
     * Dispatch menu item selected.
     *
     * @param menu the menu
     * @param item the item
     * @return true, if successful
     * @see com.actionbarsherlock.internal.view.menu.MenuBuilder#dispatchMenuItemSelected(com.actionbarsherlock.internal.view.menu.MenuBuilder, com.actionbarsherlock.view.MenuItem)
     */
    @Override
    boolean dispatchMenuItemSelected(MenuBuilder menu, MenuItem item) {
        return super.dispatchMenuItemSelected(menu, item) ||
                mParentMenu.dispatchMenuItemSelected(menu, item);
    }

    /**
     * Sets the icon.
     *
     * @param icon the icon
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setIcon(android.graphics.drawable.Drawable)
     */
    public SubMenu setIcon(Drawable icon) {
        mItem.setIcon(icon);
        return this;
    }

    /**
     * Sets the icon.
     *
     * @param iconRes the icon res
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setIcon(int)
     */
    public SubMenu setIcon(int iconRes) {
        mItem.setIcon(iconRes);
        return this;
    }

    /**
     * Sets the header icon.
     *
     * @param icon the icon
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setHeaderIcon(android.graphics.drawable.Drawable)
     */
    public SubMenu setHeaderIcon(Drawable icon) {
        return (SubMenu) super.setHeaderIconInt(icon);
    }

    /**
     * Sets the header icon.
     *
     * @param iconRes the icon res
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setHeaderIcon(int)
     */
    public SubMenu setHeaderIcon(int iconRes) {
        return (SubMenu) super.setHeaderIconInt(iconRes);
    }

    /**
     * Sets the header title.
     *
     * @param title the title
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setHeaderTitle(java.lang.CharSequence)
     */
    public SubMenu setHeaderTitle(CharSequence title) {
        return (SubMenu) super.setHeaderTitleInt(title);
    }

    /**
     * Sets the header title.
     *
     * @param titleRes the title res
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setHeaderTitle(int)
     */
    public SubMenu setHeaderTitle(int titleRes) {
        return (SubMenu) super.setHeaderTitleInt(titleRes);
    }

    /**
     * Sets the header view.
     *
     * @param view the view
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setHeaderView(android.view.View)
     */
    public SubMenu setHeaderView(View view) {
        return (SubMenu) super.setHeaderViewInt(view);
    }

    /**
     * Expand item action view.
     *
     * @param item the item
     * @return true, if successful
     * @see com.actionbarsherlock.internal.view.menu.MenuBuilder#expandItemActionView(com.actionbarsherlock.internal.view.menu.MenuItemImpl)
     */
    @Override
    public boolean expandItemActionView(MenuItemImpl item) {
        return mParentMenu.expandItemActionView(item);
    }

    /**
     * Collapse item action view.
     *
     * @param item the item
     * @return true, if successful
     * @see com.actionbarsherlock.internal.view.menu.MenuBuilder#collapseItemActionView(com.actionbarsherlock.internal.view.menu.MenuItemImpl)
     */
    @Override
    public boolean collapseItemActionView(MenuItemImpl item) {
        return mParentMenu.collapseItemActionView(item);
    }

    /**
     * Gets the action view states key.
     *
     * @return the action view states key
     * @see com.actionbarsherlock.internal.view.menu.MenuBuilder#getActionViewStatesKey()
     */
    @Override
    public String getActionViewStatesKey() {
        final int itemId = mItem != null ? mItem.getItemId() : 0;
        if (itemId == 0) {
            return null;
        }
        return super.getActionViewStatesKey() + ":" + itemId;
    }
}
