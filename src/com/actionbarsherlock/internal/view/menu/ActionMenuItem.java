/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.actionbarsherlock.internal.view.menu;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;

import com.actionbarsherlock.view.ActionProvider;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionMenuItem.
 *
 * @hide
 */
public class ActionMenuItem implements MenuItem {
    
    /** The m id. */
    private final int mId;
    
    /** The m group. */
    private final int mGroup;
    //UNUSED private final int mCategoryOrder;
    /** The m ordering. */
    private final int mOrdering;

    /** The m title. */
    private CharSequence mTitle;
    
    /** The m title condensed. */
    private CharSequence mTitleCondensed;
    
    /** The m intent. */
    private Intent mIntent;
    
    /** The m shortcut numeric char. */
    private char mShortcutNumericChar;
    
    /** The m shortcut alphabetic char. */
    private char mShortcutAlphabeticChar;

    /** The m icon drawable. */
    private Drawable mIconDrawable;
    //UNUSED private int mIconResId = NO_ICON;

    /** The m context. */
    private Context mContext;

    /** The m click listener. */
    private MenuItem.OnMenuItemClickListener mClickListener;

    //UNUSED private static final int NO_ICON = 0;

    /** The m flags. */
    private int mFlags = ENABLED;
    
    /** The Constant CHECKABLE. */
    private static final int CHECKABLE      = 0x00000001;
    
    /** The Constant CHECKED. */
    private static final int CHECKED        = 0x00000002;
    
    /** The Constant EXCLUSIVE. */
    private static final int EXCLUSIVE      = 0x00000004;
    
    /** The Constant HIDDEN. */
    private static final int HIDDEN         = 0x00000008;
    
    /** The Constant ENABLED. */
    private static final int ENABLED        = 0x00000010;

    /**
     * Instantiates a new action menu item.
     *
     * @param context the context
     * @param group the group
     * @param id the id
     * @param categoryOrder the category order
     * @param ordering the ordering
     * @param title the title
     */
    public ActionMenuItem(Context context, int group, int id, int categoryOrder, int ordering,
            CharSequence title) {
        mContext = context;
        mId = id;
        mGroup = group;
        //UNUSED mCategoryOrder = categoryOrder;
        mOrdering = ordering;
        mTitle = title;
    }

    /**
     * Gets the alphabetic shortcut.
     *
     * @return the alphabetic shortcut
     * @see com.actionbarsherlock.view.MenuItem#getAlphabeticShortcut()
     */
    public char getAlphabeticShortcut() {
        return mShortcutAlphabeticChar;
    }

    /**
     * Gets the group id.
     *
     * @return the group id
     * @see com.actionbarsherlock.view.MenuItem#getGroupId()
     */
    public int getGroupId() {
        return mGroup;
    }

    /**
     * Gets the icon.
     *
     * @return the icon
     * @see com.actionbarsherlock.view.MenuItem#getIcon()
     */
    public Drawable getIcon() {
        return mIconDrawable;
    }

    /**
     * Gets the intent.
     *
     * @return the intent
     * @see com.actionbarsherlock.view.MenuItem#getIntent()
     */
    public Intent getIntent() {
        return mIntent;
    }

    /**
     * Gets the item id.
     *
     * @return the item id
     * @see com.actionbarsherlock.view.MenuItem#getItemId()
     */
    public int getItemId() {
        return mId;
    }

    /**
     * Gets the menu info.
     *
     * @return the menu info
     * @see com.actionbarsherlock.view.MenuItem#getMenuInfo()
     */
    public ContextMenuInfo getMenuInfo() {
        return null;
    }

    /**
     * Gets the numeric shortcut.
     *
     * @return the numeric shortcut
     * @see com.actionbarsherlock.view.MenuItem#getNumericShortcut()
     */
    public char getNumericShortcut() {
        return mShortcutNumericChar;
    }

    /**
     * Gets the order.
     *
     * @return the order
     * @see com.actionbarsherlock.view.MenuItem#getOrder()
     */
    public int getOrder() {
        return mOrdering;
    }

    /**
     * Gets the sub menu.
     *
     * @return the sub menu
     * @see com.actionbarsherlock.view.MenuItem#getSubMenu()
     */
    public SubMenu getSubMenu() {
        return null;
    }

    /**
     * Gets the title.
     *
     * @return the title
     * @see com.actionbarsherlock.view.MenuItem#getTitle()
     */
    public CharSequence getTitle() {
        return mTitle;
    }

    /**
     * Gets the title condensed.
     *
     * @return the title condensed
     * @see com.actionbarsherlock.view.MenuItem#getTitleCondensed()
     */
    public CharSequence getTitleCondensed() {
        return mTitleCondensed;
    }

    /**
     * Checks for sub menu.
     *
     * @return true, if successful
     * @see com.actionbarsherlock.view.MenuItem#hasSubMenu()
     */
    public boolean hasSubMenu() {
        return false;
    }

    /**
     * Checks if is checkable.
     *
     * @return true, if is checkable
     * @see com.actionbarsherlock.view.MenuItem#isCheckable()
     */
    public boolean isCheckable() {
        return (mFlags & CHECKABLE) != 0;
    }

    /**
     * Checks if is checked.
     *
     * @return true, if is checked
     * @see com.actionbarsherlock.view.MenuItem#isChecked()
     */
    public boolean isChecked() {
        return (mFlags & CHECKED) != 0;
    }

    /**
     * Checks if is enabled.
     *
     * @return true, if is enabled
     * @see com.actionbarsherlock.view.MenuItem#isEnabled()
     */
    public boolean isEnabled() {
        return (mFlags & ENABLED) != 0;
    }

    /**
     * Checks if is visible.
     *
     * @return true, if is visible
     * @see com.actionbarsherlock.view.MenuItem#isVisible()
     */
    public boolean isVisible() {
        return (mFlags & HIDDEN) == 0;
    }

    /**
     * Sets the alphabetic shortcut.
     *
     * @param alphaChar the alpha char
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setAlphabeticShortcut(char)
     */
    public MenuItem setAlphabeticShortcut(char alphaChar) {
        mShortcutAlphabeticChar = alphaChar;
        return this;
    }

    /**
     * Sets the checkable.
     *
     * @param checkable the checkable
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setCheckable(boolean)
     */
    public MenuItem setCheckable(boolean checkable) {
        mFlags = (mFlags & ~CHECKABLE) | (checkable ? CHECKABLE : 0);
        return this;
    }

    /**
     * Sets the exclusive checkable.
     *
     * @param exclusive the exclusive
     * @return the action menu item
     */
    public ActionMenuItem setExclusiveCheckable(boolean exclusive) {
        mFlags = (mFlags & ~EXCLUSIVE) | (exclusive ? EXCLUSIVE : 0);
        return this;
    }

    /**
     * Sets the checked.
     *
     * @param checked the checked
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setChecked(boolean)
     */
    public MenuItem setChecked(boolean checked) {
        mFlags = (mFlags & ~CHECKED) | (checked ? CHECKED : 0);
        return this;
    }

    /**
     * Sets the enabled.
     *
     * @param enabled the enabled
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setEnabled(boolean)
     */
    public MenuItem setEnabled(boolean enabled) {
        mFlags = (mFlags & ~ENABLED) | (enabled ? ENABLED : 0);
        return this;
    }

    /**
     * Sets the icon.
     *
     * @param icon the icon
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setIcon(android.graphics.drawable.Drawable)
     */
    public MenuItem setIcon(Drawable icon) {
        mIconDrawable = icon;
        //UNUSED mIconResId = NO_ICON;
        return this;
    }

    /**
     * Sets the icon.
     *
     * @param iconRes the icon res
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setIcon(int)
     */
    public MenuItem setIcon(int iconRes) {
        //UNUSED mIconResId = iconRes;
        mIconDrawable = mContext.getResources().getDrawable(iconRes);
        return this;
    }

    /**
     * Sets the intent.
     *
     * @param intent the intent
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setIntent(android.content.Intent)
     */
    public MenuItem setIntent(Intent intent) {
        mIntent = intent;
        return this;
    }

    /**
     * Sets the numeric shortcut.
     *
     * @param numericChar the numeric char
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setNumericShortcut(char)
     */
    public MenuItem setNumericShortcut(char numericChar) {
        mShortcutNumericChar = numericChar;
        return this;
    }

    /**
     * Sets the on menu item click listener.
     *
     * @param menuItemClickListener the menu item click listener
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setOnMenuItemClickListener(com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener)
     */
    public MenuItem setOnMenuItemClickListener(OnMenuItemClickListener menuItemClickListener) {
        mClickListener = menuItemClickListener;
        return this;
    }

    /**
     * Sets the shortcut.
     *
     * @param numericChar the numeric char
     * @param alphaChar the alpha char
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setShortcut(char, char)
     */
    public MenuItem setShortcut(char numericChar, char alphaChar) {
        mShortcutNumericChar = numericChar;
        mShortcutAlphabeticChar = alphaChar;
        return this;
    }

    /**
     * Sets the title.
     *
     * @param title the title
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setTitle(java.lang.CharSequence)
     */
    public MenuItem setTitle(CharSequence title) {
        mTitle = title;
        return this;
    }

    /**
     * Sets the title.
     *
     * @param title the title
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setTitle(int)
     */
    public MenuItem setTitle(int title) {
        mTitle = mContext.getResources().getString(title);
        return this;
    }

    /**
     * Sets the title condensed.
     *
     * @param title the title
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setTitleCondensed(java.lang.CharSequence)
     */
    public MenuItem setTitleCondensed(CharSequence title) {
        mTitleCondensed = title;
        return this;
    }

    /**
     * Sets the visible.
     *
     * @param visible the visible
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setVisible(boolean)
     */
    public MenuItem setVisible(boolean visible) {
        mFlags = (mFlags & HIDDEN) | (visible ? 0 : HIDDEN);
        return this;
    }

    /**
     * Invoke.
     *
     * @return true, if successful
     */
    public boolean invoke() {
        if (mClickListener != null && mClickListener.onMenuItemClick(this)) {
            return true;
        }

        if (mIntent != null) {
            mContext.startActivity(mIntent);
            return true;
        }

        return false;
    }

    /**
     * Sets the show as action.
     *
     * @param show the new show as action
     * @see com.actionbarsherlock.view.MenuItem#setShowAsAction(int)
     */
    public void setShowAsAction(int show) {
        // Do nothing. ActionMenuItems always show as action buttons.
    }

    /**
     * Sets the action view.
     *
     * @param actionView the action view
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setActionView(android.view.View)
     */
    public MenuItem setActionView(View actionView) {
        throw new UnsupportedOperationException();
    }

    /**
     * Gets the action view.
     *
     * @return the action view
     * @see com.actionbarsherlock.view.MenuItem#getActionView()
     */
    public View getActionView() {
        return null;
    }

    /**
     * Sets the action view.
     *
     * @param resId the res id
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setActionView(int)
     */
    @Override
    public MenuItem setActionView(int resId) {
        throw new UnsupportedOperationException();
    }

    /**
     * Gets the action provider.
     *
     * @return the action provider
     * @see com.actionbarsherlock.view.MenuItem#getActionProvider()
     */
    @Override
    public ActionProvider getActionProvider() {
        return null;
    }

    /**
     * Sets the action provider.
     *
     * @param actionProvider the action provider
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setActionProvider(com.actionbarsherlock.view.ActionProvider)
     */
    @Override
    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    /**
     * Sets the show as action flags.
     *
     * @param actionEnum the action enum
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setShowAsActionFlags(int)
     */
    @Override
    public MenuItem setShowAsActionFlags(int actionEnum) {
        setShowAsAction(actionEnum);
        return this;
    }

    /**
     * Expand action view.
     *
     * @return true, if successful
     * @see com.actionbarsherlock.view.MenuItem#expandActionView()
     */
    @Override
    public boolean expandActionView() {
        return false;
    }

    /**
     * Collapse action view.
     *
     * @return true, if successful
     * @see com.actionbarsherlock.view.MenuItem#collapseActionView()
     */
    @Override
    public boolean collapseActionView() {
        return false;
    }

    /**
     * Checks if is action view expanded.
     *
     * @return true, if is action view expanded
     * @see com.actionbarsherlock.view.MenuItem#isActionViewExpanded()
     */
    @Override
    public boolean isActionViewExpanded() {
        return false;
    }

    /**
     * Sets the on action expand listener.
     *
     * @param listener the listener
     * @return the menu item
     * @see com.actionbarsherlock.view.MenuItem#setOnActionExpandListener(com.actionbarsherlock.view.MenuItem.OnActionExpandListener)
     */
    @Override
    public MenuItem setOnActionExpandListener(OnActionExpandListener listener) {
        // No need to save the listener; ActionMenuItem does not support collapsing items.
        return this;
    }
}
