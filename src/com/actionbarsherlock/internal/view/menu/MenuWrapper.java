package com.actionbarsherlock.internal.view.menu;

import java.util.WeakHashMap;
import android.content.ComponentName;
import android.content.Intent;
import android.view.KeyEvent;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;

// TODO: Auto-generated Javadoc
/**
 * The Class MenuWrapper.
 */
public class MenuWrapper implements Menu {
    
    /** The m native menu. */
    private final android.view.Menu mNativeMenu;

    /** The m native map. */
    private final WeakHashMap<android.view.MenuItem, MenuItem> mNativeMap =
            new WeakHashMap<android.view.MenuItem, MenuItem>();


    /**
     * Instantiates a new menu wrapper.
     *
     * @param nativeMenu the native menu
     */
    public MenuWrapper(android.view.Menu nativeMenu) {
        mNativeMenu = nativeMenu;
    }

    /**
     * Unwrap.
     *
     * @return the android.view. menu
     */
    public android.view.Menu unwrap() {
        return mNativeMenu;
    }

    /**
     * Adds the internal.
     *
     * @param nativeItem the native item
     * @return the menu item
     */
    private MenuItem addInternal(android.view.MenuItem nativeItem) {
        MenuItem item = new MenuItemWrapper(nativeItem);
        mNativeMap.put(nativeItem, item);
        return item;
    }

    /**
     * Adds the.
     *
     * @param title the title
     * @return the menu item
     * @see com.actionbarsherlock.view.Menu#add(java.lang.CharSequence)
     */
    @Override
    public MenuItem add(CharSequence title) {
        return addInternal(mNativeMenu.add(title));
    }

    /**
     * Adds the.
     *
     * @param titleRes the title res
     * @return the menu item
     * @see com.actionbarsherlock.view.Menu#add(int)
     */
    @Override
    public MenuItem add(int titleRes) {
        return addInternal(mNativeMenu.add(titleRes));
    }

    /**
     * Adds the.
     *
     * @param groupId the group id
     * @param itemId the item id
     * @param order the order
     * @param title the title
     * @return the menu item
     * @see com.actionbarsherlock.view.Menu#add(int, int, int, java.lang.CharSequence)
     */
    @Override
    public MenuItem add(int groupId, int itemId, int order, CharSequence title) {
        return addInternal(mNativeMenu.add(groupId, itemId, order, title));
    }

    /**
     * Adds the.
     *
     * @param groupId the group id
     * @param itemId the item id
     * @param order the order
     * @param titleRes the title res
     * @return the menu item
     * @see com.actionbarsherlock.view.Menu#add(int, int, int, int)
     */
    @Override
    public MenuItem add(int groupId, int itemId, int order, int titleRes) {
        return addInternal(mNativeMenu.add(groupId, itemId, order, titleRes));
    }

    /**
     * Adds the internal.
     *
     * @param nativeSubMenu the native sub menu
     * @return the sub menu
     */
    private SubMenu addInternal(android.view.SubMenu nativeSubMenu) {
        SubMenu subMenu = new SubMenuWrapper(nativeSubMenu);
        android.view.MenuItem nativeItem = nativeSubMenu.getItem();
        MenuItem item = subMenu.getItem();
        mNativeMap.put(nativeItem, item);
        return subMenu;
    }

    /**
     * Adds the sub menu.
     *
     * @param title the title
     * @return the sub menu
     * @see com.actionbarsherlock.view.Menu#addSubMenu(java.lang.CharSequence)
     */
    @Override
    public SubMenu addSubMenu(CharSequence title) {
        return addInternal(mNativeMenu.addSubMenu(title));
    }

    /**
     * Adds the sub menu.
     *
     * @param titleRes the title res
     * @return the sub menu
     * @see com.actionbarsherlock.view.Menu#addSubMenu(int)
     */
    @Override
    public SubMenu addSubMenu(int titleRes) {
        return addInternal(mNativeMenu.addSubMenu(titleRes));
    }

    /**
     * Adds the sub menu.
     *
     * @param groupId the group id
     * @param itemId the item id
     * @param order the order
     * @param title the title
     * @return the sub menu
     * @see com.actionbarsherlock.view.Menu#addSubMenu(int, int, int, java.lang.CharSequence)
     */
    @Override
    public SubMenu addSubMenu(int groupId, int itemId, int order, CharSequence title) {
        return addInternal(mNativeMenu.addSubMenu(groupId, itemId, order, title));
    }

    /**
     * Adds the sub menu.
     *
     * @param groupId the group id
     * @param itemId the item id
     * @param order the order
     * @param titleRes the title res
     * @return the sub menu
     * @see com.actionbarsherlock.view.Menu#addSubMenu(int, int, int, int)
     */
    @Override
    public SubMenu addSubMenu(int groupId, int itemId, int order, int titleRes) {
        return addInternal(mNativeMenu.addSubMenu(groupId, itemId, order, titleRes));
    }

    /**
     * Adds the intent options.
     *
     * @param groupId the group id
     * @param itemId the item id
     * @param order the order
     * @param caller the caller
     * @param specifics the specifics
     * @param intent the intent
     * @param flags the flags
     * @param outSpecificItems the out specific items
     * @return the int
     * @see com.actionbarsherlock.view.Menu#addIntentOptions(int, int, int, android.content.ComponentName, android.content.Intent[], android.content.Intent, int, com.actionbarsherlock.view.MenuItem[])
     */
    @Override
    public int addIntentOptions(int groupId, int itemId, int order, ComponentName caller, Intent[] specifics, Intent intent, int flags, MenuItem[] outSpecificItems) {
        android.view.MenuItem[] nativeOutItems = new android.view.MenuItem[outSpecificItems.length];
        int result = mNativeMenu.addIntentOptions(groupId, itemId, order, caller, specifics, intent, flags, nativeOutItems);
        for (int i = 0, length = outSpecificItems.length; i < length; i++) {
            outSpecificItems[i] = new MenuItemWrapper(nativeOutItems[i]);
        }
        return result;
    }

    /**
     * Removes the item.
     *
     * @param id the id
     * @see com.actionbarsherlock.view.Menu#removeItem(int)
     */
    @Override
    public void removeItem(int id) {
        mNativeMenu.removeItem(id);
    }

    /**
     * Removes the group.
     *
     * @param groupId the group id
     * @see com.actionbarsherlock.view.Menu#removeGroup(int)
     */
    @Override
    public void removeGroup(int groupId) {
        mNativeMenu.removeGroup(groupId);
    }

    /**
     * Clear.
     *
     * @see com.actionbarsherlock.view.Menu#clear()
     */
    @Override
    public void clear() {
        mNativeMap.clear();
        mNativeMenu.clear();
    }

    /**
     * Sets the group checkable.
     *
     * @param group the group
     * @param checkable the checkable
     * @param exclusive the exclusive
     * @see com.actionbarsherlock.view.Menu#setGroupCheckable(int, boolean, boolean)
     */
    @Override
    public void setGroupCheckable(int group, boolean checkable, boolean exclusive) {
        mNativeMenu.setGroupCheckable(group, checkable, exclusive);
    }

    /**
     * Sets the group visible.
     *
     * @param group the group
     * @param visible the visible
     * @see com.actionbarsherlock.view.Menu#setGroupVisible(int, boolean)
     */
    @Override
    public void setGroupVisible(int group, boolean visible) {
        mNativeMenu.setGroupVisible(group, visible);
    }

    /**
     * Sets the group enabled.
     *
     * @param group the group
     * @param enabled the enabled
     * @see com.actionbarsherlock.view.Menu#setGroupEnabled(int, boolean)
     */
    @Override
    public void setGroupEnabled(int group, boolean enabled) {
        mNativeMenu.setGroupEnabled(group, enabled);
    }

    /**
     * Checks for visible items.
     *
     * @return true, if successful
     * @see com.actionbarsherlock.view.Menu#hasVisibleItems()
     */
    @Override
    public boolean hasVisibleItems() {
        return mNativeMenu.hasVisibleItems();
    }

    /**
     * Find item.
     *
     * @param id the id
     * @return the menu item
     * @see com.actionbarsherlock.view.Menu#findItem(int)
     */
    @Override
    public MenuItem findItem(int id) {
        android.view.MenuItem nativeItem = mNativeMenu.findItem(id);
        return findItem(nativeItem);
    }

    /**
     * Find item.
     *
     * @param nativeItem the native item
     * @return the menu item
     */
    public MenuItem findItem(android.view.MenuItem nativeItem) {
        if (nativeItem == null) {
            return null;
        }

        MenuItem wrapped = mNativeMap.get(nativeItem);
        if (wrapped != null) {
            return wrapped;
        }

        return addInternal(nativeItem);
    }

    /**
     * Size.
     *
     * @return the int
     * @see com.actionbarsherlock.view.Menu#size()
     */
    @Override
    public int size() {
        return mNativeMenu.size();
    }

    /**
     * Gets the item.
     *
     * @param index the index
     * @return the item
     * @see com.actionbarsherlock.view.Menu#getItem(int)
     */
    @Override
    public MenuItem getItem(int index) {
        android.view.MenuItem nativeItem = mNativeMenu.getItem(index);
        return findItem(nativeItem);
    }

    /**
     * Close.
     *
     * @see com.actionbarsherlock.view.Menu#close()
     */
    @Override
    public void close() {
        mNativeMenu.close();
    }

    /**
     * Perform shortcut.
     *
     * @param keyCode the key code
     * @param event the event
     * @param flags the flags
     * @return true, if successful
     * @see com.actionbarsherlock.view.Menu#performShortcut(int, android.view.KeyEvent, int)
     */
    @Override
    public boolean performShortcut(int keyCode, KeyEvent event, int flags) {
        return mNativeMenu.performShortcut(keyCode, event, flags);
    }

    /**
     * Checks if is shortcut key.
     *
     * @param keyCode the key code
     * @param event the event
     * @return true, if is shortcut key
     * @see com.actionbarsherlock.view.Menu#isShortcutKey(int, android.view.KeyEvent)
     */
    @Override
    public boolean isShortcutKey(int keyCode, KeyEvent event) {
        return mNativeMenu.isShortcutKey(keyCode, event);
    }

    /**
     * Perform identifier action.
     *
     * @param id the id
     * @param flags the flags
     * @return true, if successful
     * @see com.actionbarsherlock.view.Menu#performIdentifierAction(int, int)
     */
    @Override
    public boolean performIdentifierAction(int id, int flags) {
        return mNativeMenu.performIdentifierAction(id, flags);
    }

    /**
     * Sets the qwerty mode.
     *
     * @param isQwerty the new qwerty mode
     * @see com.actionbarsherlock.view.Menu#setQwertyMode(boolean)
     */
    @Override
    public void setQwertyMode(boolean isQwerty) {
        mNativeMenu.setQwertyMode(isQwerty);
    }
}
