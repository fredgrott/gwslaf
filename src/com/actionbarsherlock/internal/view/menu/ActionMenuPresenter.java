/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.actionbarsherlock.internal.view.menu;

import static com.actionbarsherlock.internal.ResourcesCompat.getResources_getInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseBooleanArray;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ImageButton;
import org.bitbucket.fredgrott.gwslaf.R;
import com.actionbarsherlock.internal.view.View_HasStateListenerSupport;
import com.actionbarsherlock.internal.view.View_OnAttachStateChangeListener;
import com.actionbarsherlock.internal.view.menu.ActionMenuView.ActionMenuChildView;
import com.actionbarsherlock.view.ActionProvider;
import com.actionbarsherlock.view.MenuItem;

// TODO: Auto-generated Javadoc
/**
 * MenuPresenter for building action menus as seen in the action bar and action modes.
 */
public class ActionMenuPresenter extends BaseMenuPresenter
        implements ActionProvider.SubUiVisibilityListener {
    //UNUSED private static final String TAG = "ActionMenuPresenter";

    /** The m overflow button. */
    private View mOverflowButton;
    
    /** The m reserve overflow. */
    private boolean mReserveOverflow;
    
    /** The m reserve overflow set. */
    private boolean mReserveOverflowSet;
    
    /** The m width limit. */
    private int mWidthLimit;
    
    /** The m action item width limit. */
    private int mActionItemWidthLimit;
    
    /** The m max items. */
    private int mMaxItems;
    
    /** The m max items set. */
    private boolean mMaxItemsSet;
    
    /** The m strict width limit. */
    private boolean mStrictWidthLimit;
    
    /** The m width limit set. */
    private boolean mWidthLimitSet;
    
    /** The m expanded action views exclusive. */
    private boolean mExpandedActionViewsExclusive;

    /** The m min cell size. */
    private int mMinCellSize;

    // Group IDs that have been added as actions - used temporarily, allocated here for reuse.
    /** The m action button groups. */
    private final SparseBooleanArray mActionButtonGroups = new SparseBooleanArray();

    /** The m scrap action button view. */
    private View mScrapActionButtonView;

    /** The m overflow popup. */
    private OverflowPopup mOverflowPopup;
    
    /** The m action button popup. */
    private ActionButtonSubmenu mActionButtonPopup;

    /** The m posted open runnable. */
    private OpenOverflowRunnable mPostedOpenRunnable;

    /** The m popup presenter callback. */
    final PopupPresenterCallback mPopupPresenterCallback = new PopupPresenterCallback();
    
    /** The m open sub menu id. */
    int mOpenSubMenuId;

    /**
     * Instantiates a new action menu presenter.
     *
     * @param context the context
     */
    public ActionMenuPresenter(Context context) {
        super(context, R.layout.abs__action_menu_layout,
                R.layout.abs__action_menu_item_layout);
    }

    /* (non-Javadoc)
     * @see com.actionbarsherlock.internal.view.menu.BaseMenuPresenter#initForMenu(android.content.Context, com.actionbarsherlock.internal.view.menu.MenuBuilder)
     */
    @Override
    public void initForMenu(Context context, MenuBuilder menu) {
        super.initForMenu(context, menu);

        final Resources res = context.getResources();

        if (!mReserveOverflowSet) {
            mReserveOverflow = reserveOverflow(mContext);
        }

        if (!mWidthLimitSet) {
            mWidthLimit = res.getDisplayMetrics().widthPixels / 2;
        }

        // Measure for initial configuration
        if (!mMaxItemsSet) {
            mMaxItems = getResources_getInteger(context, R.integer.abs__max_action_buttons);
        }

        int width = mWidthLimit;
        if (mReserveOverflow) {
            if (mOverflowButton == null) {
                mOverflowButton = new OverflowMenuButton(mSystemContext);
                final int spec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
                mOverflowButton.measure(spec, spec);
            }
            width -= mOverflowButton.getMeasuredWidth();
        } else {
            mOverflowButton = null;
        }

        mActionItemWidthLimit = width;

        mMinCellSize = (int) (ActionMenuView.MIN_CELL_SIZE * res.getDisplayMetrics().density);

        // Drop a scrap view as it may no longer reflect the proper context/config.
        mScrapActionButtonView = null;
    }

    /**
     * Reserve overflow.
     *
     * @param context the context
     * @return true, if successful
     */
    public static boolean reserveOverflow(Context context) {
        //Check for theme-forced overflow action item
        TypedArray a = context.getTheme().obtainStyledAttributes(R.styleable.SherlockTheme);
        boolean result = a.getBoolean(R.styleable.SherlockTheme_absForceOverflow, false);
        a.recycle();
        if (result) {
            return true;
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB);
        } else {
            return !HasPermanentMenuKey.get(context);
        }
    }

    /**
     * The Class HasPermanentMenuKey.
     */
    private static class HasPermanentMenuKey {
        
        /**
         * Gets the.
         *
         * @param context the context
         * @return true, if successful
         */
        public static boolean get(Context context) {
            return ViewConfiguration.get(context).hasPermanentMenuKey();
        }
    }

    /**
     * On configuration changed.
     *
     * @param newConfig the new config
     */
    public void onConfigurationChanged(Configuration newConfig) {
        if (!mMaxItemsSet) {
            mMaxItems = getResources_getInteger(mContext,
                    R.integer.abs__max_action_buttons);
            if (mMenu != null) {
                mMenu.onItemsChanged(true);
            }
        }
    }

    /**
     * Sets the width limit.
     *
     * @param width the width
     * @param strict the strict
     */
    public void setWidthLimit(int width, boolean strict) {
        mWidthLimit = width;
        mStrictWidthLimit = strict;
        mWidthLimitSet = true;
    }

    /**
     * Sets the reserve overflow.
     *
     * @param reserveOverflow the new reserve overflow
     */
    public void setReserveOverflow(boolean reserveOverflow) {
        mReserveOverflow = reserveOverflow;
        mReserveOverflowSet = true;
    }

    /**
     * Sets the item limit.
     *
     * @param itemCount the new item limit
     */
    public void setItemLimit(int itemCount) {
        mMaxItems = itemCount;
        mMaxItemsSet = true;
    }

    /**
     * Sets the expanded action views exclusive.
     *
     * @param isExclusive the new expanded action views exclusive
     */
    public void setExpandedActionViewsExclusive(boolean isExclusive) {
        mExpandedActionViewsExclusive = isExclusive;
    }

    /* (non-Javadoc)
     * @see com.actionbarsherlock.internal.view.menu.BaseMenuPresenter#getMenuView(android.view.ViewGroup)
     */
    @Override
    public MenuView getMenuView(ViewGroup root) {
        MenuView result = super.getMenuView(root);
        ((ActionMenuView) result).setPresenter(this);
        return result;
    }

    /**
     * Gets the item view.
     *
     * @param item the item
     * @param convertView the convert view
     * @param parent the parent
     * @return the item view
     * @see com.actionbarsherlock.internal.view.menu.BaseMenuPresenter#getItemView(com.actionbarsherlock.internal.view.menu.MenuItemImpl, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getItemView(MenuItemImpl item, View convertView, ViewGroup parent) {
        View actionView = item.getActionView();
        if (actionView == null || item.hasCollapsibleActionView()) {
            if (!(convertView instanceof ActionMenuItemView)) {
                convertView = null;
            }
            actionView = super.getItemView(item, convertView, parent);
        }
        actionView.setVisibility(item.isActionViewExpanded() ? View.GONE : View.VISIBLE);

        final ActionMenuView menuParent = (ActionMenuView) parent;
        final ViewGroup.LayoutParams lp = actionView.getLayoutParams();
        if (!menuParent.checkLayoutParams(lp)) {
            actionView.setLayoutParams(menuParent.generateLayoutParams(lp));
        }
        return actionView;
    }

    /**
     * Bind item view.
     *
     * @param item the item
     * @param itemView the item view
     * @see com.actionbarsherlock.internal.view.menu.BaseMenuPresenter#bindItemView(com.actionbarsherlock.internal.view.menu.MenuItemImpl, com.actionbarsherlock.internal.view.menu.MenuView.ItemView)
     */
    @Override
    public void bindItemView(MenuItemImpl item, MenuView.ItemView itemView) {
        itemView.initialize(item, 0);

        final ActionMenuView menuView = (ActionMenuView) mMenuView;
        ActionMenuItemView actionItemView = (ActionMenuItemView) itemView;
        actionItemView.setItemInvoker(menuView);
    }

    /**
     * Should include item.
     *
     * @param childIndex the child index
     * @param item the item
     * @return true, if successful
     * @see com.actionbarsherlock.internal.view.menu.BaseMenuPresenter#shouldIncludeItem(int, com.actionbarsherlock.internal.view.menu.MenuItemImpl)
     */
    @Override
    public boolean shouldIncludeItem(int childIndex, MenuItemImpl item) {
        return item.isActionButton();
    }

    /**
     * Update menu view.
     *
     * @param cleared the cleared
     * @see com.actionbarsherlock.internal.view.menu.BaseMenuPresenter#updateMenuView(boolean)
     */
    @Override
    public void updateMenuView(boolean cleared) {
        super.updateMenuView(cleared);

        if (mMenu != null) {
            final ArrayList<MenuItemImpl> actionItems = mMenu.getActionItems();
            final int count = actionItems.size();
            for (int i = 0; i < count; i++) {
                final ActionProvider provider = actionItems.get(i).getActionProvider();
                if (provider != null) {
                    provider.setSubUiVisibilityListener(this);
                }
            }
        }

        final ArrayList<MenuItemImpl> nonActionItems = mMenu != null ?
                mMenu.getNonActionItems() : null;

        boolean hasOverflow = false;
        if (mReserveOverflow && nonActionItems != null) {
            final int count = nonActionItems.size();
            if (count == 1) {
                hasOverflow = !nonActionItems.get(0).isActionViewExpanded();
            } else {
                hasOverflow = count > 0;
            }
        }

        if (hasOverflow) {
            if (mOverflowButton == null) {
                mOverflowButton = new OverflowMenuButton(mSystemContext);
            }
            ViewGroup parent = (ViewGroup) mOverflowButton.getParent();
            if (parent != mMenuView) {
                if (parent != null) {
                    parent.removeView(mOverflowButton);
                }
                ActionMenuView menuView = (ActionMenuView) mMenuView;
                menuView.addView(mOverflowButton, menuView.generateOverflowButtonLayoutParams());
            }
        } else if (mOverflowButton != null && mOverflowButton.getParent() == mMenuView) {
            ((ViewGroup) mMenuView).removeView(mOverflowButton);
        }

        ((ActionMenuView) mMenuView).setOverflowReserved(mReserveOverflow);
    }

    /**
     * Filter leftover view.
     *
     * @param parent the parent
     * @param childIndex the child index
     * @return true, if successful
     * @see com.actionbarsherlock.internal.view.menu.BaseMenuPresenter#filterLeftoverView(android.view.ViewGroup, int)
     */
    @Override
    public boolean filterLeftoverView(ViewGroup parent, int childIndex) {
        if (parent.getChildAt(childIndex) == mOverflowButton) return false;
        return super.filterLeftoverView(parent, childIndex);
    }

    /**
     * On sub menu selected.
     *
     * @param subMenu the sub menu
     * @return true, if successful
     * @see com.actionbarsherlock.internal.view.menu.BaseMenuPresenter#onSubMenuSelected(com.actionbarsherlock.internal.view.menu.SubMenuBuilder)
     */
    public boolean onSubMenuSelected(SubMenuBuilder subMenu) {
        if (!subMenu.hasVisibleItems()) return false;

        SubMenuBuilder topSubMenu = subMenu;
        while (topSubMenu.getParentMenu() != mMenu) {
            topSubMenu = (SubMenuBuilder) topSubMenu.getParentMenu();
        }
        View anchor = findViewForItem(topSubMenu.getItem());
        if (anchor == null) {
            if (mOverflowButton == null) return false;
            anchor = mOverflowButton;
        }

        mOpenSubMenuId = subMenu.getItem().getItemId();
        mActionButtonPopup = new ActionButtonSubmenu(mContext, subMenu);
        mActionButtonPopup.setAnchorView(anchor);
        mActionButtonPopup.show();
        super.onSubMenuSelected(subMenu);
        return true;
    }

    /**
     * Find view for item.
     *
     * @param item the item
     * @return the view
     */
    private View findViewForItem(MenuItem item) {
        final ViewGroup parent = (ViewGroup) mMenuView;
        if (parent == null) return null;

        final int count = parent.getChildCount();
        for (int i = 0; i < count; i++) {
            final View child = parent.getChildAt(i);
            if (child instanceof MenuView.ItemView &&
                    ((MenuView.ItemView) child).getItemData() == item) {
                return child;
            }
        }
        return null;
    }

    /**
     * Display the overflow menu if one is present.
     * @return true if the overflow menu was shown, false otherwise.
     */
    public boolean showOverflowMenu() {
        if (mReserveOverflow && !isOverflowMenuShowing() && mMenu != null && mMenuView != null &&
                mPostedOpenRunnable == null && !mMenu.getNonActionItems().isEmpty()) {
            OverflowPopup popup = new OverflowPopup(mContext, mMenu, mOverflowButton, true);
            mPostedOpenRunnable = new OpenOverflowRunnable(popup);
            // Post this for later; we might still need a layout for the anchor to be right.
            ((View) mMenuView).post(mPostedOpenRunnable);

            // ActionMenuPresenter uses null as a callback argument here
            // to indicate overflow is opening.
            super.onSubMenuSelected(null);

            return true;
        }
        return false;
    }

    /**
     * Hide the overflow menu if it is currently showing.
     *
     * @return true if the overflow menu was hidden, false otherwise.
     */
    public boolean hideOverflowMenu() {
        if (mPostedOpenRunnable != null && mMenuView != null) {
            ((View) mMenuView).removeCallbacks(mPostedOpenRunnable);
            mPostedOpenRunnable = null;
            return true;
        }

        MenuPopupHelper popup = mOverflowPopup;
        if (popup != null) {
            popup.dismiss();
            return true;
        }
        return false;
    }

    /**
     * Dismiss all popup menus - overflow and submenus.
     * @return true if popups were dismissed, false otherwise. (This can be because none were open.)
     */
    public boolean dismissPopupMenus() {
        boolean result = hideOverflowMenu();
        result |= hideSubMenus();
        return result;
    }

    /**
     * Dismiss all submenu popups.
     *
     * @return true if popups were dismissed, false otherwise. (This can be because none were open.)
     */
    public boolean hideSubMenus() {
        if (mActionButtonPopup != null) {
            mActionButtonPopup.dismiss();
            return true;
        }
        return false;
    }

    /**
     * Checks if is overflow menu showing.
     *
     * @return true if the overflow menu is currently showing
     */
    public boolean isOverflowMenuShowing() {
        return mOverflowPopup != null && mOverflowPopup.isShowing();
    }

    /**
     * Checks if is overflow reserved.
     *
     * @return true if space has been reserved in the action menu for an overflow item.
     */
    public boolean isOverflowReserved() {
        return mReserveOverflow;
    }

    /**
     * Flag action items.
     *
     * @return true, if successful
     * @see com.actionbarsherlock.internal.view.menu.BaseMenuPresenter#flagActionItems()
     */
    public boolean flagActionItems() {
        final ArrayList<MenuItemImpl> visibleItems = mMenu.getVisibleItems();
        final int itemsSize = visibleItems.size();
        int maxActions = mMaxItems;
        int widthLimit = mActionItemWidthLimit;
        final int querySpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        final ViewGroup parent = (ViewGroup) mMenuView;

        int requiredItems = 0;
        int requestedItems = 0;
        int firstActionWidth = 0;
        boolean hasOverflow = false;
        for (int i = 0; i < itemsSize; i++) {
            MenuItemImpl item = visibleItems.get(i);
            if (item.requiresActionButton()) {
                requiredItems++;
            } else if (item.requestsActionButton()) {
                requestedItems++;
            } else {
                hasOverflow = true;
            }
            if (mExpandedActionViewsExclusive && item.isActionViewExpanded()) {
                // Overflow everything if we have an expanded action view and we're
                // space constrained.
                maxActions = 0;
            }
        }

        // Reserve a spot for the overflow item if needed.
        if (mReserveOverflow &&
                (hasOverflow || requiredItems + requestedItems > maxActions)) {
            maxActions--;
        }
        maxActions -= requiredItems;

        final SparseBooleanArray seenGroups = mActionButtonGroups;
        seenGroups.clear();

        int cellSize = 0;
        int cellsRemaining = 0;
        if (mStrictWidthLimit) {
            cellsRemaining = widthLimit / mMinCellSize;
            final int cellSizeRemaining = widthLimit % mMinCellSize;
            cellSize = mMinCellSize + cellSizeRemaining / cellsRemaining;
        }

        // Flag as many more requested items as will fit.
        for (int i = 0; i < itemsSize; i++) {
            MenuItemImpl item = visibleItems.get(i);

            if (item.requiresActionButton()) {
                View v = getItemView(item, mScrapActionButtonView, parent);
                if (mScrapActionButtonView == null) {
                    mScrapActionButtonView = v;
                }
                if (mStrictWidthLimit) {
                    cellsRemaining -= ActionMenuView.measureChildForCells(v,
                            cellSize, cellsRemaining, querySpec, 0);
                } else {
                    v.measure(querySpec, querySpec);
                }
                final int measuredWidth = v.getMeasuredWidth();
                widthLimit -= measuredWidth;
                if (firstActionWidth == 0) {
                    firstActionWidth = measuredWidth;
                }
                final int groupId = item.getGroupId();
                if (groupId != 0) {
                    seenGroups.put(groupId, true);
                }
                item.setIsActionButton(true);
            } else if (item.requestsActionButton()) {
                // Items in a group with other items that already have an action slot
                // can break the max actions rule, but not the width limit.
                final int groupId = item.getGroupId();
                final boolean inGroup = seenGroups.get(groupId);
                boolean isAction = (maxActions > 0 || inGroup) && widthLimit > 0 &&
                        (!mStrictWidthLimit || cellsRemaining > 0);

                if (isAction) {
                    View v = getItemView(item, mScrapActionButtonView, parent);
                    if (mScrapActionButtonView == null) {
                        mScrapActionButtonView = v;
                    }
                    if (mStrictWidthLimit) {
                        final int cells = ActionMenuView.measureChildForCells(v,
                                cellSize, cellsRemaining, querySpec, 0);
                        cellsRemaining -= cells;
                        if (cells == 0) {
                            isAction = false;
                        }
                    } else {
                        v.measure(querySpec, querySpec);
                    }
                    final int measuredWidth = v.getMeasuredWidth();
                    widthLimit -= measuredWidth;
                    if (firstActionWidth == 0) {
                        firstActionWidth = measuredWidth;
                    }

                    if (mStrictWidthLimit) {
                        isAction &= widthLimit >= 0;
                    } else {
                        // Did this push the entire first item past the limit?
                        isAction &= widthLimit + firstActionWidth > 0;
                    }
                }

                if (isAction && groupId != 0) {
                    seenGroups.put(groupId, true);
                } else if (inGroup) {
                    // We broke the width limit. Demote the whole group, they all overflow now.
                    seenGroups.put(groupId, false);
                    for (int j = 0; j < i; j++) {
                        MenuItemImpl areYouMyGroupie = visibleItems.get(j);
                        if (areYouMyGroupie.getGroupId() == groupId) {
                            // Give back the action slot
                            if (areYouMyGroupie.isActionButton()) maxActions++;
                            areYouMyGroupie.setIsActionButton(false);
                        }
                    }
                }

                if (isAction) maxActions--;

                item.setIsActionButton(isAction);
            }
        }
        return true;
    }

    /**
     * On close menu.
     *
     * @param menu the menu
     * @param allMenusAreClosing the all menus are closing
     * @see com.actionbarsherlock.internal.view.menu.BaseMenuPresenter#onCloseMenu(com.actionbarsherlock.internal.view.menu.MenuBuilder, boolean)
     */
    @Override
    public void onCloseMenu(MenuBuilder menu, boolean allMenusAreClosing) {
        dismissPopupMenus();
        super.onCloseMenu(menu, allMenusAreClosing);
    }

    /**
     * On save instance state.
     *
     * @return the parcelable
     * @see com.actionbarsherlock.internal.view.menu.MenuPresenter#onSaveInstanceState()
     */
    @Override
    public Parcelable onSaveInstanceState() {
        SavedState state = new SavedState();
        state.openSubMenuId = mOpenSubMenuId;
        return state;
    }

    /**
     * On restore instance state.
     *
     * @param state the state
     * @see com.actionbarsherlock.internal.view.menu.MenuPresenter#onRestoreInstanceState(android.os.Parcelable)
     */
    @Override
    public void onRestoreInstanceState(Parcelable state) {
        SavedState saved = (SavedState) state;
        if (saved.openSubMenuId > 0) {
            MenuItem item = mMenu.findItem(saved.openSubMenuId);
            if (item != null) {
                SubMenuBuilder subMenu = (SubMenuBuilder) item.getSubMenu();
                onSubMenuSelected(subMenu);
            }
        }
    }

    /**
     * On sub ui visibility changed.
     *
     * @param isVisible the is visible
     * @see com.actionbarsherlock.view.ActionProvider.SubUiVisibilityListener#onSubUiVisibilityChanged(boolean)
     */
    @Override
    public void onSubUiVisibilityChanged(boolean isVisible) {
        if (isVisible) {
            // Not a submenu, but treat it like one.
            super.onSubMenuSelected(null);
        } else {
            mMenu.close(false);
        }
    }

    /**
     * The Class SavedState.
     */
    private static class SavedState implements Parcelable {
        
        /** The open sub menu id. */
        public int openSubMenuId;

        /**
         * Instantiates a new saved state.
         */
        SavedState() {
        }

        /**
         * Instantiates a new saved state.
         *
         * @param in the in
         */
        SavedState(Parcel in) {
            openSubMenuId = in.readInt();
        }

        /**
         * Describe contents.
         *
         * @return the int
         * @see android.os.Parcelable#describeContents()
         */
        @Override
        public int describeContents() {
            return 0;
        }

        /**
         * Write to parcel.
         *
         * @param dest the dest
         * @param flags the flags
         * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
         */
        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(openSubMenuId);
        }

        /** The Constant CREATOR. */
        @SuppressWarnings("unused")
        public static final Parcelable.Creator<SavedState> CREATOR
                = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }

    /**
     * The Class OverflowMenuButton.
     */
    private class OverflowMenuButton extends ImageButton implements ActionMenuChildView, View_HasStateListenerSupport {
        
        /** The m listeners. */
        private final Set<View_OnAttachStateChangeListener> mListeners = new HashSet<View_OnAttachStateChangeListener>();

        /**
         * Instantiates a new overflow menu button.
         *
         * @param context the context
         */
        public OverflowMenuButton(Context context) {
            super(context, null, R.attr.actionOverflowButtonStyle);

            setClickable(true);
            setFocusable(true);
            setVisibility(VISIBLE);
            setEnabled(true);
        }

        /**
         * Perform click.
         *
         * @return true, if successful
         * @see android.view.View#performClick()
         */
        @Override
        public boolean performClick() {
            if (super.performClick()) {
                return true;
            }

            playSoundEffect(SoundEffectConstants.CLICK);
            showOverflowMenu();
            return true;
        }

        /**
         * Needs divider before.
         *
         * @return true, if successful
         * @see com.actionbarsherlock.internal.view.menu.ActionMenuView.ActionMenuChildView#needsDividerBefore()
         */
        public boolean needsDividerBefore() {
            return false;
        }

        /**
         * Needs divider after.
         *
         * @return true, if successful
         * @see com.actionbarsherlock.internal.view.menu.ActionMenuView.ActionMenuChildView#needsDividerAfter()
         */
        public boolean needsDividerAfter() {
            return false;
        }

        /* (non-Javadoc)
         * @see android.widget.ImageView#onAttachedToWindow()
         */
        @Override
        protected void onAttachedToWindow() {
            super.onAttachedToWindow();
            for (View_OnAttachStateChangeListener listener : mListeners) {
                listener.onViewAttachedToWindow(this);
            }
        }

        /* (non-Javadoc)
         * @see android.widget.ImageView#onDetachedFromWindow()
         */
        @Override
        protected void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            for (View_OnAttachStateChangeListener listener : mListeners) {
                listener.onViewDetachedFromWindow(this);
            }
        }

        /**
         * Adds the on attach state change listener.
         *
         * @param listener the listener
         * @see com.actionbarsherlock.internal.view.View_HasStateListenerSupport#addOnAttachStateChangeListener(com.actionbarsherlock.internal.view.View_OnAttachStateChangeListener)
         */
        @Override
        public void addOnAttachStateChangeListener(View_OnAttachStateChangeListener listener) {
            mListeners.add(listener);
        }

        /**
         * Removes the on attach state change listener.
         *
         * @param listener the listener
         * @see com.actionbarsherlock.internal.view.View_HasStateListenerSupport#removeOnAttachStateChangeListener(com.actionbarsherlock.internal.view.View_OnAttachStateChangeListener)
         */
        @Override
        public void removeOnAttachStateChangeListener(View_OnAttachStateChangeListener listener) {
            mListeners.remove(listener);
        }
    }

    /**
     * The Class OverflowPopup.
     */
    private class OverflowPopup extends MenuPopupHelper {
        
        /**
         * Instantiates a new overflow popup.
         *
         * @param context the context
         * @param menu the menu
         * @param anchorView the anchor view
         * @param overflowOnly the overflow only
         */
        public OverflowPopup(Context context, MenuBuilder menu, View anchorView,
                boolean overflowOnly) {
            super(context, menu, anchorView, overflowOnly);
            setCallback(mPopupPresenterCallback);
        }

        /**
         * On dismiss.
         *
         * @see com.actionbarsherlock.internal.view.menu.MenuPopupHelper#onDismiss()
         */
        @Override
        public void onDismiss() {
            super.onDismiss();
            mMenu.close();
            mOverflowPopup = null;
        }
    }

    /**
     * The Class ActionButtonSubmenu.
     */
    private class ActionButtonSubmenu extends MenuPopupHelper {
        //UNUSED private SubMenuBuilder mSubMenu;

        /**
         * Instantiates a new action button submenu.
         *
         * @param context the context
         * @param subMenu the sub menu
         */
        public ActionButtonSubmenu(Context context, SubMenuBuilder subMenu) {
            super(context, subMenu);
            //UNUSED mSubMenu = subMenu;

            MenuItemImpl item = (MenuItemImpl) subMenu.getItem();
            if (!item.isActionButton()) {
                // Give a reasonable anchor to nested submenus.
                setAnchorView(mOverflowButton == null ? (View) mMenuView : mOverflowButton);
            }

            setCallback(mPopupPresenterCallback);

            boolean preserveIconSpacing = false;
            final int count = subMenu.size();
            for (int i = 0; i < count; i++) {
                MenuItem childItem = subMenu.getItem(i);
                if (childItem.isVisible() && childItem.getIcon() != null) {
                    preserveIconSpacing = true;
                    break;
                }
            }
            setForceShowIcon(preserveIconSpacing);
        }

        /**
         * On dismiss.
         *
         * @see com.actionbarsherlock.internal.view.menu.MenuPopupHelper#onDismiss()
         */
        @Override
        public void onDismiss() {
            super.onDismiss();
            mActionButtonPopup = null;
            mOpenSubMenuId = 0;
        }
    }

    /**
     * The Class PopupPresenterCallback.
     */
    private class PopupPresenterCallback implements MenuPresenter.Callback {

        /**
         * On open sub menu.
         *
         * @param subMenu the sub menu
         * @return true, if successful
         * @see com.actionbarsherlock.internal.view.menu.MenuPresenter.Callback#onOpenSubMenu(com.actionbarsherlock.internal.view.menu.MenuBuilder)
         */
        @Override
        public boolean onOpenSubMenu(MenuBuilder subMenu) {
            if (subMenu == null) return false;

            mOpenSubMenuId = ((SubMenuBuilder) subMenu).getItem().getItemId();
            return false;
        }

        /**
         * On close menu.
         *
         * @param menu the menu
         * @param allMenusAreClosing the all menus are closing
         * @see com.actionbarsherlock.internal.view.menu.MenuPresenter.Callback#onCloseMenu(com.actionbarsherlock.internal.view.menu.MenuBuilder, boolean)
         */
        @Override
        public void onCloseMenu(MenuBuilder menu, boolean allMenusAreClosing) {
            if (menu instanceof SubMenuBuilder) {
                ((SubMenuBuilder) menu).getRootMenu().close(false);
            }
        }
    }

    /**
     * The Class OpenOverflowRunnable.
     */
    private class OpenOverflowRunnable implements Runnable {
        
        /** The m popup. */
        private OverflowPopup mPopup;

        /**
         * Instantiates a new open overflow runnable.
         *
         * @param popup the popup
         */
        public OpenOverflowRunnable(OverflowPopup popup) {
            mPopup = popup;
        }

        /**
         * Run.
         *
         * @see java.lang.Runnable#run()
         */
        public void run() {
            mMenu.changeMenuMode();
            final View menuView = (View) mMenuView;
            if (menuView != null && menuView.getWindowToken() != null && mPopup.tryShow()) {
                mOverflowPopup = mPopup;
            }
            mPostedOpenRunnable = null;
        }
    }
}
