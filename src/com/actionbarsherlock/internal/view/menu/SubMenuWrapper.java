package com.actionbarsherlock.internal.view.menu;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;

// TODO: Auto-generated Javadoc
/**
 * The Class SubMenuWrapper.
 */
public class SubMenuWrapper extends MenuWrapper implements SubMenu {
    
    /** The m native sub menu. */
    private final android.view.SubMenu mNativeSubMenu;
    
    /** The m item. */
    private MenuItem mItem = null;

    /**
     * Instantiates a new sub menu wrapper.
     *
     * @param nativeSubMenu the native sub menu
     */
    public SubMenuWrapper(android.view.SubMenu nativeSubMenu) {
        super(nativeSubMenu);
        mNativeSubMenu = nativeSubMenu;
    }


    /**
     * Sets the header title.
     *
     * @param titleRes the title res
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setHeaderTitle(int)
     */
    @Override
    public SubMenu setHeaderTitle(int titleRes) {
        mNativeSubMenu.setHeaderTitle(titleRes);
        return this;
    }

    /**
     * Sets the header title.
     *
     * @param title the title
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setHeaderTitle(java.lang.CharSequence)
     */
    @Override
    public SubMenu setHeaderTitle(CharSequence title) {
        mNativeSubMenu.setHeaderTitle(title);
        return this;
    }

    /**
     * Sets the header icon.
     *
     * @param iconRes the icon res
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setHeaderIcon(int)
     */
    @Override
    public SubMenu setHeaderIcon(int iconRes) {
        mNativeSubMenu.setHeaderIcon(iconRes);
        return this;
    }

    /**
     * Sets the header icon.
     *
     * @param icon the icon
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setHeaderIcon(android.graphics.drawable.Drawable)
     */
    @Override
    public SubMenu setHeaderIcon(Drawable icon) {
        mNativeSubMenu.setHeaderIcon(icon);
        return this;
    }

    /**
     * Sets the header view.
     *
     * @param view the view
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setHeaderView(android.view.View)
     */
    @Override
    public SubMenu setHeaderView(View view) {
        mNativeSubMenu.setHeaderView(view);
        return this;
    }

    /**
     * Clear header.
     *
     * @see com.actionbarsherlock.view.SubMenu#clearHeader()
     */
    @Override
    public void clearHeader() {
        mNativeSubMenu.clearHeader();
    }

    /**
     * Sets the icon.
     *
     * @param iconRes the icon res
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setIcon(int)
     */
    @Override
    public SubMenu setIcon(int iconRes) {
        mNativeSubMenu.setIcon(iconRes);
        return this;
    }

    /**
     * Sets the icon.
     *
     * @param icon the icon
     * @return the sub menu
     * @see com.actionbarsherlock.view.SubMenu#setIcon(android.graphics.drawable.Drawable)
     */
    @Override
    public SubMenu setIcon(Drawable icon) {
        mNativeSubMenu.setIcon(icon);
        return this;
    }

    /**
     * Gets the item.
     *
     * @return the item
     * @see com.actionbarsherlock.view.SubMenu#getItem()
     */
    @Override
    public MenuItem getItem() {
        if (mItem == null) {
            mItem = new MenuItemWrapper(mNativeSubMenu.getItem());
        }
        return mItem;
    }
}
