/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.actionbarsherlock.internal.app;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.widget.SpinnerAdapter;
import org.bitbucket.fredgrott.gwslaf.R;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.internal.nineoldandroids.animation.Animator;
import com.actionbarsherlock.internal.nineoldandroids.animation.AnimatorListenerAdapter;
import com.actionbarsherlock.internal.nineoldandroids.animation.AnimatorSet;
import com.actionbarsherlock.internal.nineoldandroids.animation.ObjectAnimator;
import com.actionbarsherlock.internal.nineoldandroids.animation.Animator.AnimatorListener;
import com.actionbarsherlock.internal.nineoldandroids.widget.NineFrameLayout;
import com.actionbarsherlock.internal.view.menu.MenuBuilder;
import com.actionbarsherlock.internal.view.menu.MenuPopupHelper;
import com.actionbarsherlock.internal.view.menu.SubMenuBuilder;
import com.actionbarsherlock.internal.widget.ActionBarContainer;
import com.actionbarsherlock.internal.widget.ActionBarContextView;
import com.actionbarsherlock.internal.widget.ActionBarView;
import com.actionbarsherlock.internal.widget.ScrollingTabContainerView;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import static com.actionbarsherlock.internal.ResourcesCompat.getResources_getBoolean;

// TODO: Auto-generated Javadoc
/**
 * ActionBarImpl is the ActionBar implementation used
 * by devices of all screen sizes. If it detects a compatible decor,
 * it will split contextual modes across both the ActionBarView at
 * the top of the screen and a horizontal LinearLayout at the bottom
 * which is normally hidden.
 */
public class ActionBarImpl extends ActionBar {
    //UNUSED private static final String TAG = "ActionBarImpl";

    /** The m context. */
    private Context mContext;
    
    /** The m themed context. */
    private Context mThemedContext;
    
    /** The m activity. */
    private Activity mActivity;
    //UNUSED private Dialog mDialog;

    /** The m container view. */
    private ActionBarContainer mContainerView;
    
    /** The m action view. */
    private ActionBarView mActionView;
    
    /** The m context view. */
    private ActionBarContextView mContextView;
    
    /** The m split view. */
    private ActionBarContainer mSplitView;
    
    /** The m content view. */
    private NineFrameLayout mContentView;
    
    /** The m tab scroll view. */
    private ScrollingTabContainerView mTabScrollView;

    /** The m tabs. */
    private ArrayList<TabImpl> mTabs = new ArrayList<TabImpl>();

    /** The m selected tab. */
    private TabImpl mSelectedTab;
    
    /** The m saved tab position. */
    private int mSavedTabPosition = INVALID_POSITION;

    /** The m action mode. */
    ActionModeImpl mActionMode;
    
    /** The m deferred destroy action mode. */
    ActionMode mDeferredDestroyActionMode;
    
    /** The m deferred mode destroy callback. */
    ActionMode.Callback mDeferredModeDestroyCallback;

    /** The m last menu visibility. */
    private boolean mLastMenuVisibility;
    
    /** The m menu visibility listeners. */
    private ArrayList<OnMenuVisibilityListener> mMenuVisibilityListeners =
            new ArrayList<OnMenuVisibilityListener>();

    /** The Constant CONTEXT_DISPLAY_NORMAL. */
    private static final int CONTEXT_DISPLAY_NORMAL = 0;
    
    /** The Constant CONTEXT_DISPLAY_SPLIT. */
    private static final int CONTEXT_DISPLAY_SPLIT = 1;

    /** The Constant INVALID_POSITION. */
    private static final int INVALID_POSITION = -1;

    /** The m context display mode. */
    private int mContextDisplayMode;
    
    /** The m has embedded tabs. */
    private boolean mHasEmbeddedTabs;

    /** The m handler. */
    final Handler mHandler = new Handler();
    
    /** The m tab selector. */
    Runnable mTabSelector;

    /** The m current show anim. */
    private Animator mCurrentShowAnim;
    
    /** The m current mode anim. */
    private Animator mCurrentModeAnim;
    
    /** The m show hide animation enabled. */
    private boolean mShowHideAnimationEnabled;
    
    /** The m was hidden before mode. */
    boolean mWasHiddenBeforeMode;

    /** The m hide listener. */
    final AnimatorListener mHideListener = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
            if (mContentView != null) {
                mContentView.setTranslationY(0);
                mContainerView.setTranslationY(0);
            }
            if (mSplitView != null && mContextDisplayMode == CONTEXT_DISPLAY_SPLIT) {
                mSplitView.setVisibility(View.GONE);
            }
            mContainerView.setVisibility(View.GONE);
            mContainerView.setTransitioning(false);
            mCurrentShowAnim = null;
            completeDeferredDestroyActionMode();
        }
    };

    /** The m show listener. */
    final AnimatorListener mShowListener = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
            mCurrentShowAnim = null;
            mContainerView.requestLayout();
        }
    };

    /**
     * Instantiates a new action bar impl.
     *
     * @param activity the activity
     * @param features the features
     */
    public ActionBarImpl(Activity activity, int features) {
        mActivity = activity;
        Window window = activity.getWindow();
        View decor = window.getDecorView();
        init(decor);

        //window.hasFeature() workaround for pre-3.0
        if ((features & (1 << Window.FEATURE_ACTION_BAR_OVERLAY)) == 0) {
            mContentView = (NineFrameLayout)decor.findViewById(android.R.id.content);
        }
    }

    /**
     * Instantiates a new action bar impl.
     *
     * @param dialog the dialog
     */
    public ActionBarImpl(Dialog dialog) {
        //UNUSED mDialog = dialog;
        init(dialog.getWindow().getDecorView());
    }

    /**
     * Inits the.
     *
     * @param decor the decor
     */
    private void init(View decor) {
        mContext = decor.getContext();
        mActionView = (ActionBarView) decor.findViewById(R.id.abs__action_bar);
        mContextView = (ActionBarContextView) decor.findViewById(
                R.id.abs__action_context_bar);
        mContainerView = (ActionBarContainer) decor.findViewById(
                R.id.abs__action_bar_container);
        mSplitView = (ActionBarContainer) decor.findViewById(
                R.id.abs__split_action_bar);

        if (mActionView == null || mContextView == null || mContainerView == null) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " +
                    "with a compatible window decor layout");
        }

        mActionView.setContextView(mContextView);
        mContextDisplayMode = mActionView.isSplitActionBar() ?
                CONTEXT_DISPLAY_SPLIT : CONTEXT_DISPLAY_NORMAL;

        // Older apps get the home button interaction enabled by default.
        // Newer apps need to enable it explicitly.
        setHomeButtonEnabled(mContext.getApplicationInfo().targetSdkVersion < 14);

        setHasEmbeddedTabs(getResources_getBoolean(mContext,
                R.bool.abs__action_bar_embed_tabs));
    }

    /**
     * On configuration changed.
     *
     * @param newConfig the new config
     */
    public void onConfigurationChanged(Configuration newConfig) {
        setHasEmbeddedTabs(getResources_getBoolean(mContext,
                R.bool.abs__action_bar_embed_tabs));

        //Manually dispatch a configuration change to the action bar view on pre-2.2
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
            mActionView.onConfigurationChanged(newConfig);
            if (mContextView != null) {
                mContextView.onConfigurationChanged(newConfig);
            }
        }
    }

    /**
     * Sets the checks for embedded tabs.
     *
     * @param hasEmbeddedTabs the new checks for embedded tabs
     */
    private void setHasEmbeddedTabs(boolean hasEmbeddedTabs) {
        mHasEmbeddedTabs = hasEmbeddedTabs;
        // Switch tab layout configuration if needed
        if (!mHasEmbeddedTabs) {
            mActionView.setEmbeddedTabView(null);
            mContainerView.setTabContainer(mTabScrollView);
        } else {
            mContainerView.setTabContainer(null);
            mActionView.setEmbeddedTabView(mTabScrollView);
        }
        final boolean isInTabMode = getNavigationMode() == NAVIGATION_MODE_TABS;
        if (mTabScrollView != null) {
            mTabScrollView.setVisibility(isInTabMode ? View.VISIBLE : View.GONE);
        }
        mActionView.setCollapsable(!mHasEmbeddedTabs && isInTabMode);
    }

    /**
     * Ensure tabs exist.
     */
    private void ensureTabsExist() {
        if (mTabScrollView != null) {
            return;
        }

        ScrollingTabContainerView tabScroller = new ScrollingTabContainerView(mContext);

        if (mHasEmbeddedTabs) {
            tabScroller.setVisibility(View.VISIBLE);
            mActionView.setEmbeddedTabView(tabScroller);
        } else {
            tabScroller.setVisibility(getNavigationMode() == NAVIGATION_MODE_TABS ?
                    View.VISIBLE : View.GONE);
            mContainerView.setTabContainer(tabScroller);
        }
        mTabScrollView = tabScroller;
    }

    /**
     * Complete deferred destroy action mode.
     */
    void completeDeferredDestroyActionMode() {
        if (mDeferredModeDestroyCallback != null) {
            mDeferredModeDestroyCallback.onDestroyActionMode(mDeferredDestroyActionMode);
            mDeferredDestroyActionMode = null;
            mDeferredModeDestroyCallback = null;
        }
    }

    /**
     * Enables or disables animation between show/hide states.
     * If animation is disabled using this method, animations in progress
     * will be finished.
     *
     * @param enabled true to animate, false to not animate.
     */
    public void setShowHideAnimationEnabled(boolean enabled) {
        mShowHideAnimationEnabled = enabled;
        if (!enabled && mCurrentShowAnim != null) {
            mCurrentShowAnim.end();
        }
    }

    /* (non-Javadoc)
     * @see com.actionbarsherlock.app.ActionBar#addOnMenuVisibilityListener(com.actionbarsherlock.app.ActionBar.OnMenuVisibilityListener)
     */
    public void addOnMenuVisibilityListener(OnMenuVisibilityListener listener) {
        mMenuVisibilityListeners.add(listener);
    }

    /* (non-Javadoc)
     * @see com.actionbarsherlock.app.ActionBar#removeOnMenuVisibilityListener(com.actionbarsherlock.app.ActionBar.OnMenuVisibilityListener)
     */
    public void removeOnMenuVisibilityListener(OnMenuVisibilityListener listener) {
        mMenuVisibilityListeners.remove(listener);
    }

    /**
     * Dispatch menu visibility changed.
     *
     * @param isVisible the is visible
     */
    public void dispatchMenuVisibilityChanged(boolean isVisible) {
        if (isVisible == mLastMenuVisibility) {
            return;
        }
        mLastMenuVisibility = isVisible;

        final int count = mMenuVisibilityListeners.size();
        for (int i = 0; i < count; i++) {
            mMenuVisibilityListeners.get(i).onMenuVisibilityChanged(isVisible);
        }
    }

    /**
     * Sets the custom view.
     *
     * @param resId the new custom view
     * @see com.actionbarsherlock.app.ActionBar#setCustomView(int)
     */
    @Override
    public void setCustomView(int resId) {
        setCustomView(LayoutInflater.from(getThemedContext()).inflate(resId, mActionView, false));
    }

    /**
     * Sets the display use logo enabled.
     *
     * @param useLogo the new display use logo enabled
     * @see com.actionbarsherlock.app.ActionBar#setDisplayUseLogoEnabled(boolean)
     */
    @Override
    public void setDisplayUseLogoEnabled(boolean useLogo) {
        setDisplayOptions(useLogo ? DISPLAY_USE_LOGO : 0, DISPLAY_USE_LOGO);
    }

    /**
     * Sets the display show home enabled.
     *
     * @param showHome the new display show home enabled
     * @see com.actionbarsherlock.app.ActionBar#setDisplayShowHomeEnabled(boolean)
     */
    @Override
    public void setDisplayShowHomeEnabled(boolean showHome) {
        setDisplayOptions(showHome ? DISPLAY_SHOW_HOME : 0, DISPLAY_SHOW_HOME);
    }

    /**
     * Sets the display home as up enabled.
     *
     * @param showHomeAsUp the new display home as up enabled
     * @see com.actionbarsherlock.app.ActionBar#setDisplayHomeAsUpEnabled(boolean)
     */
    @Override
    public void setDisplayHomeAsUpEnabled(boolean showHomeAsUp) {
        setDisplayOptions(showHomeAsUp ? DISPLAY_HOME_AS_UP : 0, DISPLAY_HOME_AS_UP);
    }

    /**
     * Sets the display show title enabled.
     *
     * @param showTitle the new display show title enabled
     * @see com.actionbarsherlock.app.ActionBar#setDisplayShowTitleEnabled(boolean)
     */
    @Override
    public void setDisplayShowTitleEnabled(boolean showTitle) {
        setDisplayOptions(showTitle ? DISPLAY_SHOW_TITLE : 0, DISPLAY_SHOW_TITLE);
    }

    /**
     * Sets the display show custom enabled.
     *
     * @param showCustom the new display show custom enabled
     * @see com.actionbarsherlock.app.ActionBar#setDisplayShowCustomEnabled(boolean)
     */
    @Override
    public void setDisplayShowCustomEnabled(boolean showCustom) {
        setDisplayOptions(showCustom ? DISPLAY_SHOW_CUSTOM : 0, DISPLAY_SHOW_CUSTOM);
    }

    /**
     * Sets the home button enabled.
     *
     * @param enable the new home button enabled
     * @see com.actionbarsherlock.app.ActionBar#setHomeButtonEnabled(boolean)
     */
    @Override
    public void setHomeButtonEnabled(boolean enable) {
        mActionView.setHomeButtonEnabled(enable);
    }

    /**
     * Sets the title.
     *
     * @param resId the new title
     * @see com.actionbarsherlock.app.ActionBar#setTitle(int)
     */
    @Override
    public void setTitle(int resId) {
        setTitle(mContext.getString(resId));
    }

    /**
     * Sets the subtitle.
     *
     * @param resId the new subtitle
     * @see com.actionbarsherlock.app.ActionBar#setSubtitle(int)
     */
    @Override
    public void setSubtitle(int resId) {
        setSubtitle(mContext.getString(resId));
    }

    /**
     * Sets the selected navigation item.
     *
     * @param position the new selected navigation item
     * @see com.actionbarsherlock.app.ActionBar#setSelectedNavigationItem(int)
     */
    public void setSelectedNavigationItem(int position) {
        switch (mActionView.getNavigationMode()) {
        case NAVIGATION_MODE_TABS:
            selectTab(mTabs.get(position));
            break;
        case NAVIGATION_MODE_LIST:
            mActionView.setDropdownSelectedPosition(position);
            break;
        default:
            throw new IllegalStateException(
                    "setSelectedNavigationIndex not valid for current navigation mode");
        }
    }

    /**
     * Removes the all tabs.
     *
     * @see com.actionbarsherlock.app.ActionBar#removeAllTabs()
     */
    public void removeAllTabs() {
        cleanupTabs();
    }

    /**
     * Cleanup tabs.
     */
    private void cleanupTabs() {
        if (mSelectedTab != null) {
            selectTab(null);
        }
        mTabs.clear();
        if (mTabScrollView != null) {
            mTabScrollView.removeAllTabs();
        }
        mSavedTabPosition = INVALID_POSITION;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     * @see com.actionbarsherlock.app.ActionBar#setTitle(java.lang.CharSequence)
     */
    public void setTitle(CharSequence title) {
        mActionView.setTitle(title);
    }

    /**
     * Sets the subtitle.
     *
     * @param subtitle the new subtitle
     * @see com.actionbarsherlock.app.ActionBar#setSubtitle(java.lang.CharSequence)
     */
    public void setSubtitle(CharSequence subtitle) {
        mActionView.setSubtitle(subtitle);
    }

    /* (non-Javadoc)
     * @see com.actionbarsherlock.app.ActionBar#setDisplayOptions(int)
     */
    public void setDisplayOptions(int options) {
        mActionView.setDisplayOptions(options);
    }

    /**
     * Sets the display options.
     *
     * @param options the options
     * @param mask the mask
     * @see com.actionbarsherlock.app.ActionBar#setDisplayOptions(int, int)
     */
    public void setDisplayOptions(int options, int mask) {
        final int current = mActionView.getDisplayOptions();
        mActionView.setDisplayOptions((options & mask) | (current & ~mask));
    }

    /**
     * Sets the background drawable.
     *
     * @param d the new background drawable
     * @see com.actionbarsherlock.app.ActionBar#setBackgroundDrawable(android.graphics.drawable.Drawable)
     */
    public void setBackgroundDrawable(Drawable d) {
        mContainerView.setPrimaryBackground(d);
    }

    /**
     * Sets the stacked background drawable.
     *
     * @param d the new stacked background drawable
     * @see com.actionbarsherlock.app.ActionBar#setStackedBackgroundDrawable(android.graphics.drawable.Drawable)
     */
    public void setStackedBackgroundDrawable(Drawable d) {
        mContainerView.setStackedBackground(d);
    }

    /**
     * Sets the split background drawable.
     *
     * @param d the new split background drawable
     * @see com.actionbarsherlock.app.ActionBar#setSplitBackgroundDrawable(android.graphics.drawable.Drawable)
     */
    public void setSplitBackgroundDrawable(Drawable d) {
        if (mSplitView != null) {
            mSplitView.setSplitBackground(d);
        }
    }

    /**
     * Gets the custom view.
     *
     * @return the custom view
     * @see com.actionbarsherlock.app.ActionBar#getCustomView()
     */
    public View getCustomView() {
        return mActionView.getCustomNavigationView();
    }

    /**
     * Gets the title.
     *
     * @return the title
     * @see com.actionbarsherlock.app.ActionBar#getTitle()
     */
    public CharSequence getTitle() {
        return mActionView.getTitle();
    }

    /**
     * Gets the subtitle.
     *
     * @return the subtitle
     * @see com.actionbarsherlock.app.ActionBar#getSubtitle()
     */
    public CharSequence getSubtitle() {
        return mActionView.getSubtitle();
    }

    /**
     * Gets the navigation mode.
     *
     * @return the navigation mode
     * @see com.actionbarsherlock.app.ActionBar#getNavigationMode()
     */
    public int getNavigationMode() {
        return mActionView.getNavigationMode();
    }

    /**
     * Gets the display options.
     *
     * @return the display options
     * @see com.actionbarsherlock.app.ActionBar#getDisplayOptions()
     */
    public int getDisplayOptions() {
        return mActionView.getDisplayOptions();
    }

    /**
     * Start action mode.
     *
     * @param callback the callback
     * @return the action mode
     */
    public ActionMode startActionMode(ActionMode.Callback callback) {
        boolean wasHidden = false;
        if (mActionMode != null) {
            wasHidden = mWasHiddenBeforeMode;
            mActionMode.finish();
        }

        mContextView.killMode();
        ActionModeImpl mode = new ActionModeImpl(callback);
        if (mode.dispatchOnCreate()) {
            mWasHiddenBeforeMode = !isShowing() || wasHidden;
            mode.invalidate();
            mContextView.initForMode(mode);
            animateToMode(true);
            if (mSplitView != null && mContextDisplayMode == CONTEXT_DISPLAY_SPLIT) {
                // TODO animate this
                mSplitView.setVisibility(View.VISIBLE);
            }
            mContextView.sendAccessibilityEvent(AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED);
            mActionMode = mode;
            return mode;
        }
        return null;
    }

    /**
     * Configure tab.
     *
     * @param tab the tab
     * @param position the position
     */
    private void configureTab(Tab tab, int position) {
        final TabImpl tabi = (TabImpl) tab;
        final ActionBar.TabListener callback = tabi.getCallback();

        if (callback == null) {
            throw new IllegalStateException("Action Bar Tab must have a Callback");
        }

        tabi.setPosition(position);
        mTabs.add(position, tabi);

        final int count = mTabs.size();
        for (int i = position + 1; i < count; i++) {
            mTabs.get(i).setPosition(i);
        }
    }

    /**
     * Adds the tab.
     *
     * @param tab the tab
     * @see com.actionbarsherlock.app.ActionBar#addTab(com.actionbarsherlock.app.ActionBar.Tab)
     */
    @Override
    public void addTab(Tab tab) {
        addTab(tab, mTabs.isEmpty());
    }

    /**
     * Adds the tab.
     *
     * @param tab the tab
     * @param position the position
     * @see com.actionbarsherlock.app.ActionBar#addTab(com.actionbarsherlock.app.ActionBar.Tab, int)
     */
    @Override
    public void addTab(Tab tab, int position) {
        addTab(tab, position, mTabs.isEmpty());
    }

    /**
     * Adds the tab.
     *
     * @param tab the tab
     * @param setSelected the set selected
     * @see com.actionbarsherlock.app.ActionBar#addTab(com.actionbarsherlock.app.ActionBar.Tab, boolean)
     */
    @Override
    public void addTab(Tab tab, boolean setSelected) {
        ensureTabsExist();
        mTabScrollView.addTab(tab, setSelected);
        configureTab(tab, mTabs.size());
        if (setSelected) {
            selectTab(tab);
        }
    }

    /**
     * Adds the tab.
     *
     * @param tab the tab
     * @param position the position
     * @param setSelected the set selected
     * @see com.actionbarsherlock.app.ActionBar#addTab(com.actionbarsherlock.app.ActionBar.Tab, int, boolean)
     */
    @Override
    public void addTab(Tab tab, int position, boolean setSelected) {
        ensureTabsExist();
        mTabScrollView.addTab(tab, position, setSelected);
        configureTab(tab, position);
        if (setSelected) {
            selectTab(tab);
        }
    }

    /**
     * New tab.
     *
     * @return the tab
     * @see com.actionbarsherlock.app.ActionBar#newTab()
     */
    @Override
    public Tab newTab() {
        return new TabImpl();
    }

    /**
     * Removes the tab.
     *
     * @param tab the tab
     * @see com.actionbarsherlock.app.ActionBar#removeTab(com.actionbarsherlock.app.ActionBar.Tab)
     */
    @Override
    public void removeTab(Tab tab) {
        removeTabAt(tab.getPosition());
    }

    /**
     * Removes the tab at.
     *
     * @param position the position
     * @see com.actionbarsherlock.app.ActionBar#removeTabAt(int)
     */
    @Override
    public void removeTabAt(int position) {
        if (mTabScrollView == null) {
            // No tabs around to remove
            return;
        }

        int selectedTabPosition = mSelectedTab != null
                ? mSelectedTab.getPosition() : mSavedTabPosition;
        mTabScrollView.removeTabAt(position);
        TabImpl removedTab = mTabs.remove(position);
        if (removedTab != null) {
            removedTab.setPosition(-1);
        }

        final int newTabCount = mTabs.size();
        for (int i = position; i < newTabCount; i++) {
            mTabs.get(i).setPosition(i);
        }

        if (selectedTabPosition == position) {
            selectTab(mTabs.isEmpty() ? null : mTabs.get(Math.max(0, position - 1)));
        }
    }

    /**
     * Select tab.
     *
     * @param tab the tab
     * @see com.actionbarsherlock.app.ActionBar#selectTab(com.actionbarsherlock.app.ActionBar.Tab)
     */
    @Override
    public void selectTab(Tab tab) {
        if (getNavigationMode() != NAVIGATION_MODE_TABS) {
            mSavedTabPosition = tab != null ? tab.getPosition() : INVALID_POSITION;
            return;
        }

        FragmentTransaction trans = null;
        if (mActivity instanceof SherlockFragmentActivity) {
            trans = ((SherlockFragmentActivity)mActivity).getSupportFragmentManager().beginTransaction()
                    .disallowAddToBackStack();
        }

        if (mSelectedTab == tab) {
            if (mSelectedTab != null) {
                mSelectedTab.getCallback().onTabReselected(mSelectedTab, trans);
                mTabScrollView.animateToTab(tab.getPosition());
            }
        } else {
            mTabScrollView.setTabSelected(tab != null ? tab.getPosition() : Tab.INVALID_POSITION);
            if (mSelectedTab != null) {
                mSelectedTab.getCallback().onTabUnselected(mSelectedTab, trans);
            }
            mSelectedTab = (TabImpl) tab;
            if (mSelectedTab != null) {
                mSelectedTab.getCallback().onTabSelected(mSelectedTab, trans);
            }
        }

        if (trans != null && !trans.isEmpty()) {
            trans.commit();
        }
    }

    /**
     * Gets the selected tab.
     *
     * @return the selected tab
     * @see com.actionbarsherlock.app.ActionBar#getSelectedTab()
     */
    @Override
    public Tab getSelectedTab() {
        return mSelectedTab;
    }

    /**
     * Gets the height.
     *
     * @return the height
     * @see com.actionbarsherlock.app.ActionBar#getHeight()
     */
    @Override
    public int getHeight() {
        return mContainerView.getHeight();
    }

    /**
     * Show.
     *
     * @see com.actionbarsherlock.app.ActionBar#show()
     */
    @Override
    public void show() {
        show(true);
    }

    /**
     * Show.
     *
     * @param markHiddenBeforeMode the mark hidden before mode
     */
    void show(boolean markHiddenBeforeMode) {
        if (mCurrentShowAnim != null) {
            mCurrentShowAnim.end();
        }
        if (mContainerView.getVisibility() == View.VISIBLE) {
            if (markHiddenBeforeMode) mWasHiddenBeforeMode = false;
            return;
        }
        mContainerView.setVisibility(View.VISIBLE);

        if (mShowHideAnimationEnabled) {
            mContainerView.setAlpha(0);
            AnimatorSet anim = new AnimatorSet();
            AnimatorSet.Builder b = anim.play(ObjectAnimator.ofFloat(mContainerView, "alpha", 1));
            if (mContentView != null) {
                b.with(ObjectAnimator.ofFloat(mContentView, "translationY",
                        -mContainerView.getHeight(), 0));
                mContainerView.setTranslationY(-mContainerView.getHeight());
                b.with(ObjectAnimator.ofFloat(mContainerView, "translationY", 0));
            }
            if (mSplitView != null && mContextDisplayMode == CONTEXT_DISPLAY_SPLIT) {
                mSplitView.setAlpha(0);
                mSplitView.setVisibility(View.VISIBLE);
                b.with(ObjectAnimator.ofFloat(mSplitView, "alpha", 1));
            }
            anim.addListener(mShowListener);
            mCurrentShowAnim = anim;
            anim.start();
        } else {
            mContainerView.setAlpha(1);
            mContainerView.setTranslationY(0);
            mShowListener.onAnimationEnd(null);
        }
    }

    /**
     * Hide.
     *
     * @see com.actionbarsherlock.app.ActionBar#hide()
     */
    @Override
    public void hide() {
        if (mCurrentShowAnim != null) {
            mCurrentShowAnim.end();
        }
        if (mContainerView.getVisibility() == View.GONE) {
            return;
        }

        if (mShowHideAnimationEnabled) {
            mContainerView.setAlpha(1);
            mContainerView.setTransitioning(true);
            AnimatorSet anim = new AnimatorSet();
            AnimatorSet.Builder b = anim.play(ObjectAnimator.ofFloat(mContainerView, "alpha", 0));
            if (mContentView != null) {
                b.with(ObjectAnimator.ofFloat(mContentView, "translationY",
                        0, -mContainerView.getHeight()));
                b.with(ObjectAnimator.ofFloat(mContainerView, "translationY",
                        -mContainerView.getHeight()));
            }
            if (mSplitView != null && mSplitView.getVisibility() == View.VISIBLE) {
                mSplitView.setAlpha(1);
                b.with(ObjectAnimator.ofFloat(mSplitView, "alpha", 0));
            }
            anim.addListener(mHideListener);
            mCurrentShowAnim = anim;
            anim.start();
        } else {
            mHideListener.onAnimationEnd(null);
        }
    }

    /**
     * Checks if is showing.
     *
     * @return true, if is showing
     * @see com.actionbarsherlock.app.ActionBar#isShowing()
     */
    public boolean isShowing() {
        return mContainerView.getVisibility() == View.VISIBLE;
    }

    /**
     * Animate to mode.
     *
     * @param toActionMode the to action mode
     */
    void animateToMode(boolean toActionMode) {
        if (toActionMode) {
            show(false);
        }
        if (mCurrentModeAnim != null) {
            mCurrentModeAnim.end();
        }

        mActionView.animateToVisibility(toActionMode ? View.GONE : View.VISIBLE);
        mContextView.animateToVisibility(toActionMode ? View.VISIBLE : View.GONE);
        if (mTabScrollView != null && !mActionView.hasEmbeddedTabs() && mActionView.isCollapsed()) {
            mTabScrollView.animateToVisibility(toActionMode ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Gets the themed context.
     *
     * @return the themed context
     * @see com.actionbarsherlock.app.ActionBar#getThemedContext()
     */
    public Context getThemedContext() {
        if (mThemedContext == null) {
            TypedValue outValue = new TypedValue();
            Resources.Theme currentTheme = mContext.getTheme();
            currentTheme.resolveAttribute(R.attr.actionBarWidgetTheme,
                    outValue, true);
            final int targetThemeRes = outValue.resourceId;

            if (targetThemeRes != 0) { //XXX && mContext.getThemeResId() != targetThemeRes) {
                mThemedContext = new ContextThemeWrapper(mContext, targetThemeRes);
            } else {
                mThemedContext = mContext;
            }
        }
        return mThemedContext;
    }

    /**
     * The Class ActionModeImpl.
     *
     * @hide
     */
    public class ActionModeImpl extends ActionMode implements MenuBuilder.Callback {
        
        /** The m callback. */
        private ActionMode.Callback mCallback;
        
        /** The m menu. */
        private MenuBuilder mMenu;
        
        /** The m custom view. */
        private WeakReference<View> mCustomView;

        /**
         * Instantiates a new action mode impl.
         *
         * @param callback the callback
         */
        public ActionModeImpl(ActionMode.Callback callback) {
            mCallback = callback;
            mMenu = new MenuBuilder(getThemedContext())
                    .setDefaultShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            mMenu.setCallback(this);
        }

        /**
         * Gets the menu inflater.
         *
         * @return the menu inflater
         * @see com.actionbarsherlock.view.ActionMode#getMenuInflater()
         */
        @Override
        public MenuInflater getMenuInflater() {
            return new MenuInflater(getThemedContext());
        }

        /**
         * Gets the menu.
         *
         * @return the menu
         * @see com.actionbarsherlock.view.ActionMode#getMenu()
         */
        @Override
        public Menu getMenu() {
            return mMenu;
        }

        /**
         * Finish.
         *
         * @see com.actionbarsherlock.view.ActionMode#finish()
         */
        @Override
        public void finish() {
            if (mActionMode != this) {
                // Not the active action mode - no-op
                return;
            }

            // If we were hidden before the mode was shown, defer the onDestroy
            // callback until the animation is finished and associated relayout
            // is about to happen. This lets apps better anticipate visibility
            // and layout behavior.
            if (mWasHiddenBeforeMode) {
                mDeferredDestroyActionMode = this;
                mDeferredModeDestroyCallback = mCallback;
            } else {
                mCallback.onDestroyActionMode(this);
            }
            mCallback = null;
            animateToMode(false);

            // Clear out the context mode views after the animation finishes
            mContextView.closeMode();
            mActionView.sendAccessibilityEvent(AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED);

            mActionMode = null;

            if (mWasHiddenBeforeMode) {
                hide();
            }
        }

        /**
         * Invalidate.
         *
         * @see com.actionbarsherlock.view.ActionMode#invalidate()
         */
        @Override
        public void invalidate() {
            mMenu.stopDispatchingItemsChanged();
            try {
                mCallback.onPrepareActionMode(this, mMenu);
            } finally {
                mMenu.startDispatchingItemsChanged();
            }
        }

        /**
         * Dispatch on create.
         *
         * @return true, if successful
         */
        public boolean dispatchOnCreate() {
            mMenu.stopDispatchingItemsChanged();
            try {
                return mCallback.onCreateActionMode(this, mMenu);
            } finally {
                mMenu.startDispatchingItemsChanged();
            }
        }

        /**
         * Sets the custom view.
         *
         * @param view the new custom view
         * @see com.actionbarsherlock.view.ActionMode#setCustomView(android.view.View)
         */
        @Override
        public void setCustomView(View view) {
            mContextView.setCustomView(view);
            mCustomView = new WeakReference<View>(view);
        }

        /**
         * Sets the subtitle.
         *
         * @param subtitle the new subtitle
         * @see com.actionbarsherlock.view.ActionMode#setSubtitle(java.lang.CharSequence)
         */
        @Override
        public void setSubtitle(CharSequence subtitle) {
            mContextView.setSubtitle(subtitle);
        }

        /**
         * Sets the title.
         *
         * @param title the new title
         * @see com.actionbarsherlock.view.ActionMode#setTitle(java.lang.CharSequence)
         */
        @Override
        public void setTitle(CharSequence title) {
            mContextView.setTitle(title);
        }

        /**
         * Sets the title.
         *
         * @param resId the new title
         * @see com.actionbarsherlock.view.ActionMode#setTitle(int)
         */
        @Override
        public void setTitle(int resId) {
            setTitle(mContext.getResources().getString(resId));
        }

        /**
         * Sets the subtitle.
         *
         * @param resId the new subtitle
         * @see com.actionbarsherlock.view.ActionMode#setSubtitle(int)
         */
        @Override
        public void setSubtitle(int resId) {
            setSubtitle(mContext.getResources().getString(resId));
        }

        /**
         * Gets the title.
         *
         * @return the title
         * @see com.actionbarsherlock.view.ActionMode#getTitle()
         */
        @Override
        public CharSequence getTitle() {
            return mContextView.getTitle();
        }

        /**
         * Gets the subtitle.
         *
         * @return the subtitle
         * @see com.actionbarsherlock.view.ActionMode#getSubtitle()
         */
        @Override
        public CharSequence getSubtitle() {
            return mContextView.getSubtitle();
        }

        /**
         * Gets the custom view.
         *
         * @return the custom view
         * @see com.actionbarsherlock.view.ActionMode#getCustomView()
         */
        @Override
        public View getCustomView() {
            return mCustomView != null ? mCustomView.get() : null;
        }

        /**
         * On menu item selected.
         *
         * @param menu the menu
         * @param item the item
         * @return true, if successful
         * @see com.actionbarsherlock.internal.view.menu.MenuBuilder.Callback#onMenuItemSelected(com.actionbarsherlock.internal.view.menu.MenuBuilder, com.actionbarsherlock.view.MenuItem)
         */
        public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
            if (mCallback != null) {
                return mCallback.onActionItemClicked(this, item);
            } else {
                return false;
            }
        }

        /**
         * On close menu.
         *
         * @param menu the menu
         * @param allMenusAreClosing the all menus are closing
         */
        public void onCloseMenu(MenuBuilder menu, boolean allMenusAreClosing) {
        }

        /**
         * On sub menu selected.
         *
         * @param subMenu the sub menu
         * @return true, if successful
         */
        public boolean onSubMenuSelected(SubMenuBuilder subMenu) {
            if (mCallback == null) {
                return false;
            }

            if (!subMenu.hasVisibleItems()) {
                return true;
            }

            new MenuPopupHelper(getThemedContext(), subMenu).show();
            return true;
        }

        /**
         * On close sub menu.
         *
         * @param menu the menu
         */
        public void onCloseSubMenu(SubMenuBuilder menu) {
        }

        /**
         * On menu mode change.
         *
         * @param menu the menu
         * @see com.actionbarsherlock.internal.view.menu.MenuBuilder.Callback#onMenuModeChange(com.actionbarsherlock.internal.view.menu.MenuBuilder)
         */
        public void onMenuModeChange(MenuBuilder menu) {
            if (mCallback == null) {
                return;
            }
            invalidate();
            mContextView.showOverflowMenu();
        }
    }

    /**
     * The Class TabImpl.
     *
     * @hide
     */
    public class TabImpl extends ActionBar.Tab {
        
        /** The m callback. */
        private ActionBar.TabListener mCallback;
        
        /** The m tag. */
        private Object mTag;
        
        /** The m icon. */
        private Drawable mIcon;
        
        /** The m text. */
        private CharSequence mText;
        
        /** The m content desc. */
        private CharSequence mContentDesc;
        
        /** The m position. */
        private int mPosition = -1;
        
        /** The m custom view. */
        private View mCustomView;

        /**
         * Gets the tag.
         *
         * @return the tag
         * @see com.actionbarsherlock.app.ActionBar.Tab#getTag()
         */
        @Override
        public Object getTag() {
            return mTag;
        }

        /**
         * Sets the tag.
         *
         * @param tag the tag
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setTag(java.lang.Object)
         */
        @Override
        public Tab setTag(Object tag) {
            mTag = tag;
            return this;
        }

        /**
         * Gets the callback.
         *
         * @return the callback
         */
        public ActionBar.TabListener getCallback() {
            return mCallback;
        }

        /**
         * Sets the tab listener.
         *
         * @param callback the callback
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setTabListener(com.actionbarsherlock.app.ActionBar.TabListener)
         */
        @Override
        public Tab setTabListener(ActionBar.TabListener callback) {
            mCallback = callback;
            return this;
        }

        /**
         * Gets the custom view.
         *
         * @return the custom view
         * @see com.actionbarsherlock.app.ActionBar.Tab#getCustomView()
         */
        @Override
        public View getCustomView() {
            return mCustomView;
        }

        /**
         * Sets the custom view.
         *
         * @param view the view
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setCustomView(android.view.View)
         */
        @Override
        public Tab setCustomView(View view) {
            mCustomView = view;
            if (mPosition >= 0) {
                mTabScrollView.updateTab(mPosition);
            }
            return this;
        }

        /**
         * Sets the custom view.
         *
         * @param layoutResId the layout res id
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setCustomView(int)
         */
        @Override
        public Tab setCustomView(int layoutResId) {
            return setCustomView(LayoutInflater.from(getThemedContext())
                    .inflate(layoutResId, null));
        }

        /**
         * Gets the icon.
         *
         * @return the icon
         * @see com.actionbarsherlock.app.ActionBar.Tab#getIcon()
         */
        @Override
        public Drawable getIcon() {
            return mIcon;
        }

        /**
         * Gets the position.
         *
         * @return the position
         * @see com.actionbarsherlock.app.ActionBar.Tab#getPosition()
         */
        @Override
        public int getPosition() {
            return mPosition;
        }

        /**
         * Sets the position.
         *
         * @param position the new position
         */
        public void setPosition(int position) {
            mPosition = position;
        }

        /**
         * Gets the text.
         *
         * @return the text
         * @see com.actionbarsherlock.app.ActionBar.Tab#getText()
         */
        @Override
        public CharSequence getText() {
            return mText;
        }

        /**
         * Sets the icon.
         *
         * @param icon the icon
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setIcon(android.graphics.drawable.Drawable)
         */
        @Override
        public Tab setIcon(Drawable icon) {
            mIcon = icon;
            if (mPosition >= 0) {
                mTabScrollView.updateTab(mPosition);
            }
            return this;
        }

        /**
         * Sets the icon.
         *
         * @param resId the res id
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setIcon(int)
         */
        @Override
        public Tab setIcon(int resId) {
            return setIcon(mContext.getResources().getDrawable(resId));
        }

        /**
         * Sets the text.
         *
         * @param text the text
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setText(java.lang.CharSequence)
         */
        @Override
        public Tab setText(CharSequence text) {
            mText = text;
            if (mPosition >= 0) {
                mTabScrollView.updateTab(mPosition);
            }
            return this;
        }

        /**
         * Sets the text.
         *
         * @param resId the res id
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setText(int)
         */
        @Override
        public Tab setText(int resId) {
            return setText(mContext.getResources().getText(resId));
        }

        /**
         * Select.
         *
         * @see com.actionbarsherlock.app.ActionBar.Tab#select()
         */
        @Override
        public void select() {
            selectTab(this);
        }

        /**
         * Sets the content description.
         *
         * @param resId the res id
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setContentDescription(int)
         */
        @Override
        public Tab setContentDescription(int resId) {
            return setContentDescription(mContext.getResources().getText(resId));
        }

        /**
         * Sets the content description.
         *
         * @param contentDesc the content desc
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setContentDescription(java.lang.CharSequence)
         */
        @Override
        public Tab setContentDescription(CharSequence contentDesc) {
            mContentDesc = contentDesc;
            if (mPosition >= 0) {
                mTabScrollView.updateTab(mPosition);
            }
            return this;
        }

        /**
         * Gets the content description.
         *
         * @return the content description
         * @see com.actionbarsherlock.app.ActionBar.Tab#getContentDescription()
         */
        @Override
        public CharSequence getContentDescription() {
            return mContentDesc;
        }
    }

    /**
     * Sets the custom view.
     *
     * @param view the new custom view
     * @see com.actionbarsherlock.app.ActionBar#setCustomView(android.view.View)
     */
    @Override
    public void setCustomView(View view) {
        mActionView.setCustomNavigationView(view);
    }

    /**
     * Sets the custom view.
     *
     * @param view the view
     * @param layoutParams the layout params
     * @see com.actionbarsherlock.app.ActionBar#setCustomView(android.view.View, com.actionbarsherlock.app.ActionBar.LayoutParams)
     */
    @Override
    public void setCustomView(View view, LayoutParams layoutParams) {
        view.setLayoutParams(layoutParams);
        mActionView.setCustomNavigationView(view);
    }

    /**
     * Sets the list navigation callbacks.
     *
     * @param adapter the adapter
     * @param callback the callback
     * @see com.actionbarsherlock.app.ActionBar#setListNavigationCallbacks(android.widget.SpinnerAdapter, com.actionbarsherlock.app.ActionBar.OnNavigationListener)
     */
    @Override
    public void setListNavigationCallbacks(SpinnerAdapter adapter, OnNavigationListener callback) {
        mActionView.setDropdownAdapter(adapter);
        mActionView.setCallback(callback);
    }

    /**
     * Gets the selected navigation index.
     *
     * @return the selected navigation index
     * @see com.actionbarsherlock.app.ActionBar#getSelectedNavigationIndex()
     */
    @Override
    public int getSelectedNavigationIndex() {
        switch (mActionView.getNavigationMode()) {
            case NAVIGATION_MODE_TABS:
                return mSelectedTab != null ? mSelectedTab.getPosition() : -1;
            case NAVIGATION_MODE_LIST:
                return mActionView.getDropdownSelectedPosition();
            default:
                return -1;
        }
    }

    /**
     * Gets the navigation item count.
     *
     * @return the navigation item count
     * @see com.actionbarsherlock.app.ActionBar#getNavigationItemCount()
     */
    @Override
    public int getNavigationItemCount() {
        switch (mActionView.getNavigationMode()) {
            case NAVIGATION_MODE_TABS:
                return mTabs.size();
            case NAVIGATION_MODE_LIST:
                SpinnerAdapter adapter = mActionView.getDropdownAdapter();
                return adapter != null ? adapter.getCount() : 0;
            default:
                return 0;
        }
    }

    /**
     * Gets the tab count.
     *
     * @return the tab count
     * @see com.actionbarsherlock.app.ActionBar#getTabCount()
     */
    @Override
    public int getTabCount() {
        return mTabs.size();
    }

    /**
     * Sets the navigation mode.
     *
     * @param mode the new navigation mode
     * @see com.actionbarsherlock.app.ActionBar#setNavigationMode(int)
     */
    @Override
    public void setNavigationMode(int mode) {
        final int oldMode = mActionView.getNavigationMode();
        switch (oldMode) {
            case NAVIGATION_MODE_TABS:
                mSavedTabPosition = getSelectedNavigationIndex();
                selectTab(null);
                mTabScrollView.setVisibility(View.GONE);
                break;
        }
        mActionView.setNavigationMode(mode);
        switch (mode) {
            case NAVIGATION_MODE_TABS:
                ensureTabsExist();
                mTabScrollView.setVisibility(View.VISIBLE);
                if (mSavedTabPosition != INVALID_POSITION) {
                    setSelectedNavigationItem(mSavedTabPosition);
                    mSavedTabPosition = INVALID_POSITION;
                }
                break;
        }
        mActionView.setCollapsable(mode == NAVIGATION_MODE_TABS && !mHasEmbeddedTabs);
    }

    /**
     * Gets the tab at.
     *
     * @param index the index
     * @return the tab at
     * @see com.actionbarsherlock.app.ActionBar#getTabAt(int)
     */
    @Override
    public Tab getTabAt(int index) {
        return mTabs.get(index);
    }


    /**
     * Sets the icon.
     *
     * @param resId the new icon
     * @see com.actionbarsherlock.app.ActionBar#setIcon(int)
     */
    @Override
    public void setIcon(int resId) {
        mActionView.setIcon(resId);
    }

    /**
     * Sets the icon.
     *
     * @param icon the new icon
     * @see com.actionbarsherlock.app.ActionBar#setIcon(android.graphics.drawable.Drawable)
     */
    @Override
    public void setIcon(Drawable icon) {
        mActionView.setIcon(icon);
    }

    /**
     * Sets the logo.
     *
     * @param resId the new logo
     * @see com.actionbarsherlock.app.ActionBar#setLogo(int)
     */
    @Override
    public void setLogo(int resId) {
        mActionView.setLogo(resId);
    }

    /**
     * Sets the logo.
     *
     * @param logo the new logo
     * @see com.actionbarsherlock.app.ActionBar#setLogo(android.graphics.drawable.Drawable)
     */
    @Override
    public void setLogo(Drawable logo) {
        mActionView.setLogo(logo);
    }
}
