package com.actionbarsherlock.internal.app;

import java.util.HashSet;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.SpinnerAdapter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionBarWrapper.
 */
public class ActionBarWrapper extends ActionBar implements android.app.ActionBar.OnNavigationListener, android.app.ActionBar.OnMenuVisibilityListener {
    
    /** The m activity. */
    private final Activity mActivity;
    
    /** The m action bar. */
    private final android.app.ActionBar mActionBar;
    
    /** The m navigation listener. */
    private ActionBar.OnNavigationListener mNavigationListener;
    
    /** The m menu visibility listeners. */
    private Set<OnMenuVisibilityListener> mMenuVisibilityListeners = new HashSet<OnMenuVisibilityListener>(1);
    
    /** The m fragment transaction. */
    private FragmentTransaction mFragmentTransaction;


    /**
     * Instantiates a new action bar wrapper.
     *
     * @param activity the activity
     */
    public ActionBarWrapper(Activity activity) {
        mActivity = activity;
        mActionBar = activity.getActionBar();
        if (mActionBar != null) {
            mActionBar.addOnMenuVisibilityListener(this);
        }
    }


    /**
     * @see com.actionbarsherlock.app.ActionBar#setHomeButtonEnabled(boolean)
     */
    @Override
    public void setHomeButtonEnabled(boolean enabled) {
        mActionBar.setHomeButtonEnabled(enabled);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#getThemedContext()
     */
    @Override
    public Context getThemedContext() {
        return mActionBar.getThemedContext();
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setCustomView(android.view.View)
     */
    @Override
    public void setCustomView(View view) {
        mActionBar.setCustomView(view);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setCustomView(android.view.View, com.actionbarsherlock.app.ActionBar.LayoutParams)
     */
    @Override
    public void setCustomView(View view, LayoutParams layoutParams) {
        android.app.ActionBar.LayoutParams lp = new android.app.ActionBar.LayoutParams(layoutParams);
        lp.gravity = layoutParams.gravity;
        lp.bottomMargin = layoutParams.bottomMargin;
        lp.topMargin = layoutParams.topMargin;
        lp.leftMargin = layoutParams.leftMargin;
        lp.rightMargin = layoutParams.rightMargin;
        mActionBar.setCustomView(view, lp);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setCustomView(int)
     */
    @Override
    public void setCustomView(int resId) {
        mActionBar.setCustomView(resId);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setIcon(int)
     */
    @Override
    public void setIcon(int resId) {
        mActionBar.setIcon(resId);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setIcon(android.graphics.drawable.Drawable)
     */
    @Override
    public void setIcon(Drawable icon) {
        mActionBar.setIcon(icon);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setLogo(int)
     */
    @Override
    public void setLogo(int resId) {
        mActionBar.setLogo(resId);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setLogo(android.graphics.drawable.Drawable)
     */
    @Override
    public void setLogo(Drawable logo) {
        mActionBar.setLogo(logo);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setListNavigationCallbacks(android.widget.SpinnerAdapter, com.actionbarsherlock.app.ActionBar.OnNavigationListener)
     */
    @Override
    public void setListNavigationCallbacks(SpinnerAdapter adapter, OnNavigationListener callback) {
        mNavigationListener = callback;
        mActionBar.setListNavigationCallbacks(adapter, (callback != null) ? this : null);
    }

    /**
     * @see android.app.ActionBar.OnNavigationListener#onNavigationItemSelected(int, long)
     */
    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        //This should never be a NullPointerException since we only set
        //ourselves as the listener when the callback is not null.
        return mNavigationListener.onNavigationItemSelected(itemPosition, itemId);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setSelectedNavigationItem(int)
     */
    @Override
    public void setSelectedNavigationItem(int position) {
        mActionBar.setSelectedNavigationItem(position);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#getSelectedNavigationIndex()
     */
    @Override
    public int getSelectedNavigationIndex() {
        return mActionBar.getSelectedNavigationIndex();
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#getNavigationItemCount()
     */
    @Override
    public int getNavigationItemCount() {
        return mActionBar.getNavigationItemCount();
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setTitle(java.lang.CharSequence)
     */
    @Override
    public void setTitle(CharSequence title) {
        mActionBar.setTitle(title);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setTitle(int)
     */
    @Override
    public void setTitle(int resId) {
        mActionBar.setTitle(resId);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setSubtitle(java.lang.CharSequence)
     */
    @Override
    public void setSubtitle(CharSequence subtitle) {
        mActionBar.setSubtitle(subtitle);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setSubtitle(int)
     */
    @Override
    public void setSubtitle(int resId) {
        mActionBar.setSubtitle(resId);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setDisplayOptions(int)
     */
    @Override
    public void setDisplayOptions(int options) {
        mActionBar.setDisplayOptions(options);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setDisplayOptions(int, int)
     */
    @Override
    public void setDisplayOptions(int options, int mask) {
        mActionBar.setDisplayOptions(options, mask);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setDisplayUseLogoEnabled(boolean)
     */
    @Override
    public void setDisplayUseLogoEnabled(boolean useLogo) {
        mActionBar.setDisplayUseLogoEnabled(useLogo);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setDisplayShowHomeEnabled(boolean)
     */
    @Override
    public void setDisplayShowHomeEnabled(boolean showHome) {
        mActionBar.setDisplayShowHomeEnabled(showHome);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setDisplayHomeAsUpEnabled(boolean)
     */
    @Override
    public void setDisplayHomeAsUpEnabled(boolean showHomeAsUp) {
        mActionBar.setDisplayHomeAsUpEnabled(showHomeAsUp);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setDisplayShowTitleEnabled(boolean)
     */
    @Override
    public void setDisplayShowTitleEnabled(boolean showTitle) {
        mActionBar.setDisplayShowTitleEnabled(showTitle);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setDisplayShowCustomEnabled(boolean)
     */
    @Override
    public void setDisplayShowCustomEnabled(boolean showCustom) {
        mActionBar.setDisplayShowCustomEnabled(showCustom);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setBackgroundDrawable(android.graphics.drawable.Drawable)
     */
    @Override
    public void setBackgroundDrawable(Drawable d) {
        mActionBar.setBackgroundDrawable(d);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setStackedBackgroundDrawable(android.graphics.drawable.Drawable)
     */
    @Override
    public void setStackedBackgroundDrawable(Drawable d) {
        mActionBar.setStackedBackgroundDrawable(d);
    }

    /**     
     * @see com.actionbarsherlock.app.ActionBar#setSplitBackgroundDrawable(android.graphics.drawable.Drawable)
     */
    @Override
    public void setSplitBackgroundDrawable(Drawable d) {
        mActionBar.setSplitBackgroundDrawable(d);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#getCustomView()
     */
    @Override
    public View getCustomView() {
        return mActionBar.getCustomView();
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#getTitle()
     */
    @Override
    public CharSequence getTitle() {
        return mActionBar.getTitle();
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#getSubtitle()
     */
    @Override
    public CharSequence getSubtitle() {
        return mActionBar.getSubtitle();
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#getNavigationMode()
     */
    @Override
    public int getNavigationMode() {
        return mActionBar.getNavigationMode();
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#setNavigationMode(int)
     */
    @Override
    public void setNavigationMode(int mode) {
        mActionBar.setNavigationMode(mode);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#getDisplayOptions()
     */
    @Override
    public int getDisplayOptions() {
        return mActionBar.getDisplayOptions();
    }

    /**
     * The Class TabWrapper.
     */
    public class TabWrapper extends ActionBar.Tab implements android.app.ActionBar.TabListener {
        
        /** The m native tab. */
        final android.app.ActionBar.Tab mNativeTab;
        
        /** The m tag. */
        private Object mTag;
        
        /** The m listener. */
        private TabListener mListener;

        /**
         * Instantiates a new tab wrapper.
         *
         * @param nativeTab the native tab
         */
        public TabWrapper(android.app.ActionBar.Tab nativeTab) {
            mNativeTab = nativeTab;
            mNativeTab.setTag(this);
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#getPosition()
         */
        @Override
        public int getPosition() {
            return mNativeTab.getPosition();
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#getIcon()
         */
        @Override
        public Drawable getIcon() {
            return mNativeTab.getIcon();
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#getText()
         */
        @Override
        public CharSequence getText() {
            return mNativeTab.getText();
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#setIcon(android.graphics.drawable.Drawable)
         */
        @Override
        public Tab setIcon(Drawable icon) {
            mNativeTab.setIcon(icon);
            return this;
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#setIcon(int)
         */
        @Override
        public Tab setIcon(int resId) {
            mNativeTab.setIcon(resId);
            return this;
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#setText(java.lang.CharSequence)
         */
        @Override
        public Tab setText(CharSequence text) {
            mNativeTab.setText(text);
            return this;
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#setText(int)
         */
        @Override
        public Tab setText(int resId) {
            mNativeTab.setText(resId);
            return this;
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#setCustomView(android.view.View)
         */
        @Override
        public Tab setCustomView(View view) {
            mNativeTab.setCustomView(view);
            return this;
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#setCustomView(int)
         */
        @Override
        public Tab setCustomView(int layoutResId) {
            mNativeTab.setCustomView(layoutResId);
            return this;
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#getCustomView()
         */
        @Override
        public View getCustomView() {
            return mNativeTab.getCustomView();
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#setTag(java.lang.Object)
         */
        @Override
        public Tab setTag(Object obj) {
            mTag = obj;
            return this;
        }

        /**         
         * @see com.actionbarsherlock.app.ActionBar.Tab#getTag()
         */
        @Override
        public Object getTag() {
            return mTag;
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#setTabListener(com.actionbarsherlock.app.ActionBar.TabListener)
         */
        @Override
        public Tab setTabListener(TabListener listener) {
            mNativeTab.setTabListener(listener != null ? this : null);
            mListener = listener;
            return this;
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#select()
         */
        @Override
        public void select() {
            mNativeTab.select();
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#setContentDescription(int)
         */
        @Override
        public Tab setContentDescription(int resId) {
            mNativeTab.setContentDescription(resId);
            return this;
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#setContentDescription(java.lang.CharSequence)
         */
        @Override
        public Tab setContentDescription(CharSequence contentDesc) {
            mNativeTab.setContentDescription(contentDesc);
            return this;
        }

        /**
         * @see com.actionbarsherlock.app.ActionBar.Tab#getContentDescription()
         */
        @Override
        public CharSequence getContentDescription() {
            return mNativeTab.getContentDescription();
        }

        /**
         * @see android.app.ActionBar.TabListener#onTabReselected(android.app.ActionBar.Tab, android.app.FragmentTransaction)
         */
        @Override
        public void onTabReselected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {
            if (mListener != null) {
                FragmentTransaction trans = null;
                if (mActivity instanceof SherlockFragmentActivity) {
                    trans = ((SherlockFragmentActivity)mActivity).getSupportFragmentManager().beginTransaction()
                            .disallowAddToBackStack();
                }

                mListener.onTabReselected(this, trans);

                if (trans != null && !trans.isEmpty()) {
                    trans.commit();
                }
            }
        }

        /**
         * @see android.app.ActionBar.TabListener#onTabSelected(android.app.ActionBar.Tab, android.app.FragmentTransaction)
         */
        @Override
        public void onTabSelected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {
            if (mListener != null) {

                if (mFragmentTransaction == null && mActivity instanceof SherlockFragmentActivity) {
                    mFragmentTransaction = ((SherlockFragmentActivity)mActivity).getSupportFragmentManager().beginTransaction()
                            .disallowAddToBackStack();
                }

                mListener.onTabSelected(this, mFragmentTransaction);

                if (mFragmentTransaction != null) {
                    if (!mFragmentTransaction.isEmpty()) {
                        mFragmentTransaction.commit();
                    }
                    mFragmentTransaction = null;
                }
            }
        }

        /**
         * @see android.app.ActionBar.TabListener#onTabUnselected(android.app.ActionBar.Tab, android.app.FragmentTransaction)
         */
        @Override
        public void onTabUnselected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {
            if (mListener != null) {
                FragmentTransaction trans = null;
                if (mActivity instanceof SherlockFragmentActivity) {
                    trans = ((SherlockFragmentActivity)mActivity).getSupportFragmentManager().beginTransaction()
                            .disallowAddToBackStack();
                    mFragmentTransaction = trans;
                }

                mListener.onTabUnselected(this, trans);
            }
        }
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#newTab()
     */
    @Override
    public Tab newTab() {
        return new TabWrapper(mActionBar.newTab());
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#addTab(com.actionbarsherlock.app.ActionBar.Tab)
     */
    @Override
    public void addTab(Tab tab) {
        mActionBar.addTab(((TabWrapper)tab).mNativeTab);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#addTab(com.actionbarsherlock.app.ActionBar.Tab, boolean)
     */
    @Override
    public void addTab(Tab tab, boolean setSelected) {
        mActionBar.addTab(((TabWrapper)tab).mNativeTab, setSelected);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#addTab(com.actionbarsherlock.app.ActionBar.Tab, int)
     */
    @Override
    public void addTab(Tab tab, int position) {
        mActionBar.addTab(((TabWrapper)tab).mNativeTab, position);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#addTab(com.actionbarsherlock.app.ActionBar.Tab, int, boolean)
     */
    @Override
    public void addTab(Tab tab, int position, boolean setSelected) {
        mActionBar.addTab(((TabWrapper)tab).mNativeTab, position, setSelected);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#removeTab(com.actionbarsherlock.app.ActionBar.Tab)
     */
    @Override
    public void removeTab(Tab tab) {
        mActionBar.removeTab(((TabWrapper)tab).mNativeTab);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#removeTabAt(int)
     */
    @Override
    public void removeTabAt(int position) {
        mActionBar.removeTabAt(position);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#removeAllTabs()
     */
    @Override
    public void removeAllTabs() {
        mActionBar.removeAllTabs();
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#selectTab(com.actionbarsherlock.app.ActionBar.Tab)
     */
    @Override
    public void selectTab(Tab tab) {
        mActionBar.selectTab(((TabWrapper)tab).mNativeTab);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#getSelectedTab()
     */
    @Override
    public Tab getSelectedTab() {
        android.app.ActionBar.Tab selected = mActionBar.getSelectedTab();
        return (selected != null) ? (Tab)selected.getTag() : null;
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#getTabAt(int)
     */
    @Override
    public Tab getTabAt(int index) {
        android.app.ActionBar.Tab selected = mActionBar.getTabAt(index);
        return (selected != null) ? (Tab)selected.getTag() : null;
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#getTabCount()
     */
    @Override
    public int getTabCount() {
        return mActionBar.getTabCount();
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#getHeight()
     */
    @Override
    public int getHeight() {
        return mActionBar.getHeight();
    }

    /* (non-Javadoc)*
     * @see com.actionbarsherlock.app.ActionBar#show()
     */
    @Override
    public void show() {
        mActionBar.show();
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#hide()
     */
    @Override
    public void hide() {
        mActionBar.hide();
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#isShowing()
     */
    @Override
    public boolean isShowing() {
        return mActionBar.isShowing();
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#addOnMenuVisibilityListener(com.actionbarsherlock.app.ActionBar.OnMenuVisibilityListener)
     */
    @Override
    public void addOnMenuVisibilityListener(OnMenuVisibilityListener listener) {
        mMenuVisibilityListeners.add(listener);
    }

    /**
     * @see com.actionbarsherlock.app.ActionBar#removeOnMenuVisibilityListener(com.actionbarsherlock.app.ActionBar.OnMenuVisibilityListener)
     */
    @Override
    public void removeOnMenuVisibilityListener(OnMenuVisibilityListener listener) {
        mMenuVisibilityListeners.remove(listener);
    }

    /**
     * @see android.app.ActionBar.OnMenuVisibilityListener#onMenuVisibilityChanged(boolean)
     */
    @Override
    public void onMenuVisibilityChanged(boolean isVisible) {
        for (OnMenuVisibilityListener listener : mMenuVisibilityListeners) {
            listener.onMenuVisibilityChanged(isVisible);
        }
    }
}
