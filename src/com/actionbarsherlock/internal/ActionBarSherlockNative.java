package com.actionbarsherlock.internal;

import com.actionbarsherlock.ActionBarSherlock;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.internal.app.ActionBarWrapper;
import com.actionbarsherlock.internal.view.menu.MenuWrapper;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.MenuInflater;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionBarSherlockNative.
 */
@ActionBarSherlock.Implementation(api = 14)
public class ActionBarSherlockNative extends ActionBarSherlock {
    
    /** The m action bar. */
    private ActionBarWrapper mActionBar;
    
    /** The m action mode. */
    private ActionModeWrapper mActionMode;
    
    /** The m menu. */
    private MenuWrapper mMenu;

    /**
     * Instantiates a new action bar sherlock native.
     *
     * @param activity the activity
     * @param flags the flags
     */
    public ActionBarSherlockNative(Activity activity, int flags) {
        super(activity, flags);
    }


    /* (non-Javadoc)
     * @see com.actionbarsherlock.ActionBarSherlock#getActionBar()
     */
    @Override
    public ActionBar getActionBar() {
        if (DEBUG) Log.d(TAG, "[getActionBar]");

        initActionBar();
        return mActionBar;
    }

    /**
     * Inits the action bar.
     */
    private void initActionBar() {
        if (mActionBar != null || mActivity.getActionBar() == null) {
            return;
        }

        mActionBar = new ActionBarWrapper(mActivity);
    }

    /**
     * Dispatch invalidate options menu.
     *
     * @see com.actionbarsherlock.ActionBarSherlock#dispatchInvalidateOptionsMenu()
     */
    @Override
    public void dispatchInvalidateOptionsMenu() {
        if (DEBUG) Log.d(TAG, "[dispatchInvalidateOptionsMenu]");

        mActivity.getWindow().invalidatePanelMenu(Window.FEATURE_OPTIONS_PANEL);
    }

    /**
     * Dispatch create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     * @see com.actionbarsherlock.ActionBarSherlock#dispatchCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean dispatchCreateOptionsMenu(android.view.Menu menu) {
        if (DEBUG) Log.d(TAG, "[dispatchCreateOptionsMenu] menu: " + menu);

        if (mMenu == null || menu != mMenu.unwrap()) {
            mMenu = new MenuWrapper(menu);
        }

        final boolean result = callbackCreateOptionsMenu(mMenu);
        if (DEBUG) Log.d(TAG, "[dispatchCreateOptionsMenu] returning " + result);
        return result;
    }

    /**
     * Dispatch prepare options menu.
     *
     * @param menu the menu
     * @return true, if successful
     * @see com.actionbarsherlock.ActionBarSherlock#dispatchPrepareOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean dispatchPrepareOptionsMenu(android.view.Menu menu) {
        if (DEBUG) Log.d(TAG, "[dispatchPrepareOptionsMenu] menu: " + menu);

        final boolean result = callbackPrepareOptionsMenu(mMenu);
        if (DEBUG) Log.d(TAG, "[dispatchPrepareOptionsMenu] returning " + result);
        return result;
    }

    /**
     * Dispatch options item selected.
     *
     * @param item the item
     * @return true, if successful
     * @see com.actionbarsherlock.ActionBarSherlock#dispatchOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean dispatchOptionsItemSelected(android.view.MenuItem item) {
        if (DEBUG) Log.d(TAG, "[dispatchOptionsItemSelected] item: " + item.getTitleCondensed());

        final boolean result = callbackOptionsItemSelected(mMenu.findItem(item));
        if (DEBUG) Log.d(TAG, "[dispatchOptionsItemSelected] returning " + result);
        return result;
    }

    /**
     * Checks for feature.
     *
     * @param feature the feature
     * @return true, if successful
     * @see com.actionbarsherlock.ActionBarSherlock#hasFeature(int)
     */
    @Override
    public boolean hasFeature(int feature) {
        if (DEBUG) Log.d(TAG, "[hasFeature] feature: " + feature);

        final boolean result = mActivity.getWindow().hasFeature(feature);
        if (DEBUG) Log.d(TAG, "[hasFeature] returning " + result);
        return result;
    }

    /**
     * Request feature.
     *
     * @param featureId the feature id
     * @return true, if successful
     * @see com.actionbarsherlock.ActionBarSherlock#requestFeature(int)
     */
    @Override
    public boolean requestFeature(int featureId) {
        if (DEBUG) Log.d(TAG, "[requestFeature] featureId: " + featureId);

        final boolean result = mActivity.getWindow().requestFeature(featureId);
        if (DEBUG) Log.d(TAG, "[requestFeature] returning " + result);
        return result;
    }

    /**
     * Sets the ui options.
     *
     * @param uiOptions the new ui options
     * @see com.actionbarsherlock.ActionBarSherlock#setUiOptions(int)
     */
    @Override
    public void setUiOptions(int uiOptions) {
        if (DEBUG) Log.d(TAG, "[setUiOptions] uiOptions: " + uiOptions);

        mActivity.getWindow().setUiOptions(uiOptions);
    }

    /**
     * Sets the ui options.
     *
     * @param uiOptions the ui options
     * @param mask the mask
     * @see com.actionbarsherlock.ActionBarSherlock#setUiOptions(int, int)
     */
    @Override
    public void setUiOptions(int uiOptions, int mask) {
        if (DEBUG) Log.d(TAG, "[setUiOptions] uiOptions: " + uiOptions + ", mask: " + mask);

        mActivity.getWindow().setUiOptions(uiOptions, mask);
    }

    /**
     * Sets the content view.
     *
     * @param layoutResId the new content view
     * @see com.actionbarsherlock.ActionBarSherlock#setContentView(int)
     */
    @Override
    public void setContentView(int layoutResId) {
        if (DEBUG) Log.d(TAG, "[setContentView] layoutResId: " + layoutResId);

        mActivity.getWindow().setContentView(layoutResId);
        initActionBar();
    }

    /**
     * Sets the content view.
     *
     * @param view the view
     * @param params the params
     * @see com.actionbarsherlock.ActionBarSherlock#setContentView(android.view.View, android.view.ViewGroup.LayoutParams)
     */
    @Override
    public void setContentView(View view, LayoutParams params) {
        if (DEBUG) Log.d(TAG, "[setContentView] view: " + view + ", params: " + params);

        mActivity.getWindow().setContentView(view, params);
        initActionBar();
    }

    /**
     * Adds the content view.
     *
     * @param view the view
     * @param params the params
     * @see com.actionbarsherlock.ActionBarSherlock#addContentView(android.view.View, android.view.ViewGroup.LayoutParams)
     */
    @Override
    public void addContentView(View view, LayoutParams params) {
        if (DEBUG) Log.d(TAG, "[addContentView] view: " + view + ", params: " + params);

        mActivity.getWindow().addContentView(view, params);
        initActionBar();
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     * @see com.actionbarsherlock.ActionBarSherlock#setTitle(java.lang.CharSequence)
     */
    @Override
    public void setTitle(CharSequence title) {
        if (DEBUG) Log.d(TAG, "[setTitle] title: " + title);

        mActivity.getWindow().setTitle(title);
    }

    /**
     * Sets the progress bar visibility.
     *
     * @param visible the new progress bar visibility
     * @see com.actionbarsherlock.ActionBarSherlock#setProgressBarVisibility(boolean)
     */
    @Override
    public void setProgressBarVisibility(boolean visible) {
        if (DEBUG) Log.d(TAG, "[setProgressBarVisibility] visible: " + visible);

        mActivity.setProgressBarVisibility(visible);
    }

    /**
     * Sets the progress bar indeterminate visibility.
     *
     * @param visible the new progress bar indeterminate visibility
     * @see com.actionbarsherlock.ActionBarSherlock#setProgressBarIndeterminateVisibility(boolean)
     */
    @Override
    public void setProgressBarIndeterminateVisibility(boolean visible) {
        if (DEBUG) Log.d(TAG, "[setProgressBarIndeterminateVisibility] visible: " + visible);

        mActivity.setProgressBarIndeterminateVisibility(visible);
    }

    /**
     * Sets the progress bar indeterminate.
     *
     * @param indeterminate the new progress bar indeterminate
     * @see com.actionbarsherlock.ActionBarSherlock#setProgressBarIndeterminate(boolean)
     */
    @Override
    public void setProgressBarIndeterminate(boolean indeterminate) {
        if (DEBUG) Log.d(TAG, "[setProgressBarIndeterminate] indeterminate: " + indeterminate);

        mActivity.setProgressBarIndeterminate(indeterminate);
    }

    /**
     * Sets the progress.
     *
     * @param progress the new progress
     * @see com.actionbarsherlock.ActionBarSherlock#setProgress(int)
     */
    @Override
    public void setProgress(int progress) {
        if (DEBUG) Log.d(TAG, "[setProgress] progress: " + progress);

        mActivity.setProgress(progress);
    }

    /**
     * Sets the secondary progress.
     *
     * @param secondaryProgress the new secondary progress
     * @see com.actionbarsherlock.ActionBarSherlock#setSecondaryProgress(int)
     */
    @Override
    public void setSecondaryProgress(int secondaryProgress) {
        if (DEBUG) Log.d(TAG, "[setSecondaryProgress] secondaryProgress: " + secondaryProgress);

        mActivity.setSecondaryProgress(secondaryProgress);
    }

    /**
     * Gets the themed context.
     *
     * @return the themed context
     * @see com.actionbarsherlock.ActionBarSherlock#getThemedContext()
     */
    @Override
    protected Context getThemedContext() {
        Context context = mActivity;
        TypedValue outValue = new TypedValue();
        mActivity.getTheme().resolveAttribute(android.R.attr.actionBarWidgetTheme, outValue, true);
        if (outValue.resourceId != 0) {
            //We are unable to test if this is the same as our current theme
            //so we just wrap it and hope that if the attribute was specified
            //then the user is intentionally specifying an alternate theme.
            context = new ContextThemeWrapper(context, outValue.resourceId);
        }
        return context;
    }

    /**
     * Start action mode.
     *
     * @param callback the callback
     * @return the action mode
     * @see com.actionbarsherlock.ActionBarSherlock#startActionMode(com.actionbarsherlock.view.ActionMode.Callback)
     */
    @Override
    public ActionMode startActionMode(com.actionbarsherlock.view.ActionMode.Callback callback) {
        if (DEBUG) Log.d(TAG, "[startActionMode] callback: " + callback);

        if (mActionMode != null) {
            mActionMode.finish();
        }
        ActionModeCallbackWrapper wrapped = null;
        if (callback != null) {
            wrapped = new ActionModeCallbackWrapper(callback);
        }

        //Calling this will trigger the callback wrapper's onCreate which
        //is where we will set the new instance to mActionMode since we need
        //to pass it through to the sherlock callbacks and the call below
        //will not have returned yet to store its value.
        mActivity.startActionMode(wrapped);

        return mActionMode;
    }

    /**
     * The Class ActionModeCallbackWrapper.
     */
    private class ActionModeCallbackWrapper implements android.view.ActionMode.Callback {
        
        /** The m callback. */
        private final ActionMode.Callback mCallback;

        /**
         * Instantiates a new action mode callback wrapper.
         *
         * @param callback the callback
         */
        public ActionModeCallbackWrapper(ActionMode.Callback callback) {
            mCallback = callback;
        }

        /**
         * On create action mode.
         *
         * @param mode the mode
         * @param menu the menu
         * @return true, if successful
         * @see android.view.ActionMode.Callback#onCreateActionMode(android.view.ActionMode, android.view.Menu)
         */
        @Override
        public boolean onCreateActionMode(android.view.ActionMode mode, android.view.Menu menu) {
            //See ActionBarSherlockNative#startActionMode
            mActionMode = new ActionModeWrapper(mode);

            return mCallback.onCreateActionMode(mActionMode, mActionMode.getMenu());
        }

        /**
         * On prepare action mode.
         *
         * @param mode the mode
         * @param menu the menu
         * @return true, if successful
         * @see android.view.ActionMode.Callback#onPrepareActionMode(android.view.ActionMode, android.view.Menu)
         */
        @Override
        public boolean onPrepareActionMode(android.view.ActionMode mode, android.view.Menu menu) {
            return mCallback.onPrepareActionMode(mActionMode, mActionMode.getMenu());
        }

        /**
         * On action item clicked.
         *
         * @param mode the mode
         * @param item the item
         * @return true, if successful
         * @see android.view.ActionMode.Callback#onActionItemClicked(android.view.ActionMode, android.view.MenuItem)
         */
        @Override
        public boolean onActionItemClicked(android.view.ActionMode mode, android.view.MenuItem item) {
            return mCallback.onActionItemClicked(mActionMode, mActionMode.getMenu().findItem(item));
        }

        /**
         * On destroy action mode.
         *
         * @param mode the mode
         * @see android.view.ActionMode.Callback#onDestroyActionMode(android.view.ActionMode)
         */
        @Override
        public void onDestroyActionMode(android.view.ActionMode mode) {
            mCallback.onDestroyActionMode(mActionMode);
        }
    }

    /**
     * The Class ActionModeWrapper.
     */
    private class ActionModeWrapper extends ActionMode {
        
        /** The m action mode. */
        private final android.view.ActionMode mActionMode;
        
        /** The m menu. */
        private MenuWrapper mMenu = null;

        /**
         * Instantiates a new action mode wrapper.
         *
         * @param actionMode the action mode
         */
        ActionModeWrapper(android.view.ActionMode actionMode) {
            mActionMode = actionMode;
        }

        /**
         * Sets the title.
         *
         * @param title the new title
         * @see com.actionbarsherlock.view.ActionMode#setTitle(java.lang.CharSequence)
         */
        @Override
        public void setTitle(CharSequence title) {
            mActionMode.setTitle(title);
        }

        /**
         * Sets the title.
         *
         * @param resId the new title
         * @see com.actionbarsherlock.view.ActionMode#setTitle(int)
         */
        @Override
        public void setTitle(int resId) {
            mActionMode.setTitle(resId);
        }

        /**
         * Sets the subtitle.
         *
         * @param subtitle the new subtitle
         * @see com.actionbarsherlock.view.ActionMode#setSubtitle(java.lang.CharSequence)
         */
        @Override
        public void setSubtitle(CharSequence subtitle) {
            mActionMode.setSubtitle(subtitle);
        }

        /**
         * Sets the subtitle.
         *
         * @param resId the new subtitle
         * @see com.actionbarsherlock.view.ActionMode#setSubtitle(int)
         */
        @Override
        public void setSubtitle(int resId) {
            mActionMode.setSubtitle(resId);
        }

        /**
         * Sets the custom view.
         *
         * @param view the new custom view
         * @see com.actionbarsherlock.view.ActionMode#setCustomView(android.view.View)
         */
        @Override
        public void setCustomView(View view) {
            mActionMode.setCustomView(view);
        }

        /**
         * Invalidate.
         *
         * @see com.actionbarsherlock.view.ActionMode#invalidate()
         */
        @Override
        public void invalidate() {
            mActionMode.invalidate();
        }

        /**
         * Finish.
         *
         * @see com.actionbarsherlock.view.ActionMode#finish()
         */
        @Override
        public void finish() {
            mActionMode.finish();
        }

        /**
         * Gets the menu.
         *
         * @return the menu
         * @see com.actionbarsherlock.view.ActionMode#getMenu()
         */
        @Override
        public MenuWrapper getMenu() {
            if (mMenu == null) {
                mMenu = new MenuWrapper(mActionMode.getMenu());
            }
            return mMenu;
        }

        /**
         * Gets the title.
         *
         * @return the title
         * @see com.actionbarsherlock.view.ActionMode#getTitle()
         */
        @Override
        public CharSequence getTitle() {
            return mActionMode.getTitle();
        }

        /**
         * Gets the subtitle.
         *
         * @return the subtitle
         * @see com.actionbarsherlock.view.ActionMode#getSubtitle()
         */
        @Override
        public CharSequence getSubtitle() {
            return mActionMode.getSubtitle();
        }

        /**
         * Gets the custom view.
         *
         * @return the custom view
         * @see com.actionbarsherlock.view.ActionMode#getCustomView()
         */
        @Override
        public View getCustomView() {
            return mActionMode.getCustomView();
        }

        /**
         * Gets the menu inflater.
         *
         * @return the menu inflater
         * @see com.actionbarsherlock.view.ActionMode#getMenuInflater()
         */
        @Override
        public MenuInflater getMenuInflater() {
            return ActionBarSherlockNative.this.getMenuInflater();
        }

        /**
         * Sets the tag.
         *
         * @param tag the new tag
         * @see com.actionbarsherlock.view.ActionMode#setTag(java.lang.Object)
         */
        @Override
        public void setTag(Object tag) {
            mActionMode.setTag(tag);
        }

        /**
         * Gets the tag.
         *
         * @return the tag
         * @see com.actionbarsherlock.view.ActionMode#getTag()
         */
        @Override
        public Object getTag() {
            return mActionMode.getTag();
        }
    }
}
