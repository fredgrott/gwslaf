/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.handmark.pulltorefresh.library.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class LoadingLayout.
 */
public class LoadingLayout extends FrameLayout {

	/** The Constant DEFAULT_ROTATION_ANIMATION_DURATION. */
	static final int DEFAULT_ROTATION_ANIMATION_DURATION = 1200;

	/** The m header image. */
	private final ImageView mHeaderImage;
	
	/** The m header image matrix. */
	private final Matrix mHeaderImageMatrix;

	/** The m header text. */
	private final TextView mHeaderText;
	
	/** The m sub header text. */
	private final TextView mSubHeaderText;

	/** The m pull label. */
	private String mPullLabel;
	
	/** The m refreshing label. */
	private String mRefreshingLabel;
	
	/** The m release label. */
	private String mReleaseLabel;

	/** The m rotation pivot y. */
	private float mRotationPivotX, mRotationPivotY;

	/** The m rotate animation. */
	private final Animation mRotateAnimation;

	/**
	 * Instantiates a new loading layout.
	 *
	 * @param context the context
	 * @param mode the mode
	 * @param attrs the attrs
	 */
	public LoadingLayout(Context context, final Mode mode, TypedArray attrs) {
		super(context);
		ViewGroup header = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.pull_to_refresh_header, this);
		mHeaderText = (TextView) header.findViewById(R.id.pull_to_refresh_text);
		mSubHeaderText = (TextView) header.findViewById(R.id.pull_to_refresh_sub_text);
		mHeaderImage = (ImageView) header.findViewById(R.id.pull_to_refresh_image);

		mHeaderImage.setScaleType(ScaleType.MATRIX);
		mHeaderImageMatrix = new Matrix();
		mHeaderImage.setImageMatrix(mHeaderImageMatrix);

		final Interpolator interpolator = new LinearInterpolator();
		mRotateAnimation = new RotateAnimation(0, 720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		mRotateAnimation.setInterpolator(interpolator);
		mRotateAnimation.setDuration(DEFAULT_ROTATION_ANIMATION_DURATION);
		mRotateAnimation.setRepeatCount(Animation.INFINITE);
		mRotateAnimation.setRepeatMode(Animation.RESTART);

		switch (mode) {
			case PULL_UP_TO_REFRESH:
				// Load in labels
				mPullLabel = context.getString(R.string.pull_to_refresh_from_bottom_pull_label);
				mRefreshingLabel = context.getString(R.string.pull_to_refresh_from_bottom_refreshing_label);
				mReleaseLabel = context.getString(R.string.pull_to_refresh_from_bottom_release_label);
				break;

			case PULL_DOWN_TO_REFRESH:
			default:
				// Load in labels
				mPullLabel = context.getString(R.string.pull_to_refresh_pull_label);
				mRefreshingLabel = context.getString(R.string.pull_to_refresh_refreshing_label);
				mReleaseLabel = context.getString(R.string.pull_to_refresh_release_label);
				break;
		}

		if (attrs.hasValue(R.styleable.PullToRefresh_ptrHeaderTextColor)) {
			ColorStateList colors = attrs.getColorStateList(R.styleable.PullToRefresh_ptrHeaderTextColor);
			setTextColor(null != colors ? colors : ColorStateList.valueOf(0xFF000000));
		}
		if (attrs.hasValue(R.styleable.PullToRefresh_ptrHeaderSubTextColor)) {
			ColorStateList colors = attrs.getColorStateList(R.styleable.PullToRefresh_ptrHeaderSubTextColor);
			setSubTextColor(null != colors ? colors : ColorStateList.valueOf(0xFF000000));
		}
		if (attrs.hasValue(R.styleable.PullToRefresh_ptrHeaderBackground)) {
			Drawable background = attrs.getDrawable(R.styleable.PullToRefresh_ptrHeaderBackground);
			if (null != background) {
				setBackgroundDrawable(background);
			}
		}

		// Try and get defined drawable from Attrs
		Drawable imageDrawable = null;
		if (attrs.hasValue(R.styleable.PullToRefresh_ptrDrawable)) {
			imageDrawable = attrs.getDrawable(R.styleable.PullToRefresh_ptrDrawable);
		}

		// If we don't have a user defined drawable, load the default
		if (null == imageDrawable) {
			imageDrawable = context.getResources().getDrawable(R.drawable.default_ptr_drawable);
		}

		// Set Drawable, and save width/height
		setLoadingDrawable(imageDrawable);

		reset();
	}

	/**
	 * Reset.
	 */
	public void reset() {
		mHeaderText.setText(wrapHtmlLabel(mPullLabel));
		mHeaderImage.setVisibility(View.VISIBLE);
		mHeaderImage.clearAnimation();

		resetImageRotation();

		if (TextUtils.isEmpty(mSubHeaderText.getText())) {
			mSubHeaderText.setVisibility(View.GONE);
		} else {
			mSubHeaderText.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * Release to refresh.
	 */
	public void releaseToRefresh() {
		mHeaderText.setText(wrapHtmlLabel(mReleaseLabel));
	}

	/**
	 * Sets the pull label.
	 *
	 * @param pullLabel the new pull label
	 */
	public void setPullLabel(String pullLabel) {
		mPullLabel = pullLabel;
	}

	/**
	 * Refreshing.
	 */
	public void refreshing() {
		mHeaderText.setText(wrapHtmlLabel(mRefreshingLabel));
		mHeaderImage.startAnimation(mRotateAnimation);

		mSubHeaderText.setVisibility(View.GONE);
	}

	/**
	 * Sets the refreshing label.
	 *
	 * @param refreshingLabel the new refreshing label
	 */
	public void setRefreshingLabel(String refreshingLabel) {
		mRefreshingLabel = refreshingLabel;
	}

	/**
	 * Sets the release label.
	 *
	 * @param releaseLabel the new release label
	 */
	public void setReleaseLabel(String releaseLabel) {
		mReleaseLabel = releaseLabel;
	}

	/**
	 * Pull to refresh.
	 */
	public void pullToRefresh() {
		mHeaderText.setText(wrapHtmlLabel(mPullLabel));
	}

	/**
	 * Sets the text color.
	 *
	 * @param color the new text color
	 */
	public void setTextColor(ColorStateList color) {
		mHeaderText.setTextColor(color);
		mSubHeaderText.setTextColor(color);
	}

	/**
	 * Sets the sub text color.
	 *
	 * @param color the new sub text color
	 */
	public void setSubTextColor(ColorStateList color) {
		mSubHeaderText.setTextColor(color);
	}

	/**
	 * Sets the text color.
	 *
	 * @param color the new text color
	 */
	public void setTextColor(int color) {
		setTextColor(ColorStateList.valueOf(color));
	}

	/**
	 * Sets the loading drawable.
	 *
	 * @param imageDrawable the new loading drawable
	 */
	public void setLoadingDrawable(Drawable imageDrawable) {
		// Set Drawable, and save width/height
		mHeaderImage.setImageDrawable(imageDrawable);
		mRotationPivotX = imageDrawable.getIntrinsicWidth() / 2f;
		mRotationPivotY = imageDrawable.getIntrinsicHeight() / 2f;
	}

	/**
	 * Sets the sub text color.
	 *
	 * @param color the new sub text color
	 */
	public void setSubTextColor(int color) {
		setSubTextColor(ColorStateList.valueOf(color));
	}

	/**
	 * Sets the sub header text.
	 *
	 * @param label the new sub header text
	 */
	public void setSubHeaderText(CharSequence label) {
		if (TextUtils.isEmpty(label)) {
			mSubHeaderText.setVisibility(View.GONE);
		} else {
			mSubHeaderText.setText(label);
			mSubHeaderText.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * On pull y.
	 *
	 * @param scaleOfHeight the scale of height
	 */
	public void onPullY(float scaleOfHeight) {
		mHeaderImageMatrix.setRotate(scaleOfHeight * 90, mRotationPivotX, mRotationPivotY);
		mHeaderImage.setImageMatrix(mHeaderImageMatrix);
	}

	/**
	 * Reset image rotation.
	 */
	private void resetImageRotation() {
		mHeaderImageMatrix.reset();
		mHeaderImage.setImageMatrix(mHeaderImageMatrix);
	}

	/**
	 * Wrap html label.
	 *
	 * @param label the label
	 * @return the char sequence
	 */
	private CharSequence wrapHtmlLabel(String label) {
		if (!isInEditMode()) {
			return Html.fromHtml(label);
		} else {
			return label;
		}
	}
}
