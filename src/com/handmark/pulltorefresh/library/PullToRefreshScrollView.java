package com.handmark.pulltorefresh.library;

import org.bitbucket.fredgrott.gwslaf.R;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

// TODO: Auto-generated Javadoc
/**
 * The Class PullToRefreshScrollView.
 */
public class PullToRefreshScrollView extends PullToRefreshBase<ScrollView> {

	/**
	 * Instantiates a new pull to refresh scroll view.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public PullToRefreshScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * Creates the refreshable view.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @return the scroll view
	 * @see com.handmark.pulltorefresh.library.PullToRefreshBase#createRefreshableView(android.content.Context, android.util.AttributeSet)
	 */
	@Override
	protected ScrollView createRefreshableView(Context context, AttributeSet attrs) {
		ScrollView scrollView;
		if (VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD) {
			scrollView = new InternalScrollViewSDK9(context, attrs);
		} else {
			scrollView = new ScrollView(context, attrs);
		}

		scrollView.setId(R.id.scrollview);
		return scrollView;
	}

	/**
	 * Checks if is ready for pull down.
	 *
	 * @return true, if is ready for pull down
	 * @see com.handmark.pulltorefresh.library.PullToRefreshBase#isReadyForPullDown()
	 */
	@Override
	protected boolean isReadyForPullDown() {
		return mRefreshableView.getScrollY() == 0;
	}

	/**
	 * Checks if is ready for pull up.
	 *
	 * @return true, if is ready for pull up
	 * @see com.handmark.pulltorefresh.library.PullToRefreshBase#isReadyForPullUp()
	 */
	@Override
	protected boolean isReadyForPullUp() {
		View scrollViewChild = mRefreshableView.getChildAt(0);
		if (null != scrollViewChild) {
			return mRefreshableView.getScrollY() >= (scrollViewChild.getHeight() - getHeight());
		}
		return false;
	}

	/**
	 * The Class InternalScrollViewSDK9.
	 */
	@TargetApi(9)
	final class InternalScrollViewSDK9 extends ScrollView {

		/**
		 * Instantiates a new internal scroll view sd k9.
		 *
		 * @param context the context
		 * @param attrs the attrs
		 */
		public InternalScrollViewSDK9(Context context, AttributeSet attrs) {
			super(context, attrs);
		}

		/**
		 * Over scroll by.
		 *
		 * @param deltaX the delta x
		 * @param deltaY the delta y
		 * @param scrollX the scroll x
		 * @param scrollY the scroll y
		 * @param scrollRangeX the scroll range x
		 * @param scrollRangeY the scroll range y
		 * @param maxOverScrollX the max over scroll x
		 * @param maxOverScrollY the max over scroll y
		 * @param isTouchEvent the is touch event
		 * @return true, if successful
		 * @see android.view.View#overScrollBy(int, int, int, int, int, int, int, int, boolean)
		 */
		@Override
		protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX,
				int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {

			final boolean returnValue = super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX,
					scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);

			// Does all of the hard work...
			OverscrollHelper
					.overScrollBy(PullToRefreshScrollView.this, deltaY, scrollY, getScrollRange(), isTouchEvent);

			return returnValue;
		}

		/**
		 * Taken from the AOSP ScrollView source.
		 *
		 * @return the scroll range
		 */
		private int getScrollRange() {
			int scrollRange = 0;
			if (getChildCount() > 0) {
				View child = getChildAt(0);
				scrollRange = Math.max(0, child.getHeight() - (getHeight() - getPaddingBottom() - getPaddingTop()));
			}
			return scrollRange;
		}
	}
}
