/*
 * Copyright 2012 Greg Billetdeaux
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.deaux.gestures;

import android.view.View;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving panGesture events.
 * The class that is interested in processing a panGesture
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addPanGestureListener<code> method. When
 * the panGesture event occurs, that object's appropriate
 * method is invoked.
 *
 * @see PanGestureEvent
 */
public interface PanGestureListener {

	/**
	 * On pan.
	 *
	 * @param v the v
	 * @param deltax the deltax
	 * @param deltaY the delta y
	 */
	public abstract void onPan(View v, float deltax, float deltaY);
	
	/**
	 * On lift.
	 *
	 * @param v the v
	 * @param velocityX the velocity x
	 * @param velocityY the velocity y
	 */
	public abstract void onLift(View v, float velocityX, float velocityY);
}
