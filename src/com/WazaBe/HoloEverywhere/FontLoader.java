package com.WazaBe.HoloEverywhere;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.bitbucket.fredgrott.gwslaf.R;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build.VERSION;
import android.os.Environment;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

// TODO: Auto-generated Javadoc
/**
 * The Class FontLoader.
 */
public final class FontLoader {
	
	/**
	 * The Class HoloFont.
	 */
	public static class HoloFont {
		
		/** The Constant ALEGREYASC_BLACK. */
		public static final HoloFont ALEGREYASC_BLACK = new HoloFont(R.raw.alegreyasc_black);
		
		/** The Constant ALEGREYASC_BLACKITALITC. */
		public static final HoloFont ALEGREYASC_BLACKITALITC = new HoloFont(R.raw.alegreyasc_blackitalic);
		
		/** The Constant ALEGREYASC_BOLD. */
		public static final HoloFont ALEGREYASC_BOLD = new HoloFont(R.raw.alegreyasc_bold);
		
		/** The Constant ALEGREYASC_BOLDITALIC. */
		public static final HoloFont ALEGREYASC_BOLDITALIC = new HoloFont(R.raw.alegreyasc_bolditalic);
		
		/** The Constant ALEGREYASC_ITALIC. */
		public static final HoloFont ALEGREYASC_ITALIC = new HoloFont(R.raw.alegreyasc_italic);
		
		/** The Constant ALEGREYASC_REGULAR. */
		public static final HoloFont ALEGREYASC_REGULAR = new HoloFont(R.raw.alegreyasc_regular);
		
		/** The Constant CANTARELL_BOLD. */
		public static final HoloFont CANTARELL_BOLD = new HoloFont(R.raw.cantarell_bold);
		
		/** The Constant CANTARELL_BOLDOBLIQUE. */
		public static final HoloFont CANTARELL_BOLDOBLIQUE = new HoloFont(R.raw.cantarell_boldoblique);
		
		/** The Constant CANTARELL_REGULAR. */
		public static final HoloFont CANTARELL_REGULAR = new HoloFont(R.raw.cantarell_regular);
		
		/** The Constant CAUDEX_BOLD. */
		public static final HoloFont CAUDEX_BOLD = new HoloFont(R.raw.caudex_bold);
		
		/** The Constant CAUDEX_BOLDITALIC. */
		public static final HoloFont CAUDEX_BOLDITALIC = new HoloFont(R.raw.caudex_bolditalic);
		
		/** The Constant CAUDEX_ITALIC. */
		public static final HoloFont CAUDEX_ITALIC = new HoloFont(R.raw.caudex_italic);
		
		/** The Constant CAUDEX_REGULAR. */
		public static final HoloFont CAUDEX_REGULAR = new HoloFont(R.raw.caudex_regular);
		
		/** The Constant DROIDKUFI_BOLD. */
		public static final HoloFont DROIDKUFI_BOLD = new HoloFont(R.raw.droidkufi_bold);
		
		/** The Constant DROIDKUFI_REGULAR. */
		public static final HoloFont DROIDKUFI_REGULAR = new HoloFont(R.raw.droidkufi_regular);
		
		/** The Constant DROIDNASKH_REGULAR. */
		public static final HoloFont DROIDNASKH_REGULAR = new HoloFont(R.raw.droidnaskh_regular);
		
		/** The Constant DROIDNASKH_BOLD. */
		public static final HoloFont DROIDNASKH_BOLD = new HoloFont(R.raw.droidnaskh_bold);
		
		/** The Constant DROIDSANS_BOLD. */
		public static final HoloFont DROIDSANS_BOLD = new HoloFont(R.raw.droidsans_bold);
		
		/** The Constant DROIDSANS. */
		public static final HoloFont DROIDSANS = new HoloFont(R.raw.droidsans);
		
		/** The Constant DROIDSANSETHIOPIC_BOLD. */
		public static final HoloFont DROIDSANSETHIOPIC_BOLD = new HoloFont(R.raw.droidsansethiopic_bold);
		
		/** The Constant DROIDSANSETHIOPIC_REGULAR. */
		public static final HoloFont DROIDSANSETHIOPIC_REGULAR = new HoloFont(R.raw.droidsansethiopic_regular);
		
		/** The Constant DROIDSANSMONO. */
		public static final HoloFont DROIDSANSMONO = new HoloFont(R.raw.droidsansmono);
		
		/** The Constant DROIDSANSTHAI_BOLD. */
		public static final HoloFont DROIDSANSTHAI_BOLD = new HoloFont(R.raw.droidsansthai_bold);
		
		/** The Constant DROIDSANSTHAI_REGULAR. */
		public static final HoloFont DROIDSANSTHAI_REGULAR = new HoloFont(R.raw.droidsansthai_regular);
		
		/** The Constant DROIDSERIF_BOLD. */
		public static final HoloFont DROIDSERIF_BOLD = new HoloFont(R.raw.droidserif_bold);
		
		/** The Constant DROIDSERIF_BOLDITALIC. */
		public static final HoloFont DROIDSERIF_BOLDITALIC = new HoloFont(R.raw.droidserif_bolditalic);
		
		/** The Constant DROIDSERIF_ITALIC. */
		public static final HoloFont DROIDSERIF_ITALIC = new HoloFont(R.raw.droidserif_italic);
		
		/** The Constant DROIDSERIF_REGULAR. */
		public static final HoloFont DROIDSERIF_REGULAR = new HoloFont(R.raw.droidserif_regular);
		
		/** The Constant ECONOMICA_BOLD. */
		public static final HoloFont ECONOMICA_BOLD = new HoloFont(R.raw.economica_bold);
		
		/** The Constant ECONOMICA_BOLDITALIC. */
		public static final HoloFont ECONOMICA_BOLDITALIC = new HoloFont(R.raw.economica_bolditalic);
		
		/** The Constant ECONOMICA_ITALIC. */
		public static final HoloFont ECONOMICA_ITALIC = new HoloFont(R.raw.economica_italic);
		
		/** The Constant ECONOMICA_REGULAR. */
		public static final HoloFont ECONOMICA_REGULAR = new HoloFont(R.raw.economica_regular);
		
		/** The Constant EXPLETUSSANS_BOLD. */
		public static final HoloFont EXPLETUSSANS_BOLD = new HoloFont(R.raw.expletussans_bold);
		
		/** The Constant EXPLETUSSANS_BOLDITALIC. */
		public static final HoloFont EXPLETUSSANS_BOLDITALIC = new HoloFont(R.raw.expletussans_bolditalic);
		
		/** The Constant EXPLETUSSANS_ITALIC. */
		public static final HoloFont EXPLETUSSANS_ITALIC = new HoloFont(R.raw.expletussans_italic);
		
		/** The Constant EXPLETUSSANS_MEDIUM. */
		public static final HoloFont EXPLETUSSANS_MEDIUM = new HoloFont(R.raw.expletussans_medium);
		
		/** The Constant EXPLETUSSANS_MEDIUMITALIC. */
		public static final HoloFont EXPLETUSSANS_MEDIUMITALIC = new HoloFont(R.raw.expletussans_mediumitalic);
		
		/** The Constant EXPLETUSSANS_REGULAR. */
		public static final HoloFont EXPLETUSSANS_REGULAR = new HoloFont(R.raw.expletussans_regular);
		
		/** The Constant EXPLETUSSANS_SEMIBOLD. */
		public static final HoloFont EXPLETUSSANS_SEMIBOLD = new HoloFont(R.raw.expletussans_semibold);
		
		/** The Constant EXLPETUSSANS_SEMIBOLDITALIC. */
		public static final HoloFont EXLPETUSSANS_SEMIBOLDITALIC = new HoloFont(R.raw.expletussans_semibolditalic);
		
		/** The Constant GENBASB. */
		public static final HoloFont GENBASB = new HoloFont(R.raw.genbasb);
		
		/** The Constant GENBASBI. */
		public static final HoloFont GENBASBI = new HoloFont(R.raw.genbasbi);
		
		/** The Constant GENBASI. */
		public static final HoloFont GENBASI = new HoloFont(R.raw.genbasi);
		
		/** The Constant GENBASR. */
		public static final HoloFont GENBASR = new HoloFont(R.raw.genbasr);
		
		/** The Constant ROBOTO_BLACK. */
		public static final HoloFont ROBOTO_BLACK = new HoloFont(R.raw.roboto_black);
		
		/** The Constant ROBOTO_BLACKITALIC. */
		public static final HoloFont ROBOTO_BLACKITALIC = new HoloFont(R.raw.roboto_blackitalic);
		
		/** The Constant ROBOTO_BOLDCONDENSED. */
		public static final HoloFont ROBOTO_BOLDCONDENSED = new HoloFont(R.raw.roboto_boldcondensed);
		
		/** The Constant ROBOTO_BOLDCONDENSEDITALIC. */
		public static final HoloFont ROBOTO_BOLDCONDENSEDITALIC = new HoloFont(R.raw.roboto_boldcondenseditalic);
		
		/** The Constant ROBOTO_BOLDITALIC. */
		public static final HoloFont ROBOTO_BOLDITALIC = new HoloFont(R.raw.roboto_bolditalic);
		
		/** The Constant ROBOTO_CONDENSED. */
		public static final HoloFont ROBOTO_CONDENSED = new HoloFont(R.raw.roboto_condensed);
		
		/** The Constant ROBOTO_CONDENSEDITALIC. */
		public static final HoloFont ROBOTO_CONDENSEDITALIC = new HoloFont(R.raw.roboto_condenseditalic);
		
		/** The Constant ROBOTO_LIGHT. */
		public static final HoloFont ROBOTO_LIGHT = new HoloFont(R.raw.roboto_light);
		
		/** The Constant ROBOTO_LIGHTITALIC. */
		public static final HoloFont ROBOTO_LIGHTITALIC = new HoloFont(R.raw.roboto_lightitalic);
		
		/** The Constant ROBOTO_MEDIUM. */
		public static final HoloFont ROBOTO_MEDIUM = new HoloFont(R.raw.roboto_medium);
		
		/** The Constant ROBOTO_MEDIUMITALIC. */
		public static final HoloFont ROBOTO_MEDIUMITALIC = new HoloFont(R.raw.roboto_mediumitalic);
		
		/** The Constant ROBOTO_THIN. */
		public static final HoloFont ROBOTO_THIN = new HoloFont(R.raw.roboto_thin);
		
		/** The Constant ROBOTO_THINITALIC. */
		public static final HoloFont ROBOTO_THINITALIC = new HoloFont(R.raw.roboto_thinitalic);
		
		/** The Constant THABIT_BOLD. */
		public static final HoloFont THABIT_BOLD = new HoloFont(R.raw.thabit);
		
		/** The Constant THABIT_BOLDOBLIQUE. */
		public static final HoloFont THABIT_BOLDOBLIQUE = new HoloFont(R.raw.thabit_boldoblique);
		
		/** The Constant THABIT_OBLIQUE. */
		public static final HoloFont THABIT_OBLIQUE = new HoloFont(R.raw.thabit_oblique);
		
		/** The Constant THABIT. */
		public static final HoloFont THABIT = new HoloFont(R.raw.thabit);
		
		
		
		/** The Constant ROBOTO_BOLD. */
		public static final HoloFont ROBOTO_BOLD = new HoloFont(
				R.raw.roboto_bold);
		
		
		
		/** The Constant ROBOTO_BOLD_ITALIC. */
		public static final HoloFont ROBOTO_BOLD_ITALIC = new HoloFont(
				R.raw.roboto_bolditalic);
		
		
		/** The Constant ROBOTO_ITALIC. */
		public static final HoloFont ROBOTO_ITALIC = new HoloFont(
				R.raw.roboto_italic);
		
		
		
		/** The Constant ROBOTO_REGULAR. */
		public static final HoloFont ROBOTO_REGULAR = new HoloFont(
				R.raw.roboto_regular);
		
		
		
		/** The Constant ROBOTO. */
		public static final HoloFont ROBOTO = new HoloFont(-1);

		/** The font. */
		protected final int font;
		
		/** The ignore. */
		protected final boolean ignore;

		/**
		 * Instantiates a new holo font.
		 *
		 * @param font the font
		 */
		private HoloFont(int font) {
			this(font, VERSION.SDK_INT >= 11);
		}

		/**
		 * Instantiates a new holo font.
		 *
		 * @param font the font
		 * @param ignore the ignore
		 */
		private HoloFont(int font, boolean ignore) {
			this.font = font;
			this.ignore = ignore;
		}
	}

	/** The Constant fontArray. */
	private static final SparseArray<Typeface> fontArray = new SparseArray<Typeface>();
	
	/** The Constant TAG. */
	private static final String TAG = "FontLoader";

	/**
	 * Apply.
	 *
	 * @param view the view
	 * @return the view
	 */
	public static View apply(View view) {
		return applyDefaultStyles(view);
	}

	/**
	 * Apply.
	 *
	 * @param view the view
	 * @param font the font
	 * @return the view
	 */
	public static View apply(View view, HoloFont font) {
		if (font.ignore) {
			return view;
		}
		return apply(view, font.font);
	}

	/**
	 * Load typeface.
	 *
	 * @param context the context
	 * @param font the font
	 * @return the typeface
	 */
	public static Typeface loadTypeface(Context context, int font) {
		if (fontArray.get(font) == null) {
			try {
				File file = new File(Environment.getDataDirectory(), "data/"
						+ context.getPackageName() + "/fonts");
				if (!file.exists()) {
					file.mkdirs();
				}
				file = new File(file, Integer.toHexString(font));
				if (file.exists()) {
					file.delete();
				}
				Resources res = context.getResources();
				InputStream is = res.openRawResource(font);
				OutputStream os = new FileOutputStream(file);
				byte[] buffer = new byte[8192];
				int read;
				while ((read = is.read(buffer)) > 0) {
					os.write(buffer, 0, read);
				}
				os.flush();
				os.close();
				is.close();
				fontArray.put(font, Typeface.createFromFile(file));
			} catch (Exception e) {
				Log.e(TAG, "Error of loading font", e);
			}
		}
		return fontArray.get(font);
	}

	/**
	 * Apply.
	 *
	 * @param view the view
	 * @param font the font
	 * @return the view
	 */
	
	public static View apply(View view, int font) {
		if (view == null || view.getContext() == null
				|| view.getContext().isRestricted()) {
			Log.e(TAG, "View or context is invalid");
			return view;
		}
		if (font < 0) {
			return applyDefaultStyles(view);
		}
		Typeface typeface = loadTypeface(view.getContext(), font);
		if (typeface == null) {
			Log.v(TAG, "Font " + font + " not found in resources");
			return view;
		} else {
			return apply(view, typeface);
		}
	}

	/**
	 * Apply default styles.
	 *
	 * @param view the view
	 * @return the view
	 */
	public static View applyDefaultStyles(View view) {
		if (view == null || view.getContext() == null
				|| view.getContext().isRestricted()) {
			return view;
		}
		if (view instanceof TextView) {
			TextView text = (TextView) view;
			Typeface typeface = text.getTypeface();
			if (typeface == null) {
				text.setTypeface(loadTypeface(view.getContext(),
						HoloFont.ROBOTO_REGULAR.font));
				return view;
			}
			HoloFont font;
			boolean isBold = typeface.isBold(), isItalic = typeface.isItalic();
			if (isBold && isItalic) {
				font = HoloFont.ROBOTO_BOLD_ITALIC;
			} else if (isBold && !isItalic) {
				font = HoloFont.ROBOTO_BOLD;
			} else if (!isBold && isItalic) {
				font = HoloFont.ROBOTO_ITALIC;
			} else {
				font = HoloFont.ROBOTO_REGULAR;
			}
			if (!font.ignore) {
				typeface = loadTypeface(view.getContext(), font.font);
				if (typeface != null) {
					text.setTypeface(typeface);
				}
			}
		}
		if (view instanceof ViewGroup) {
			ViewGroup group = (ViewGroup) view;
			for (int i = 0; i < group.getChildCount(); i++) {
				applyDefaultStyles(group.getChildAt(i));
			}
		}
		return view;
	}

	/**
	 * Apply.
	 *
	 * @param view the view
	 * @param typeface the typeface
	 * @return the view
	 */
	public static View apply(View view, Typeface typeface) {
		if (view == null || view.getContext() == null
				|| view.getContext().isRestricted()) {
			return view;
		}
		if (typeface == null) {
			Log.v(TAG, "Font is null");
			return view;
		}
		if (view instanceof TextView) {
			((TextView) view).setTypeface(typeface);
		}
		if (view instanceof ViewGroup) {
			ViewGroup group = (ViewGroup) view;
			for (int i = 0; i < group.getChildCount(); i++) {
				apply(group.getChildAt(i), typeface);
			}
		}
		return view;
	}

	/**
	 * Inflate.
	 *
	 * @param context the context
	 * @param res the res
	 * @return the view
	 */
	public static View inflate(Context context, int res) {
		return apply(LayoutInflater.inflate(context, res));
	}

	/**
	 * Inflate.
	 *
	 * @param context the context
	 * @param res the res
	 * @param parent the parent
	 * @return the view
	 */
	public static View inflate(Context context, int res, ViewGroup parent) {
		return apply(LayoutInflater.inflate(context, res, parent));
	}

	/**
	 * Instantiates a new font loader.
	 */
	private FontLoader() {
	}
}
