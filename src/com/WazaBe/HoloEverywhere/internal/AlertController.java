package com.WazaBe.HoloEverywhere.internal;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.WazaBe.HoloEverywhere.ArrayAdapter;
import com.WazaBe.HoloEverywhere.LayoutInflater;
import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class AlertController.
 */
public class AlertController {
	
	/**
	 * The Class AlertParams.
	 */
	public static class AlertParams {
		
		/**
		 * The listener interface for receiving onPrepareListView events.
		 * The class that is interested in processing a onPrepareListView
		 * event implements this interface, and the object created
		 * with that class is registered with a component using the
		 * component's <code>addOnPrepareListViewListener<code> method. When
		 * the onPrepareListView event occurs, that object's appropriate
		 * method is invoked.
		 *
		 * @see OnPrepareListViewEvent
		 */
		public interface OnPrepareListViewListener {
			
			/**
			 * On prepare list view.
			 *
			 * @param listView the list view
			 */
			void onPrepareListView(ListView listView);
		}

		/** The m adapter. */
		public ListAdapter mAdapter;
		
		/** The m cancelable. */
		public boolean mCancelable;
		
		/** The m checked item. */
		public int mCheckedItem = -1;
		
		/** The m checked items. */
		public boolean[] mCheckedItems;
		
		/** The m context. */
		public final Context mContext;
		
		/** The m cursor. */
		public Cursor mCursor;
		
		/** The m custom title view. */
		public View mCustomTitleView;
		
		/** The m force inverse background. */
		public boolean mForceInverseBackground;
		
		/** The m icon. */
		public Drawable mIcon;
		
		/** The m icon id. */
		public int mIconId = 0;
		
		/** The m inflater. */
		public final LayoutInflater mInflater;
		
		/** The m is checked column. */
		public String mIsCheckedColumn;
		
		/** The m is multi choice. */
		public boolean mIsMultiChoice;
		
		/** The m is single choice. */
		public boolean mIsSingleChoice;
		
		/** The m items. */
		public CharSequence[] mItems;
		
		/** The m label column. */
		public String mLabelColumn;
		
		/** The m message. */
		public CharSequence mMessage;
		
		/** The m negative button listener. */
		public DialogInterface.OnClickListener mNegativeButtonListener;
		
		/** The m negative button text. */
		public CharSequence mNegativeButtonText;
		
		/** The m neutral button listener. */
		public DialogInterface.OnClickListener mNeutralButtonListener;
		
		/** The m neutral button text. */
		public CharSequence mNeutralButtonText;
		
		/** The m on cancel listener. */
		public DialogInterface.OnCancelListener mOnCancelListener;
		
		/** The m on checkbox click listener. */
		public DialogInterface.OnMultiChoiceClickListener mOnCheckboxClickListener;
		
		/** The m on click listener. */
		public DialogInterface.OnClickListener mOnClickListener;
		
		/** The m on item selected listener. */
		public AdapterView.OnItemSelectedListener mOnItemSelectedListener;
		
		/** The m on key listener. */
		public DialogInterface.OnKeyListener mOnKeyListener;
		
		/** The m on prepare list view listener. */
		public OnPrepareListViewListener mOnPrepareListViewListener;
		
		/** The m positive button listener. */
		public DialogInterface.OnClickListener mPositiveButtonListener;
		
		/** The m positive button text. */
		public CharSequence mPositiveButtonText;
		
		/** The m recycle on measure. */
		public boolean mRecycleOnMeasure = true;
		
		/** The m theme. */
		public int mTheme;
		
		/** The m title. */
		public CharSequence mTitle;
		
		/** The m view. */
		public View mView;
		
		/** The m view spacing bottom. */
		public int mViewSpacingBottom;
		
		/** The m view spacing left. */
		public int mViewSpacingLeft;
		
		/** The m view spacing right. */
		public int mViewSpacingRight;
		
		/** The m view spacing specified. */
		public boolean mViewSpacingSpecified = false;
		
		/** The m view spacing top. */
		public int mViewSpacingTop;

		/**
		 * Instantiates a new alert params.
		 *
		 * @param context the context
		 */
		public AlertParams(Context context) {
			mContext = context;
			mCancelable = true;
			mInflater = LayoutInflater.from(context);
		}

		/**
		 * Apply.
		 *
		 * @param dialog the dialog
		 */
		public void apply(AlertController dialog) {
			if (mCustomTitleView != null) {
				dialog.setCustomTitle(mCustomTitleView);
			} else {
				if (mTitle != null) {
					dialog.setTitle(mTitle);
				}
				if (mIcon != null) {
					dialog.setIcon(mIcon);
				}
				if (mIconId >= 0) {
					dialog.setIcon(mIconId);
				}
			}
			if (mMessage != null) {
				dialog.setMessage(mMessage);
			}
			if (mPositiveButtonText != null) {
				dialog.setButton(DialogInterface.BUTTON_POSITIVE,
						mPositiveButtonText, mPositiveButtonListener, null);
			}
			if (mNegativeButtonText != null) {
				dialog.setButton(DialogInterface.BUTTON_NEGATIVE,
						mNegativeButtonText, mNegativeButtonListener, null);
			}
			if (mNeutralButtonText != null) {
				dialog.setButton(DialogInterface.BUTTON_NEUTRAL,
						mNeutralButtonText, mNeutralButtonListener, null);
			}
			if (mForceInverseBackground) {
				dialog.setInverseBackgroundForced(true);
			}
			if (mItems != null || mCursor != null || mAdapter != null) {
				createListView(dialog);
			}
			if (mView != null) {
				if (mViewSpacingSpecified) {
					dialog.setView(mView, mViewSpacingLeft, mViewSpacingTop,
							mViewSpacingRight, mViewSpacingBottom);
				} else {
					dialog.setView(mView);
				}
			}
		}

		/**
		 * Creates the list view.
		 *
		 * @param dialog the dialog
		 */
		private void createListView(final AlertController dialog) {
			final RecycleListView listView = (RecycleListView) mInflater
					.inflate(dialog.mListLayout, null);
			ListAdapter adapter;
			if (mIsMultiChoice) {
				if (mCursor == null) {
					adapter = new ArrayAdapter<CharSequence>(mContext,
							dialog.mMultiChoiceItemLayout, R.id.text1, mItems) {
						@Override
						public View getView(int position, View convertView,
								ViewGroup parent) {
							View view = super.getView(position, convertView,
									parent);
							if (mCheckedItems != null) {
								boolean isItemChecked = mCheckedItems[position];
								if (isItemChecked) {
									listView.setItemChecked(position, true);
								}
							}
							return view;
						}
					};
				} else {
					adapter = new CursorAdapter(mContext, mCursor, false) {
						private final int mIsCheckedIndex;
						private final int mLabelIndex;

						{
							final Cursor cursor = getCursor();
							mLabelIndex = cursor
									.getColumnIndexOrThrow(mLabelColumn);
							mIsCheckedIndex = cursor
									.getColumnIndexOrThrow(mIsCheckedColumn);
						}

						@Override
						public void bindView(View view, Context context,
								Cursor cursor) {
							CheckedTextView text = (CheckedTextView) view
									.findViewById(R.id.text1);
							text.setText(cursor.getString(mLabelIndex));
							listView.setItemChecked(cursor.getPosition(),
									cursor.getInt(mIsCheckedIndex) == 1);
						}

						@Override
						public View newView(Context context, Cursor cursor,
								ViewGroup parent) {
							return mInflater.inflate(
									dialog.mMultiChoiceItemLayout, parent,
									false);
						}

					};
				}
			} else {
				int layout = mIsSingleChoice ? dialog.mSingleChoiceItemLayout
						: dialog.mListItemLayout;
				if (mCursor == null) {
					adapter = mAdapter != null ? mAdapter
							: new ArrayAdapter<CharSequence>(mContext, layout,
									R.id.text1, mItems);
				} else {
					adapter = new SimpleCursorAdapter(mContext, layout,
							mCursor, new String[] { mLabelColumn },
							new int[] { R.id.text1 },
							CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
				}
			}

			if (mOnPrepareListViewListener != null) {
				mOnPrepareListViewListener.onPrepareListView(listView);
			}

			dialog.mAdapter = adapter;
			dialog.mCheckedItem = mCheckedItem;

			if (mOnClickListener != null) {
				listView.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View v,
							int position, long id) {
						mOnClickListener.onClick(dialog.mDialogInterface,
								position);
						if (!mIsSingleChoice) {
							dialog.mDialogInterface.dismiss();
						}
					}
				});
			} else if (mOnCheckboxClickListener != null) {
				listView.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View v,
							int position, long id) {
						if (mCheckedItems != null) {
							mCheckedItems[position] = listView
									.isItemChecked(position);
						}
						mOnCheckboxClickListener.onClick(
								dialog.mDialogInterface, position,
								listView.isItemChecked(position));
					}
				});
			}
			if (mOnItemSelectedListener != null) {
				listView.setOnItemSelectedListener(mOnItemSelectedListener);
			}
			if (mIsSingleChoice) {
				listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
			} else if (mIsMultiChoice) {
				listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
			}
			listView.mRecycleOnMeasure = mRecycleOnMeasure;
			dialog.mListView = listView;
		}
	}

	/**
	 * The Class ButtonHandler.
	 */
	private static final class ButtonHandler extends Handler {
		
		/** The Constant MSG_DISMISS_DIALOG. */
		private static final int MSG_DISMISS_DIALOG = 1;
		
		/** The m dialog. */
		private WeakReference<DialogInterface> mDialog;

		/**
		 * Instantiates a new button handler.
		 *
		 * @param dialog the dialog
		 */
		public ButtonHandler(DialogInterface dialog) {
			mDialog = new WeakReference<DialogInterface>(dialog);
		}

		/**
		 * Handle message.
		 *
		 * @param msg the msg
		 * @see android.os.Handler#handleMessage(android.os.Message)
		 */
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case DialogInterface.BUTTON_POSITIVE:
			case DialogInterface.BUTTON_NEGATIVE:
			case DialogInterface.BUTTON_NEUTRAL:
				((DialogInterface.OnClickListener) msg.obj).onClick(
						mDialog.get(), msg.what);
				break;

			case MSG_DISMISS_DIALOG:
				((DialogInterface) msg.obj).dismiss();
			}
		}
	}

	/**
	 * The Class RecycleListView.
	 */
	public static class RecycleListView extends ListView {
		
		/** The m recycle on measure. */
		boolean mRecycleOnMeasure = true;

		/**
		 * Instantiates a new recycle list view.
		 *
		 * @param context the context
		 */
		public RecycleListView(Context context) {
			super(context);
		}

		/**
		 * Instantiates a new recycle list view.
		 *
		 * @param context the context
		 * @param attrs the attrs
		 */
		public RecycleListView(Context context, AttributeSet attrs) {
			super(context, attrs);
		}

		/**
		 * Instantiates a new recycle list view.
		 *
		 * @param context the context
		 * @param attrs the attrs
		 * @param defStyle the def style
		 */
		public RecycleListView(Context context, AttributeSet attrs, int defStyle) {
			super(context, attrs, defStyle);
		}

		/**
		 * Recycle on measure.
		 *
		 * @return true, if successful
		 */
		public boolean recycleOnMeasure() {
			return mRecycleOnMeasure;
		}
	}

	/**
	 * Can text input.
	 *
	 * @param v the v
	 * @return true, if successful
	 */
	static boolean canTextInput(View v) {
		if (v.onCheckIsTextEditor()) {
			return true;
		}
		if (!(v instanceof ViewGroup)) {
			return false;
		}
		ViewGroup vg = (ViewGroup) v;
		int i = vg.getChildCount();
		while (i > 0) {
			i--;
			v = vg.getChildAt(i);
			if (canTextInput(v)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Should center single button.
	 *
	 * @param context the context
	 * @return true, if successful
	 */
	private static boolean shouldCenterSingleButton(Context context) {
		TypedValue outValue = new TypedValue();
		context.getTheme().resolveAttribute(R.attr.alertDialogCenterButtons,
				outValue, true);
		return outValue.data != 0;
	}

	/** The m adapter. */
	private ListAdapter mAdapter;
	
	/** The m alert dialog layout. */
	private int mAlertDialogLayout;
	
	/** The m button handler. */
	View.OnClickListener mButtonHandler = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Message m = null;
			if (v == mButtonPositive && mButtonPositiveMessage != null) {
				m = Message.obtain(mButtonPositiveMessage);
			} else if (v == mButtonNegative && mButtonNegativeMessage != null) {
				m = Message.obtain(mButtonNegativeMessage);
			} else if (v == mButtonNeutral && mButtonNeutralMessage != null) {
				m = Message.obtain(mButtonNeutralMessage);
			}
			if (m != null) {
				m.sendToTarget();
			}
			mHandler.obtainMessage(ButtonHandler.MSG_DISMISS_DIALOG,
					mDialogInterface).sendToTarget();
		}
	};
	
	/** The m button negative. */
	private Button mButtonNegative;
	
	/** The m button negative message. */
	private Message mButtonNegativeMessage;
	
	/** The m button negative text. */
	private CharSequence mButtonNegativeText;
	
	/** The m button neutral. */
	private Button mButtonNeutral;
	
	/** The m button neutral message. */
	private Message mButtonNeutralMessage;
	
	/** The m button neutral text. */
	private CharSequence mButtonNeutralText;
	
	/** The m button positive. */
	private Button mButtonPositive;
	
	/** The m button positive message. */
	private Message mButtonPositiveMessage;
	
	/** The m button positive text. */
	private CharSequence mButtonPositiveText;
	
	/** The m checked item. */
	private int mCheckedItem = -1;
	
	/** The m context. */
	private final Context mContext;
	
	/** The m custom title view. */
	private View mCustomTitleView;
	
	/** The m dialog interface. */
	private final DialogInterface mDialogInterface;
	
	/** The m force inverse background. */
	private boolean mForceInverseBackground;
	
	/** The m handler. */
	private Handler mHandler;
	
	/** The m icon. */
	private Drawable mIcon;
	
	/** The m icon id. */
	private int mIconId = -1;
	
	/** The m icon view. */
	private ImageView mIconView;
	
	/** The m list item layout. */
	private int mListItemLayout;
	
	/** The m list layout. */
	private int mListLayout;
	
	/** The m list view. */
	private ListView mListView;
	
	/** The m message. */
	private CharSequence mMessage;
	
	/** The m message view. */
	private TextView mMessageView;
	
	/** The m multi choice item layout. */
	private int mMultiChoiceItemLayout;
	
	/** The m scroll view. */
	private ScrollView mScrollView;
	
	/** The m single choice item layout. */
	private int mSingleChoiceItemLayout;
	
	/** The m title. */
	private CharSequence mTitle;
	
	/** The m title view. */
	private TextView mTitleView;
	
	/** The m view. */
	private View mView;

	/** The m view spacing bottom. */
	private int mViewSpacingBottom;

	/** The m view spacing left. */
	private int mViewSpacingLeft;

	/** The m view spacing right. */
	private int mViewSpacingRight;

	/** The m view spacing specified. */
	private boolean mViewSpacingSpecified = false;

	/** The m view spacing top. */
	private int mViewSpacingTop;

	/** The m window. */
	private final Window mWindow;

	/**
	 * Instantiates a new alert controller.
	 *
	 * @param context the context
	 * @param di the di
	 * @param window the window
	 */
	public AlertController(Context context, DialogInterface di, Window window) {
		mContext = context;
		mDialogInterface = di;
		mWindow = window;
		mHandler = new ButtonHandler(di);
		TypedArray a = context.obtainStyledAttributes(null,
				R.styleable.AlertDialog, R.attr.alertDialogStyle,
				R.style.Holo_AlertDialog);
		mAlertDialogLayout = a.getResourceId(R.styleable.AlertDialog_layout,
				R.layout.alert_dialog_holo);
		mListLayout = a.getResourceId(R.styleable.AlertDialog_listLayout,
				R.layout.select_dialog_holo);
		mMultiChoiceItemLayout = a.getResourceId(
				R.styleable.AlertDialog_multiChoiceItemLayout,
				R.layout.select_dialog_multichoice_holo);
		mSingleChoiceItemLayout = a.getResourceId(
				R.styleable.AlertDialog_singleChoiceItemLayout,
				R.layout.select_dialog_singlechoice_holo);
		mListItemLayout = a.getResourceId(
				R.styleable.AlertDialog_listItemLayout,
				R.layout.select_dialog_item_holo);
		a.recycle();
	}

	/**
	 * Center button.
	 *
	 * @param button the button
	 */
	private void centerButton(Button button) {
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) button
				.getLayoutParams();
		params.gravity = Gravity.CENTER_HORIZONTAL;
		params.weight = 0.5f;
		button.setLayoutParams(params);
		View leftSpacer = mWindow.findViewById(R.id.leftSpacer);
		if (leftSpacer != null) {
			leftSpacer.setVisibility(View.VISIBLE);
		}
		View rightSpacer = mWindow.findViewById(R.id.rightSpacer);
		if (rightSpacer != null) {
			rightSpacer.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * Gets the button.
	 *
	 * @param whichButton the which button
	 * @return the button
	 */
	public Button getButton(int whichButton) {
		switch (whichButton) {
		case DialogInterface.BUTTON_POSITIVE:
			return mButtonPositive;
		case DialogInterface.BUTTON_NEGATIVE:
			return mButtonNegative;
		case DialogInterface.BUTTON_NEUTRAL:
			return mButtonNeutral;
		default:
			return null;
		}
	}

	/**
	 * Gets the list view.
	 *
	 * @return the list view
	 */
	public ListView getListView() {
		return mListView;
	}

	/**
	 * Install content.
	 */
	public void installContent() {
		mWindow.requestFeature(Window.FEATURE_NO_TITLE);
		if (mView == null || !canTextInput(mView)) {
			mWindow.setFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM,
					WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		}
		mWindow.setContentView(mAlertDialogLayout);
		setupView();
	}

	/**
	 * On key down.
	 *
	 * @param keyCode the key code
	 * @param event the event
	 * @return true, if successful
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return mScrollView != null && mScrollView.executeKeyEvent(event);
	}

	/**
	 * On key up.
	 *
	 * @param keyCode the key code
	 * @param event the event
	 * @return true, if successful
	 */
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		return mScrollView != null && mScrollView.executeKeyEvent(event);
	}

	/**
	 * Sets the background.
	 *
	 * @param topPanel the top panel
	 * @param contentPanel the content panel
	 * @param customPanel the custom panel
	 * @param hasButtons the has buttons
	 * @param a the a
	 * @param hasTitle the has title
	 * @param buttonPanel the button panel
	 */
	private void setBackground(LinearLayout topPanel,
			LinearLayout contentPanel, View customPanel, boolean hasButtons,
			TypedArray a, boolean hasTitle, View buttonPanel) {
		int fullDark = a.getResourceId(R.styleable.AlertDialog_fullDark,
				R.drawable.dialog_full_holo_dark);
		int topDark = a.getResourceId(R.styleable.AlertDialog_topDark,
				R.drawable.dialog_top_holo_dark);
		int centerDark = a.getResourceId(R.styleable.AlertDialog_centerDark,
				R.drawable.dialog_middle_holo_dark);
		int bottomDark = a.getResourceId(R.styleable.AlertDialog_bottomDark,
				R.drawable.dialog_bottom_holo_dark);
		int fullBright = a.getResourceId(R.styleable.AlertDialog_fullBright,
				R.drawable.dialog_full_holo_dark);
		int topBright = a.getResourceId(R.styleable.AlertDialog_topBright,
				R.drawable.dialog_top_holo_dark);
		int centerBright = a.getResourceId(
				R.styleable.AlertDialog_centerBright,
				R.drawable.dialog_middle_holo_dark);
		int bottomBright = a.getResourceId(
				R.styleable.AlertDialog_bottomBright,
				R.drawable.dialog_bottom_holo_dark);
		int bottomMedium = a.getResourceId(
				R.styleable.AlertDialog_bottomMedium,
				R.drawable.dialog_bottom_holo_dark);
		View[] views = new View[4];
		boolean[] light = new boolean[4];
		View lastView = null;
		boolean lastLight = false;

		int pos = 0;
		if (hasTitle) {
			views[pos] = topPanel;
			light[pos] = false;
			pos++;
		}
		views[pos] = contentPanel.getVisibility() == View.GONE ? null
				: contentPanel;
		light[pos] = mListView != null;
		pos++;
		if (customPanel != null) {
			views[pos] = customPanel;
			light[pos] = mForceInverseBackground;
			pos++;
		}
		if (hasButtons) {
			views[pos] = buttonPanel;
			light[pos] = true;
		}
		boolean setView = false;
		for (pos = 0; pos < views.length; pos++) {
			View v = views[pos];
			if (v == null) {
				continue;
			}
			if (lastView != null) {
				if (!setView) {
					lastView.setBackgroundResource(lastLight ? topBright
							: topDark);
				} else {
					lastView.setBackgroundResource(lastLight ? centerBright
							: centerDark);
				}
				setView = true;
			}
			lastView = v;
			lastLight = light[pos];
		}

		if (lastView != null) {
			if (setView) {
				lastView.setBackgroundResource(lastLight ? hasButtons ? bottomMedium
						: bottomBright
						: bottomDark);
			} else {
				lastView.setBackgroundResource(lastLight ? fullBright
						: fullDark);
			}
		}

		if (mListView != null && mAdapter != null) {
			mListView.setAdapter(mAdapter);
			if (mCheckedItem > -1) {
				mListView.setItemChecked(mCheckedItem, true);
				mListView.setSelection(mCheckedItem);
			}
		}
	}

	/**
	 * Sets the button.
	 *
	 * @param whichButton the which button
	 * @param text the text
	 * @param listener the listener
	 * @param msg the msg
	 */
	public void setButton(int whichButton, CharSequence text,
			DialogInterface.OnClickListener listener, Message msg) {
		if (msg == null && listener != null) {
			msg = mHandler.obtainMessage(whichButton, listener);
		}
		switch (whichButton) {
		case DialogInterface.BUTTON_POSITIVE:
			mButtonPositiveText = text;
			mButtonPositiveMessage = msg;
			break;
		case DialogInterface.BUTTON_NEGATIVE:
			mButtonNegativeText = text;
			mButtonNegativeMessage = msg;
			break;
		case DialogInterface.BUTTON_NEUTRAL:
			mButtonNeutralText = text;
			mButtonNeutralMessage = msg;
			break;
		default:
			throw new IllegalArgumentException("Button does not exist");
		}
	}

	/**
	 * Sets the custom title.
	 *
	 * @param customTitleView the new custom title
	 */
	public void setCustomTitle(View customTitleView) {
		mCustomTitleView = customTitleView;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(Drawable icon) {
		mIcon = icon;
		if (mIconView != null && mIcon != null) {
			mIconView.setImageDrawable(icon);
		}
	}

	/**
	 * Sets the icon.
	 *
	 * @param resId the new icon
	 */
	public void setIcon(int resId) {
		mIconId = resId;
		if (mIconView != null) {
			if (resId > 0) {
				mIconView.setImageResource(mIconId);
			} else if (resId == 0) {
				mIconView.setVisibility(View.GONE);
			}
		}
	}

	/**
	 * Sets the inverse background forced.
	 *
	 * @param forceInverseBackground the new inverse background forced
	 */
	public void setInverseBackgroundForced(boolean forceInverseBackground) {
		mForceInverseBackground = forceInverseBackground;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(CharSequence message) {
		mMessage = message;
		if (mMessageView != null) {
			mMessageView.setText(message);
		}
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(CharSequence title) {
		mTitle = title;
		if (mTitleView != null) {
			mTitleView.setText(title);
		}
	}

	/**
	 * Setup buttons.
	 *
	 * @return true, if successful
	 */
	private boolean setupButtons() {
		int BIT_BUTTON_POSITIVE = 1;
		int BIT_BUTTON_NEGATIVE = 2;
		int BIT_BUTTON_NEUTRAL = 4;
		int whichButtons = 0;
		mButtonPositive = (Button) mWindow.findViewById(R.id.button1);
		mButtonPositive.setOnClickListener(mButtonHandler);
		if (TextUtils.isEmpty(mButtonPositiveText)) {
			mButtonPositive.setVisibility(View.GONE);
		} else {
			mButtonPositive.setText(mButtonPositiveText);
			mButtonPositive.setVisibility(View.VISIBLE);
			whichButtons = whichButtons | BIT_BUTTON_POSITIVE;
		}
		mButtonNegative = (Button) mWindow.findViewById(R.id.button2);
		mButtonNegative.setOnClickListener(mButtonHandler);
		if (TextUtils.isEmpty(mButtonNegativeText)) {
			mButtonNegative.setVisibility(View.GONE);
		} else {
			mButtonNegative.setText(mButtonNegativeText);
			mButtonNegative.setVisibility(View.VISIBLE);

			whichButtons = whichButtons | BIT_BUTTON_NEGATIVE;
		}
		mButtonNeutral = (Button) mWindow.findViewById(R.id.button3);
		mButtonNeutral.setOnClickListener(mButtonHandler);
		if (TextUtils.isEmpty(mButtonNeutralText)) {
			mButtonNeutral.setVisibility(View.GONE);
		} else {
			mButtonNeutral.setText(mButtonNeutralText);
			mButtonNeutral.setVisibility(View.VISIBLE);

			whichButtons = whichButtons | BIT_BUTTON_NEUTRAL;
		}
		if (shouldCenterSingleButton(mContext)) {
			if (whichButtons == BIT_BUTTON_POSITIVE) {
				centerButton(mButtonPositive);
			} else if (whichButtons == BIT_BUTTON_NEGATIVE) {
				centerButton(mButtonNeutral);
			} else if (whichButtons == BIT_BUTTON_NEUTRAL) {
				centerButton(mButtonNeutral);
			}
		}
		return whichButtons != 0;
	}

	/**
	 * Sets the up content.
	 *
	 * @param contentPanel the new up content
	 */
	private void setupContent(LinearLayout contentPanel) {
		mScrollView = (ScrollView) mWindow.findViewById(R.id.scrollView);
		mScrollView.setFocusable(false);
		mMessageView = (TextView) mWindow.findViewById(R.id.message);
		if (mMessageView == null) {
			return;
		}
		if (mMessage != null) {
			mMessageView.setText(mMessage);
		} else {
			mMessageView.setVisibility(View.GONE);
			mScrollView.removeView(mMessageView);
			if (mListView != null) {
				contentPanel.removeView(mWindow.findViewById(R.id.scrollView));
				contentPanel.addView(mListView, new LinearLayout.LayoutParams(
						MATCH_PARENT, MATCH_PARENT));
				contentPanel.setLayoutParams(new LinearLayout.LayoutParams(
						MATCH_PARENT, 0, 1.0f));
			} else {
				contentPanel.setVisibility(View.GONE);
			}
		}
	}

	/**
	 * Setup title.
	 *
	 * @param topPanel the top panel
	 * @return true, if successful
	 */
	private boolean setupTitle(LinearLayout topPanel) {
		boolean hasTitle = true;
		if (mCustomTitleView != null) {
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);

			topPanel.addView(mCustomTitleView, 0, lp);
			View titleTemplate = mWindow.findViewById(R.id.title_template);
			titleTemplate.setVisibility(View.GONE);
		} else {
			final boolean hasTextTitle = !TextUtils.isEmpty(mTitle);
			mIconView = (ImageView) mWindow.findViewById(R.id.icon);
			if (hasTextTitle) {
				mTitleView = (TextView) mWindow.findViewById(R.id.alertTitle);
				mTitleView.setText(mTitle);
				if (mIconId > 0) {
					mIconView.setImageResource(mIconId);
				} else if (mIcon != null) {
					mIconView.setImageDrawable(mIcon);
				} else if (mIconId == 0) {
					mTitleView.setPadding(mIconView.getPaddingLeft(),
							mIconView.getPaddingTop(),
							mIconView.getPaddingRight(),
							mIconView.getPaddingBottom());
					mIconView.setVisibility(View.GONE);
				}
			} else {
				View titleTemplate = mWindow.findViewById(R.id.title_template);
				titleTemplate.setVisibility(View.GONE);
				mIconView.setVisibility(View.GONE);
				topPanel.setVisibility(View.GONE);
				hasTitle = false;
			}
		}
		return hasTitle;
	}

	/**
	 * Setup view.
	 */
	private void setupView() {
		LinearLayout contentPanel = (LinearLayout) mWindow
				.findViewById(R.id.contentPanel);
		setupContent(contentPanel);
		boolean hasButtons = setupButtons();
		LinearLayout topPanel = (LinearLayout) mWindow
				.findViewById(R.id.topPanel);
		TypedArray a = mContext.obtainStyledAttributes(null,
				R.styleable.AlertDialog, R.attr.alertDialogStyle,
				R.style.Holo_AlertDialog);
		boolean hasTitle = setupTitle(topPanel);
		View buttonPanel = mWindow.findViewById(R.id.buttonPanel);
		if (!hasButtons) {
			buttonPanel.setVisibility(View.GONE);
			// mWindow.setCloseOnTouchOutsideIfNotSet(true);
		}
		FrameLayout customPanel = null;
		if (mView != null) {
			customPanel = (FrameLayout) mWindow.findViewById(R.id.customPanel);
			FrameLayout custom = (FrameLayout) mWindow
					.findViewById(R.id.custom);
			custom.addView(mView, new LayoutParams(MATCH_PARENT, MATCH_PARENT));
			if (mViewSpacingSpecified) {
				custom.setPadding(mViewSpacingLeft, mViewSpacingTop,
						mViewSpacingRight, mViewSpacingBottom);
			}
			if (mListView != null) {
				((LinearLayout.LayoutParams) customPanel.getLayoutParams()).weight = 0;
			}
		} else {
			mWindow.findViewById(R.id.customPanel).setVisibility(View.GONE);
		}

		if (hasTitle) {
			View divider = null;
			if (mMessage != null || mView != null || mListView != null) {
				divider = mWindow.findViewById(R.id.titleDivider);
			} else {
				divider = mWindow.findViewById(R.id.titleDividerTop);
			}

			if (divider != null) {
				divider.setVisibility(View.VISIBLE);
			}
		}
		setBackground(topPanel, contentPanel, customPanel, hasButtons, a,
				hasTitle, buttonPanel);
		a.recycle();
	}

	/**
	 * Sets the view.
	 *
	 * @param view the new view
	 */
	public void setView(View view) {
		mView = view;
		mViewSpacingSpecified = false;
	}

	/**
	 * Sets the view.
	 *
	 * @param view the view
	 * @param viewSpacingLeft the view spacing left
	 * @param viewSpacingTop the view spacing top
	 * @param viewSpacingRight the view spacing right
	 * @param viewSpacingBottom the view spacing bottom
	 */
	public void setView(View view, int viewSpacingLeft, int viewSpacingTop,
			int viewSpacingRight, int viewSpacingBottom) {
		mView = view;
		mViewSpacingSpecified = true;
		mViewSpacingLeft = viewSpacingLeft;
		mViewSpacingTop = viewSpacingTop;
		mViewSpacingRight = viewSpacingRight;
		mViewSpacingBottom = viewSpacingBottom;
	}

}
