package com.WazaBe.HoloEverywhere.app;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Message;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import org.bitbucket.fredgrott.gwslaf.R;
import com.WazaBe.HoloEverywhere.internal.AlertController;

// TODO: Auto-generated Javadoc
/**
 * The Class AlertDialog.
 */
public class AlertDialog extends Dialog implements DialogInterface {
	
	/**
	 * The Class Builder.
	 */
	public static class Builder {
		
		/** The p. */
		private final AlertController.AlertParams P;

		/**
		 * Instantiates a new builder.
		 *
		 * @param context the context
		 */
		public Builder(Context context) {
			this(context, resolveDialogTheme(context, 0));
		}

		/**
		 * Instantiates a new builder.
		 *
		 * @param context the context
		 * @param theme the theme
		 */
		public Builder(Context context, int theme) {
			P = new AlertController.AlertParams(new ContextThemeWrapper(
					context, resolveDialogTheme(context, theme)));
			P.mTheme = theme;
		}

		/**
		 * Creates the.
		 *
		 * @return the alert dialog
		 */
		public AlertDialog create() {
			final AlertDialog dialog = new AlertDialog(P.mContext, P.mTheme);
			P.apply(dialog.mAlert);
			dialog.setCancelable(P.mCancelable);
			if (P.mCancelable) {
				dialog.setCanceledOnTouchOutside(true);
			}
			dialog.setOnCancelListener(P.mOnCancelListener);
			if (P.mOnKeyListener != null) {
				dialog.setOnKeyListener(P.mOnKeyListener);
			}
			return dialog;
		}

		/**
		 * Gets the context.
		 *
		 * @return the context
		 */
		public Context getContext() {
			return P.mContext;
		}

		/**
		 * Sets the adapter.
		 *
		 * @param adapter the adapter
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setAdapter(final ListAdapter adapter,
				final OnClickListener listener) {
			P.mAdapter = adapter;
			P.mOnClickListener = listener;
			return this;
		}

		/**
		 * Sets the cancelable.
		 *
		 * @param cancelable the cancelable
		 * @return the builder
		 */
		public Builder setCancelable(boolean cancelable) {
			P.mCancelable = cancelable;
			return this;
		}

		/**
		 * Sets the cursor.
		 *
		 * @param cursor the cursor
		 * @param listener the listener
		 * @param labelColumn the label column
		 * @return the builder
		 */
		public Builder setCursor(final Cursor cursor,
				final OnClickListener listener, String labelColumn) {
			P.mCursor = cursor;
			P.mLabelColumn = labelColumn;
			P.mOnClickListener = listener;
			return this;
		}

		/**
		 * Sets the custom title.
		 *
		 * @param customTitleView the custom title view
		 * @return the builder
		 */
		public Builder setCustomTitle(View customTitleView) {
			P.mCustomTitleView = customTitleView;
			return this;
		}

		/**
		 * Sets the icon.
		 *
		 * @param icon the icon
		 * @return the builder
		 */
		public Builder setIcon(Drawable icon) {
			P.mIcon = icon;
			return this;
		}

		/**
		 * Sets the icon.
		 *
		 * @param iconId the icon id
		 * @return the builder
		 */
		public Builder setIcon(int iconId) {
			P.mIconId = iconId;
			return this;
		}

		/**
		 * Sets the icon attribute.
		 *
		 * @param attrId the attr id
		 * @return the builder
		 */
		public Builder setIconAttribute(int attrId) {
			TypedValue out = new TypedValue();
			P.mContext.getTheme().resolveAttribute(attrId, out, true);
			P.mIconId = out.resourceId;
			return this;
		}

		/**
		 * Sets the inverse background forced.
		 *
		 * @param useInverseBackground the use inverse background
		 * @return the builder
		 */
		public Builder setInverseBackgroundForced(boolean useInverseBackground) {
			P.mForceInverseBackground = useInverseBackground;
			return this;
		}

		/**
		 * Sets the items.
		 *
		 * @param items the items
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setItems(CharSequence[] items,
				final OnClickListener listener) {
			P.mItems = items;
			P.mOnClickListener = listener;
			return this;
		}

		/**
		 * Sets the items.
		 *
		 * @param itemsId the items id
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setItems(int itemsId, final OnClickListener listener) {
			P.mItems = P.mContext.getResources().getTextArray(itemsId);
			P.mOnClickListener = listener;
			return this;
		}

		/**
		 * Sets the message.
		 *
		 * @param message the message
		 * @return the builder
		 */
		public Builder setMessage(CharSequence message) {
			P.mMessage = message;
			return this;
		}

		/**
		 * Sets the message.
		 *
		 * @param messageId the message id
		 * @return the builder
		 */
		public Builder setMessage(int messageId) {
			P.mMessage = P.mContext.getText(messageId);
			return this;
		}

		/**
		 * Sets the multi choice items.
		 *
		 * @param items the items
		 * @param checkedItems the checked items
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setMultiChoiceItems(CharSequence[] items,
				boolean[] checkedItems,
				final OnMultiChoiceClickListener listener) {
			P.mItems = items;
			P.mOnCheckboxClickListener = listener;
			P.mCheckedItems = checkedItems;
			P.mIsMultiChoice = true;
			return this;
		}

		/**
		 * Sets the multi choice items.
		 *
		 * @param cursor the cursor
		 * @param isCheckedColumn the is checked column
		 * @param labelColumn the label column
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setMultiChoiceItems(Cursor cursor,
				String isCheckedColumn, String labelColumn,
				final OnMultiChoiceClickListener listener) {
			P.mCursor = cursor;
			P.mOnCheckboxClickListener = listener;
			P.mIsCheckedColumn = isCheckedColumn;
			P.mLabelColumn = labelColumn;
			P.mIsMultiChoice = true;
			return this;
		}

		/**
		 * Sets the multi choice items.
		 *
		 * @param itemsId the items id
		 * @param checkedItems the checked items
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setMultiChoiceItems(int itemsId, boolean[] checkedItems,
				final OnMultiChoiceClickListener listener) {
			P.mItems = P.mContext.getResources().getTextArray(itemsId);
			P.mOnCheckboxClickListener = listener;
			P.mCheckedItems = checkedItems;
			P.mIsMultiChoice = true;
			return this;
		}

		/**
		 * Sets the negative button.
		 *
		 * @param text the text
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setNegativeButton(CharSequence text,
				final OnClickListener listener) {
			P.mNegativeButtonText = text;
			P.mNegativeButtonListener = listener;
			return this;
		}

		/**
		 * Sets the negative button.
		 *
		 * @param textId the text id
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setNegativeButton(int textId,
				final OnClickListener listener) {
			P.mNegativeButtonText = P.mContext.getText(textId);
			P.mNegativeButtonListener = listener;
			return this;
		}

		/**
		 * Sets the neutral button.
		 *
		 * @param text the text
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setNeutralButton(CharSequence text,
				final OnClickListener listener) {
			P.mNeutralButtonText = text;
			P.mNeutralButtonListener = listener;
			return this;
		}

		/**
		 * Sets the neutral button.
		 *
		 * @param textId the text id
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setNeutralButton(int textId,
				final OnClickListener listener) {
			P.mNeutralButtonText = P.mContext.getText(textId);
			P.mNeutralButtonListener = listener;
			return this;
		}

		/**
		 * Sets the on cancel listener.
		 *
		 * @param onCancelListener the on cancel listener
		 * @return the builder
		 */
		public Builder setOnCancelListener(OnCancelListener onCancelListener) {
			P.mOnCancelListener = onCancelListener;
			return this;
		}

		/**
		 * Sets the on item selected listener.
		 *
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setOnItemSelectedListener(
				final AdapterView.OnItemSelectedListener listener) {
			P.mOnItemSelectedListener = listener;
			return this;
		}

		/**
		 * Sets the on key listener.
		 *
		 * @param onKeyListener the on key listener
		 * @return the builder
		 */
		public Builder setOnKeyListener(OnKeyListener onKeyListener) {
			P.mOnKeyListener = onKeyListener;
			return this;
		}

		/**
		 * Sets the positive button.
		 *
		 * @param text the text
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setPositiveButton(CharSequence text,
				final OnClickListener listener) {
			P.mPositiveButtonText = text;
			P.mPositiveButtonListener = listener;
			return this;
		}

		/**
		 * Sets the positive button.
		 *
		 * @param textId the text id
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setPositiveButton(int textId,
				final OnClickListener listener) {
			P.mPositiveButtonText = P.mContext.getText(textId);
			P.mPositiveButtonListener = listener;
			return this;
		}

		/**
		 * Sets the recycle on measure enabled.
		 *
		 * @param enabled the enabled
		 * @return the builder
		 */
		public Builder setRecycleOnMeasureEnabled(boolean enabled) {
			P.mRecycleOnMeasure = enabled;
			return this;
		}

		/**
		 * Sets the single choice items.
		 *
		 * @param items the items
		 * @param checkedItem the checked item
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setSingleChoiceItems(CharSequence[] items,
				int checkedItem, final OnClickListener listener) {
			P.mItems = items;
			P.mOnClickListener = listener;
			P.mCheckedItem = checkedItem;
			P.mIsSingleChoice = true;
			return this;
		}

		/**
		 * Sets the single choice items.
		 *
		 * @param cursor the cursor
		 * @param checkedItem the checked item
		 * @param labelColumn the label column
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setSingleChoiceItems(Cursor cursor, int checkedItem,
				String labelColumn, final OnClickListener listener) {
			P.mCursor = cursor;
			P.mOnClickListener = listener;
			P.mCheckedItem = checkedItem;
			P.mLabelColumn = labelColumn;
			P.mIsSingleChoice = true;
			return this;
		}

		/**
		 * Sets the single choice items.
		 *
		 * @param itemsId the items id
		 * @param checkedItem the checked item
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setSingleChoiceItems(int itemsId, int checkedItem,
				final OnClickListener listener) {
			P.mItems = P.mContext.getResources().getTextArray(itemsId);
			P.mOnClickListener = listener;
			P.mCheckedItem = checkedItem;
			P.mIsSingleChoice = true;
			return this;
		}

		/**
		 * Sets the single choice items.
		 *
		 * @param adapter the adapter
		 * @param checkedItem the checked item
		 * @param listener the listener
		 * @return the builder
		 */
		public Builder setSingleChoiceItems(ListAdapter adapter,
				int checkedItem, final OnClickListener listener) {
			P.mAdapter = adapter;
			P.mOnClickListener = listener;
			P.mCheckedItem = checkedItem;
			P.mIsSingleChoice = true;
			return this;
		}

		/**
		 * Sets the title.
		 *
		 * @param title the title
		 * @return the builder
		 */
		public Builder setTitle(CharSequence title) {
			P.mTitle = title;
			return this;
		}

		/**
		 * Sets the title.
		 *
		 * @param titleId the title id
		 * @return the builder
		 */
		public Builder setTitle(int titleId) {
			P.mTitle = P.mContext.getText(titleId);
			return this;
		}

		/**
		 * Sets the view.
		 *
		 * @param view the view
		 * @return the builder
		 */
		public Builder setView(View view) {
			P.mView = view;
			P.mViewSpacingSpecified = false;
			return this;
		}

		/**
		 * Sets the view.
		 *
		 * @param view the view
		 * @param viewSpacingLeft the view spacing left
		 * @param viewSpacingTop the view spacing top
		 * @param viewSpacingRight the view spacing right
		 * @param viewSpacingBottom the view spacing bottom
		 * @return the builder
		 */
		public Builder setView(View view, int viewSpacingLeft,
				int viewSpacingTop, int viewSpacingRight, int viewSpacingBottom) {
			P.mView = view;
			P.mViewSpacingSpecified = true;
			P.mViewSpacingLeft = viewSpacingLeft;
			P.mViewSpacingTop = viewSpacingTop;
			P.mViewSpacingRight = viewSpacingRight;
			P.mViewSpacingBottom = viewSpacingBottom;
			return this;
		}

		/**
		 * Show.
		 *
		 * @return the alert dialog
		 */
		public AlertDialog show() {
			AlertDialog dialog = create();
			dialog.show();
			return dialog;
		}
	}

	/** The Constant THEME_HOLO_DARK. */
	public static final int THEME_HOLO_DARK = 1;
	
	/** The Constant THEME_HOLO_LIGHT. */
	public static final int THEME_HOLO_LIGHT = 2;

	/**
	 * Resolve dialog theme.
	 *
	 * @param context the context
	 * @param resid the resid
	 * @return the int
	 */
	static int resolveDialogTheme(Context context, int resid) {
		if (resid == THEME_HOLO_DARK) {
			return R.style.Holo_Theme_Dialog_Alert;
		} else if (resid == THEME_HOLO_LIGHT) {
			return R.style.Holo_Theme_Dialog_Alert_Light;
		} else if (resid >= 0x01000000) {
			return resid;
		} else {
			TypedValue outValue = new TypedValue();
			context.getTheme().resolveAttribute(R.attr.alertDialogTheme,
					outValue, true);
			return outValue.resourceId;
		}
	}

	/** The m alert. */
	private final AlertController mAlert;

	/**
	 * Instantiates a new alert dialog.
	 *
	 * @param context the context
	 */
	protected AlertDialog(Context context) {
		this(context, false, null, resolveDialogTheme(context, 0));
	}

	/**
	 * Instantiates a new alert dialog.
	 *
	 * @param context the context
	 * @param cancelable the cancelable
	 * @param cancelListener the cancel listener
	 */
	protected AlertDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		this(context, cancelable, cancelListener,
				resolveDialogTheme(context, 0));
	}

	/**
	 * Instantiates a new alert dialog.
	 *
	 * @param context the context
	 * @param cancelable the cancelable
	 * @param cancelListener the cancel listener
	 * @param theme the theme
	 */
	protected AlertDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener, int theme) {
		super(context, resolveDialogTheme(context, theme));
		setCancelable(cancelable);
		setOnCancelListener(cancelListener);
		mAlert = new AlertController(context, this, getWindow());
	}

	/**
	 * Instantiates a new alert dialog.
	 *
	 * @param context the context
	 * @param theme the theme
	 */
	protected AlertDialog(Context context, int theme) {
		this(context, false, null, theme);
	}

	/**
	 * Gets the button.
	 *
	 * @param whichButton the which button
	 * @return the button
	 */
	public Button getButton(int whichButton) {
		return mAlert.getButton(whichButton);
	}

	/**
	 * Gets the list view.
	 *
	 * @return the list view
	 */
	public ListView getListView() {
		return mAlert.getListView();
	}

	/* (non-Javadoc)
	 * @see android.app.Dialog#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAlert.installContent();
	}

	/* (non-Javadoc)
	 * @see android.app.Dialog#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (mAlert.onKeyDown(keyCode, event)) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * On key up.
	 *
	 * @param keyCode the key code
	 * @param event the event
	 * @return true, if successful
	 * @see android.app.Dialog#onKeyUp(int, android.view.KeyEvent)
	 */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (mAlert.onKeyUp(keyCode, event)) {
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

	/**
	 * Sets the button.
	 *
	 * @param text the text
	 * @param msg the msg
	 */
	@Deprecated
	public void setButton(CharSequence text, Message msg) {
		setButton(BUTTON_POSITIVE, text, msg);
	}

	/**
	 * Sets the button.
	 *
	 * @param text the text
	 * @param listener the listener
	 */
	@Deprecated
	public void setButton(CharSequence text, final OnClickListener listener) {
		setButton(BUTTON_POSITIVE, text, listener);
	}

	/**
	 * Sets the button.
	 *
	 * @param whichButton the which button
	 * @param text the text
	 * @param msg the msg
	 */
	public void setButton(int whichButton, CharSequence text, Message msg) {
		mAlert.setButton(whichButton, text, null, msg);
	}

	/**
	 * Sets the button.
	 *
	 * @param whichButton the which button
	 * @param text the text
	 * @param listener the listener
	 */
	public void setButton(int whichButton, CharSequence text,
			OnClickListener listener) {
		mAlert.setButton(whichButton, text, listener, null);
	}

	/**
	 * Sets the button2.
	 *
	 * @param text the text
	 * @param msg the msg
	 */
	@Deprecated
	public void setButton2(CharSequence text, Message msg) {
		setButton(BUTTON_NEGATIVE, text, msg);
	}

	/**
	 * Sets the button2.
	 *
	 * @param text the text
	 * @param listener the listener
	 */
	@Deprecated
	public void setButton2(CharSequence text, final OnClickListener listener) {
		setButton(BUTTON_NEGATIVE, text, listener);
	}

	/**
	 * Sets the button3.
	 *
	 * @param text the text
	 * @param msg the msg
	 */
	@Deprecated
	public void setButton3(CharSequence text, Message msg) {
		setButton(BUTTON_NEUTRAL, text, msg);
	}

	/**
	 * Sets the button3.
	 *
	 * @param text the text
	 * @param listener the listener
	 */
	@Deprecated
	public void setButton3(CharSequence text, final OnClickListener listener) {
		setButton(BUTTON_NEUTRAL, text, listener);
	}

	/**
	 * Sets the custom title.
	 *
	 * @param customTitleView the new custom title
	 */
	public void setCustomTitle(View customTitleView) {
		mAlert.setCustomTitle(customTitleView);
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(Drawable icon) {
		mAlert.setIcon(icon);
	}

	/**
	 * Sets the icon.
	 *
	 * @param resId the new icon
	 */
	public void setIcon(int resId) {
		mAlert.setIcon(resId);
	}

	/**
	 * Sets the icon attribute.
	 *
	 * @param attrId the new icon attribute
	 */
	public void setIconAttribute(int attrId) {
		TypedValue out = new TypedValue();
		getContext().getTheme().resolveAttribute(attrId, out, true);
		mAlert.setIcon(out.resourceId);
	}

	/**
	 * Sets the inverse background forced.
	 *
	 * @param forceInverseBackground the new inverse background forced
	 */
	public void setInverseBackgroundForced(boolean forceInverseBackground) {
		mAlert.setInverseBackgroundForced(forceInverseBackground);
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(CharSequence message) {
		mAlert.setMessage(message);
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 * @see android.app.Dialog#setTitle(java.lang.CharSequence)
	 */
	@Override
	public void setTitle(CharSequence title) {
		super.setTitle(title);
		mAlert.setTitle(title);
	}

	/**
	 * Sets the view.
	 *
	 * @param view the new view
	 */
	public void setView(View view) {
		mAlert.setView(view);
	}

	/**
	 * Sets the view.
	 *
	 * @param view the view
	 * @param viewSpacingLeft the view spacing left
	 * @param viewSpacingTop the view spacing top
	 * @param viewSpacingRight the view spacing right
	 * @param viewSpacingBottom the view spacing bottom
	 */
	public void setView(View view, int viewSpacingLeft, int viewSpacingTop,
			int viewSpacingRight, int viewSpacingBottom) {
		mAlert.setView(view, viewSpacingLeft, viewSpacingTop, viewSpacingRight,
				viewSpacingBottom);
	}
}