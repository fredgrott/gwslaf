package com.WazaBe.HoloEverywhere.app;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class ListActivity.
 */
public abstract class ListActivity extends Activity {
	
	/** The m adapter. */
	protected ListAdapter mAdapter;
	
	/** The m finished start. */
	private boolean mFinishedStart = false;

	/** The m handler. */
	private Handler mHandler = new Handler();
	
	/** The m list. */
	protected ListView mList;

	/** The m on click listener. */
	private AdapterView.OnItemClickListener mOnClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position,
				long id) {
			onListItemClick((ListView) parent, v, position, id);
		}
	};

	/** The m request focus. */
	private Runnable mRequestFocus = new Runnable() {
		@Override
		public void run() {
			mList.focusableViewAvailable(mList);
		}
	};

	/**
	 * Ensure list.
	 */
	private void ensureList() {
		if (mList != null) {
			return;
		}
		setContentView(R.layout.list_content);
	}

	/**
	 * Gets the list adapter.
	 *
	 * @return the list adapter
	 */
	public ListAdapter getListAdapter() {
		return mAdapter;
	}

	/**
	 * Gets the list view.
	 *
	 * @return the list view
	 */
	public ListView getListView() {
		ensureList();
		return mList;
	}

	/**
	 * Gets the selected item id.
	 *
	 * @return the selected item id
	 */
	public long getSelectedItemId() {
		return mList.getSelectedItemId();
	}

	/**
	 * Gets the selected item position.
	 *
	 * @return the selected item position
	 */
	public int getSelectedItemPosition() {
		return mList.getSelectedItemPosition();
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onContentChanged()
	 */
	@Override
	public void onContentChanged() {
		super.onContentChanged();
		View emptyView = findViewById(android.R.id.empty);
		mList = (ListView) findViewById(android.R.id.list);
		if (mList == null) {
			throw new RuntimeException(
					"Your content must have a ListView whose id attribute is "
							+ "'android.R.id.list'");
		}
		if (emptyView != null) {
			mList.setEmptyView(emptyView);
		}
		mList.setOnItemClickListener(mOnClickListener);
		if (mFinishedStart) {
			setListAdapter(mAdapter);
		}
		mHandler.post(mRequestFocus);
		mFinishedStart = true;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		mHandler.removeCallbacks(mRequestFocus);
		super.onDestroy();
	}

	/**
	 * On list item click.
	 *
	 * @param l the l
	 * @param v the v
	 * @param position the position
	 * @param id the id
	 */
	protected void onListItemClick(ListView l, View v, int position, long id) {
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onRestoreInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onRestoreInstanceState(Bundle state) {
		ensureList();
		super.onRestoreInstanceState(state);
	}

	/**
	 * Sets the list adapter.
	 *
	 * @param adapter the new list adapter
	 */
	public void setListAdapter(ListAdapter adapter) {
		synchronized (this) {
			ensureList();
			mAdapter = adapter;
			mList.setAdapter(adapter);
		}
	}

	/**
	 * Sets the selection.
	 *
	 * @param position the new selection
	 */
	public void setSelection(int position) {
		mList.setSelection(position);
	}
}