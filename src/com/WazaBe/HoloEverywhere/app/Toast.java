package com.WazaBe.HoloEverywhere.app;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class Toast.
 */
public class Toast extends android.widget.Toast {
	
	/** The Constant TAG. */
	private static final String TAG = "Toast";

	/**
	 * Make text.
	 *
	 * @param context the context
	 * @param s the s
	 * @param duration the duration
	 * @return the toast
	 */
	public static Toast makeText(Context context, CharSequence s, int duration) {
		Toast toast = new Toast(context);
		toast.setDuration(duration);
		TextView view = new TextView(context);
		view.setText(s);
		view.setTextColor(0xFFDADADA);
		view.setGravity(Gravity.CENTER);
		view.setBackgroundResource(R.drawable.toast_frame);
		toast.setView(view);
		return toast;
	}

	/**
	 * Make text.
	 *
	 * @param context the context
	 * @param resId the res id
	 * @param duration the duration
	 * @return the toast
	 */
	public static Toast makeText(Context context, int resId, int duration) {
		return Toast.makeText(context, context.getResources().getString(resId),
				duration);
	}

	/**
	 * Instantiates a new toast.
	 *
	 * @param context the context
	 */
	public Toast(Context context) {
		super(context);
	}

	/* (non-Javadoc)
	 * @see android.widget.Toast#setText(java.lang.CharSequence)
	 */
	@Override
	public void setText(CharSequence s) {
		if (getView() == null) {
			return;
		}
		try {
			((TextView) getView()).setText(s);
		} catch (ClassCastException e) {
			Log.e(TAG, "This Toast was not created with Toast.makeText", e);
		}
	}
}
