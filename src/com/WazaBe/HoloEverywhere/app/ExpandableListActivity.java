package com.WazaBe.HoloEverywhere.app;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class ExpandableListActivity.
 */
public abstract class ExpandableListActivity extends Activity implements
		OnCreateContextMenuListener, ExpandableListView.OnChildClickListener,
		ExpandableListView.OnGroupCollapseListener,
		ExpandableListView.OnGroupExpandListener {
	
	/** The m adapter. */
	ExpandableListAdapter mAdapter;
	
	/** The m finished start. */
	boolean mFinishedStart = false;
	
	/** The m list. */
	ExpandableListView mList;

	/**
	 * Ensure list.
	 */
	private void ensureList() {
		if (mList != null) {
			return;
		}
		setContentView(R.layout.expandable_list_content);
	}

	/**
	 * Gets the expandable list adapter.
	 *
	 * @return the expandable list adapter
	 */
	public ExpandableListAdapter getExpandableListAdapter() {
		return mAdapter;
	}

	/**
	 * Gets the expandable list view.
	 *
	 * @return the expandable list view
	 */
	public ExpandableListView getExpandableListView() {
		ensureList();
		return mList;
	}

	/**
	 * Gets the selected id.
	 *
	 * @return the selected id
	 */
	public long getSelectedId() {
		return mList.getSelectedId();
	}

	/**
	 * Gets the selected position.
	 *
	 * @return the selected position
	 */
	public long getSelectedPosition() {
		return mList.getSelectedPosition();
	}

	/**
	 * On child click.
	 *
	 * @param parent the parent
	 * @param v the v
	 * @param groupPosition the group position
	 * @param childPosition the child position
	 * @param id the id
	 * @return true, if successful
	 * @see android.widget.ExpandableListView.OnChildClickListener#onChildClick(android.widget.ExpandableListView, android.view.View, int, int, long)
	 */
	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		return false;
	}

	/**
	 * On content changed.
	 *
	 * @see android.app.Activity#onContentChanged()
	 */
	@Override
	public void onContentChanged() {
		super.onContentChanged();
		View emptyView = findViewById(R.id.empty);
		mList = (ExpandableListView) findViewById(android.R.id.list);
		if (mList == null) {
			throw new RuntimeException(
					"Your content must have a ExpandableListView whose id attribute is "
							+ "'android.R.id.list'");
		}
		if (emptyView != null) {
			mList.setEmptyView(emptyView);
		}
		mList.setOnChildClickListener(this);
		mList.setOnGroupExpandListener(this);
		mList.setOnGroupCollapseListener(this);

		if (mFinishedStart) {
			setListAdapter(mAdapter);
		}
		mFinishedStart = true;
	}

	/**
	 * On create context menu.
	 *
	 * @param menu the menu
	 * @param v the v
	 * @param menuInfo the menu info
	 * @see android.app.Activity#onCreateContextMenu(android.view.ContextMenu, android.view.View, android.view.ContextMenu.ContextMenuInfo)
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
	}

	/**
	 * On group collapse.
	 *
	 * @param groupPosition the group position
	 * @see android.widget.ExpandableListView.OnGroupCollapseListener#onGroupCollapse(int)
	 */
	@Override
	public void onGroupCollapse(int groupPosition) {
	}

	/**
	 * On group expand.
	 *
	 * @param groupPosition the group position
	 * @see android.widget.ExpandableListView.OnGroupExpandListener#onGroupExpand(int)
	 */
	@Override
	public void onGroupExpand(int groupPosition) {
	}

	/**
	 * On restore instance state.
	 *
	 * @param state the state
	 * @see android.app.Activity#onRestoreInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onRestoreInstanceState(Bundle state) {
		ensureList();
		super.onRestoreInstanceState(state);
	}

	/**
	 * Sets the list adapter.
	 *
	 * @param adapter the new list adapter
	 */
	public void setListAdapter(ExpandableListAdapter adapter) {
		synchronized (this) {
			ensureList();
			mAdapter = adapter;
			mList.setAdapter(adapter);
		}
	}

	/**
	 * Sets the selected child.
	 *
	 * @param groupPosition the group position
	 * @param childPosition the child position
	 * @param shouldExpandGroup the should expand group
	 * @return true, if successful
	 */
	public boolean setSelectedChild(int groupPosition, int childPosition,
			boolean shouldExpandGroup) {
		return mList.setSelectedChild(groupPosition, childPosition,
				shouldExpandGroup);
	}

	/**
	 * Sets the selected group.
	 *
	 * @param groupPosition the new selected group
	 */
	public void setSelectedGroup(int groupPosition) {
		mList.setSelectedGroup(groupPosition);
	}
}
