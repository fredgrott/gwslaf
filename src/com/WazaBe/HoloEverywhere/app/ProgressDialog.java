package com.WazaBe.HoloEverywhere.app;

import java.text.NumberFormat;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;

import com.WazaBe.HoloEverywhere.LayoutInflater;
import org.bitbucket.fredgrott.gwslaf.R;
import com.WazaBe.HoloEverywhere.widget.ProgressBar;

// TODO: Auto-generated Javadoc
/**
 * The Class ProgressDialog.
 */
public class ProgressDialog extends AlertDialog {
	
	/** The Constant STYLE_HORIZONTAL. */
	public static final int STYLE_HORIZONTAL = 1;
	
	/** The Constant STYLE_SPINNER. */
	public static final int STYLE_SPINNER = 0;

	/**
	 * Show.
	 *
	 * @param context the context
	 * @param title the title
	 * @param message the message
	 * @return the progress dialog
	 */
	public static ProgressDialog show(Context context, CharSequence title,
			CharSequence message) {
		return show(context, title, message, false);
	}

	/**
	 * Show.
	 *
	 * @param context the context
	 * @param title the title
	 * @param message the message
	 * @param indeterminate the indeterminate
	 * @return the progress dialog
	 */
	public static ProgressDialog show(Context context, CharSequence title,
			CharSequence message, boolean indeterminate) {
		return show(context, title, message, indeterminate, false, null);
	}

	/**
	 * Show.
	 *
	 * @param context the context
	 * @param title the title
	 * @param message the message
	 * @param indeterminate the indeterminate
	 * @param cancelable the cancelable
	 * @return the progress dialog
	 */
	public static ProgressDialog show(Context context, CharSequence title,
			CharSequence message, boolean indeterminate, boolean cancelable) {
		return show(context, title, message, indeterminate, cancelable, null);
	}

	/**
	 * Show.
	 *
	 * @param context the context
	 * @param title the title
	 * @param message the message
	 * @param indeterminate the indeterminate
	 * @param cancelable the cancelable
	 * @param cancelListener the cancel listener
	 * @return the progress dialog
	 */
	public static ProgressDialog show(Context context, CharSequence title,
			CharSequence message, boolean indeterminate, boolean cancelable,
			OnCancelListener cancelListener) {
		ProgressDialog dialog = new ProgressDialog(context);
		dialog.setTitle(title);
		dialog.setMessage(message);
		dialog.setIndeterminate(indeterminate);
		dialog.setCancelable(cancelable);
		dialog.setOnCancelListener(cancelListener);
		dialog.show();
		return dialog;
	}

	/** The m has started. */
	private boolean mHasStarted;
	
	/** The m increment by. */
	private int mIncrementBy;
	
	/** The m increment secondary by. */
	private int mIncrementSecondaryBy;
	
	/** The m indeterminate. */
	private boolean mIndeterminate;
	
	/** The m indeterminate drawable. */
	private Drawable mIndeterminateDrawable;
	
	/** The m max. */
	private int mMax;
	
	/** The m message. */
	private CharSequence mMessage;
	
	/** The m message view. */
	private TextView mMessageView;
	
	/** The m progress. */
	private ProgressBar mProgress;
	
	/** The m progress drawable. */
	private Drawable mProgressDrawable;
	
	/** The m progress number. */
	private TextView mProgressNumber;
	
	/** The m progress number format. */
	private String mProgressNumberFormat;
	
	/** The m progress percent. */
	private TextView mProgressPercent;
	
	/** The m progress percent format. */
	private NumberFormat mProgressPercentFormat;
	
	/** The m progress style. */
	private int mProgressStyle = STYLE_SPINNER;
	
	/** The m progress val. */
	private int mProgressVal;
	
	/** The m secondary progress val. */
	private int mSecondaryProgressVal;
	
	/** The m view update handler. */
	private Handler mViewUpdateHandler;

	/**
	 * Instantiates a new progress dialog.
	 *
	 * @param context the context
	 */
	public ProgressDialog(Context context) {
		super(context);
		initFormats();
	}

	/**
	 * Instantiates a new progress dialog.
	 *
	 * @param context the context
	 * @param theme the theme
	 */
	public ProgressDialog(Context context, int theme) {
		super(context, theme);
		initFormats();
	}

	/* (non-Javadoc)
	 * @see android.app.Dialog#getLayoutInflater()
	 */
	@Override
	public LayoutInflater getLayoutInflater() {
		return LayoutInflater.from(getContext());
	}

	/**
	 * Gets the max.
	 *
	 * @return the max
	 */
	public int getMax() {
		if (mProgress != null) {
			return mProgress.getMax();
		}
		return mMax;
	}

	/**
	 * Gets the progress.
	 *
	 * @return the progress
	 */
	public int getProgress() {
		if (mProgress != null) {
			return mProgress.getProgress();
		}
		return mProgressVal;
	}

	/**
	 * Gets the secondary progress.
	 *
	 * @return the secondary progress
	 */
	public int getSecondaryProgress() {
		if (mProgress != null) {
			return mProgress.getSecondaryProgress();
		}
		return mSecondaryProgressVal;
	}

	/**
	 * Increment progress by.
	 *
	 * @param diff the diff
	 */
	public void incrementProgressBy(int diff) {
		if (mProgress != null) {
			mProgress.incrementProgressBy(diff);
			onProgressChanged();
		} else {
			mIncrementBy += diff;
		}
	}

	/**
	 * Increment secondary progress by.
	 *
	 * @param diff the diff
	 */
	public void incrementSecondaryProgressBy(int diff) {
		if (mProgress != null) {
			mProgress.incrementSecondaryProgressBy(diff);
			onProgressChanged();
		} else {
			mIncrementSecondaryBy += diff;
		}
	}

	/**
	 * Inits the formats.
	 */
	private void initFormats() {
		mProgressNumberFormat = "%1d/%2d";
		mProgressPercentFormat = NumberFormat.getPercentInstance();
		mProgressPercentFormat.setMaximumFractionDigits(0);
	}

	/**
	 * Checks if is indeterminate.
	 *
	 * @return true, if is indeterminate
	 */
	public boolean isIndeterminate() {
		if (mProgress != null) {
			return mProgress.isIndeterminate();
		}
		return mIndeterminate;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.app.AlertDialog#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		TypedArray a = getContext().obtainStyledAttributes(null,
				R.styleable.AlertDialog, R.attr.alertDialogStyle, 0);
		View view;
		if (mProgressStyle == STYLE_HORIZONTAL) {
			mViewUpdateHandler = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					super.handleMessage(msg);
					int progress = mProgress.getProgress();
					int max = mProgress.getMax();
					if (mProgressNumberFormat != null) {
						String format = mProgressNumberFormat;
						mProgressNumber.setText(String.format(format, progress,
								max));
					} else {
						mProgressNumber.setText("");
					}
					if (mProgressPercentFormat != null) {
						double percent = (double) progress / (double) max;
						SpannableString tmp = new SpannableString(
								mProgressPercentFormat.format(percent));
						tmp.setSpan(new StyleSpan(
								android.graphics.Typeface.BOLD), 0, tmp
								.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
						mProgressPercent.setText(tmp);
					} else {
						mProgressPercent.setText("");
					}
				}
			};
			view = getLayoutInflater().inflate(
					a.getResourceId(
							R.styleable.AlertDialog_horizontalProgressLayout,
							R.layout.alert_dialog_progress_holo));
			mProgress = (ProgressBar) view.findViewById(R.id.progress);
			mProgressNumber = (TextView) view
					.findViewById(R.id.progress_number);
			mProgressPercent = (TextView) view
					.findViewById(R.id.progress_percent);
		} else {
			view = getLayoutInflater().inflate(
					a.getResourceId(R.styleable.AlertDialog_progressLayout,
							R.layout.progress_dialog_holo));
			mProgress = (ProgressBar) view.findViewById(R.id.progress);
			mMessageView = (TextView) view.findViewById(R.id.message);
		}
		setView(view);
		a.recycle();
		if (mMax > 0) {
			setMax(mMax);
		}
		if (mProgressVal > 0) {
			setProgress(mProgressVal);
		}
		if (mSecondaryProgressVal > 0) {
			setSecondaryProgress(mSecondaryProgressVal);
		}
		if (mIncrementBy > 0) {
			incrementProgressBy(mIncrementBy);
		}
		if (mIncrementSecondaryBy > 0) {
			incrementSecondaryProgressBy(mIncrementSecondaryBy);
		}
		if (mProgressDrawable != null) {
			setProgressDrawable(mProgressDrawable);
		}
		if (mIndeterminateDrawable != null) {
			setIndeterminateDrawable(mIndeterminateDrawable);
		}
		if (mMessage != null) {
			setMessage(mMessage);
		}
		setIndeterminate(mIndeterminate);
		onProgressChanged();
		super.onCreate(savedInstanceState);
	}

	/**
	 * On progress changed.
	 */
	private void onProgressChanged() {
		if (mProgressStyle == STYLE_HORIZONTAL) {
			if (mViewUpdateHandler != null
					&& !mViewUpdateHandler.hasMessages(0)) {
				mViewUpdateHandler.sendEmptyMessage(0);
			}
		}
	}

	/* (non-Javadoc)
	 * @see android.app.Dialog#onStart()
	 */
	@Override
	public void onStart() {
		super.onStart();
		mHasStarted = true;
	}

	/* (non-Javadoc)
	 * @see android.app.Dialog#onStop()
	 */
	@Override
	protected void onStop() {
		super.onStop();
		mHasStarted = false;
	}

	/**
	 * Sets the indeterminate.
	 *
	 * @param indeterminate the new indeterminate
	 */
	public void setIndeterminate(boolean indeterminate) {
		if (mProgress != null) {
			mProgress.setIndeterminate(indeterminate);
		} else {
			mIndeterminate = indeterminate;
		}
	}

	/**
	 * Sets the indeterminate drawable.
	 *
	 * @param d the new indeterminate drawable
	 */
	public void setIndeterminateDrawable(Drawable d) {
		if (mProgress != null) {
			mProgress.setIndeterminateDrawable(d);
		} else {
			mIndeterminateDrawable = d;
		}
	}

	/**
	 * Sets the max.
	 *
	 * @param max the new max
	 */
	public void setMax(int max) {
		if (mProgress != null) {
			mProgress.setMax(max);
			onProgressChanged();
		} else {
			mMax = max;
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.app.AlertDialog#setMessage(java.lang.CharSequence)
	 */
	@Override
	public void setMessage(CharSequence message) {
		if (mProgress != null) {
			if (mProgressStyle == STYLE_HORIZONTAL) {
				super.setMessage(message);
			} else {
				mMessageView.setText(message);
			}
		} else {
			mMessage = message;
		}
	}

	/**
	 * Sets the progress.
	 *
	 * @param value the new progress
	 */
	public void setProgress(int value) {
		if (mHasStarted) {
			mProgress.setProgress(value);
			onProgressChanged();
		} else {
			mProgressVal = value;
		}
	}

	/**
	 * Sets the progress drawable.
	 *
	 * @param d the new progress drawable
	 */
	public void setProgressDrawable(Drawable d) {
		if (mProgress != null) {
			mProgress.setProgressDrawable(d);
		} else {
			mProgressDrawable = d;
		}
	}

	/**
	 * Sets the progress number format.
	 *
	 * @param format the new progress number format
	 */
	public void setProgressNumberFormat(String format) {
		mProgressNumberFormat = format;
		onProgressChanged();
	}

	/**
	 * Sets the progress percent format.
	 *
	 * @param format the new progress percent format
	 */
	public void setProgressPercentFormat(NumberFormat format) {
		mProgressPercentFormat = format;
		onProgressChanged();
	}

	/**
	 * Sets the progress style.
	 *
	 * @param style the new progress style
	 */
	public void setProgressStyle(int style) {
		mProgressStyle = style;
	}

	/**
	 * Sets the secondary progress.
	 *
	 * @param secondaryProgress the new secondary progress
	 */
	public void setSecondaryProgress(int secondaryProgress) {
		if (mProgress != null) {
			mProgress.setSecondaryProgress(secondaryProgress);
			onProgressChanged();
		} else {
			mSecondaryProgressVal = secondaryProgress;
		}
	}
}