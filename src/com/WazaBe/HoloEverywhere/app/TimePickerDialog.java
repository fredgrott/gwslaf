package com.WazaBe.HoloEverywhere.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;

import com.WazaBe.HoloEverywhere.LayoutInflater;
import org.bitbucket.fredgrott.gwslaf.R;
import com.WazaBe.HoloEverywhere.widget.TimePicker;

// TODO: Auto-generated Javadoc
/**
 * The Class TimePickerDialog.
 */
public class TimePickerDialog extends AlertDialog implements OnClickListener,
		TimePicker.OnTimeChangedListener {
	
	/**
	 * The listener interface for receiving onTimeSet events.
	 * The class that is interested in processing a onTimeSet
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addOnTimeSetListener<code> method. When
	 * the onTimeSet event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see OnTimeSetEvent
	 */
	public interface OnTimeSetListener {
		
		/**
		 * On time set.
		 *
		 * @param view the view
		 * @param hourOfDay the hour of day
		 * @param minute the minute
		 */
		public void onTimeSet(TimePicker view, int hourOfDay, int minute);
	}

	/** The Constant HOUR. */
	private static final String HOUR = "hour";
	
	/** The Constant IS_24_HOUR. */
	private static final String IS_24_HOUR = "is24hour";
	
	/** The Constant MINUTE. */
	private static final String MINUTE = "minute";

	/** The m callback. */
	private final OnTimeSetListener mCallback;
	
	/** The m initial minute. */
	int mInitialHourOfDay, mInitialMinute;

	/** The m is24 hour view. */
	boolean mIs24HourView;
	
	/** The m time picker. */
	private final TimePicker mTimePicker;

	/**
	 * Instantiates a new time picker dialog.
	 *
	 * @param context the context
	 * @param theme the theme
	 * @param callBack the call back
	 * @param hourOfDay the hour of day
	 * @param minute the minute
	 * @param is24HourView the is24 hour view
	 */
	public TimePickerDialog(Context context, int theme,
			OnTimeSetListener callBack, int hourOfDay, int minute,
			boolean is24HourView) {
		super(context, theme);
		mCallback = callBack;
		mInitialHourOfDay = hourOfDay;
		mInitialMinute = minute;
		mIs24HourView = is24HourView;
		setIcon(0);
		setTitle(R.string.time_picker_dialog_title);
		Context themeContext = getContext();
		setButton(BUTTON_POSITIVE,
				themeContext.getText(R.string.date_time_set), this);
		setButton(BUTTON_NEGATIVE,
				themeContext.getText(android.R.string.cancel),
				(OnClickListener) null);
		View view = LayoutInflater.inflate(themeContext,
				R.layout.time_picker_dialog);
		setView(view);
		mTimePicker = (TimePicker) view.findViewById(R.id.timePicker);
		mTimePicker.setIs24HourView(mIs24HourView);
		mTimePicker.setCurrentHour(mInitialHourOfDay);
		mTimePicker.setCurrentMinute(mInitialMinute);
		mTimePicker.setOnTimeChangedListener(this);
	}

	/**
	 * Instantiates a new time picker dialog.
	 *
	 * @param context the context
	 * @param callBack the call back
	 * @param hourOfDay the hour of day
	 * @param minute the minute
	 * @param is24HourView the is24 hour view
	 */
	public TimePickerDialog(Context context, OnTimeSetListener callBack,
			int hourOfDay, int minute, boolean is24HourView) {
		this(context, 0, callBack, hourOfDay, minute, is24HourView);
	}

	/* (non-Javadoc)
	 * @see android.content.DialogInterface.OnClickListener#onClick(android.content.DialogInterface, int)
	 */
	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (mCallback != null) {
			mTimePicker.clearFocus();
			mCallback.onTimeSet(mTimePicker, mTimePicker.getCurrentHour(),
					mTimePicker.getCurrentMinute());
		}
	}

	/* (non-Javadoc)
	 * @see android.app.Dialog#onRestoreInstanceState(android.os.Bundle)
	 */
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		int hour = savedInstanceState.getInt(HOUR);
		int minute = savedInstanceState.getInt(MINUTE);
		mTimePicker.setIs24HourView(savedInstanceState.getBoolean(IS_24_HOUR));
		mTimePicker.setCurrentHour(hour);
		mTimePicker.setCurrentMinute(minute);
	}

	/* (non-Javadoc)
	 * @see android.app.Dialog#onSaveInstanceState()
	 */
	@Override
	public Bundle onSaveInstanceState() {
		Bundle state = super.onSaveInstanceState();
		state.putInt(HOUR, mTimePicker.getCurrentHour());
		state.putInt(MINUTE, mTimePicker.getCurrentMinute());
		state.putBoolean(IS_24_HOUR, mTimePicker.is24HourView());
		return state;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.widget.TimePicker.OnTimeChangedListener#onTimeChanged(com.WazaBe.HoloEverywhere.widget.TimePicker, int, int)
	 */
	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
	}

	/**
	 * Update time.
	 *
	 * @param hourOfDay the hour of day
	 * @param minutOfHour the minut of hour
	 */
	public void updateTime(int hourOfDay, int minutOfHour) {
		mTimePicker.setCurrentHour(hourOfDay);
		mTimePicker.setCurrentMinute(minutOfHour);
	}
}