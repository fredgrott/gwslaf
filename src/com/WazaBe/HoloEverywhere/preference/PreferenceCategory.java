package com.WazaBe.HoloEverywhere.preference;

import android.content.Context;
import android.util.AttributeSet;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class PreferenceCategory.
 */
public class PreferenceCategory extends PreferenceGroup {
	
	/** The Constant TAG. */
	private static final String TAG = "PreferenceCategory";

	/**
	 * Instantiates a new preference category.
	 *
	 * @param context the context
	 */
	public PreferenceCategory(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new preference category.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public PreferenceCategory(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.preferenceCategoryStyle);
	}

	/**
	 * Instantiates a new preference category.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public PreferenceCategory(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		return false;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.PreferenceGroup#onPrepareAddPreference(com.WazaBe.HoloEverywhere.preference.Preference)
	 */
	@Override
	protected boolean onPrepareAddPreference(Preference preference) {
		if (preference instanceof PreferenceCategory) {
			throw new IllegalArgumentException("Cannot add a " + TAG
					+ " directly to a " + TAG);
		}
		return super.onPrepareAddPreference(preference);
	}
}