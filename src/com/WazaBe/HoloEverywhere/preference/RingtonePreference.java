package com.WazaBe.HoloEverywhere.preference;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.media.RingtoneManager;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class RingtonePreference.
 */
public class RingtonePreference extends Preference implements
		PreferenceManager.OnActivityResultListener {

	/** The m request code. */
	private int mRequestCode;
	
	/** The m ringtone type. */
	private int mRingtoneType;
	
	/** The m show default. */
	private boolean mShowDefault;

	/** The m show silent. */
	private boolean mShowSilent;

	/**
	 * Instantiates a new ringtone preference.
	 *
	 * @param context the context
	 */
	public RingtonePreference(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new ringtone preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public RingtonePreference(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.ringtonePreferenceStyle);
	}

	/**
	 * Instantiates a new ringtone preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public RingtonePreference(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.RingtonePreference, defStyle, 0);
		mRingtoneType = a.getInt(R.styleable.RingtonePreference_ringtoneType,
				RingtoneManager.TYPE_RINGTONE);
		mShowDefault = a.getBoolean(R.styleable.RingtonePreference_showDefault,
				true);
		mShowSilent = a.getBoolean(R.styleable.RingtonePreference_showSilent,
				true);
		a.recycle();
	}

	/**
	 * Gets the ringtone type.
	 *
	 * @return the ringtone type
	 */
	public int getRingtoneType() {
		return mRingtoneType;
	}

	/**
	 * Gets the show default.
	 *
	 * @return the show default
	 */
	public boolean getShowDefault() {
		return mShowDefault;
	}

	/**
	 * Gets the show silent.
	 *
	 * @return the show silent
	 */
	public boolean getShowSilent() {
		return mShowSilent;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.PreferenceManager.OnActivityResultListener#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == mRequestCode) {
			if (data != null) {
				Uri uri = data
						.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
				if (callChangeListener(uri != null ? uri.toString() : "")) {
					onSaveRingtone(uri);
				}
			}
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onAttachedToHierarchy(com.WazaBe.HoloEverywhere.preference.PreferenceManager)
	 */
	@Override
	protected void onAttachedToHierarchy(PreferenceManager preferenceManager) {
		super.onAttachedToHierarchy(preferenceManager);
		preferenceManager.registerOnActivityResultListener(this);
		mRequestCode = preferenceManager.getNextRequestCode();
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onClick()
	 */
	@Override
	protected void onClick() {
		Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
		onPrepareRingtonePickerIntent(intent);
		PreferenceFragment owningFragment = getPreferenceManager()
				.getFragment();
		if (owningFragment != null) {
			owningFragment.startActivityForResult(intent, mRequestCode);
		} else {
			getPreferenceManager().getActivity().startActivityForResult(intent,
					mRequestCode);
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onGetDefaultValue(android.content.res.TypedArray, int)
	 */
	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		return a.getString(index);
	}

	/**
	 * On prepare ringtone picker intent.
	 *
	 * @param ringtonePickerIntent the ringtone picker intent
	 */
	protected void onPrepareRingtonePickerIntent(Intent ringtonePickerIntent) {
		ringtonePickerIntent.putExtra(
				RingtoneManager.EXTRA_RINGTONE_EXISTING_URI,
				onRestoreRingtone());
		ringtonePickerIntent.putExtra(
				RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, mShowDefault);
		if (mShowDefault) {
			ringtonePickerIntent.putExtra(
					RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI,
					RingtoneManager.getDefaultUri(getRingtoneType()));
		}
		ringtonePickerIntent.putExtra(
				RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, mShowSilent);
		ringtonePickerIntent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE,
				mRingtoneType);
		ringtonePickerIntent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE,
				getTitle());
	}

	/**
	 * On restore ringtone.
	 *
	 * @return the uri
	 */
	protected Uri onRestoreRingtone() {
		final String uriString = getPersistedString(null);
		return !TextUtils.isEmpty(uriString) ? Uri.parse(uriString) : null;
	}

	/**
	 * On save ringtone.
	 *
	 * @param ringtoneUri the ringtone uri
	 */
	protected void onSaveRingtone(Uri ringtoneUri) {
		persistString(ringtoneUri != null ? ringtoneUri.toString() : "");
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onSetInitialValue(boolean, java.lang.Object)
	 */
	@Override
	protected void onSetInitialValue(boolean restorePersistedValue,
			Object defaultValueObj) {
		String defaultValue = (String) defaultValueObj;
		if (restorePersistedValue) {
			return;
		}
		if (!TextUtils.isEmpty(defaultValue)) {
			onSaveRingtone(Uri.parse(defaultValue));
		}
	}

	/**
	 * Sets the ringtone type.
	 *
	 * @param type the new ringtone type
	 */
	public void setRingtoneType(int type) {
		mRingtoneType = type;
	}

	/**
	 * Sets the show default.
	 *
	 * @param showDefault the new show default
	 */
	public void setShowDefault(boolean showDefault) {
		mShowDefault = showDefault;
	}

	/**
	 * Sets the show silent.
	 *
	 * @param showSilent the new show silent
	 */
	public void setShowSilent(boolean showSilent) {
		mShowSilent = showSilent;
	}

}