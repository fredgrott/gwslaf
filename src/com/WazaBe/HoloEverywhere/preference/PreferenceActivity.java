package com.WazaBe.HoloEverywhere.preference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.util.Xml;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.WazaBe.HoloEverywhere.ArrayAdapter;
import com.WazaBe.HoloEverywhere.LayoutInflater;
import org.bitbucket.fredgrott.gwslaf.R;
import com.WazaBe.HoloEverywhere.app.FragmentBreadCrumbs;
import com.WazaBe.HoloEverywhere.app.ListActivity;
import com.WazaBe.HoloEverywhere.util.XmlUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class PreferenceActivity.
 */
public abstract class PreferenceActivity extends ListActivity implements
		PreferenceManager.OnPreferenceTreeClickListener,
		PreferenceFragment.OnPreferenceStartFragmentCallback {
	
	/**
	 * The Class Header.
	 */
	public static final class Header implements Parcelable {
		
		/** The Constant CREATOR. */
		public static final Creator<Header> CREATOR = new Creator<Header>() {
			@Override
			public Header createFromParcel(Parcel source) {
				return new Header(source);
			}

			@Override
			public Header[] newArray(int size) {
				return new Header[size];
			}
		};
		
		/** The bread crumb short title. */
		public CharSequence breadCrumbShortTitle;
		
		/** The bread crumb short title res. */
		public int breadCrumbShortTitleRes;
		
		/** The bread crumb title. */
		public CharSequence breadCrumbTitle;
		
		/** The bread crumb title res. */
		public int breadCrumbTitleRes;
		
		/** The extras. */
		public Bundle extras;
		
		/** The fragment. */
		public String fragment;
		
		/** The fragment arguments. */
		public Bundle fragmentArguments;
		
		/** The icon res. */
		public int iconRes;
		
		/** The id. */
		public long id = HEADER_ID_UNDEFINED;
		
		/** The intent. */
		public Intent intent;
		
		/** The summary. */
		public CharSequence summary;
		
		/** The summary res. */
		public int summaryRes;
		
		/** The title. */
		public CharSequence title;

		/** The title res. */
		public int titleRes;

		/**
		 * Instantiates a new header.
		 */
		public Header() {
		}

		/**
		 * Instantiates a new header.
		 *
		 * @param in the in
		 */
		Header(Parcel in) {
			readFromParcel(in);
		}

		/* (non-Javadoc)
		 * @see android.os.Parcelable#describeContents()
		 */
		@Override
		public int describeContents() {
			return 0;
		}

		/**
		 * Gets the bread crumb short title.
		 *
		 * @param res the res
		 * @return the bread crumb short title
		 */
		public CharSequence getBreadCrumbShortTitle(Resources res) {
			if (breadCrumbShortTitleRes != 0) {
				return res.getText(breadCrumbShortTitleRes);
			}
			return breadCrumbShortTitle;
		}

		/**
		 * Gets the bread crumb title.
		 *
		 * @param res the res
		 * @return the bread crumb title
		 */
		public CharSequence getBreadCrumbTitle(Resources res) {
			if (breadCrumbTitleRes != 0) {
				return res.getText(breadCrumbTitleRes);
			}
			return breadCrumbTitle;
		}

		/**
		 * Gets the summary.
		 *
		 * @param res the res
		 * @return the summary
		 */
		public CharSequence getSummary(Resources res) {
			if (summaryRes != 0) {
				return res.getText(summaryRes);
			}
			return summary;
		}

		/**
		 * Gets the title.
		 *
		 * @param res the res
		 * @return the title
		 */
		public CharSequence getTitle(Resources res) {
			if (titleRes != 0) {
				return res.getText(titleRes);
			}
			return title;
		}

		/**
		 * Read from parcel.
		 *
		 * @param in the in
		 */
		public void readFromParcel(Parcel in) {
			id = in.readLong();
			titleRes = in.readInt();
			title = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
			summaryRes = in.readInt();
			summary = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
			breadCrumbTitleRes = in.readInt();
			breadCrumbTitle = TextUtils.CHAR_SEQUENCE_CREATOR
					.createFromParcel(in);
			breadCrumbShortTitleRes = in.readInt();
			breadCrumbShortTitle = TextUtils.CHAR_SEQUENCE_CREATOR
					.createFromParcel(in);
			iconRes = in.readInt();
			fragment = in.readString();
			fragmentArguments = in.readBundle();
			if (in.readInt() != 0) {
				intent = Intent.CREATOR.createFromParcel(in);
			}
			extras = in.readBundle();
		}

		/* (non-Javadoc)
		 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
		 */
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeLong(id);
			dest.writeInt(titleRes);
			TextUtils.writeToParcel(title, dest, flags);
			dest.writeInt(summaryRes);
			TextUtils.writeToParcel(summary, dest, flags);
			dest.writeInt(breadCrumbTitleRes);
			TextUtils.writeToParcel(breadCrumbTitle, dest, flags);
			dest.writeInt(breadCrumbShortTitleRes);
			TextUtils.writeToParcel(breadCrumbShortTitle, dest, flags);
			dest.writeInt(iconRes);
			dest.writeString(fragment);
			dest.writeBundle(fragmentArguments);
			if (intent != null) {
				dest.writeInt(1);
				intent.writeToParcel(dest, flags);
			} else {
				dest.writeInt(0);
			}
			dest.writeBundle(extras);
		}
	}

	/**
	 * The Class HeaderAdapter.
	 */
	private static class HeaderAdapter extends ArrayAdapter<Header> {
		
		/**
		 * The Class HeaderViewHolder.
		 */
		private static class HeaderViewHolder {
			
			/** The icon. */
			ImageView icon;
			
			/** The summary. */
			TextView summary;
			
			/** The title. */
			TextView title;
		}

		/** The m inflater. */
		private LayoutInflater mInflater;

		/**
		 * Instantiates a new header adapter.
		 *
		 * @param context the context
		 * @param objects the objects
		 */
		public HeaderAdapter(Context context, List<Header> objects) {
			super(context, 0, objects);
			mInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			HeaderViewHolder holder;
			View view;
			if (convertView == null) {
				view = mInflater.inflate(R.layout.preference_header_item,
						parent, false);
				holder = new HeaderViewHolder();
				holder.icon = (ImageView) view.findViewById(R.id.icon);
				holder.title = (TextView) view.findViewById(R.id.title);
				holder.summary = (TextView) view.findViewById(R.id.summary);
				view.setTag(holder);
			} else {
				view = convertView;
				holder = (HeaderViewHolder) view.getTag();
			}
			Header header = getItem(position);
			holder.icon.setImageResource(header.iconRes);
			holder.title.setText(header.getTitle(getContext().getResources()));
			CharSequence summary = header.getSummary(getContext()
					.getResources());
			if (!TextUtils.isEmpty(summary)) {
				holder.summary.setVisibility(View.VISIBLE);
				holder.summary.setText(summary);
			} else {
				holder.summary.setVisibility(View.GONE);
			}

			return view;
		}
	}

	/** The Constant BACK_STACK_PREFS. */
	private static final String BACK_STACK_PREFS = ":android:prefs";
	
	/** The Constant CUR_HEADER_TAG. */
	private static final String CUR_HEADER_TAG = ":android:cur_header";
	
	/** The Constant EXTRA_NO_HEADERS. */
	public static final String EXTRA_NO_HEADERS = ":android:no_headers";
	
	/** The Constant EXTRA_PREFS_SET_BACK_TEXT. */
	private static final String EXTRA_PREFS_SET_BACK_TEXT = "extra_prefs_set_back_text";
	
	/** The Constant EXTRA_PREFS_SET_NEXT_TEXT. */
	private static final String EXTRA_PREFS_SET_NEXT_TEXT = "extra_prefs_set_next_text";
	
	/** The Constant EXTRA_PREFS_SHOW_BUTTON_BAR. */
	private static final String EXTRA_PREFS_SHOW_BUTTON_BAR = "extra_prefs_show_button_bar";
	
	/** The Constant EXTRA_PREFS_SHOW_SKIP. */
	private static final String EXTRA_PREFS_SHOW_SKIP = "extra_prefs_show_skip";
	
	/** The Constant EXTRA_SHOW_FRAGMENT. */
	public static final String EXTRA_SHOW_FRAGMENT = ":android:show_fragment";
	
	/** The Constant EXTRA_SHOW_FRAGMENT_ARGUMENTS. */
	public static final String EXTRA_SHOW_FRAGMENT_ARGUMENTS = ":android:show_fragment_args";
	
	/** The Constant EXTRA_SHOW_FRAGMENT_SHORT_TITLE. */
	public static final String EXTRA_SHOW_FRAGMENT_SHORT_TITLE = ":android:show_fragment_short_title";
	
	/** The Constant EXTRA_SHOW_FRAGMENT_TITLE. */
	public static final String EXTRA_SHOW_FRAGMENT_TITLE = ":android:show_fragment_title";
	
	/** The Constant FIRST_REQUEST_CODE. */
	private static final int FIRST_REQUEST_CODE = 100;
	
	/** The Constant HEADER_ID_UNDEFINED. */
	public static final long HEADER_ID_UNDEFINED = -1;
	
	/** The Constant HEADERS_TAG. */
	private static final String HEADERS_TAG = ":android:headers";
	
	/** The Constant MSG_BIND_PREFERENCES. */
	private static final int MSG_BIND_PREFERENCES = 1;
	
	/** The Constant MSG_BUILD_HEADERS. */
	private static final int MSG_BUILD_HEADERS = 2;
	
	/** The Constant PREFERENCES_TAG. */
	private static final String PREFERENCES_TAG = ":android:preferences";
	
	/** The m cur header. */
	private Header mCurHeader;
	
	/** The m fragment bread crumbs. */
	private FragmentBreadCrumbs mFragmentBreadCrumbs;
	
	/** The m handler. */
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_BIND_PREFERENCES: {
				bindPreferences();
			}
				break;
			case MSG_BUILD_HEADERS: {
				ArrayList<Header> oldHeaders = new ArrayList<Header>(mHeaders);
				mHeaders.clear();
				onBuildHeaders(mHeaders);
				if (getListAdapter() instanceof BaseAdapter) {
					((BaseAdapter) getListAdapter()).notifyDataSetChanged();
				}
				Header header = onGetNewHeader();
				if (header != null && header.fragment != null) {
					Header mappedHeader = findBestMatchingHeader(header,
							oldHeaders);
					if (mappedHeader == null || mCurHeader != mappedHeader) {
						switchToHeader(header);
					}
				} else if (mCurHeader != null) {
					Header mappedHeader = findBestMatchingHeader(mCurHeader,
							mHeaders);
					if (mappedHeader != null) {
						setSelectedHeader(mappedHeader);
					}
				}
			}
				break;
			}
		}
	};
	
	/** The m headers. */
	private final ArrayList<Header> mHeaders = new ArrayList<Header>();
	
	/** The m list footer. */
	private FrameLayout mListFooter;
	
	/** The m next button. */
	private Button mNextButton;
	
	/** The m preference manager. */
	private PreferenceManager mPreferenceManager;

	/** The m prefs container. */
	private ViewGroup mPrefsContainer;

	/** The m saved instance state. */
	private Bundle mSavedInstanceState;

	/** The m single pane. */
	private boolean mSinglePane;

	/**
	 * Adds the preferences from intent.
	 *
	 * @param intent the intent
	 */
	@Deprecated
	public void addPreferencesFromIntent(Intent intent) {
		requirePreferenceManager();

		setPreferenceScreen(mPreferenceManager.inflateFromIntent(intent,
				getPreferenceScreen()));
	}

	/**
	 * Adds the preferences from resource.
	 *
	 * @param preferencesResId the preferences res id
	 */
	@Deprecated
	public void addPreferencesFromResource(int preferencesResId) {
		requirePreferenceManager();

		setPreferenceScreen(mPreferenceManager.inflateFromResource(this,
				preferencesResId, getPreferenceScreen()));
	}

	/**
	 * Bind preferences.
	 */
	private void bindPreferences() {
		final PreferenceScreen preferenceScreen = getPreferenceScreen();
		if (preferenceScreen != null) {
			preferenceScreen.bind(getListView());
			if (mSavedInstanceState != null) {
				super.onRestoreInstanceState(mSavedInstanceState);
				mSavedInstanceState = null;
			}
		}
	}

	/**
	 * Find best matching header.
	 *
	 * @param cur the cur
	 * @param from the from
	 * @return the header
	 */
	Header findBestMatchingHeader(Header cur, ArrayList<Header> from) {
		ArrayList<Header> matches = new ArrayList<Header>();
		for (int j = 0; j < from.size(); j++) {
			Header oh = from.get(j);
			if (cur == oh || cur.id != HEADER_ID_UNDEFINED && cur.id == oh.id) {
				matches.clear();
				matches.add(oh);
				break;
			}
			if (cur.fragment != null) {
				if (cur.fragment.equals(oh.fragment)) {
					matches.add(oh);
				}
			} else if (cur.intent != null) {
				if (cur.intent.equals(oh.intent)) {
					matches.add(oh);
				}
			} else if (cur.title != null) {
				if (cur.title.equals(oh.title)) {
					matches.add(oh);
				}
			}
		}
		final int NM = matches.size();
		if (NM == 1) {
			return matches.get(0);
		} else if (NM > 1) {
			for (int j = 0; j < NM; j++) {
				Header oh = matches.get(j);
				if (cur.fragmentArguments != null
						&& cur.fragmentArguments.equals(oh.fragmentArguments)) {
					return oh;
				}
				if (cur.extras != null && cur.extras.equals(oh.extras)) {
					return oh;
				}
				if (cur.title != null && cur.title.equals(oh.title)) {
					return oh;
				}
			}
		}
		return null;
	}

	/**
	 * Find preference.
	 *
	 * @param key the key
	 * @return the preference
	 */
	@Deprecated
	public Preference findPreference(CharSequence key) {
		if (mPreferenceManager == null) {
			return null;
		}
		return mPreferenceManager.findPreference(key);
	}

	/**
	 * Finish preference panel.
	 *
	 * @param caller the caller
	 * @param resultCode the result code
	 * @param resultData the result data
	 */
	public void finishPreferencePanel(Fragment caller, int resultCode,
			Intent resultData) {
		if (mSinglePane) {
			setResult(resultCode, resultData);
			finish();
		} else {
			onSupportBackPressed();
			if (caller != null) {
				if (caller.getTargetFragment() != null) {
					caller.getTargetFragment().onActivityResult(
							caller.getTargetRequestCode(), resultCode,
							resultData);
				}
			}
		}
	}

	/**
	 * Gets the headers.
	 *
	 * @return the headers
	 */
	public List<Header> getHeaders() {
		return mHeaders;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.app.Activity#getLayoutInflater()
	 */
	@Override
	public LayoutInflater getLayoutInflater() {
		return LayoutInflater.from(this);
	}

	/**
	 * Gets the next button.
	 *
	 * @return the next button
	 */
	protected Button getNextButton() {
		return mNextButton;
	}

	/**
	 * Gets the preference manager.
	 *
	 * @return the preference manager
	 */
	@Deprecated
	public PreferenceManager getPreferenceManager() {
		return mPreferenceManager;
	}

	/**
	 * Gets the preference screen.
	 *
	 * @return the preference screen
	 */
	@Deprecated
	public PreferenceScreen getPreferenceScreen() {
		if (mPreferenceManager != null) {
			return mPreferenceManager.getPreferenceScreen();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.app.Activity#getSystemService(java.lang.String)
	 */
	@Override
	public Object getSystemService(String name) {
		return LayoutInflater.getSystemService(super.getSystemService(name));
	}

	/**
	 * Checks for headers.
	 *
	 * @return true, if successful
	 */
	public boolean hasHeaders() {
		return getListView().getVisibility() == View.VISIBLE
				&& mPreferenceManager == null;
	}

	/**
	 * Checks for next button.
	 *
	 * @return true, if successful
	 */
	protected boolean hasNextButton() {
		return mNextButton != null;
	}

	/**
	 * Invalidate headers.
	 */
	public void invalidateHeaders() {
		if (!mHandler.hasMessages(MSG_BUILD_HEADERS)) {
			mHandler.sendEmptyMessage(MSG_BUILD_HEADERS);
		}
	}

	/**
	 * Checks if is multi pane.
	 *
	 * @return true, if is multi pane
	 */
	public boolean isMultiPane() {
		return hasHeaders() && mPrefsContainer.getVisibility() == View.VISIBLE;
	}

	/**
	 * Load headers from resource.
	 *
	 * @param resid the resid
	 * @param target the target
	 */
	public void loadHeadersFromResource(int resid, List<Header> target) {
		XmlResourceParser parser = null;
		try {
			parser = getResources().getXml(resid);
			AttributeSet attrs = Xml.asAttributeSet(parser);

			int type;
			while ((type = parser.next()) != XmlPullParser.END_DOCUMENT
					&& type != XmlPullParser.START_TAG) {
				// Parse next until start tag is found
			}

			String nodeName = parser.getName();
			if (!"preference-headers".equals(nodeName)) {
				throw new RuntimeException(
						"XML document must start with <preference-headers> tag; found"
								+ nodeName + " at "
								+ parser.getPositionDescription());
			}

			Bundle curBundle = null;

			final int outerDepth = parser.getDepth();
			while ((type = parser.next()) != XmlPullParser.END_DOCUMENT
					&& (type != XmlPullParser.END_TAG || parser.getDepth() > outerDepth)) {
				if (type == XmlPullParser.END_TAG || type == XmlPullParser.TEXT) {
					continue;
				}

				nodeName = parser.getName();
				if ("header".equals(nodeName)) {
					Header header = new Header();

					TypedArray sa = getResources().obtainAttributes(attrs,
							R.styleable.PreferenceHeader);
					header.id = sa.getResourceId(
							R.styleable.PreferenceHeader_id,
							(int) HEADER_ID_UNDEFINED);
					TypedValue tv = sa
							.peekValue(R.styleable.PreferenceHeader_title);
					if (tv != null && tv.type == TypedValue.TYPE_STRING) {
						if (tv.resourceId != 0) {
							header.titleRes = tv.resourceId;
						} else {
							header.title = tv.string;
						}
					}
					tv = sa.peekValue(R.styleable.PreferenceHeader_summary);
					if (tv != null && tv.type == TypedValue.TYPE_STRING) {
						if (tv.resourceId != 0) {
							header.summaryRes = tv.resourceId;
						} else {
							header.summary = tv.string;
						}
					}
					tv = sa.peekValue(R.styleable.PreferenceHeader_breadCrumbTitle);
					if (tv != null && tv.type == TypedValue.TYPE_STRING) {
						if (tv.resourceId != 0) {
							header.breadCrumbTitleRes = tv.resourceId;
						} else {
							header.breadCrumbTitle = tv.string;
						}
					}
					tv = sa.peekValue(R.styleable.PreferenceHeader_breadCrumbShortTitle);
					if (tv != null && tv.type == TypedValue.TYPE_STRING) {
						if (tv.resourceId != 0) {
							header.breadCrumbShortTitleRes = tv.resourceId;
						} else {
							header.breadCrumbShortTitle = tv.string;
						}
					}
					header.iconRes = sa.getResourceId(
							R.styleable.PreferenceHeader_icon, 0);
					header.fragment = sa
							.getString(R.styleable.PreferenceHeader_fragment);
					sa.recycle();

					if (curBundle == null) {
						curBundle = new Bundle();
					}

					final int innerDepth = parser.getDepth();
					while ((type = parser.next()) != XmlPullParser.END_DOCUMENT
							&& (type != XmlPullParser.END_TAG || parser
									.getDepth() > innerDepth)) {
						if (type == XmlPullParser.END_TAG
								|| type == XmlPullParser.TEXT) {
							continue;
						}

						String innerNodeName = parser.getName();
						if (innerNodeName.equals("extra")) {
							getResources().parseBundleExtra("extra", attrs,
									curBundle);
							XmlUtils.skipCurrentTag(parser);

						} else if (innerNodeName.equals("intent")) {
							header.intent = Intent.parseIntent(getResources(),
									parser, attrs);

						} else {
							XmlUtils.skipCurrentTag(parser);
						}
					}

					if (curBundle.size() > 0) {
						header.fragmentArguments = curBundle;
						curBundle = null;
					}

					target.add(header);
				} else {
					XmlUtils.skipCurrentTag(parser);
				}
			}

		} catch (XmlPullParserException e) {
			throw new RuntimeException("Error parsing headers", e);
		} catch (IOException e) {
			throw new RuntimeException("Error parsing headers", e);
		} finally {
			if (parser != null) {
				parser.close();
			}
		}

	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (mPreferenceManager != null) {
			mPreferenceManager.dispatchActivityResult(requestCode, resultCode,
					data);
		}
	}

	/**
	 * On build headers.
	 *
	 * @param target the target
	 */
	public void onBuildHeaders(List<Header> target) {
	}

	/**
	 * On build start fragment intent.
	 *
	 * @param fragmentName the fragment name
	 * @param args the args
	 * @param titleRes the title res
	 * @param shortTitleRes the short title res
	 * @return the intent
	 */
	public Intent onBuildStartFragmentIntent(String fragmentName, Bundle args,
			int titleRes, int shortTitleRes) {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.setClass(this, getClass());
		intent.putExtra(EXTRA_SHOW_FRAGMENT, fragmentName);
		intent.putExtra(EXTRA_SHOW_FRAGMENT_ARGUMENTS, args);
		intent.putExtra(EXTRA_SHOW_FRAGMENT_TITLE, titleRes);
		intent.putExtra(EXTRA_SHOW_FRAGMENT_SHORT_TITLE, shortTitleRes);
		intent.putExtra(EXTRA_NO_HEADERS, true);
		return intent;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.app.ListActivity#onContentChanged()
	 */
	@Override
	public void onContentChanged() {
		super.onContentChanged();

		if (mPreferenceManager != null) {
			postBindPreferences();
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.preference_list_content);
		mListFooter = (FrameLayout) findViewById(R.id.list_footer);
		mPrefsContainer = (ViewGroup) findViewById(R.id.prefs_frame);
		boolean hidingHeaders = onIsHidingHeaders();
		mSinglePane = hidingHeaders || !onIsMultiPane();
		String initialFragment = getIntent()
				.getStringExtra(EXTRA_SHOW_FRAGMENT);
		Bundle initialArguments = getIntent().getBundleExtra(
				EXTRA_SHOW_FRAGMENT_ARGUMENTS);
		int initialTitle = getIntent()
				.getIntExtra(EXTRA_SHOW_FRAGMENT_TITLE, 0);
		int initialShortTitle = getIntent().getIntExtra(
				EXTRA_SHOW_FRAGMENT_SHORT_TITLE, 0);
		if (savedInstanceState != null) {
			ArrayList<Header> headers = savedInstanceState
					.getParcelableArrayList(HEADERS_TAG);
			if (headers != null) {
				mHeaders.addAll(headers);
				int curHeader = savedInstanceState.getInt(CUR_HEADER_TAG,
						(int) HEADER_ID_UNDEFINED);
				if (curHeader >= 0 && curHeader < mHeaders.size()) {
					setSelectedHeader(mHeaders.get(curHeader));
				}
			}
		} else {
			if (initialFragment != null && mSinglePane) {
				switchToHeader(initialFragment, initialArguments);
				if (initialTitle != 0) {
					CharSequence initialTitleStr = getText(initialTitle);
					CharSequence initialShortTitleStr = initialShortTitle != 0 ? getText(initialShortTitle)
							: null;
					showBreadCrumbs(initialTitleStr, initialShortTitleStr);
				}
			} else {
				onBuildHeaders(mHeaders);
				if (mHeaders.size() > 0) {
					if (!mSinglePane) {
						if (initialFragment == null) {
							Header h = onGetInitialHeader();
							switchToHeader(h);
						} else {
							switchToHeader(initialFragment, initialArguments);
						}
					}
				}
			}
		}
		if (initialFragment != null && mSinglePane) {
			findViewById(R.id.headers).setVisibility(View.GONE);
			mPrefsContainer.setVisibility(View.VISIBLE);
			if (initialTitle != 0) {
				CharSequence initialTitleStr = getText(initialTitle);
				CharSequence initialShortTitleStr = initialShortTitle != 0 ? getText(initialShortTitle)
						: null;
				showBreadCrumbs(initialTitleStr, initialShortTitleStr);
			}
		} else if (mHeaders.size() > 0) {
			setListAdapter(new HeaderAdapter(this, mHeaders));
			if (!mSinglePane) {
				getListView().setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
				if (mCurHeader != null) {
					setSelectedHeader(mCurHeader);
				}
				mPrefsContainer.setVisibility(View.VISIBLE);
			}
		} else {
			setContentView(R.layout.preference_list_content_single);
			mListFooter = (FrameLayout) findViewById(R.id.list_footer);
			mPrefsContainer = (ViewGroup) findViewById(R.id.prefs);
			mPreferenceManager = new PreferenceManager(this, FIRST_REQUEST_CODE);
			mPreferenceManager.setOnPreferenceTreeClickListener(this);
		}

		Intent intent = getIntent();
		if (intent.getBooleanExtra(EXTRA_PREFS_SHOW_BUTTON_BAR, false)) {

			findViewById(R.id.button_bar).setVisibility(View.VISIBLE);

			Button backButton = (Button) findViewById(R.id.back_button);
			backButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					setResult(RESULT_CANCELED);
					finish();
				}
			});
			Button skipButton = (Button) findViewById(R.id.skip_button);
			skipButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					setResult(RESULT_OK);
					finish();
				}
			});
			mNextButton = (Button) findViewById(R.id.next_button);
			mNextButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					setResult(RESULT_OK);
					finish();
				}
			});

			if (intent.hasExtra(EXTRA_PREFS_SET_NEXT_TEXT)) {
				String buttonText = intent
						.getStringExtra(EXTRA_PREFS_SET_NEXT_TEXT);
				if (TextUtils.isEmpty(buttonText)) {
					mNextButton.setVisibility(View.GONE);
				} else {
					mNextButton.setText(buttonText);
				}
			}
			if (intent.hasExtra(EXTRA_PREFS_SET_BACK_TEXT)) {
				String buttonText = intent
						.getStringExtra(EXTRA_PREFS_SET_BACK_TEXT);
				if (TextUtils.isEmpty(buttonText)) {
					backButton.setVisibility(View.GONE);
				} else {
					backButton.setText(buttonText);
				}
			}
			if (intent.getBooleanExtra(EXTRA_PREFS_SHOW_SKIP, false)) {
				skipButton.setVisibility(View.VISIBLE);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.app.ListActivity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (mPreferenceManager != null) {
			mPreferenceManager.dispatchActivityDestroy();
		}
	}

	/**
	 * On get initial header.
	 *
	 * @return the header
	 */
	public Header onGetInitialHeader() {
		return mHeaders.get(0);
	}

	/**
	 * On get new header.
	 *
	 * @return the header
	 */
	public Header onGetNewHeader() {
		return null;
	}

	/**
	 * On header click.
	 *
	 * @param header the header
	 * @param position the position
	 */
	public void onHeaderClick(Header header, int position) {
		if (header.fragment != null) {
			if (mSinglePane) {
				int titleRes = header.breadCrumbTitleRes;
				int shortTitleRes = header.breadCrumbShortTitleRes;
				if (titleRes == 0) {
					titleRes = header.titleRes;
					shortTitleRes = 0;
				}
				startWithFragment(header.fragment, header.fragmentArguments,
						null, 0, titleRes, shortTitleRes);
			} else {
				switchToHeader(header);
			}
		} else if (header.intent != null) {
			startActivity(header.intent);
		}
	}

	/**
	 * On is hiding headers.
	 *
	 * @return true, if successful
	 */
	public boolean onIsHidingHeaders() {
		return getIntent().getBooleanExtra(EXTRA_NO_HEADERS, false);
	}

	/**
	 * On is multi pane.
	 *
	 * @return true, if successful
	 */
	public boolean onIsMultiPane() {
		boolean preferMultiPane = getResources().getBoolean(
				R.bool.preferences_prefer_dual_pane);
		return preferMultiPane;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.app.ListActivity#onListItemClick(android.widget.ListView, android.view.View, int, long)
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		if (getListAdapter() != null) {
			Object item = getListAdapter().getItem(position);
			if (item instanceof Header) {
				onHeaderClick((Header) item, position);
			}
		}
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onNewIntent(android.content.Intent)
	 */
	@Override
	protected void onNewIntent(Intent intent) {
		if (mPreferenceManager != null) {
			mPreferenceManager.dispatchNewIntent(intent);
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.PreferenceFragment.OnPreferenceStartFragmentCallback#onPreferenceStartFragment(com.WazaBe.HoloEverywhere.preference.PreferenceFragment, com.WazaBe.HoloEverywhere.preference.Preference)
	 */
	@Override
	public boolean onPreferenceStartFragment(PreferenceFragment caller,
			Preference pref) {
		startPreferencePanel(pref.getFragment(), pref.getExtras(),
				pref.getTitleRes(), pref.getTitle(), null, 0);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.PreferenceManager.OnPreferenceTreeClickListener#onPreferenceTreeClick(com.WazaBe.HoloEverywhere.preference.PreferenceScreen, com.WazaBe.HoloEverywhere.preference.Preference)
	 */
	@Override
	@Deprecated
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
			Preference preference) {
		return false;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.app.ListActivity#onRestoreInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onRestoreInstanceState(Bundle state) {
		if (mPreferenceManager != null) {
			Bundle container = state.getBundle(PREFERENCES_TAG);
			if (container != null) {
				final PreferenceScreen preferenceScreen = getPreferenceScreen();
				if (preferenceScreen != null) {
					preferenceScreen.restoreHierarchyState(container);
					mSavedInstanceState = state;
					return;
				}
			}
		}
		super.onRestoreInstanceState(state);
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onSaveInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		if (mHeaders.size() > 0) {
			outState.putParcelableArrayList(HEADERS_TAG, mHeaders);
			if (mCurHeader != null) {
				int index = mHeaders.indexOf(mCurHeader);
				if (index >= 0) {
					outState.putInt(CUR_HEADER_TAG, index);
				}
			}
		}

		if (mPreferenceManager != null) {
			final PreferenceScreen preferenceScreen = getPreferenceScreen();
			if (preferenceScreen != null) {
				Bundle container = new Bundle();
				preferenceScreen.saveHierarchyState(container);
				outState.putBundle(PREFERENCES_TAG, container);
			}
		}
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onStop()
	 */
	@Override
	protected void onStop() {
		super.onStop();

		if (mPreferenceManager != null) {
			mPreferenceManager.dispatchActivityStop();
		}
	}

	/**
	 * Post bind preferences.
	 */
	private void postBindPreferences() {
		if (mHandler.hasMessages(MSG_BIND_PREFERENCES)) {
			return;
		}
		mHandler.obtainMessage(MSG_BIND_PREFERENCES).sendToTarget();
	}

	/**
	 * Require preference manager.
	 */
	private void requirePreferenceManager() {
		if (mPreferenceManager == null) {
			if (mAdapter == null) {
				throw new RuntimeException(
						"This should be called after super.onCreate.");
			}
			throw new RuntimeException(
					"Modern two-pane PreferenceActivity requires use of a PreferenceFragment");
		}
	}

	/**
	 * Set a footer that should be shown at the bottom of the header list.
	 *
	 * @param view the new list footer
	 */
	public void setListFooter(View view) {
		mListFooter.removeAllViews();
		mListFooter.addView(view, new FrameLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
	}

	/**
	 * Sets the parent title.
	 *
	 * @param title the title
	 * @param shortTitle the short title
	 * @param listener the listener
	 */
	public void setParentTitle(CharSequence title, CharSequence shortTitle,
			OnClickListener listener) {
		if (mFragmentBreadCrumbs != null) {
			mFragmentBreadCrumbs.setParentTitle(title, shortTitle, listener);
		}
	}

	/**
	 * Sets the preference screen.
	 *
	 * @param preferenceScreen the new preference screen
	 */
	@Deprecated
	public void setPreferenceScreen(PreferenceScreen preferenceScreen) {
		requirePreferenceManager();

		if (mPreferenceManager.setPreferences(preferenceScreen)
				&& preferenceScreen != null) {
			postBindPreferences();
			CharSequence title = getPreferenceScreen().getTitle();
			if (title != null) {
				setTitle(title);
			}
		}
	}

	/**
	 * Sets the selected header.
	 *
	 * @param header the new selected header
	 */
	void setSelectedHeader(Header header) {
		mCurHeader = header;
		int index = mHeaders.indexOf(header);
		if (index >= 0) {
			getListView().setItemChecked(index, true);
		} else {
			getListView().clearChoices();
		}
		showBreadCrumbs(header);
	}

	/**
	 * Show bread crumbs.
	 *
	 * @param title the title
	 * @param shortTitle the short title
	 */
	public void showBreadCrumbs(CharSequence title, CharSequence shortTitle) {
		if (mFragmentBreadCrumbs == null) {
			View crumbs = findViewById(R.id.title);
			try {
				mFragmentBreadCrumbs = (FragmentBreadCrumbs) crumbs;
			} catch (ClassCastException e) {
				return;
			}
			if (mFragmentBreadCrumbs == null) {
				if (title != null) {
					setTitle(title);
				}
				return;
			}
			mFragmentBreadCrumbs.setMaxVisible(2);
			mFragmentBreadCrumbs.setActivity(this);
		}
		mFragmentBreadCrumbs.setTitle(title, shortTitle);
		mFragmentBreadCrumbs.setParentTitle(null, null, null);
	}

	/**
	 * Show bread crumbs.
	 *
	 * @param header the header
	 */
	void showBreadCrumbs(Header header) {
		if (header != null) {
			CharSequence title = header.getBreadCrumbTitle(getResources());
			if (title == null) {
				title = header.getTitle(getResources());
			}
			if (title == null) {
				title = getTitle();
			}
			showBreadCrumbs(title,
					header.getBreadCrumbShortTitle(getResources()));
		} else {
			showBreadCrumbs(getTitle(), null);
		}
	}

	/**
	 * Start preference fragment.
	 *
	 * @param fragment the fragment
	 * @param push the push
	 */
	public void startPreferenceFragment(Fragment fragment, boolean push) {
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.prefs, fragment);
		if (push) {
			transaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			transaction.addToBackStack(BACK_STACK_PREFS);
		} else {
			transaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		}
		transaction.commitAllowingStateLoss();
	}

	/**
	 * Start preference panel.
	 *
	 * @param fragmentClass the fragment class
	 * @param args the args
	 * @param titleRes the title res
	 * @param titleText the title text
	 * @param resultTo the result to
	 * @param resultRequestCode the result request code
	 */
	public void startPreferencePanel(String fragmentClass, Bundle args,
			int titleRes, CharSequence titleText, Fragment resultTo,
			int resultRequestCode) {
		if (mSinglePane) {
			startWithFragment(fragmentClass, args, resultTo, resultRequestCode,
					titleRes, 0);
		} else {
			Fragment f = Fragment.instantiate(this, fragmentClass, args);
			if (resultTo != null) {
				f.setTargetFragment(resultTo, resultRequestCode);
			}
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			transaction.replace(R.id.prefs, f);
			if (titleRes != 0) {
				transaction.setBreadCrumbTitle(titleRes);
			} else if (titleText != null) {
				transaction.setBreadCrumbTitle(titleText);
			}
			transaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			transaction.addToBackStack(BACK_STACK_PREFS);
			transaction.commitAllowingStateLoss();
		}
	}

	/**
	 * Start with fragment.
	 *
	 * @param fragmentName the fragment name
	 * @param args the args
	 * @param resultTo the result to
	 * @param resultRequestCode the result request code
	 */
	public void startWithFragment(String fragmentName, Bundle args,
			Fragment resultTo, int resultRequestCode) {
		startWithFragment(fragmentName, args, resultTo, resultRequestCode, 0, 0);
	}

	/**
	 * Start with fragment.
	 *
	 * @param fragmentName the fragment name
	 * @param args the args
	 * @param resultTo the result to
	 * @param resultRequestCode the result request code
	 * @param titleRes the title res
	 * @param shortTitleRes the short title res
	 */
	public void startWithFragment(String fragmentName, Bundle args,
			Fragment resultTo, int resultRequestCode, int titleRes,
			int shortTitleRes) {
		Intent intent = onBuildStartFragmentIntent(fragmentName, args,
				titleRes, shortTitleRes);
		if (resultTo == null) {
			startActivity(intent);
		} else {
			resultTo.startActivityForResult(intent, resultRequestCode);
		}
	}

	/**
	 * Switch to header.
	 *
	 * @param header the header
	 */
	public void switchToHeader(Header header) {
		if (mCurHeader == header) {
			getSupportFragmentManager().popBackStack(BACK_STACK_PREFS,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
		} else {
			int direction = mHeaders.indexOf(header)
					- mHeaders.indexOf(mCurHeader);
			switchToHeaderInner(header.fragment, header.fragmentArguments,
					direction);
			setSelectedHeader(header);
		}
	}

	/**
	 * Switch to header.
	 *
	 * @param fragmentName the fragment name
	 * @param args the args
	 */
	public void switchToHeader(String fragmentName, Bundle args) {
		setSelectedHeader(null);
		switchToHeaderInner(fragmentName, args, 0);
	}

	/**
	 * Switch to header inner.
	 *
	 * @param fragmentName the fragment name
	 * @param args the args
	 * @param direction the direction
	 */
	private void switchToHeaderInner(String fragmentName, Bundle args,
			int direction) {
		getSupportFragmentManager().popBackStack(BACK_STACK_PREFS,
				FragmentManager.POP_BACK_STACK_INCLUSIVE);
		Fragment f = Fragment.instantiate(this, fragmentName, args);
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		transaction.replace(R.id.prefs, f);
		transaction.commitAllowingStateLoss();
	}
}