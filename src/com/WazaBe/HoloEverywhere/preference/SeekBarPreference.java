package com.WazaBe.HoloEverywhere.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class SeekBarPreference.
 */
public class SeekBarPreference extends Preference implements
		OnSeekBarChangeListener {
	
	/**
	 * The Class SavedState.
	 */
	private static class SavedState extends BaseSavedState {
		
		/** The max. */
		int max;
		
		/** The progress. */
		int progress;

		/**
		 * Instantiates a new saved state.
		 *
		 * @param source the source
		 */
		public SavedState(Parcel source) {
			super(source);
			progress = source.readInt();
			max = source.readInt();
		}

		/**
		 * Instantiates a new saved state.
		 *
		 * @param superState the super state
		 */
		public SavedState(Parcelable superState) {
			super(superState);
		}

		/* (non-Javadoc)
		 * @see android.view.AbsSavedState#writeToParcel(android.os.Parcel, int)
		 */
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(progress);
			dest.writeInt(max);
		}
	}

	/** The m max. */
	private int mMax;
	
	/** The m progress. */
	private int mProgress;

	/** The m tracking touch. */
	private boolean mTrackingTouch;

	/**
	 * Instantiates a new seek bar preference.
	 *
	 * @param context the context
	 */
	public SeekBarPreference(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new seek bar preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public SeekBarPreference(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.seekBarPreferenceStyle);
	}

	/**
	 * Instantiates a new seek bar preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public SeekBarPreference(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.SeekBarPreference, defStyle, 0);
		setMax(a.getInt(R.styleable.SeekBarPreference_max, mMax));
		a.recycle();
	}

	/**
	 * Gets the progress.
	 *
	 * @return the progress
	 */
	public int getProgress() {
		return mProgress;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#getSummary()
	 */
	@Override
	public CharSequence getSummary() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onBindView(android.view.View)
	 */
	@Override
	protected void onBindView(View view) {
		super.onBindView(view);
		SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekbar);
		seekBar.setOnSeekBarChangeListener(this);
		seekBar.setMax(mMax);
		seekBar.setProgress(mProgress);
		seekBar.setEnabled(isEnabled());
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onGetDefaultValue(android.content.res.TypedArray, int)
	 */
	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		return a.getInt(index, 0);
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onKey(android.view.View, int, android.view.KeyEvent)
	 */
	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (event.getAction() != KeyEvent.ACTION_UP) {
			if (keyCode == KeyEvent.KEYCODE_PLUS
					|| keyCode == KeyEvent.KEYCODE_EQUALS) {
				setProgress(getProgress() + 1);
				return true;
			}
			if (keyCode == KeyEvent.KEYCODE_MINUS) {
				setProgress(getProgress() - 1);
				return true;
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see android.widget.SeekBar.OnSeekBarChangeListener#onProgressChanged(android.widget.SeekBar, int, boolean)
	 */
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		if (fromUser && !mTrackingTouch) {
			syncProgress(seekBar);
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onRestoreInstanceState(android.os.Parcelable)
	 */
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (!state.getClass().equals(SavedState.class)) {
			super.onRestoreInstanceState(state);
			return;
		}
		SavedState myState = (SavedState) state;
		super.onRestoreInstanceState(myState.getSuperState());
		mProgress = myState.progress;
		mMax = myState.max;
		notifyChanged();
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onSaveInstanceState()
	 */
	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState = super.onSaveInstanceState();
		if (isPersistent()) {
			return superState;
		}
		final SavedState myState = new SavedState(superState);
		myState.progress = mProgress;
		myState.max = mMax;
		return myState;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onSetInitialValue(boolean, java.lang.Object)
	 */
	@Override
	protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
		setProgress(restoreValue ? getPersistedInt(mProgress)
				: (Integer) defaultValue);
	}

	/* (non-Javadoc)
	 * @see android.widget.SeekBar.OnSeekBarChangeListener#onStartTrackingTouch(android.widget.SeekBar)
	 */
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		mTrackingTouch = true;
	}

	/* (non-Javadoc)
	 * @see android.widget.SeekBar.OnSeekBarChangeListener#onStopTrackingTouch(android.widget.SeekBar)
	 */
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		mTrackingTouch = false;
		if (seekBar.getProgress() != mProgress) {
			syncProgress(seekBar);
		}
	}

	/**
	 * Sets the max.
	 *
	 * @param max the new max
	 */
	public void setMax(int max) {
		if (max != mMax) {
			mMax = max;
			notifyChanged();
		}
	}

	/**
	 * Sets the progress.
	 *
	 * @param progress the new progress
	 */
	public void setProgress(int progress) {
		setProgress(progress, true);
	}

	/**
	 * Sets the progress.
	 *
	 * @param progress the progress
	 * @param notifyChanged the notify changed
	 */
	private void setProgress(int progress, boolean notifyChanged) {
		if (progress > mMax) {
			progress = mMax;
		}
		if (progress < 0) {
			progress = 0;
		}
		if (progress != mProgress) {
			mProgress = progress;
			persistInt(progress);
			if (notifyChanged) {
				notifyChanged();
			}
		}
	}

	/**
	 * Sync progress.
	 *
	 * @param seekBar the seek bar
	 */
	void syncProgress(SeekBar seekBar) {
		int progress = seekBar.getProgress();
		if (progress != mProgress) {
			if (callChangeListener(progress)) {
				setProgress(progress, false);
			} else {
				seekBar.setProgress(mProgress);
			}
		}
	}
}