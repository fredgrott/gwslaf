package com.WazaBe.HoloEverywhere.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.CompoundButton;

import org.bitbucket.fredgrott.gwslaf.R;
import com.WazaBe.HoloEverywhere.widget.Switch;

// TODO: Auto-generated Javadoc
/**
 * The Class SwitchPreference.
 */
public class SwitchPreference extends TwoStatePreference {
	
	/**
	 * The Class Listener.
	 */
	private class Listener implements CompoundButton.OnCheckedChangeListener {
		
		/* (non-Javadoc)
		 * @see android.widget.CompoundButton.OnCheckedChangeListener#onCheckedChanged(android.widget.CompoundButton, boolean)
		 */
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (!callChangeListener(isChecked)) {
				buttonView.setChecked(!isChecked);
				return;
			}
			setChecked(isChecked);
		}
	}

	/** The m listener. */
	private final Listener mListener = new Listener();
	
	/** The m switch off. */
	private CharSequence mSwitchOff;

	/** The m switch on. */
	private CharSequence mSwitchOn;

	/**
	 * Instantiates a new switch preference.
	 *
	 * @param context the context
	 */
	public SwitchPreference(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new switch preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public SwitchPreference(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.switchPreferenceStyle);
	}

	/**
	 * Instantiates a new switch preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public SwitchPreference(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.SwitchPreference, defStyle, 0);
		setSummaryOn(a.getString(R.styleable.SwitchPreference_summaryOn));
		setSummaryOff(a.getString(R.styleable.SwitchPreference_summaryOff));
		setSwitchTextOn(a.getString(R.styleable.SwitchPreference_switchTextOn));
		setSwitchTextOff(a
				.getString(R.styleable.SwitchPreference_switchTextOff));
		setDisableDependentsState(a.getBoolean(
				R.styleable.SwitchPreference_disableDependentsState, false));
		a.recycle();
	}

	/**
	 * Gets the switch text off.
	 *
	 * @return the switch text off
	 */
	public CharSequence getSwitchTextOff() {
		return mSwitchOff;
	}

	/**
	 * Gets the switch text on.
	 *
	 * @return the switch text on
	 */
	public CharSequence getSwitchTextOn() {
		return mSwitchOn;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onBindView(android.view.View)
	 */
	@Override
	protected void onBindView(View view) {
		super.onBindView(view);
		View checkableView = view.findViewById(R.id.switchWidget);
		if (checkableView != null && checkableView instanceof Checkable) {
			((Checkable) checkableView).setChecked(mChecked);
			sendAccessibilityEvent(checkableView);
			if (checkableView instanceof Switch) {
				final Switch switchView = (Switch) checkableView;
				switchView.setTextOn(mSwitchOn);
				switchView.setTextOff(mSwitchOff);
				switchView.setOnCheckedChangeListener(mListener);
			}
		}
		syncSummaryView(view);
	}

	/**
	 * Sets the switch text off.
	 *
	 * @param offText the new switch text off
	 */
	public void setSwitchTextOff(CharSequence offText) {
		mSwitchOff = offText;
		notifyChanged();
	}

	/**
	 * Sets the switch text off.
	 *
	 * @param resId the new switch text off
	 */
	public void setSwitchTextOff(int resId) {
		setSwitchTextOff(getContext().getString(resId));
	}

	/**
	 * Sets the switch text on.
	 *
	 * @param onText the new switch text on
	 */
	public void setSwitchTextOn(CharSequence onText) {
		mSwitchOn = onText;
		notifyChanged();
	}

	/**
	 * Sets the switch text on.
	 *
	 * @param resId the new switch text on
	 */
	public void setSwitchTextOn(int resId) {
		setSwitchTextOn(getContext().getString(resId));
	}
}
