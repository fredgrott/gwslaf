package com.WazaBe.HoloEverywhere.preference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.TextView;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class TwoStatePreference.
 */
public abstract class TwoStatePreference extends Preference {

	/**
	 * The Class SavedState.
	 */
	static class SavedState extends BaseSavedState {
		
		/** The Constant CREATOR. */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			@Override
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			@Override
			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};

		/** The checked. */
		boolean checked;

		/**
		 * Instantiates a new saved state.
		 *
		 * @param source the source
		 */
		public SavedState(Parcel source) {
			super(source);
			checked = source.readInt() == 1;
		}

		/**
		 * Instantiates a new saved state.
		 *
		 * @param superState the super state
		 */
		public SavedState(Parcelable superState) {
			super(superState);
		}

		/* (non-Javadoc)
		 * @see android.view.AbsSavedState#writeToParcel(android.os.Parcel, int)
		 */
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(checked ? 1 : 0);
		}
	}

	/** The m checked. */
	boolean mChecked;
	
	/** The m disable dependents state. */
	private boolean mDisableDependentsState;
	
	/** The m send click accessibility event. */
	private boolean mSendClickAccessibilityEvent;
	
	/** The m summary off. */
	private CharSequence mSummaryOff;

	/** The m summary on. */
	private CharSequence mSummaryOn;

	/**
	 * Instantiates a new two state preference.
	 *
	 * @param context the context
	 */
	public TwoStatePreference(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new two state preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public TwoStatePreference(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	/**
	 * Instantiates a new two state preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public TwoStatePreference(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	/**
	 * Gets the disable dependents state.
	 *
	 * @return the disable dependents state
	 */
	public boolean getDisableDependentsState() {
		return mDisableDependentsState;
	}

	/**
	 * Gets the summary off.
	 *
	 * @return the summary off
	 */
	public CharSequence getSummaryOff() {
		return mSummaryOff;
	}

	/**
	 * Gets the summary on.
	 *
	 * @return the summary on
	 */
	public CharSequence getSummaryOn() {
		return mSummaryOn;
	}

	/**
	 * Checks if is checked.
	 *
	 * @return true, if is checked
	 */
	public boolean isChecked() {
		return mChecked;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onClick()
	 */
	@Override
	protected void onClick() {
		super.onClick();

		boolean newValue = !isChecked();

		mSendClickAccessibilityEvent = true;

		if (!callChangeListener(newValue)) {
			return;
		}

		setChecked(newValue);
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onGetDefaultValue(android.content.res.TypedArray, int)
	 */
	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		return a.getBoolean(index, false);
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onRestoreInstanceState(android.os.Parcelable)
	 */
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state == null || !state.getClass().equals(SavedState.class)) {
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState myState = (SavedState) state;
		super.onRestoreInstanceState(myState.getSuperState());
		setChecked(myState.checked);
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onSaveInstanceState()
	 */
	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState = super.onSaveInstanceState();
		if (isPersistent()) {
			return superState;
		}

		final SavedState myState = new SavedState(superState);
		myState.checked = isChecked();
		return myState;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onSetInitialValue(boolean, java.lang.Object)
	 */
	@Override
	protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
		setChecked(restoreValue ? getPersistedBoolean(mChecked)
				: (Boolean) defaultValue);
	}

	/**
	 * Send accessibility event.
	 *
	 * @param view the view
	 */
	@SuppressLint("NewApi")
	void sendAccessibilityEvent(View view) {
		try {
			AccessibilityManager accessibilityManager = (AccessibilityManager) getContext()
					.getSystemService(Context.ACCESSIBILITY_SERVICE);
			if (mSendClickAccessibilityEvent
					&& accessibilityManager.isEnabled()) {
				AccessibilityEvent event = AccessibilityEvent.obtain();
				event.setEventType(AccessibilityEvent.TYPE_VIEW_CLICKED);
				if (VERSION.SDK_INT >= 14) {
					view.onInitializeAccessibilityEvent(event);
				}
				view.dispatchPopulateAccessibilityEvent(event);
				accessibilityManager.sendAccessibilityEvent(event);
			}
		} catch (Exception e) {
		}
		mSendClickAccessibilityEvent = false;
	}

	/**
	 * Sets the checked.
	 *
	 * @param checked the new checked
	 */
	public void setChecked(boolean checked) {
		if (mChecked != checked) {
			mChecked = checked;
			persistBoolean(checked);
			notifyDependencyChange(shouldDisableDependents());
			notifyChanged();
		}
	}

	/**
	 * Sets the disable dependents state.
	 *
	 * @param disableDependentsState the new disable dependents state
	 */
	public void setDisableDependentsState(boolean disableDependentsState) {
		mDisableDependentsState = disableDependentsState;
	}

	/**
	 * Sets the summary off.
	 *
	 * @param summary the new summary off
	 */
	public void setSummaryOff(CharSequence summary) {
		mSummaryOff = summary;
		if (!isChecked()) {
			notifyChanged();
		}
	}

	/**
	 * Sets the summary off.
	 *
	 * @param summaryResId the new summary off
	 */
	public void setSummaryOff(int summaryResId) {
		setSummaryOff(getContext().getString(summaryResId));
	}

	/**
	 * Sets the summary on.
	 *
	 * @param summary the new summary on
	 */
	public void setSummaryOn(CharSequence summary) {
		mSummaryOn = summary;
		if (isChecked()) {
			notifyChanged();
		}
	}

	/**
	 * Sets the summary on.
	 *
	 * @param summaryResId the new summary on
	 */
	public void setSummaryOn(int summaryResId) {
		setSummaryOn(getContext().getString(summaryResId));
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#shouldDisableDependents()
	 */
	@Override
	public boolean shouldDisableDependents() {
		boolean shouldDisable = mDisableDependentsState ? mChecked : !mChecked;
		return shouldDisable || super.shouldDisableDependents();
	}

	/**
	 * Sync summary view.
	 *
	 * @param view the view
	 */
	void syncSummaryView(View view) {
		TextView summaryView = (TextView) view.findViewById(R.id.summary);
		if (summaryView != null) {
			boolean useDefaultSummary = true;
			if (mChecked && mSummaryOn != null) {
				summaryView.setText(mSummaryOn);
				useDefaultSummary = false;
			} else if (!mChecked && mSummaryOff != null) {
				summaryView.setText(mSummaryOff);
				useDefaultSummary = false;
			}

			if (useDefaultSummary) {
				final CharSequence summary = getSummary();
				if (summary != null) {
					summaryView.setText(summary);
					useDefaultSummary = false;
				}
			}

			int newVisibility = View.GONE;
			if (!useDefaultSummary) {
				newVisibility = View.VISIBLE;
			}
			if (newVisibility != summaryView.getVisibility()) {
				summaryView.setVisibility(newVisibility);
			}
		}
	}
}