package com.WazaBe.HoloEverywhere.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.EditText;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class EditTextPreference.
 */
public class EditTextPreference extends DialogPreference {
	
	/**
	 * The Class SavedState.
	 */
	private static class SavedState extends BaseSavedState {
		
		/** The text. */
		String text;

		/**
		 * Instantiates a new saved state.
		 *
		 * @param source the source
		 */
		public SavedState(Parcel source) {
			super(source);
			text = source.readString();
		}

		/**
		 * Instantiates a new saved state.
		 *
		 * @param superState the super state
		 */
		public SavedState(Parcelable superState) {
			super(superState);
		}

		/**
		 * Write to parcel.
		 *
		 * @param dest the dest
		 * @param flags the flags
		 * @see android.view.AbsSavedState#writeToParcel(android.os.Parcel, int)
		 */
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeString(text);
		}
	}

	/** The m edit text. */
	private EditText mEditText;

	/** The m text. */
	private String mText;

	/**
	 * Instantiates a new edits the text preference.
	 *
	 * @param context the context
	 */
	public EditTextPreference(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new edits the text preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public EditTextPreference(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.editTextPreferenceStyle);
	}

	/**
	 * Instantiates a new edits the text preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public EditTextPreference(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mEditText = new EditText(context, attrs);
		mEditText.setId(R.id.edit);
		mEditText.setEnabled(true);
	}

	/**
	 * Gets the edits the text.
	 *
	 * @return the edits the text
	 */
	public EditText getEditText() {
		return mEditText;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return mText;
	}

	/**
	 * Need input method.
	 *
	 * @return true, if successful
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#needInputMethod()
	 */
	@Override
	protected boolean needInputMethod() {
		return true;
	}

	/**
	 * On add edit text to dialog view.
	 *
	 * @param dialogView the dialog view
	 * @param editText the edit text
	 */
	protected void onAddEditTextToDialogView(View dialogView, EditText editText) {
		ViewGroup container = (ViewGroup) dialogView
				.findViewById(R.id.edittext_container);
		if (container != null) {
			container.addView(editText, ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
		}
	}

	/**
	 * On bind dialog view.
	 *
	 * @param view the view
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onBindDialogView(android.view.View)
	 */
	@Override
	protected void onBindDialogView(View view) {
		super.onBindDialogView(view);

		EditText editText = mEditText;
		editText.setText(getText());

		ViewParent oldParent = editText.getParent();
		if (oldParent != view) {
			if (oldParent != null) {
				((ViewGroup) oldParent).removeView(editText);
			}
			onAddEditTextToDialogView(view, editText);
		}
	}

	/**
	 * On dialog closed.
	 *
	 * @param positiveResult the positive result
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onDialogClosed(boolean)
	 */
	@Override
	protected void onDialogClosed(boolean positiveResult) {
		super.onDialogClosed(positiveResult);

		if (positiveResult) {
			String value = mEditText.getText().toString();
			if (callChangeListener(value)) {
				setText(value);
			}
		}
	}

	/**
	 * On get default value.
	 *
	 * @param a the a
	 * @param index the index
	 * @return the object
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onGetDefaultValue(android.content.res.TypedArray, int)
	 */
	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		return a.getString(index);
	}

	/**
	 * On restore instance state.
	 *
	 * @param state the state
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onRestoreInstanceState(android.os.Parcelable)
	 */
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state == null || !state.getClass().equals(SavedState.class)) {
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState myState = (SavedState) state;
		super.onRestoreInstanceState(myState.getSuperState());
		setText(myState.text);
	}

	/**
	 * On save instance state.
	 *
	 * @return the parcelable
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onSaveInstanceState()
	 */
	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState = super.onSaveInstanceState();
		if (isPersistent()) {
			return superState;
		}
		final SavedState myState = new SavedState(superState);
		myState.text = getText();
		return myState;
	}

	/**
	 * On set initial value.
	 *
	 * @param restoreValue the restore value
	 * @param defaultValue the default value
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onSetInitialValue(boolean, java.lang.Object)
	 */
	@Override
	protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
		setText(restoreValue ? getPersistedString(mText)
				: (String) defaultValue);
	}

	/**
	 * Sets the text.
	 *
	 * @param text the new text
	 */
	public void setText(String text) {
		final boolean wasBlocking = shouldDisableDependents();

		mText = text;

		persistString(text);

		final boolean isBlocking = shouldDisableDependents();
		if (isBlocking != wasBlocking) {
			notifyDependencyChange(isBlocking);
		}
	}

	/**
	 * Should disable dependents.
	 *
	 * @return true, if successful
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#shouldDisableDependents()
	 */
	@Override
	public boolean shouldDisableDependents() {
		return TextUtils.isEmpty(mText) || super.shouldDisableDependents();
	}
}