package com.WazaBe.HoloEverywhere.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

import org.bitbucket.fredgrott.gwslaf.R;
import com.actionbarsherlock.internal.nineoldandroids.widget.NineFrameLayout;

// TODO: Auto-generated Javadoc
/**
 * The Class PreferenceFrameLayout.
 */
public class PreferenceFrameLayout extends NineFrameLayout {
	
	/**
	 * The Class LayoutParams.
	 */
	public static class LayoutParams extends NineFrameLayout.LayoutParams {
		
		/** The remove borders. */
		public boolean removeBorders = false;

		/**
		 * Instantiates a new layout params.
		 *
		 * @param c the c
		 * @param attrs the attrs
		 */
		public LayoutParams(Context c, AttributeSet attrs) {
			super(c, attrs);
			TypedArray a = c.obtainStyledAttributes(attrs,
					R.styleable.PreferenceFrameLayout_Layout);
			removeBorders = a
					.getBoolean(
							R.styleable.PreferenceFrameLayout_Layout_layout_removeBorders,
							false);
			a.recycle();
		}

		/**
		 * Instantiates a new layout params.
		 *
		 * @param width the width
		 * @param height the height
		 */
		public LayoutParams(int width, int height) {
			super(width, height);
		}
	}

	/** The Constant DEFAULT_BORDER_BOTTOM. */
	private static final int DEFAULT_BORDER_BOTTOM = 0;
	
	/** The Constant DEFAULT_BORDER_LEFT. */
	private static final int DEFAULT_BORDER_LEFT = 0;
	
	/** The Constant DEFAULT_BORDER_RIGHT. */
	private static final int DEFAULT_BORDER_RIGHT = 0;
	
	/** The Constant DEFAULT_BORDER_TOP. */
	private static final int DEFAULT_BORDER_TOP = 0;
	
	/** The m border bottom. */
	private final int mBorderBottom;
	
	/** The m border left. */
	private final int mBorderLeft;
	
	/** The m border right. */
	private final int mBorderRight;
	
	/** The m border top. */
	private final int mBorderTop;

	/** The m padding applied. */
	private boolean mPaddingApplied;

	/**
	 * Instantiates a new preference frame layout.
	 *
	 * @param context the context
	 */
	public PreferenceFrameLayout(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new preference frame layout.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public PreferenceFrameLayout(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.preferenceFrameLayoutStyle);
	}

	/**
	 * Instantiates a new preference frame layout.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public PreferenceFrameLayout(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.PreferenceFrameLayout, defStyle, 0);
		float density = context.getResources().getDisplayMetrics().density;
		int defaultBorderTop = (int) (density * DEFAULT_BORDER_TOP + 0.5f);
		int defaultBottomPadding = (int) (density * DEFAULT_BORDER_BOTTOM + 0.5f);
		int defaultLeftPadding = (int) (density * DEFAULT_BORDER_LEFT + 0.5f);
		int defaultRightPadding = (int) (density * DEFAULT_BORDER_RIGHT + 0.5f);
		mBorderTop = a.getDimensionPixelSize(
				R.styleable.PreferenceFrameLayout_borderTop, defaultBorderTop);
		mBorderBottom = a.getDimensionPixelSize(
				R.styleable.PreferenceFrameLayout_borderBottom,
				defaultBottomPadding);
		mBorderLeft = a.getDimensionPixelSize(
				R.styleable.PreferenceFrameLayout_borderLeft,
				defaultLeftPadding);
		mBorderRight = a.getDimensionPixelSize(
				R.styleable.PreferenceFrameLayout_borderRight,
				defaultRightPadding);
		a.recycle();
	}

	/* (non-Javadoc)
	 * @see android.view.ViewGroup#addView(android.view.View)
	 */
	@Override
	public void addView(View child) {
		int borderTop = getPaddingTop();
		int borderBottom = getPaddingBottom();
		int borderLeft = getPaddingLeft();
		int borderRight = getPaddingRight();

		android.view.ViewGroup.LayoutParams params = child.getLayoutParams();
		LayoutParams layoutParams = params instanceof PreferenceFrameLayout.LayoutParams ? (PreferenceFrameLayout.LayoutParams) child
				.getLayoutParams() : null;
		if (layoutParams != null && layoutParams.removeBorders) {
			if (mPaddingApplied) {
				borderTop -= mBorderTop;
				borderBottom -= mBorderBottom;
				borderLeft -= mBorderLeft;
				borderRight -= mBorderRight;
				mPaddingApplied = false;
			}
		} else {
			if (!mPaddingApplied) {
				borderTop += mBorderTop;
				borderBottom += mBorderBottom;
				borderLeft += mBorderLeft;
				borderRight += mBorderRight;
				mPaddingApplied = true;
			}
		}

		int previousTop = getPaddingTop();
		int previousBottom = getPaddingBottom();
		int previousLeft = getPaddingLeft();
		int previousRight = getPaddingRight();
		if (previousTop != borderTop || previousBottom != borderBottom
				|| previousLeft != borderLeft || previousRight != borderRight) {
			setPadding(borderLeft, borderTop, borderRight, borderBottom);
		}

		super.addView(child);
	}

	/* (non-Javadoc)
	 * @see android.widget.FrameLayout#generateLayoutParams(android.util.AttributeSet)
	 */
	@Override
	public LayoutParams generateLayoutParams(AttributeSet attrs) {
		return new LayoutParams(getContext(), attrs);
	}
}