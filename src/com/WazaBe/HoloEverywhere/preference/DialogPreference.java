package com.WazaBe.HoloEverywhere.preference;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.WazaBe.HoloEverywhere.LayoutInflater;
import org.bitbucket.fredgrott.gwslaf.R;
import com.WazaBe.HoloEverywhere.app.AlertDialog;

// TODO: Auto-generated Javadoc
/**
 * The Class DialogPreference.
 */
public abstract class DialogPreference extends Preference implements
		DialogInterface.OnClickListener, DialogInterface.OnDismissListener,
		PreferenceManager.OnActivityDestroyListener {
	
	/**
	 * The Class SavedState.
	 */
	private static class SavedState extends BaseSavedState {
		
		/** The dialog bundle. */
		Bundle dialogBundle;
		
		/** The is dialog showing. */
		boolean isDialogShowing;

		/**
		 * Instantiates a new saved state.
		 *
		 * @param source the source
		 */
		public SavedState(Parcel source) {
			super(source);
			isDialogShowing = source.readInt() == 1;
			dialogBundle = source.readBundle();
		}

		/**
		 * Instantiates a new saved state.
		 *
		 * @param superState the super state
		 */
		public SavedState(Parcelable superState) {
			super(superState);
		}

		/**
		 * Write to parcel.
		 *
		 * @param dest the dest
		 * @param flags the flags
		 * @see android.view.AbsSavedState#writeToParcel(android.os.Parcel, int)
		 */
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(isDialogShowing ? 1 : 0);
			dest.writeBundle(dialogBundle);
		}
	}

	/** The m builder. */
	private AlertDialog.Builder mBuilder;
	
	/** The m dialog. */
	private Dialog mDialog;
	
	/** The m dialog icon. */
	private Drawable mDialogIcon;
	
	/** The m dialog layout res id. */
	private int mDialogLayoutResId;
	
	/** The m dialog message. */
	private CharSequence mDialogMessage;
	
	/** The m dialog title. */
	private CharSequence mDialogTitle;
	
	/** The m negative button text. */
	private CharSequence mNegativeButtonText;
	
	/** The m positive button text. */
	private CharSequence mPositiveButtonText;

	/** The m which button clicked. */
	private int mWhichButtonClicked;

	/**
	 * Instantiates a new dialog preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public DialogPreference(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.dialogPreferenceStyle);
	}

	/**
	 * Instantiates a new dialog preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public DialogPreference(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.DialogPreference, defStyle, 0);
		mDialogTitle = a.getString(R.styleable.DialogPreference_dialogTitle);
		if (mDialogTitle == null) {
			mDialogTitle = getTitle();
		}
		mDialogMessage = a
				.getString(R.styleable.DialogPreference_dialogMessage);
		mDialogIcon = a.getDrawable(R.styleable.DialogPreference_dialogIcon);
		mPositiveButtonText = a
				.getString(R.styleable.DialogPreference_positiveButtonText);
		mNegativeButtonText = a
				.getString(R.styleable.DialogPreference_negativeButtonText);
		mDialogLayoutResId = a.getResourceId(
				R.styleable.DialogPreference_dialogLayout, mDialogLayoutResId);
		a.recycle();

	}

	/**
	 * Gets the dialog.
	 *
	 * @return the dialog
	 */
	public Dialog getDialog() {
		return mDialog;
	}

	/**
	 * Gets the dialog icon.
	 *
	 * @return the dialog icon
	 */
	public Drawable getDialogIcon() {
		return mDialogIcon;
	}

	/**
	 * Gets the dialog layout resource.
	 *
	 * @return the dialog layout resource
	 */
	public int getDialogLayoutResource() {
		return mDialogLayoutResId;
	}

	/**
	 * Gets the dialog message.
	 *
	 * @return the dialog message
	 */
	public CharSequence getDialogMessage() {
		return mDialogMessage;
	}

	/**
	 * Gets the dialog title.
	 *
	 * @return the dialog title
	 */
	public CharSequence getDialogTitle() {
		return mDialogTitle;
	}

	/**
	 * Gets the negative button text.
	 *
	 * @return the negative button text
	 */
	public CharSequence getNegativeButtonText() {
		return mNegativeButtonText;
	}

	/**
	 * Gets the positive button text.
	 *
	 * @return the positive button text
	 */
	public CharSequence getPositiveButtonText() {
		return mPositiveButtonText;
	}

	/**
	 * Need input method.
	 *
	 * @return true, if successful
	 */
	protected boolean needInputMethod() {
		return false;
	}

	/**
	 * On activity destroy.
	 *
	 * @see com.WazaBe.HoloEverywhere.preference.PreferenceManager.OnActivityDestroyListener#onActivityDestroy()
	 */
	@Override
	public void onActivityDestroy() {

		if (mDialog == null || !mDialog.isShowing()) {
			return;
		}

		mDialog.dismiss();
	}

	/**
	 * On bind dialog view.
	 *
	 * @param view the view
	 */
	protected void onBindDialogView(View view) {
		View dialogMessageView = view.findViewById(R.id.message);
		if (dialogMessageView != null) {
			final CharSequence message = getDialogMessage();
			int newVisibility = View.GONE;
			if (!TextUtils.isEmpty(message)) {
				if (dialogMessageView instanceof TextView) {
					((TextView) dialogMessageView).setText(message);
				}
				newVisibility = View.VISIBLE;
			}
			if (dialogMessageView.getVisibility() != newVisibility) {
				dialogMessageView.setVisibility(newVisibility);
			}
		}
	}

	/**
	 * On click.
	 *
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onClick()
	 */
	@Override
	protected void onClick() {
		if (mDialog != null && mDialog.isShowing()) {
			return;
		}
		showDialog(null);
	}

	/**
	 * On click.
	 *
	 * @param dialog the dialog
	 * @param which the which
	 * @see android.content.DialogInterface.OnClickListener#onClick(android.content.DialogInterface, int)
	 */
	@Override
	public void onClick(DialogInterface dialog, int which) {
		mWhichButtonClicked = which;
	}

	/**
	 * On create dialog view.
	 *
	 * @return the view
	 */
	protected View onCreateDialogView() {
		if (mDialogLayoutResId == 0) {
			return null;
		}
		return LayoutInflater.inflate(getContext(), mDialogLayoutResId);
	}

	/**
	 * On dialog closed.
	 *
	 * @param positiveResult the positive result
	 */
	protected void onDialogClosed(boolean positiveResult) {
	}

	/**
	 * On dismiss.
	 *
	 * @param dialog the dialog
	 * @see android.content.DialogInterface.OnDismissListener#onDismiss(android.content.DialogInterface)
	 */
	@Override
	public void onDismiss(DialogInterface dialog) {

		getPreferenceManager().unregisterOnActivityDestroyListener(this);

		mDialog = null;
		onDialogClosed(mWhichButtonClicked == DialogInterface.BUTTON_POSITIVE);
	}

	/**
	 * On prepare dialog builder.
	 *
	 * @param builder the builder
	 */
	protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
	}

	/**
	 * On restore instance state.
	 *
	 * @param state the state
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onRestoreInstanceState(android.os.Parcelable)
	 */
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state == null || !state.getClass().equals(SavedState.class)) {
			super.onRestoreInstanceState(state);
			return;
		}
		SavedState myState = (SavedState) state;
		super.onRestoreInstanceState(myState.getSuperState());
		if (myState.isDialogShowing) {
			showDialog(myState.dialogBundle);
		}
	}

	/**
	 * On save instance state.
	 *
	 * @return the parcelable
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onSaveInstanceState()
	 */
	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState = super.onSaveInstanceState();
		if (mDialog == null || !mDialog.isShowing()) {
			return superState;
		}
		final SavedState myState = new SavedState(superState);
		myState.isDialogShowing = true;
		myState.dialogBundle = mDialog.onSaveInstanceState();
		return myState;
	}

	/**
	 * Request input method.
	 *
	 * @param dialog the dialog
	 */
	private void requestInputMethod(Dialog dialog) {
		Window window = dialog.getWindow();
		window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
	}

	/**
	 * Sets the dialog icon.
	 *
	 * @param dialogIcon the new dialog icon
	 */
	public void setDialogIcon(Drawable dialogIcon) {
		mDialogIcon = dialogIcon;
	}

	/**
	 * Sets the dialog icon.
	 *
	 * @param dialogIconRes the new dialog icon
	 */
	public void setDialogIcon(int dialogIconRes) {
		mDialogIcon = getContext().getResources().getDrawable(dialogIconRes);
	}

	/**
	 * Sets the dialog layout resource.
	 *
	 * @param dialogLayoutResId the new dialog layout resource
	 */
	public void setDialogLayoutResource(int dialogLayoutResId) {
		mDialogLayoutResId = dialogLayoutResId;
	}

	/**
	 * Sets the dialog message.
	 *
	 * @param dialogMessage the new dialog message
	 */
	public void setDialogMessage(CharSequence dialogMessage) {
		mDialogMessage = dialogMessage;
	}

	/**
	 * Sets the dialog message.
	 *
	 * @param dialogMessageResId the new dialog message
	 */
	public void setDialogMessage(int dialogMessageResId) {
		setDialogMessage(getContext().getString(dialogMessageResId));
	}

	/**
	 * Sets the dialog title.
	 *
	 * @param dialogTitle the new dialog title
	 */
	public void setDialogTitle(CharSequence dialogTitle) {
		mDialogTitle = dialogTitle;
	}

	/**
	 * Sets the dialog title.
	 *
	 * @param dialogTitleResId the new dialog title
	 */
	public void setDialogTitle(int dialogTitleResId) {
		setDialogTitle(getContext().getString(dialogTitleResId));
	}

	/**
	 * Sets the negative button text.
	 *
	 * @param negativeButtonText the new negative button text
	 */
	public void setNegativeButtonText(CharSequence negativeButtonText) {
		mNegativeButtonText = negativeButtonText;
	}

	/**
	 * Sets the negative button text.
	 *
	 * @param negativeButtonTextResId the new negative button text
	 */
	public void setNegativeButtonText(int negativeButtonTextResId) {
		setNegativeButtonText(getContext().getString(negativeButtonTextResId));
	}

	/**
	 * Sets the positive button text.
	 *
	 * @param positiveButtonText the new positive button text
	 */
	public void setPositiveButtonText(CharSequence positiveButtonText) {
		mPositiveButtonText = positiveButtonText;
	}

	/**
	 * Sets the positive button text.
	 *
	 * @param positiveButtonTextResId the new positive button text
	 */
	public void setPositiveButtonText(int positiveButtonTextResId) {
		setPositiveButtonText(getContext().getString(positiveButtonTextResId));
	}

	/**
	 * Show dialog.
	 *
	 * @param state the state
	 */
	protected void showDialog(Bundle state) {
		Context context = getContext();
		mWhichButtonClicked = DialogInterface.BUTTON_NEGATIVE;
		mBuilder = new AlertDialog.Builder(context).setTitle(mDialogTitle)
				.setIcon(mDialogIcon)
				.setPositiveButton(mPositiveButtonText, this)
				.setNegativeButton(mNegativeButtonText, this);
		View contentView = onCreateDialogView();
		if (contentView != null) {
			onBindDialogView(contentView);
			mBuilder.setView(contentView);
		} else {
			mBuilder.setMessage(mDialogMessage);
		}
		onPrepareDialogBuilder(mBuilder);
		getPreferenceManager().registerOnActivityDestroyListener(this);
		final Dialog dialog = mDialog = mBuilder.create();
		if (state != null) {
			dialog.onRestoreInstanceState(state);
		}
		if (needInputMethod()) {
			requestInputMethod(dialog);
		}
		dialog.setOnDismissListener(this);
		dialog.show();
	}
}