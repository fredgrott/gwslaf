package com.WazaBe.HoloEverywhere.preference;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;

import org.bitbucket.fredgrott.gwslaf.R;
import com.WazaBe.HoloEverywhere.app.AlertDialog.Builder;

// TODO: Auto-generated Javadoc
/**
 * The Class MultiSelectListPreference.
 */
public class MultiSelectListPreference extends DialogPreference {
	
	/**
	 * The Class SavedState.
	 */
	private static class SavedState extends BaseSavedState {
		
		/** The values. */
		Set<String> values;

		/**
		 * Instantiates a new saved state.
		 *
		 * @param source the source
		 */
		public SavedState(Parcel source) {
			super(source);
			String[] v = source.createStringArray();
			values = new HashSet<String>(Arrays.asList(v));
		}

		/**
		 * Instantiates a new saved state.
		 *
		 * @param superState the super state
		 */
		public SavedState(Parcelable superState) {
			super(superState);
		}

		/* (non-Javadoc)
		 * @see android.view.AbsSavedState#writeToParcel(android.os.Parcel, int)
		 */
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeStringArray(values.toArray(new String[values.size()]));
		}
	}

	/** The m entries. */
	private CharSequence[] mEntries;
	
	/** The m entry values. */
	private CharSequence[] mEntryValues;
	
	/** The m new values. */
	private Set<String> mNewValues = new HashSet<String>();
	
	/** The m preference changed. */
	private boolean mPreferenceChanged;

	/** The m values. */
	private Set<String> mValues = new HashSet<String>();

	/**
	 * Instantiates a new multi select list preference.
	 *
	 * @param context the context
	 */
	public MultiSelectListPreference(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new multi select list preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public MultiSelectListPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.ListPreference, 0, 0);
		mEntries = a.getTextArray(R.styleable.ListPreference_entries);
		mEntryValues = a.getTextArray(R.styleable.ListPreference_entryValues);
		a.recycle();
	}

	/**
	 * Find index of value.
	 *
	 * @param value the value
	 * @return the int
	 */
	public int findIndexOfValue(String value) {
		if (value != null && mEntryValues != null) {
			for (int i = mEntryValues.length - 1; i >= 0; i--) {
				if (mEntryValues[i].equals(value)) {
					return i;
				}
			}
		}
		return -1;
	}

	/**
	 * Gets the entries.
	 *
	 * @return the entries
	 */
	public CharSequence[] getEntries() {
		return mEntries;
	}

	/**
	 * Gets the entry values.
	 *
	 * @return the entry values
	 */
	public CharSequence[] getEntryValues() {
		return mEntryValues;
	}

	/**
	 * Gets the selected items.
	 *
	 * @return the selected items
	 */
	private boolean[] getSelectedItems() {
		final CharSequence[] entries = mEntryValues;
		final int entryCount = entries.length;
		final Set<String> values = mValues;
		boolean[] result = new boolean[entryCount];

		for (int i = 0; i < entryCount; i++) {
			result[i] = values.contains(entries[i].toString());
		}

		return result;
	}

	/**
	 * Gets the values.
	 *
	 * @return the values
	 */
	public Set<String> getValues() {
		return mValues;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onDialogClosed(boolean)
	 */
	@Override
	protected void onDialogClosed(boolean positiveResult) {
		super.onDialogClosed(positiveResult);

		if (positiveResult && mPreferenceChanged) {
			final Set<String> values = mNewValues;
			if (callChangeListener(values)) {
				setValues(values);
			}
		}
		mPreferenceChanged = false;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onGetDefaultValue(android.content.res.TypedArray, int)
	 */
	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		final CharSequence[] defaultValues = a.getTextArray(index);
		final Set<String> result = new HashSet<String>();
		if (defaultValues != null) {
			for (CharSequence s : defaultValues) {
				result.add(s.toString());
			}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onPrepareDialogBuilder(com.WazaBe.HoloEverywhere.app.AlertDialog.Builder)
	 */
	@Override
	protected void onPrepareDialogBuilder(Builder builder) {
		super.onPrepareDialogBuilder(builder);

		if (mEntries == null || mEntryValues == null) {
			throw new IllegalStateException(
					"MultiSelectListPreference requires an entries array and "
							+ "an entryValues array.");
		}

		boolean[] checkedItems = getSelectedItems();
		builder.setMultiChoiceItems(mEntries, checkedItems,
				new DialogInterface.OnMultiChoiceClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which,
							boolean isChecked) {
						if (isChecked) {
							mPreferenceChanged |= mNewValues
									.add(mEntryValues[which].toString());
						} else {
							mPreferenceChanged |= mNewValues
									.remove(mEntryValues[which].toString());
						}
					}
				});
		mNewValues.clear();
		mNewValues.addAll(mValues);
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onSaveInstanceState()
	 */
	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState = super.onSaveInstanceState();
		if (isPersistent()) {
			// No need to save instance state
			return superState;
		}

		final SavedState myState = new SavedState(superState);
		myState.values = new HashSet<String>(getValues());
		return myState;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onSetInitialValue(boolean, java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
		setValues(restoreValue ? getPersistedStringSet(mValues)
				: (Set<String>) defaultValue);
	}

	/**
	 * Sets the entries.
	 *
	 * @param entries the new entries
	 */
	public void setEntries(CharSequence[] entries) {
		mEntries = entries;
	}

	/**
	 * Sets the entries.
	 *
	 * @param entriesResId the new entries
	 */
	public void setEntries(int entriesResId) {
		setEntries(getContext().getResources().getTextArray(entriesResId));
	}

	/**
	 * Sets the entry values.
	 *
	 * @param entryValues the new entry values
	 */
	public void setEntryValues(CharSequence[] entryValues) {
		mEntryValues = entryValues;
	}

	/**
	 * Sets the entry values.
	 *
	 * @param entryValuesResId the new entry values
	 */
	public void setEntryValues(int entryValuesResId) {
		setEntryValues(getContext().getResources().getTextArray(
				entryValuesResId));
	}

	/**
	 * Sets the values.
	 *
	 * @param values the new values
	 */
	public void setValues(Set<String> values) {
		mValues.clear();
		mValues.addAll(values);

		persistStringSet(values);
	}
}