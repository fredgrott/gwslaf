package com.WazaBe.HoloEverywhere.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class SeekBarDialogPreference.
 */
public class SeekBarDialogPreference extends DialogPreference {
	
	/**
	 * Gets the seek bar.
	 *
	 * @param dialogView the dialog view
	 * @return the seek bar
	 */
	protected static SeekBar getSeekBar(View dialogView) {
		return (SeekBar) dialogView.findViewById(R.id.seekbar);
	}

	/** The m max. */
	private int mMax;
	
	/** The m my icon. */
	private Drawable mMyIcon;

	/** The m seek bar. */
	private SeekBar mSeekBar;

	/**
	 * Instantiates a new seek bar dialog preference.
	 *
	 * @param context the context
	 */
	public SeekBarDialogPreference(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new seek bar dialog preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public SeekBarDialogPreference(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.seekBarDialogPreferenceStyle);
	}

	/**
	 * Instantiates a new seek bar dialog preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public SeekBarDialogPreference(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		createActionButtons();
		mMyIcon = getDialogIcon();
		setDialogIcon(null);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.SeekBarPreference, defStyle, 0);
		setMax(a.getInt(R.styleable.SeekBarPreference_max, 0));
	}

	/**
	 * Creates the action buttons.
	 */
	public void createActionButtons() {
		setPositiveButtonText(android.R.string.ok);
		setNegativeButtonText(android.R.string.cancel);
	}

	/**
	 * Gets the max.
	 *
	 * @return the max
	 */
	public int getMax() {
		return mMax;
	}

	/**
	 * Gets the seek bar.
	 *
	 * @return the seek bar
	 */
	public SeekBar getSeekBar() {
		return mSeekBar;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onBindDialogView(android.view.View)
	 */
	@Override
	protected void onBindDialogView(View view) {
		super.onBindDialogView(view);
		mSeekBar = (SeekBar) view.findViewById(R.id.seekbar);
		if (mSeekBar != null) {
			mSeekBar.setMax(mMax);
		}
		final ImageView iconView = (ImageView) view.findViewById(R.id.icon);
		if (mMyIcon != null) {
			iconView.setImageDrawable(mMyIcon);
		} else {
			iconView.setVisibility(View.GONE);
		}
	}

	/**
	 * Sets the max.
	 *
	 * @param max the new max
	 */
	public void setMax(int max) {
		mMax = max;
	}
}