package com.WazaBe.HoloEverywhere.preference;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.AbsSavedState;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.WazaBe.HoloEverywhere.LayoutInflater;
import org.bitbucket.fredgrott.gwslaf.R;
import com.WazaBe.HoloEverywhere.Settings;
import com.WazaBe.HoloEverywhere.util.CharSequences;

// TODO: Auto-generated Javadoc
/**
 * The Class Preference.
 */
public class Preference implements Comparable<Preference>,
		OnDependencyChangeListener {
	
	/**
	 * The Class BaseSavedState.
	 */
	public static class BaseSavedState extends AbsSavedState {
		
		/** The Constant CREATOR. */
		public static final Parcelable.Creator<BaseSavedState> CREATOR = new Parcelable.Creator<BaseSavedState>() {
			@Override
			public BaseSavedState createFromParcel(Parcel in) {
				return new BaseSavedState(in);
			}

			@Override
			public BaseSavedState[] newArray(int size) {
				return new BaseSavedState[size];
			}
		};

		/**
		 * Instantiates a new base saved state.
		 *
		 * @param source the source
		 */
		public BaseSavedState(Parcel source) {
			super(source);
		}

		/**
		 * Instantiates a new base saved state.
		 *
		 * @param superState the super state
		 */
		public BaseSavedState(Parcelable superState) {
			super(superState);
		}
	}

	/**
	 * The listener interface for receiving onPreferenceChangeInternal events.
	 * The class that is interested in processing a onPreferenceChangeInternal
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addOnPreferenceChangeInternalListener<code> method. When
	 * the onPreferenceChangeInternal event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see OnPreferenceChangeInternalEvent
	 */
	interface OnPreferenceChangeInternalListener {
		
		/**
		 * On preference change.
		 *
		 * @param preference the preference
		 */
		void onPreferenceChange(Preference preference);

		/**
		 * On preference hierarchy change.
		 *
		 * @param preference the preference
		 */
		void onPreferenceHierarchyChange(Preference preference);
	}

	/**
	 * The listener interface for receiving onPreferenceChange events.
	 * The class that is interested in processing a onPreferenceChange
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addOnPreferenceChangeListener<code> method. When
	 * the onPreferenceChange event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see OnPreferenceChangeEvent
	 */
	public interface OnPreferenceChangeListener {
		
		/**
		 * On preference change.
		 *
		 * @param preference the preference
		 * @param newValue the new value
		 * @return true, if successful
		 */
		boolean onPreferenceChange(Preference preference, Object newValue);
	}

	/**
	 * The listener interface for receiving onPreferenceClick events.
	 * The class that is interested in processing a onPreferenceClick
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addOnPreferenceClickListener<code> method. When
	 * the onPreferenceClick event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see OnPreferenceClickEvent
	 */
	public interface OnPreferenceClickListener {
		
		/**
		 * On preference click.
		 *
		 * @param preference the preference
		 * @return true, if successful
		 */
		boolean onPreferenceClick(Preference preference);
	}

	/** The Constant DEFAULT_ORDER. */
	public static final int DEFAULT_ORDER = Integer.MAX_VALUE;
	
	/** The m base method called. */
	private boolean mBaseMethodCalled;
	
	/** The m context. */
	private Context mContext;
	
	/** The m default value. */
	private Object mDefaultValue;
	
	/** The m dependency key. */
	private String mDependencyKey;
	
	/** The m dependency met. */
	private boolean mDependencyMet = true;
	
	/** The m dependents. */
	private List<Preference> mDependents;
	
	/** The m enabled. */
	private boolean mEnabled = true;
	
	/** The m extras. */
	private Bundle mExtras;
	
	/** The m fragment. */
	private String mFragment;
	
	/** The m has specified layout. */
	private boolean mHasSpecifiedLayout = false;
	
	/** The m icon. */
	private Drawable mIcon;
	
	/** The m icon res id. */
	private int mIconResId;
	
	/** The m id. */
	private long mId;
	
	/** The m intent. */
	private Intent mIntent;
	
	/** The m key. */
	private String mKey;
	
	/** The m layout res id. */
	private int mLayoutResId = R.layout.preference_holo;
	
	/** The m listener. */
	private OnPreferenceChangeInternalListener mListener;
	
	/** The m on change listener. */
	private OnPreferenceChangeListener mOnChangeListener;
	
	/** The m on click listener. */
	private OnPreferenceClickListener mOnClickListener;
	
	/** The m order. */
	private int mOrder = DEFAULT_ORDER;
	
	/** The m persistent. */
	private boolean mPersistent = true;
	
	/** The m preference manager. */
	private PreferenceManager mPreferenceManager;
	
	/** The m requires key. */
	private boolean mRequiresKey;
	
	/** The m selectable. */
	private boolean mSelectable = true;
	
	/** The m should disable view. */
	private boolean mShouldDisableView = true;
	
	/** The m summary. */
	private CharSequence mSummary;
	
	/** The m title. */
	private CharSequence mTitle;
	
	/** The m title res. */
	private int mTitleRes;
	
	/** The m widget layout res id. */
	private int mWidgetLayoutResId;

	/**
	 * Instantiates a new preference.
	 *
	 * @param context the context
	 */
	public Preference(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public Preference(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.preferenceStyle);
	}

	/**
	 * Instantiates a new preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public Preference(Context context, AttributeSet attrs, int defStyle) {
		mContext = context;
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.Preference, defStyle, 0);
		mIconResId = a.getResourceId(R.styleable.Preference_icon, 0);
		mKey = a.getString(R.styleable.Preference_key);
		mTitleRes = a.getResourceId(R.styleable.Preference_title, 0);
		mTitle = a.getString(R.styleable.Preference_title);
		mSummary = a.getString(R.styleable.Preference_summary);
		mOrder = a.getInt(R.styleable.Preference_order, mOrder);
		mFragment = a.getString(R.styleable.Preference_fragment);
		mLayoutResId = a.getResourceId(R.styleable.Preference_layout,
				mLayoutResId);
		mWidgetLayoutResId = a.getResourceId(
				R.styleable.Preference_widgetLayout, mWidgetLayoutResId);
		mEnabled = a.getBoolean(R.styleable.Preference_enabled, true);
		mSelectable = a.getBoolean(R.styleable.Preference_selectable, true);
		mPersistent = a.getBoolean(R.styleable.Preference_persistent,
				mPersistent);
		mDependencyKey = a.getString(R.styleable.Preference_dependency);
		mDefaultValue = onGetDefaultValue(a,
				R.styleable.Preference_defaultValue);
		mShouldDisableView = a.getBoolean(
				R.styleable.Preference_shouldDisableView, mShouldDisableView);
		a.recycle();
		if (!getClass().getName().startsWith(Settings.getPreferencePackage())) {
			mHasSpecifiedLayout = true;
		}
	}

	/**
	 * Call change listener.
	 *
	 * @param newValue the new value
	 * @return true, if successful
	 */
	protected boolean callChangeListener(Object newValue) {
		return mOnChangeListener == null ? true : mOnChangeListener
				.onPreferenceChange(this, newValue);
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Preference another) {
		if (mOrder != DEFAULT_ORDER || mOrder == DEFAULT_ORDER
				&& another.mOrder != DEFAULT_ORDER) {
			return mOrder - another.mOrder;
		} else if (mTitle == null) {
			return 1;
		} else if (another.mTitle == null) {
			return -1;
		} else {

			return CharSequences.compareToIgnoreCase(mTitle, another.mTitle);
		}
	}

	/**
	 * Dispatch restore instance state.
	 *
	 * @param container the container
	 */
	void dispatchRestoreInstanceState(Bundle container) {
		if (hasKey()) {
			Parcelable state = container.getParcelable(mKey);
			if (state != null) {
				mBaseMethodCalled = false;
				onRestoreInstanceState(state);
				if (!mBaseMethodCalled) {
					throw new IllegalStateException(
							"Derived class did not call super.onRestoreInstanceState()");
				}
			}
		}
	}

	/**
	 * Dispatch save instance state.
	 *
	 * @param container the container
	 */
	void dispatchSaveInstanceState(Bundle container) {
		if (hasKey()) {
			mBaseMethodCalled = false;
			Parcelable state = onSaveInstanceState();
			if (!mBaseMethodCalled) {
				throw new IllegalStateException(
						"Derived class did not call super.onSaveInstanceState()");
			}
			if (state != null) {
				container.putParcelable(mKey, state);
			}
		}
	}

	/**
	 * Dispatch set initial value.
	 */
	private void dispatchSetInitialValue() {
		final boolean shouldPersist = shouldPersist();
		if (!shouldPersist || !getSharedPreferences().contains(mKey)) {
			if (mDefaultValue != null) {
				onSetInitialValue(false, mDefaultValue);
			}
		} else {
			onSetInitialValue(true, null);
		}
	}

	/**
	 * Find preference in hierarchy.
	 *
	 * @param key the key
	 * @return the preference
	 */
	protected Preference findPreferenceInHierarchy(String key) {
		if (TextUtils.isEmpty(key) || mPreferenceManager == null) {
			return null;
		}

		return mPreferenceManager.findPreference(key);
	}

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	public Context getContext() {
		return mContext;
	}

	/**
	 * Gets the dependency.
	 *
	 * @return the dependency
	 */
	public String getDependency() {
		return mDependencyKey;
	}

	/**
	 * Gets the editor.
	 *
	 * @return the editor
	 */
	public SharedPreferences.Editor getEditor() {
		if (mPreferenceManager == null) {
			return null;
		}

		return mPreferenceManager.getEditor();
	}

	/**
	 * Gets the extras.
	 *
	 * @return the extras
	 */
	public Bundle getExtras() {
		if (mExtras == null) {
			mExtras = new Bundle();
		}
		return mExtras;
	}

	/**
	 * Gets the filterable string builder.
	 *
	 * @return the filterable string builder
	 */
	StringBuilder getFilterableStringBuilder() {
		StringBuilder sb = new StringBuilder();
		CharSequence title = getTitle();
		if (!TextUtils.isEmpty(title)) {
			sb.append(title).append(' ');
		}
		CharSequence summary = getSummary();
		if (!TextUtils.isEmpty(summary)) {
			sb.append(summary).append(' ');
		}
		if (sb.length() > 0) {
			sb.setLength(sb.length() - 1);
		}
		return sb;
	}

	/**
	 * Gets the fragment.
	 *
	 * @return the fragment
	 */
	public String getFragment() {
		return mFragment;
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Drawable getIcon() {
		return mIcon;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	long getId() {
		return mId;
	}

	/**
	 * Gets the intent.
	 *
	 * @return the intent
	 */
	public Intent getIntent() {
		return mIntent;
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public String getKey() {
		return mKey;
	}

	/**
	 * Gets the layout resource.
	 *
	 * @return the layout resource
	 */
	public int getLayoutResource() {
		return mLayoutResId;
	}

	/**
	 * Gets the on preference change listener.
	 *
	 * @return the on preference change listener
	 */
	public OnPreferenceChangeListener getOnPreferenceChangeListener() {
		return mOnChangeListener;
	}

	/**
	 * Gets the on preference click listener.
	 *
	 * @return the on preference click listener
	 */
	public OnPreferenceClickListener getOnPreferenceClickListener() {
		return mOnClickListener;
	}

	/**
	 * Gets the order.
	 *
	 * @return the order
	 */
	public int getOrder() {
		return mOrder;
	}

	/**
	 * Gets the persisted boolean.
	 *
	 * @param defaultReturnValue the default return value
	 * @return the persisted boolean
	 */
	protected boolean getPersistedBoolean(boolean defaultReturnValue) {
		if (!shouldPersist()) {
			return defaultReturnValue;
		}

		return mPreferenceManager.getSharedPreferences().getBoolean(mKey,
				defaultReturnValue);
	}

	/**
	 * Gets the persisted float.
	 *
	 * @param defaultReturnValue the default return value
	 * @return the persisted float
	 */
	protected float getPersistedFloat(float defaultReturnValue) {
		if (!shouldPersist()) {
			return defaultReturnValue;
		}

		return mPreferenceManager.getSharedPreferences().getFloat(mKey,
				defaultReturnValue);
	}

	/**
	 * Gets the persisted int.
	 *
	 * @param defaultReturnValue the default return value
	 * @return the persisted int
	 */
	protected int getPersistedInt(int defaultReturnValue) {
		if (!shouldPersist()) {
			return defaultReturnValue;
		}

		return mPreferenceManager.getSharedPreferences().getInt(mKey,
				defaultReturnValue);
	}

	/**
	 * Gets the persisted long.
	 *
	 * @param defaultReturnValue the default return value
	 * @return the persisted long
	 */
	protected long getPersistedLong(long defaultReturnValue) {
		if (!shouldPersist()) {
			return defaultReturnValue;
		}

		return mPreferenceManager.getSharedPreferences().getLong(mKey,
				defaultReturnValue);
	}

	/**
	 * Gets the persisted string.
	 *
	 * @param defaultReturnValue the default return value
	 * @return the persisted string
	 */
	protected String getPersistedString(String defaultReturnValue) {
		if (!shouldPersist()) {
			return defaultReturnValue;
		}

		return mPreferenceManager.getSharedPreferences().getString(mKey,
				defaultReturnValue);
	}

	/**
	 * Gets the persisted string set.
	 *
	 * @param defaultReturnValue the default return value
	 * @return the persisted string set
	 */
	protected Set<String> getPersistedStringSet(Set<String> defaultReturnValue) {
		if (!shouldPersist()) {
			return defaultReturnValue;
		}
		return mPreferenceManager.getSharedPreferences().getStringSet(mKey,
				defaultReturnValue);
	}

	/**
	 * Gets the preference manager.
	 *
	 * @return the preference manager
	 */
	public PreferenceManager getPreferenceManager() {
		return mPreferenceManager;
	}

	/**
	 * Gets the shared preferences.
	 *
	 * @return the shared preferences
	 */
	public SharedPreferences getSharedPreferences() {
		if (mPreferenceManager == null) {
			return null;
		}

		return mPreferenceManager.getSharedPreferences();
	}

	/**
	 * Gets the should disable view.
	 *
	 * @return the should disable view
	 */
	public boolean getShouldDisableView() {
		return mShouldDisableView;
	}

	/**
	 * Gets the summary.
	 *
	 * @return the summary
	 */
	public CharSequence getSummary() {
		return mSummary;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public CharSequence getTitle() {
		return mTitle;
	}

	/**
	 * Gets the title res.
	 *
	 * @return the title res
	 */
	public int getTitleRes() {
		return mTitleRes;
	}

	/**
	 * Gets the view.
	 *
	 * @param convertView the convert view
	 * @param parent the parent
	 * @return the view
	 */
	public View getView(View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = onCreateView(parent);
		}
		onBindView(convertView);
		return convertView;
	}

	/**
	 * Gets the widget layout resource.
	 *
	 * @return the widget layout resource
	 */
	public int getWidgetLayoutResource() {
		return mWidgetLayoutResId;
	}

	/**
	 * Checks for key.
	 *
	 * @return true, if successful
	 */
	public boolean hasKey() {
		return !TextUtils.isEmpty(mKey);
	}

	/**
	 * Checks for specified layout.
	 *
	 * @return true, if successful
	 */
	boolean hasSpecifiedLayout() {
		return mHasSpecifiedLayout;
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled && mDependencyMet;
	}

	/**
	 * Checks if is persistent.
	 *
	 * @return true, if is persistent
	 */
	public boolean isPersistent() {
		return mPersistent;
	}

	/**
	 * Checks if is selectable.
	 *
	 * @return true, if is selectable
	 */
	public boolean isSelectable() {
		return mSelectable;
	}

	/**
	 * Notify changed.
	 */
	protected void notifyChanged() {
		if (mListener != null) {
			mListener.onPreferenceChange(this);
		}
	}

	/**
	 * Notify dependency change.
	 *
	 * @param disableDependents the disable dependents
	 */
	public void notifyDependencyChange(boolean disableDependents) {
		final List<Preference> dependents = mDependents;

		if (dependents == null) {
			return;
		}

		final int dependentsCount = dependents.size();
		for (int i = 0; i < dependentsCount; i++) {
			dependents.get(i).onDependencyChanged(this, disableDependents);
		}
	}

	/**
	 * Notify hierarchy changed.
	 */
	protected void notifyHierarchyChanged() {
		if (mListener != null) {
			mListener.onPreferenceHierarchyChange(this);
		}
	}

	/**
	 * On attached to activity.
	 */
	protected void onAttachedToActivity() {
		registerDependency();
	}

	/**
	 * On attached to hierarchy.
	 *
	 * @param preferenceManager the preference manager
	 */
	protected void onAttachedToHierarchy(PreferenceManager preferenceManager) {
		mPreferenceManager = preferenceManager;

		mId = preferenceManager.getNextId();

		dispatchSetInitialValue();
	}

	/**
	 * On bind view.
	 *
	 * @param view the view
	 */
	protected void onBindView(View view) {
		final TextView titleView = (TextView) view.findViewById(R.id.title);
		if (titleView != null) {
			final CharSequence title = getTitle();
			if (!TextUtils.isEmpty(title)) {
				titleView.setText(title);
				titleView.setVisibility(View.VISIBLE);
			} else {
				titleView.setVisibility(View.GONE);
			}
		}

		final TextView summaryView = (TextView) view.findViewById(R.id.summary);
		if (summaryView != null) {
			final CharSequence summary = getSummary();
			if (!TextUtils.isEmpty(summary)) {
				summaryView.setText(summary);
				summaryView.setVisibility(View.VISIBLE);
			} else {
				summaryView.setVisibility(View.GONE);
			}
		}

		ImageView imageView = (ImageView) view.findViewById(R.id.icon);
		if (imageView != null) {
			if (mIconResId != 0 || mIcon != null) {
				if (mIcon == null) {
					mIcon = getContext().getResources().getDrawable(mIconResId);
				}
				if (mIcon != null) {
					imageView.setImageDrawable(mIcon);
				}
			}
			imageView.setVisibility(mIcon != null ? View.VISIBLE : View.GONE);
		}

		if (mShouldDisableView) {
			setEnabledStateOnViews(view, isEnabled());
		}
	}

	/**
	 * On click.
	 */
	protected void onClick() {
	}

	/**
	 * On create view.
	 *
	 * @param parent the parent
	 * @return the view
	 */
	protected View onCreateView(ViewGroup parent) {
		final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
		final View layout = layoutInflater.inflate(mLayoutResId, parent, false);
		final ViewGroup widgetFrame = (ViewGroup) layout
				.findViewById(R.id.widget_frame);
		if (widgetFrame != null) {
			if (mWidgetLayoutResId != 0) {
				layoutInflater.inflate(mWidgetLayoutResId, widgetFrame);
			} else {
				widgetFrame.setVisibility(View.GONE);
			}
		}
		return layout;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.OnDependencyChangeListener#onDependencyChanged(com.WazaBe.HoloEverywhere.preference.Preference, boolean)
	 */
	@Override
	public void onDependencyChanged(Preference dependency,
			boolean disableDependent) {
		if (mDependencyMet == disableDependent) {
			mDependencyMet = !disableDependent;
			notifyDependencyChange(shouldDisableDependents());
			notifyChanged();
		}
	}

	/**
	 * On get default value.
	 *
	 * @param a the a
	 * @param index the index
	 * @return the object
	 */
	protected Object onGetDefaultValue(TypedArray a, int index) {
		return null;
	}

	/**
	 * On key.
	 *
	 * @param v the v
	 * @param keyCode the key code
	 * @param event the event
	 * @return true, if successful
	 */
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		return false;
	}

	/**
	 * On prepare for removal.
	 */
	protected void onPrepareForRemoval() {
		unregisterDependency();
	}

	/**
	 * On restore instance state.
	 *
	 * @param state the state
	 */
	protected void onRestoreInstanceState(Parcelable state) {
		mBaseMethodCalled = true;
		if (state != AbsSavedState.EMPTY_STATE && state != null) {
			throw new IllegalArgumentException(
					"Wrong state class -- expecting Preference State");
		}
	}

	/**
	 * On save instance state.
	 *
	 * @return the parcelable
	 */
	protected Parcelable onSaveInstanceState() {
		mBaseMethodCalled = true;
		return AbsSavedState.EMPTY_STATE;
	}

	/**
	 * On set initial value.
	 *
	 * @param restorePersistedValue the restore persisted value
	 * @param defaultValue the default value
	 */
	protected void onSetInitialValue(boolean restorePersistedValue,
			Object defaultValue) {
	}

	/**
	 * Peek extras.
	 *
	 * @return the bundle
	 */
	public Bundle peekExtras() {
		return mExtras;
	}

	/**
	 * Perform click.
	 *
	 * @param preferenceScreen the preference screen
	 */
	void performClick(PreferenceScreen preferenceScreen) {

		if (!isEnabled()) {
			return;
		}

		onClick();

		if (mOnClickListener != null
				&& mOnClickListener.onPreferenceClick(this)) {
			return;
		}

		PreferenceManager preferenceManager = getPreferenceManager();
		if (preferenceManager != null) {
			PreferenceManager.OnPreferenceTreeClickListener listener = preferenceManager
					.getOnPreferenceTreeClickListener();
			if (preferenceScreen != null && listener != null
					&& listener.onPreferenceTreeClick(preferenceScreen, this)) {
				return;
			}
		}

		if (mIntent != null) {
			Context context = getContext();
			context.startActivity(mIntent);
		}
	}

	/**
	 * Persist boolean.
	 *
	 * @param value the value
	 * @return true, if successful
	 */
	protected boolean persistBoolean(boolean value) {
		if (shouldPersist()) {
			if (value == getPersistedBoolean(!value)) {
				// It's already there, so the same as persisting
				return true;
			}

			SharedPreferences.Editor editor = mPreferenceManager.getEditor();
			editor.putBoolean(mKey, value);
			tryCommit(editor);
			return true;
		}
		return false;
	}

	/**
	 * Persist float.
	 *
	 * @param value the value
	 * @return true, if successful
	 */
	protected boolean persistFloat(float value) {
		if (shouldPersist()) {
			if (value == getPersistedFloat(Float.NaN)) {
				// It's already there, so the same as persisting
				return true;
			}

			SharedPreferences.Editor editor = mPreferenceManager.getEditor();
			editor.putFloat(mKey, value);
			tryCommit(editor);
			return true;
		}
		return false;
	}

	/**
	 * Persist int.
	 *
	 * @param value the value
	 * @return true, if successful
	 */
	protected boolean persistInt(int value) {
		if (shouldPersist()) {
			if (value == getPersistedInt(~value)) {
				// It's already there, so the same as persisting
				return true;
			}

			SharedPreferences.Editor editor = mPreferenceManager.getEditor();
			editor.putInt(mKey, value);
			tryCommit(editor);
			return true;
		}
		return false;
	}

	/**
	 * Persist long.
	 *
	 * @param value the value
	 * @return true, if successful
	 */
	protected boolean persistLong(long value) {
		if (shouldPersist()) {
			if (value == getPersistedLong(~value)) {
				// It's already there, so the same as persisting
				return true;
			}

			SharedPreferences.Editor editor = mPreferenceManager.getEditor();
			editor.putLong(mKey, value);
			tryCommit(editor);
			return true;
		}
		return false;
	}

	/**
	 * Persist string.
	 *
	 * @param value the value
	 * @return true, if successful
	 */
	protected boolean persistString(String value) {
		if (shouldPersist()) {
			// Shouldn't store null
			if (value == getPersistedString(null)) {
				// It's already there, so the same as persisting
				return true;
			}

			SharedPreferences.Editor editor = mPreferenceManager.getEditor();
			editor.putString(mKey, value);
			tryCommit(editor);
			return true;
		}
		return false;
	}

	/**
	 * Persist string set.
	 *
	 * @param values the values
	 * @return true, if successful
	 */
	protected boolean persistStringSet(Set<String> values) {
		if (shouldPersist()) {
			if (values.equals(getPersistedStringSet(null))) {
				return true;
			}
			SharedPreferences.Editor editor = mPreferenceManager.getEditor();
			editor.putStringSet(mKey, values);
			tryCommit(editor);
			return true;
		}
		return false;
	}

	/**
	 * Register dependency.
	 */
	private void registerDependency() {
		if (TextUtils.isEmpty(mDependencyKey)) {
			return;
		}

		Preference preference = findPreferenceInHierarchy(mDependencyKey);
		if (preference != null) {
			preference.registerDependent(this);
		} else {
			throw new IllegalStateException("Dependency \"" + mDependencyKey
					+ "\" not found for preference \"" + mKey + "\" (title: \""
					+ mTitle + "\"");
		}
	}

	/**
	 * Register dependent.
	 *
	 * @param dependent the dependent
	 */
	private void registerDependent(Preference dependent) {
		if (mDependents == null) {
			mDependents = new ArrayList<Preference>();
		}

		mDependents.add(dependent);

		dependent.onDependencyChanged(this, shouldDisableDependents());
	}

	/**
	 * Require key.
	 */
	void requireKey() {
		if (mKey == null) {
			throw new IllegalStateException(
					"Preference does not have a key assigned.");
		}

		mRequiresKey = true;
	}

	/**
	 * Restore hierarchy state.
	 *
	 * @param container the container
	 */
	public void restoreHierarchyState(Bundle container) {
		dispatchRestoreInstanceState(container);
	}

	/**
	 * Save hierarchy state.
	 *
	 * @param container the container
	 */
	public void saveHierarchyState(Bundle container) {
		dispatchSaveInstanceState(container);
	}

	/**
	 * Sets the default value.
	 *
	 * @param defaultValue the new default value
	 */
	public void setDefaultValue(Object defaultValue) {
		mDefaultValue = defaultValue;
	}

	/**
	 * Sets the dependency.
	 *
	 * @param dependencyKey the new dependency
	 */
	public void setDependency(String dependencyKey) {
		unregisterDependency();
		mDependencyKey = dependencyKey;
		registerDependency();
	}

	/**
	 * Sets the enabled.
	 *
	 * @param enabled the new enabled
	 */
	public void setEnabled(boolean enabled) {
		if (mEnabled != enabled) {
			mEnabled = enabled;
			notifyDependencyChange(shouldDisableDependents());
			notifyChanged();
		}
	}

	/**
	 * Sets the enabled state on views.
	 *
	 * @param v the v
	 * @param enabled the enabled
	 */
	private void setEnabledStateOnViews(View v, boolean enabled) {
		v.setEnabled(enabled);

		if (v instanceof ViewGroup) {
			final ViewGroup vg = (ViewGroup) v;
			for (int i = vg.getChildCount() - 1; i >= 0; i--) {
				setEnabledStateOnViews(vg.getChildAt(i), enabled);
			}
		}
	}

	/**
	 * Sets the fragment.
	 *
	 * @param fragment the new fragment
	 */
	public void setFragment(String fragment) {
		mFragment = fragment;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(Drawable icon) {
		if (icon == null && mIcon != null || icon != null && mIcon != icon) {
			mIcon = icon;

			notifyChanged();
		}
	}

	/**
	 * Sets the icon.
	 *
	 * @param iconResId the new icon
	 */
	public void setIcon(int iconResId) {
		mIconResId = iconResId;
		setIcon(mContext.getResources().getDrawable(iconResId));
	}

	/**
	 * Sets the intent.
	 *
	 * @param intent the new intent
	 */
	public void setIntent(Intent intent) {
		mIntent = intent;
	}

	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(String key) {
		mKey = key;

		if (mRequiresKey && !hasKey()) {
			requireKey();
		}
	}

	/**
	 * Sets the layout resource.
	 *
	 * @param layoutResId the new layout resource
	 */
	public void setLayoutResource(int layoutResId) {
		if (layoutResId != mLayoutResId) {
			// Layout changed
			mHasSpecifiedLayout = true;
		}

		mLayoutResId = layoutResId;
	}

	/**
	 * Sets the on preference change internal listener.
	 *
	 * @param listener the new on preference change internal listener
	 */
	final void setOnPreferenceChangeInternalListener(
			OnPreferenceChangeInternalListener listener) {
		mListener = listener;
	}

	/**
	 * Sets the on preference change listener.
	 *
	 * @param onPreferenceChangeListener the new on preference change listener
	 */
	public void setOnPreferenceChangeListener(
			OnPreferenceChangeListener onPreferenceChangeListener) {
		mOnChangeListener = onPreferenceChangeListener;
	}

	/**
	 * Sets the on preference click listener.
	 *
	 * @param onPreferenceClickListener the new on preference click listener
	 */
	public void setOnPreferenceClickListener(
			OnPreferenceClickListener onPreferenceClickListener) {
		mOnClickListener = onPreferenceClickListener;
	}

	/**
	 * Sets the order.
	 *
	 * @param order the new order
	 */
	public void setOrder(int order) {
		if (order != mOrder) {
			mOrder = order;

			// Reorder the list
			notifyHierarchyChanged();
		}
	}

	/**
	 * Sets the persistent.
	 *
	 * @param persistent the new persistent
	 */
	public void setPersistent(boolean persistent) {
		mPersistent = persistent;
	}

	/**
	 * Sets the selectable.
	 *
	 * @param selectable the new selectable
	 */
	public void setSelectable(boolean selectable) {
		if (mSelectable != selectable) {
			mSelectable = selectable;
			notifyChanged();
		}
	}

	/**
	 * Sets the should disable view.
	 *
	 * @param shouldDisableView the new should disable view
	 */
	public void setShouldDisableView(boolean shouldDisableView) {
		mShouldDisableView = shouldDisableView;
		notifyChanged();
	}

	/**
	 * Sets the summary.
	 *
	 * @param summary the new summary
	 */
	public void setSummary(CharSequence summary) {
		if (summary == null && mSummary != null || summary != null
				&& !summary.equals(mSummary)) {
			mSummary = summary;
			notifyChanged();
		}
	}

	/**
	 * Sets the summary.
	 *
	 * @param summaryResId the new summary
	 */
	public void setSummary(int summaryResId) {
		setSummary(mContext.getString(summaryResId));
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(CharSequence title) {
		if (title == null && mTitle != null || title != null
				&& !title.equals(mTitle)) {
			mTitleRes = 0;
			mTitle = title;
			notifyChanged();
		}
	}

	/**
	 * Sets the title.
	 *
	 * @param titleResId the new title
	 */
	public void setTitle(int titleResId) {
		setTitle(mContext.getString(titleResId));
		mTitleRes = titleResId;
	}

	/**
	 * Sets the widget layout resource.
	 *
	 * @param widgetLayoutResId the new widget layout resource
	 */
	public void setWidgetLayoutResource(int widgetLayoutResId) {
		if (widgetLayoutResId != mWidgetLayoutResId) {
			mHasSpecifiedLayout = true;
		}
		mWidgetLayoutResId = widgetLayoutResId;
	}

	/**
	 * Should commit.
	 *
	 * @return true, if successful
	 */
	public boolean shouldCommit() {
		if (mPreferenceManager == null) {
			return false;
		}

		return mPreferenceManager.shouldCommit();
	}

	/**
	 * Should disable dependents.
	 *
	 * @return true, if successful
	 */
	public boolean shouldDisableDependents() {
		return !isEnabled();
	}

	/**
	 * Should persist.
	 *
	 * @return true, if successful
	 */
	protected boolean shouldPersist() {
		return mPreferenceManager != null && isPersistent() && hasKey();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getFilterableStringBuilder().toString();
	}

	/**
	 * Try commit.
	 *
	 * @param editor the editor
	 */
	private void tryCommit(SharedPreferences.Editor editor) {
		if (mPreferenceManager.shouldCommit()) {
			try {
				editor.apply();
			} catch (AbstractMethodError unused) {
				editor.commit();
			}
		}
	}

	/**
	 * Unregister dependency.
	 */
	private void unregisterDependency() {
		if (mDependencyKey != null) {
			final Preference oldDependency = findPreferenceInHierarchy(mDependencyKey);
			if (oldDependency != null) {
				oldDependency.unregisterDependent(this);
			}
		}
	}

	/**
	 * Unregister dependent.
	 *
	 * @param dependent the dependent
	 */
	private void unregisterDependent(Preference dependent) {
		if (mDependents != null) {
			mDependents.remove(dependent);
		}
	}
}