package com.WazaBe.HoloEverywhere.preference;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.WazaBe.HoloEverywhere.LayoutInflater;
import org.bitbucket.fredgrott.gwslaf.R;
import com.WazaBe.HoloEverywhere.app.AlertDialog;

// TODO: Auto-generated Javadoc
/**
 * The Class PreferenceScreen.
 */
public final class PreferenceScreen extends PreferenceGroup implements
		AdapterView.OnItemClickListener, DialogInterface.OnDismissListener {
	
	/**
	 * The Class SavedState.
	 */
	private static class SavedState extends BaseSavedState {
		
		/** The dialog bundle. */
		Bundle dialogBundle;
		
		/** The is dialog showing. */
		boolean isDialogShowing;

		/**
		 * Instantiates a new saved state.
		 *
		 * @param source the source
		 */
		public SavedState(Parcel source) {
			super(source);
			isDialogShowing = source.readInt() == 1;
			dialogBundle = source.readBundle();
		}

		/**
		 * Instantiates a new saved state.
		 *
		 * @param superState the super state
		 */
		public SavedState(Parcelable superState) {
			super(superState);
		}

		/* (non-Javadoc)
		 * @see android.view.AbsSavedState#writeToParcel(android.os.Parcel, int)
		 */
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(isDialogShowing ? 1 : 0);
			dest.writeBundle(dialogBundle);
		}
	}

	/** The m dialog. */
	private AlertDialog mDialog;
	
	/** The m list view. */
	private ListView mListView;
	
	/** The m root adapter. */
	private ListAdapter mRootAdapter;

	/**
	 * Instantiates a new preference screen.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public PreferenceScreen(Context context, AttributeSet attrs) {
		super(context, attrs, R.attr.preferenceScreenStyle);
	}

	/**
	 * Bind.
	 *
	 * @param listView the list view
	 */
	public void bind(ListView listView) {
		listView.setOnItemClickListener(this);
		listView.setAdapter(getRootAdapter());
		onAttachedToActivity();
	}

	/**
	 * Gets the dialog.
	 *
	 * @return the dialog
	 */
	public AlertDialog getDialog() {
		return mDialog;
	}

	/**
	 * Gets the root adapter.
	 *
	 * @return the root adapter
	 */
	public ListAdapter getRootAdapter() {
		if (mRootAdapter == null) {
			mRootAdapter = onCreateRootAdapter();
		}

		return mRootAdapter;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.PreferenceGroup#isOnSameScreenAsChildren()
	 */
	@Override
	protected boolean isOnSameScreenAsChildren() {
		return false;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onClick()
	 */
	@Override
	protected void onClick() {
		if (getIntent() != null || getFragment() != null
				|| getPreferenceCount() == 0) {
			return;
		}

		showDialog(null);
	}

	/**
	 * On create root adapter.
	 *
	 * @return the list adapter
	 */
	protected ListAdapter onCreateRootAdapter() {
		return new PreferenceGroupAdapter(this);
	}

	/* (non-Javadoc)
	 * @see android.content.DialogInterface.OnDismissListener#onDismiss(android.content.DialogInterface)
	 */
	@Override
	public void onDismiss(DialogInterface dialog) {
		mDialog = null;
		getPreferenceManager().removePreferencesScreen(dialog);
	}

	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (parent instanceof ListView) {
			position -= ((ListView) parent).getHeaderViewsCount();
		}
		Object item = getRootAdapter().getItem(position);
		if (!(item instanceof Preference)) {
			return;
		}

		final Preference preference = (Preference) item;
		preference.performClick(this);
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onRestoreInstanceState(android.os.Parcelable)
	 */
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state == null || !state.getClass().equals(SavedState.class)) {
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState myState = (SavedState) state;
		super.onRestoreInstanceState(myState.getSuperState());
		if (myState.isDialogShowing) {
			showDialog(myState.dialogBundle);
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onSaveInstanceState()
	 */
	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState = super.onSaveInstanceState();
		final AlertDialog dialog = mDialog;
		if (dialog == null || !dialog.isShowing()) {
			return superState;
		}

		final SavedState myState = new SavedState(superState);
		myState.isDialogShowing = true;
		myState.dialogBundle = dialog.onSaveInstanceState();
		return myState;
	}

	/**
	 * Show dialog.
	 *
	 * @param state the state
	 */
	private void showDialog(Bundle state) {
		Context context = getContext();
		if (mListView != null) {
			mListView.setAdapter(null);
		}
		LayoutInflater inflater = LayoutInflater.from(context);
		View childPrefScreen = inflater.inflate(
				R.layout.preference_list_fragment, null);
		mListView = (ListView) childPrefScreen.findViewById(android.R.id.list);
		bind(mListView);
		final CharSequence title = getTitle();
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setView(childPrefScreen);
		mDialog = builder.create();
		mDialog.setOnDismissListener(this);
		if (state != null) {
			mDialog.onRestoreInstanceState(state);
		}
		getPreferenceManager().addPreferencesScreen(mDialog);
		mDialog.show();
	}
}