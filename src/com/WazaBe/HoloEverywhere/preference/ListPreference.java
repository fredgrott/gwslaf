package com.WazaBe.HoloEverywhere.preference;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;

import org.bitbucket.fredgrott.gwslaf.R;
import com.WazaBe.HoloEverywhere.app.AlertDialog.Builder;

// TODO: Auto-generated Javadoc
/**
 * The Class ListPreference.
 */
public class ListPreference extends DialogPreference {
	
	/**
	 * The Class SavedState.
	 */
	private static class SavedState extends BaseSavedState {
		
		/** The value. */
		String value;

		/**
		 * Instantiates a new saved state.
		 *
		 * @param source the source
		 */
		public SavedState(Parcel source) {
			super(source);
			value = source.readString();
		}

		/**
		 * Instantiates a new saved state.
		 *
		 * @param superState the super state
		 */
		public SavedState(Parcelable superState) {
			super(superState);
		}

		/* (non-Javadoc)
		 * @see android.view.AbsSavedState#writeToParcel(android.os.Parcel, int)
		 */
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeString(value);
		}
	}

	/** The m clicked dialog entry index. */
	private int mClickedDialogEntryIndex;
	
	/** The m entries. */
	private CharSequence[] mEntries;
	
	/** The m entry values. */
	private CharSequence[] mEntryValues;
	
	/** The m summary. */
	private String mSummary;

	/** The m value. */
	private String mValue;

	/**
	 * Instantiates a new list preference.
	 *
	 * @param context the context
	 */
	public ListPreference(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new list preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public ListPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.ListPreference, 0, 0);
		mEntries = a.getTextArray(R.styleable.ListPreference_entries);
		mEntryValues = a.getTextArray(R.styleable.ListPreference_entryValues);
		a.recycle();
		a = context.obtainStyledAttributes(attrs, R.styleable.Preference, 0, 0);
		mSummary = a.getString(R.styleable.Preference_summary);
		a.recycle();
	}

	/**
	 * Find index of value.
	 *
	 * @param value the value
	 * @return the int
	 */
	public int findIndexOfValue(String value) {
		if (value != null && mEntryValues != null) {
			for (int i = mEntryValues.length - 1; i >= 0; i--) {
				if (mEntryValues[i].equals(value)) {
					return i;
				}
			}
		}
		return -1;
	}

	/**
	 * Gets the entries.
	 *
	 * @return the entries
	 */
	public CharSequence[] getEntries() {
		return mEntries;
	}

	/**
	 * Gets the entry.
	 *
	 * @return the entry
	 */
	public CharSequence getEntry() {
		int index = getValueIndex();
		return index >= 0 && mEntries != null ? mEntries[index] : null;
	}

	/**
	 * Gets the entry values.
	 *
	 * @return the entry values
	 */
	public CharSequence[] getEntryValues() {
		return mEntryValues;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#getSummary()
	 */
	@Override
	public CharSequence getSummary() {
		final CharSequence entry = getEntry();
		if (mSummary == null || entry == null) {
			return super.getSummary();
		} else {
			return String.format(mSummary, entry);
		}
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return mValue;
	}

	/**
	 * Gets the value index.
	 *
	 * @return the value index
	 */
	private int getValueIndex() {
		return findIndexOfValue(mValue);
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onDialogClosed(boolean)
	 */
	@Override
	protected void onDialogClosed(boolean positiveResult) {
		super.onDialogClosed(positiveResult);
		if (positiveResult && mClickedDialogEntryIndex >= 0
				&& mEntryValues != null) {
			String value = mEntryValues[mClickedDialogEntryIndex].toString();
			if (callChangeListener(value)) {
				setValue(value);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onGetDefaultValue(android.content.res.TypedArray, int)
	 */
	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		return a.getString(index);
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onPrepareDialogBuilder(com.WazaBe.HoloEverywhere.app.AlertDialog.Builder)
	 */
	@Override
	protected void onPrepareDialogBuilder(Builder builder) {
		super.onPrepareDialogBuilder(builder);
		if (mEntries == null || mEntryValues == null) {
			throw new IllegalStateException(
					"ListPreference requires an entries array and an entryValues array.");
		}
		mClickedDialogEntryIndex = getValueIndex();
		builder.setSingleChoiceItems(mEntries, mClickedDialogEntryIndex,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mClickedDialogEntryIndex = which;
						ListPreference.this.onClick(dialog,
								DialogInterface.BUTTON_POSITIVE);
						dialog.dismiss();
					}
				});
		builder.setPositiveButton(null, null);
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onRestoreInstanceState(android.os.Parcelable)
	 */
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state == null || !state.getClass().equals(SavedState.class)) {
			super.onRestoreInstanceState(state);
			return;
		}
		SavedState myState = (SavedState) state;
		super.onRestoreInstanceState(myState.getSuperState());
		setValue(myState.value);
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onSaveInstanceState()
	 */
	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState = super.onSaveInstanceState();
		if (isPersistent()) {
			return superState;
		}
		final SavedState myState = new SavedState(superState);
		myState.value = getValue();
		return myState;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onSetInitialValue(boolean, java.lang.Object)
	 */
	@Override
	protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
		setValue(restoreValue ? getPersistedString(mValue)
				: (String) defaultValue);
	}

	/**
	 * Sets the entries.
	 *
	 * @param entries the new entries
	 */
	public void setEntries(CharSequence[] entries) {
		mEntries = entries;
	}

	/**
	 * Sets the entries.
	 *
	 * @param entriesResId the new entries
	 */
	public void setEntries(int entriesResId) {
		setEntries(getContext().getResources().getTextArray(entriesResId));
	}

	/**
	 * Sets the entry values.
	 *
	 * @param entryValues the new entry values
	 */
	public void setEntryValues(CharSequence[] entryValues) {
		mEntryValues = entryValues;
	}

	/**
	 * Sets the entry values.
	 *
	 * @param entryValuesResId the new entry values
	 */
	public void setEntryValues(int entryValuesResId) {
		setEntryValues(getContext().getResources().getTextArray(
				entryValuesResId));
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#setSummary(java.lang.CharSequence)
	 */
	@Override
	public void setSummary(CharSequence summary) {
		super.setSummary(summary);
		if (summary == null && mSummary != null) {
			mSummary = null;
		} else if (summary != null && !summary.equals(mSummary)) {
			mSummary = summary.toString();
		}
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		mValue = value;
		persistString(value);
	}

	/**
	 * Sets the value index.
	 *
	 * @param index the new value index
	 */
	public void setValueIndex(int index) {
		if (mEntryValues != null) {
			setValue(mEntryValues[index].toString());
		}
	}

}