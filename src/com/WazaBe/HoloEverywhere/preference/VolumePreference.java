package com.WazaBe.HoloEverywhere.preference;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.Settings;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class VolumePreference.
 */
public class VolumePreference extends SeekBarDialogPreference implements
		PreferenceManager.OnActivityStopListener, View.OnKeyListener {
	
	/**
	 * The Class SavedState.
	 */
	private static class SavedState extends BaseSavedState {
		
		/** The m volume store. */
		VolumeStore mVolumeStore = new VolumeStore();

		/**
		 * Instantiates a new saved state.
		 *
		 * @param source the source
		 */
		public SavedState(Parcel source) {
			super(source);
			mVolumeStore.volume = source.readInt();
			mVolumeStore.originalVolume = source.readInt();
		}

		/**
		 * Instantiates a new saved state.
		 *
		 * @param superState the super state
		 */
		public SavedState(Parcelable superState) {
			super(superState);
		}

		/**
		 * Gets the volume store.
		 *
		 * @return the volume store
		 */
		VolumeStore getVolumeStore() {
			return mVolumeStore;
		}

		/* (non-Javadoc)
		 * @see android.view.AbsSavedState#writeToParcel(android.os.Parcel, int)
		 */
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(mVolumeStore.volume);
			dest.writeInt(mVolumeStore.originalVolume);
		}
	}

	/**
	 * The Class SeekBarVolumizer.
	 */
	public class SeekBarVolumizer implements OnSeekBarChangeListener, Runnable {
		
		/** The m audio manager. */
		private AudioManager mAudioManager;
		
		/** The m context. */
		private Context mContext;
		
		/** The m handler. */
		private Handler mHandler = new Handler();
		
		/** The m last progress. */
		private int mLastProgress = -1;
		
		/** The m original stream volume. */
		private int mOriginalStreamVolume;
		
		/** The m ringtone. */
		private Ringtone mRingtone;
		
		/** The m seek bar. */
		private SeekBar mSeekBar;
		
		/** The m stream type. */
		private int mStreamType;
		
		/** The m volume before mute. */
		private int mVolumeBeforeMute = -1;

		/** The m volume observer. */
		private ContentObserver mVolumeObserver = new ContentObserver(mHandler) {
			@Override
			public void onChange(boolean selfChange) {
				super.onChange(selfChange);
				if (mSeekBar != null && mAudioManager != null) {
					int volume = mAudioManager.getStreamVolume(mStreamType);
					mSeekBar.setProgress(volume);
				}
			}
		};

		/**
		 * Instantiates a new seek bar volumizer.
		 *
		 * @param context the context
		 * @param seekBar the seek bar
		 * @param streamType the stream type
		 */
		public SeekBarVolumizer(Context context, SeekBar seekBar, int streamType) {
			this(context, seekBar, streamType, null);
		}

		/**
		 * Instantiates a new seek bar volumizer.
		 *
		 * @param context the context
		 * @param seekBar the seek bar
		 * @param streamType the stream type
		 * @param defaultUri the default uri
		 */
		public SeekBarVolumizer(Context context, SeekBar seekBar,
				int streamType, Uri defaultUri) {
			mContext = context;
			mAudioManager = (AudioManager) context
					.getSystemService(Context.AUDIO_SERVICE);
			mStreamType = streamType;
			mSeekBar = seekBar;

			initSeekBar(seekBar, defaultUri);
		}

		/**
		 * Change volume by.
		 *
		 * @param amount the amount
		 */
		public void changeVolumeBy(int amount) {
			mSeekBar.incrementProgressBy(amount);
			if (!isSamplePlaying()) {
				startSample();
			}
			postSetVolume(mSeekBar.getProgress());
			mVolumeBeforeMute = -1;
		}

		/**
		 * Gets the seek bar.
		 *
		 * @return the seek bar
		 */
		public SeekBar getSeekBar() {
			return mSeekBar;
		}

		/**
		 * Inits the seek bar.
		 *
		 * @param seekBar the seek bar
		 * @param defaultUri the default uri
		 */
		@SuppressLint("NewApi")
		private void initSeekBar(SeekBar seekBar, Uri defaultUri) {
			seekBar.setMax(mAudioManager.getStreamMaxVolume(mStreamType));
			mOriginalStreamVolume = mAudioManager.getStreamVolume(mStreamType);
			seekBar.setProgress(mOriginalStreamVolume);
			seekBar.setOnSeekBarChangeListener(this);
			// TODO fix content observer
			/*
			 * mContext.getContentResolver().registerContentObserver(
			 * System.getUriFor(System.VOLUME_SETTINGS[mStreamType]), false,
			 * mVolumeObserver);
			 */
			if (defaultUri == null) {
				if (mStreamType == AudioManager.STREAM_RING) {
					defaultUri = Settings.System.DEFAULT_RINGTONE_URI;
				} else if (mStreamType == AudioManager.STREAM_NOTIFICATION) {
					defaultUri = Settings.System.DEFAULT_NOTIFICATION_URI;
				} else {
					if (VERSION.SDK_INT >= 5) {
						defaultUri = Settings.System.DEFAULT_ALARM_ALERT_URI;
					} else {
						defaultUri = Settings.System.DEFAULT_RINGTONE_URI;
					}
				}
			}
			mRingtone = RingtoneManager.getRingtone(mContext, defaultUri);

			if (mRingtone != null) {
				mRingtone.setStreamType(mStreamType);
			}
		}

		/**
		 * Checks if is sample playing.
		 *
		 * @return true, if is sample playing
		 */
		public boolean isSamplePlaying() {
			return mRingtone != null && mRingtone.isPlaying();
		}

		/**
		 * Mute volume.
		 */
		public void muteVolume() {
			if (mVolumeBeforeMute != -1) {
				mSeekBar.setProgress(mVolumeBeforeMute);
				startSample();
				postSetVolume(mVolumeBeforeMute);
				mVolumeBeforeMute = -1;
			} else {
				mVolumeBeforeMute = mSeekBar.getProgress();
				mSeekBar.setProgress(0);
				stopSample();
				postSetVolume(0);
			}
		}

		/* (non-Javadoc)
		 * @see android.widget.SeekBar.OnSeekBarChangeListener#onProgressChanged(android.widget.SeekBar, int, boolean)
		 */
		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromTouch) {
			if (!fromTouch) {
				return;
			}

			postSetVolume(progress);
		}

		/**
		 * On restore instance state.
		 *
		 * @param volumeStore the volume store
		 */
		public void onRestoreInstanceState(VolumeStore volumeStore) {
			if (volumeStore.volume != -1) {
				mOriginalStreamVolume = volumeStore.originalVolume;
				mLastProgress = volumeStore.volume;
				postSetVolume(mLastProgress);
			}
		}

		/**
		 * On save instance state.
		 *
		 * @param volumeStore the volume store
		 */
		public void onSaveInstanceState(VolumeStore volumeStore) {
			if (mLastProgress >= 0) {
				volumeStore.volume = mLastProgress;
				volumeStore.originalVolume = mOriginalStreamVolume;
			}
		}

		/* (non-Javadoc)
		 * @see android.widget.SeekBar.OnSeekBarChangeListener#onStartTrackingTouch(android.widget.SeekBar)
		 */
		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
		}

		/* (non-Javadoc)
		 * @see android.widget.SeekBar.OnSeekBarChangeListener#onStopTrackingTouch(android.widget.SeekBar)
		 */
		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			if (!isSamplePlaying()) {
				startSample();
			}
		}

		/**
		 * Post set volume.
		 *
		 * @param progress the progress
		 */
		void postSetVolume(int progress) {
			mLastProgress = progress;
			mHandler.removeCallbacks(this);
			mHandler.post(this);
		}

		/**
		 * Revert volume.
		 */
		public void revertVolume() {
			mAudioManager
					.setStreamVolume(mStreamType, mOriginalStreamVolume, 0);
		}

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			mAudioManager.setStreamVolume(mStreamType, mLastProgress, 0);
		}

		/**
		 * Start sample.
		 */
		public void startSample() {
			onSampleStarting(this);
			if (mRingtone != null) {
				mRingtone.play();
			}
		}

		/**
		 * Stop.
		 */
		public void stop() {
			stopSample();
			mContext.getContentResolver().unregisterContentObserver(
					mVolumeObserver);
			mSeekBar.setOnSeekBarChangeListener(null);
		}

		/**
		 * Stop sample.
		 */
		public void stopSample() {
			if (mRingtone != null) {
				mRingtone.stop();
			}
		}
	}

	/**
	 * The Class VolumeStore.
	 */
	public static class VolumeStore {
		
		/** The original volume. */
		public int originalVolume = -1;
		
		/** The volume. */
		public int volume = -1;
	}

	/** The m seek bar volumizer. */
	private SeekBarVolumizer mSeekBarVolumizer;

	/** The m stream type. */
	private int mStreamType;

	/**
	 * Instantiates a new volume preference.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public VolumePreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.VolumePreference, 0, 0);
		mStreamType = a.getInt(R.styleable.VolumePreference_streamType, 0);
		a.recycle();
	}

	/**
	 * Cleanup.
	 */
	private void cleanup() {
		getPreferenceManager().unregisterOnActivityStopListener(this);

		if (mSeekBarVolumizer != null) {
			Dialog dialog = getDialog();
			if (dialog != null && dialog.isShowing()) {
				View view = dialog.getWindow().getDecorView()
						.findViewById(R.id.seekbar);
				if (view != null) {
					view.setOnKeyListener(null);
				}
				mSeekBarVolumizer.revertVolume();
			}
			mSeekBarVolumizer.stop();
			mSeekBarVolumizer = null;
		}

	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.PreferenceManager.OnActivityStopListener#onActivityStop()
	 */
	@Override
	public void onActivityStop() {
		if (mSeekBarVolumizer != null) {
			mSeekBarVolumizer.stopSample();
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.SeekBarDialogPreference#onBindDialogView(android.view.View)
	 */
	@Override
	protected void onBindDialogView(View view) {
		super.onBindDialogView(view);
		final SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekbar);
		mSeekBarVolumizer = new SeekBarVolumizer(getContext(), seekBar,
				mStreamType);
		getPreferenceManager().registerOnActivityStopListener(this);
		view.setOnKeyListener(this);
		view.setFocusableInTouchMode(true);
		view.requestFocus();
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onDialogClosed(boolean)
	 */
	@Override
	protected void onDialogClosed(boolean positiveResult) {
		super.onDialogClosed(positiveResult);

		if (!positiveResult && mSeekBarVolumizer != null) {
			mSeekBarVolumizer.revertVolume();
		}

		cleanup();
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onKey(android.view.View, int, android.view.KeyEvent)
	 */
	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (mSeekBarVolumizer == null) {
			return true;
		}
		boolean isdown = event.getAction() == KeyEvent.ACTION_DOWN;
		switch (keyCode) {
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			if (isdown) {
				mSeekBarVolumizer.changeVolumeBy(-1);
			}
			return true;
		case KeyEvent.KEYCODE_VOLUME_UP:
			if (isdown) {
				mSeekBarVolumizer.changeVolumeBy(1);
			}
			return true;
		case KeyEvent.KEYCODE_VOLUME_MUTE:
			if (isdown) {
				mSeekBarVolumizer.muteVolume();
			}
			return true;
		default:
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onRestoreInstanceState(android.os.Parcelable)
	 */
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state == null || !state.getClass().equals(SavedState.class)) {
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState myState = (SavedState) state;
		super.onRestoreInstanceState(myState.getSuperState());
		if (mSeekBarVolumizer != null) {
			mSeekBarVolumizer.onRestoreInstanceState(myState.getVolumeStore());
		}
	}

	/**
	 * On sample starting.
	 *
	 * @param volumizer the volumizer
	 */
	protected void onSampleStarting(SeekBarVolumizer volumizer) {
		if (mSeekBarVolumizer != null && volumizer != mSeekBarVolumizer) {
			mSeekBarVolumizer.stopSample();
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.DialogPreference#onSaveInstanceState()
	 */
	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState = super.onSaveInstanceState();
		if (isPersistent()) {
			return superState;
		}

		final SavedState myState = new SavedState(superState);
		if (mSeekBarVolumizer != null) {
			mSeekBarVolumizer.onSaveInstanceState(myState.getVolumeStore());
		}
		return myState;
	}

	/**
	 * Sets the stream type.
	 *
	 * @param streamType the new stream type
	 */
	public void setStreamType(int streamType) {
		mStreamType = streamType;
	}
}