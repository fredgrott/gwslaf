package com.WazaBe.HoloEverywhere.preference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class PreferenceGroup.
 */
public abstract class PreferenceGroup extends Preference implements
		GenericInflater.Parent<Preference> {
	
	/** The m attached to activity. */
	private boolean mAttachedToActivity = false;
	
	/** The m current preference order. */
	private int mCurrentPreferenceOrder = 0;
	
	/** The m ordering as added. */
	private boolean mOrderingAsAdded = true;
	
	/** The m preference list. */
	private List<Preference> mPreferenceList;

	/**
	 * Instantiates a new preference group.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public PreferenceGroup(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	/**
	 * Instantiates a new preference group.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public PreferenceGroup(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mPreferenceList = new ArrayList<Preference>();
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.PreferenceGroup, defStyle, 0);
		mOrderingAsAdded = a.getBoolean(
				R.styleable.PreferenceGroup_orderingFromXml, mOrderingAsAdded);
		a.recycle();
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.GenericInflater.Parent#addItemFromInflater(java.lang.Object)
	 */
	@Override
	public void addItemFromInflater(Preference preference) {
		addPreference(preference);
	}

	/**
	 * Adds the preference.
	 *
	 * @param preference the preference
	 * @return true, if successful
	 */
	public boolean addPreference(Preference preference) {
		if (mPreferenceList.contains(preference)) {
			// Exists
			return true;
		}

		if (preference.getOrder() == Preference.DEFAULT_ORDER) {
			if (mOrderingAsAdded) {
				preference.setOrder(mCurrentPreferenceOrder++);
			}

			if (preference instanceof PreferenceGroup) {
				((PreferenceGroup) preference)
						.setOrderingAsAdded(mOrderingAsAdded);
			}
		}

		int insertionIndex = Collections.binarySearch(mPreferenceList,
				preference);
		if (insertionIndex < 0) {
			insertionIndex = insertionIndex * -1 - 1;
		}

		if (!onPrepareAddPreference(preference)) {
			return false;
		}

		synchronized (this) {
			mPreferenceList.add(insertionIndex, preference);
		}

		preference.onAttachedToHierarchy(getPreferenceManager());

		if (mAttachedToActivity) {
			preference.onAttachedToActivity();
		}

		notifyHierarchyChanged();

		return true;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#dispatchRestoreInstanceState(android.os.Bundle)
	 */
	@Override
	protected void dispatchRestoreInstanceState(Bundle container) {
		super.dispatchRestoreInstanceState(container);
		final int preferenceCount = getPreferenceCount();
		for (int i = 0; i < preferenceCount; i++) {
			getPreference(i).dispatchRestoreInstanceState(container);
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#dispatchSaveInstanceState(android.os.Bundle)
	 */
	@Override
	protected void dispatchSaveInstanceState(Bundle container) {
		super.dispatchSaveInstanceState(container);
		final int preferenceCount = getPreferenceCount();
		for (int i = 0; i < preferenceCount; i++) {
			getPreference(i).dispatchSaveInstanceState(container);
		}
	}

	/**
	 * Find preference.
	 *
	 * @param key the key
	 * @return the preference
	 */
	public Preference findPreference(CharSequence key) {
		if (TextUtils.equals(getKey(), key)) {
			return this;
		}
		final int preferenceCount = getPreferenceCount();
		for (int i = 0; i < preferenceCount; i++) {
			final Preference preference = getPreference(i);
			final String curKey = preference.getKey();

			if (curKey != null && curKey.equals(key)) {
				return preference;
			}

			if (preference instanceof PreferenceGroup) {
				final Preference returnedPreference = ((PreferenceGroup) preference)
						.findPreference(key);
				if (returnedPreference != null) {
					return returnedPreference;
				}
			}
		}

		return null;
	}

	/**
	 * Gets the preference.
	 *
	 * @param index the index
	 * @return the preference
	 */
	public Preference getPreference(int index) {
		return mPreferenceList.get(index);
	}

	/**
	 * Gets the preference count.
	 *
	 * @return the preference count
	 */
	public int getPreferenceCount() {
		return mPreferenceList.size();
	}

	/**
	 * Checks if is on same screen as children.
	 *
	 * @return true, if is on same screen as children
	 */
	protected boolean isOnSameScreenAsChildren() {
		return true;
	}

	/**
	 * Checks if is ordering as added.
	 *
	 * @return true, if is ordering as added
	 */
	public boolean isOrderingAsAdded() {
		return mOrderingAsAdded;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onAttachedToActivity()
	 */
	@Override
	protected void onAttachedToActivity() {
		super.onAttachedToActivity();
		mAttachedToActivity = true;
		final int preferenceCount = getPreferenceCount();
		for (int i = 0; i < preferenceCount; i++) {
			getPreference(i).onAttachedToActivity();
		}
	}

	/**
	 * On prepare add preference.
	 *
	 * @param preference the preference
	 * @return true, if successful
	 */
	protected boolean onPrepareAddPreference(Preference preference) {
		if (!super.isEnabled()) {
			preference.setEnabled(false);
		}

		return true;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#onPrepareForRemoval()
	 */
	@Override
	protected void onPrepareForRemoval() {
		super.onPrepareForRemoval();
		mAttachedToActivity = false;
	}

	/**
	 * Removes the all.
	 */
	public void removeAll() {
		synchronized (this) {
			List<Preference> preferenceList = mPreferenceList;
			for (int i = preferenceList.size() - 1; i >= 0; i--) {
				removePreferenceInt(preferenceList.get(0));
			}
		}
		notifyHierarchyChanged();
	}

	/**
	 * Removes the preference.
	 *
	 * @param preference the preference
	 * @return true, if successful
	 */
	public boolean removePreference(Preference preference) {
		final boolean returnValue = removePreferenceInt(preference);
		notifyHierarchyChanged();
		return returnValue;
	}

	/**
	 * Removes the preference int.
	 *
	 * @param preference the preference
	 * @return true, if successful
	 */
	private boolean removePreferenceInt(Preference preference) {
		synchronized (this) {
			preference.onPrepareForRemoval();
			return mPreferenceList.remove(preference);
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.preference.Preference#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		final int preferenceCount = getPreferenceCount();
		for (int i = 0; i < preferenceCount; i++) {
			getPreference(i).setEnabled(enabled);
		}
	}

	/**
	 * Sets the ordering as added.
	 *
	 * @param orderingAsAdded the new ordering as added
	 */
	public void setOrderingAsAdded(boolean orderingAsAdded) {
		mOrderingAsAdded = orderingAsAdded;
	}

	/**
	 * Sort preferences.
	 */
	void sortPreferences() {
		synchronized (this) {
			Collections.sort(mPreferenceList);
		}
	}
}