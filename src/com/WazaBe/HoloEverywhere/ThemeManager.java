package com.WazaBe.HoloEverywhere;

import org.bitbucket.fredgrott.gwslaf.R;

import android.content.Intent;

import com.WazaBe.HoloEverywhere.app.Activity;

// TODO: Auto-generated Javadoc
/**
 * The Class ThemeManager.
 */
public final class ThemeManager {
	
	/**
	 * The Interface ThemeGetter.
	 */
	public static interface ThemeGetter {
		
		/**
		 * Gets the theme resource.
		 *
		 * @param themeTag the theme tag
		 * @param abs the abs
		 * @return the theme resource
		 */
		public int getThemeResource(int themeTag, boolean abs);
	}

	/** The Constant HOLO_DARK. */
	public static final int HOLO_DARK = 1;
	
	/** The Constant HOLO_LIGHT. */
	public static final int HOLO_LIGHT = 2;
	
	/** The Constant THEME_DEFAULT. */
	public static final int THEME_DEFAULT = HOLO_DARK;

	/** The Constant THEME_TAG. */
	public static final String THEME_TAG = "holoeverywhere:theme";

	/** The theme getter. */
	private static ThemeGetter themeGetter;

	/**
	 * Apply theme.
	 *
	 * @param activity the activity
	 */
	public static void applyTheme(Activity activity) {
		applyTheme(activity, false);
	}

	/**
	 * Apply theme.
	 *
	 * @param activity the activity
	 * @param force the force
	 */
	public static void applyTheme(Activity activity, boolean force) {
		if (hasSpecifiedTheme(activity) || force) {
			int theme = getThemeResource(getTheme(activity),
					activity.isABSSupport());
			activity.setTheme(theme);
		}
	}

	/**
	 * Gets the theme.
	 *
	 * @param activity the activity
	 * @return the theme
	 */
	public static int getTheme(Activity activity) {
		return activity.getIntent().getIntExtra(THEME_TAG, THEME_DEFAULT);
	}

	/**
	 * Gets the theme resource.
	 *
	 * @param themeTag the theme tag
	 * @param abs the abs
	 * @return the theme resource
	 */
	public static int getThemeResource(int themeTag, boolean abs) {
		if (themeGetter != null) {
			int getterResource = themeGetter.getThemeResource(themeTag, abs);
			if (getterResource > 0) {
				return getterResource;
			}
		}
		switch (themeTag) {
		case HOLO_DARK:
		default:
			return abs ? R.style.Holo_Theme_Sherlock : R.style.Holo_Theme;
		case HOLO_LIGHT:
			return abs ? R.style.Holo_Theme_Sherlock_Light
					: R.style.Holo_Theme_Light;
		}
	}

	/**
	 * Checks for specified theme.
	 *
	 * @param activity the activity
	 * @return true, if successful
	 */
	public static boolean hasSpecifiedTheme(Activity activity) {
		return activity.getIntent().hasExtra(THEME_TAG)
				&& activity.getIntent().getIntExtra(THEME_TAG, 0) > 0;
	}

	/**
	 * Restart with theme.
	 *
	 * @param activity the activity
	 * @param theme the theme
	 */
	public static void restartWithTheme(Activity activity, int theme) {
		restartWithTheme(activity, theme, false);
	}

	/**
	 * Restart with theme.
	 *
	 * @param activity the activity
	 * @param theme the theme
	 * @param force the force
	 */
	public static void restartWithTheme(Activity activity, int theme,
			boolean force) {
		if (getTheme(activity) != theme || force) {
			Intent intent = activity.getIntent();
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra(THEME_TAG, theme);
			activity.finish();
			activity.startActivity(intent);
		}
	}

	/**
	 * Sets the theme getter.
	 *
	 * @param themeGetter the new theme getter
	 */
	public static void setThemeGetter(ThemeGetter themeGetter) {
		ThemeManager.themeGetter = themeGetter;
	}

	/**
	 * Instantiates a new theme manager.
	 */
	private ThemeManager() {
	}
}
