package com.WazaBe.HoloEverywhere.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ListAdapter;
import android.widget.SpinnerAdapter;

import org.bitbucket.fredgrott.gwslaf.R;
import com.WazaBe.HoloEverywhere.app.AlertDialog;

// TODO: Auto-generated Javadoc
/**
 * The Class Spinner.
 */
public class Spinner extends AbsSpinner implements OnClickListener {
	
	/**
	 * The Class DialogPopup.
	 */
	private class DialogPopup implements SpinnerPopup,
			DialogInterface.OnClickListener {
		
		/** The m list adapter. */
		private ListAdapter mListAdapter;
		
		/** The m popup. */
		private AlertDialog mPopup;
		
		/** The m prompt. */
		private CharSequence mPrompt;

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.widget.Spinner.SpinnerPopup#dismiss()
		 */
		@Override
		public void dismiss() {
			mPopup.dismiss();
			mPopup = null;
		}

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.widget.Spinner.SpinnerPopup#getBackground()
		 */
		@Override
		public Drawable getBackground() {
			return null;
		}

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.widget.Spinner.SpinnerPopup#getHintText()
		 */
		@Override
		public CharSequence getHintText() {
			return mPrompt;
		}

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.widget.Spinner.SpinnerPopup#getHorizontalOffset()
		 */
		@Override
		public int getHorizontalOffset() {
			return 0;
		}

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.widget.Spinner.SpinnerPopup#getVerticalOffset()
		 */
		@Override
		public int getVerticalOffset() {
			return 0;
		}

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.widget.Spinner.SpinnerPopup#isShowing()
		 */
		@Override
		public boolean isShowing() {
			return mPopup != null ? mPopup.isShowing() : false;
		}

		/* (non-Javadoc)
		 * @see android.content.DialogInterface.OnClickListener#onClick(android.content.DialogInterface, int)
		 */
		@Override
		public void onClick(DialogInterface dialog, int which) {
			setSelection(which);
			if (mOnItemClickListener != null) {
				performItemClick(null, which, mListAdapter.getItemId(which));
			}
			dismiss();
		}

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.widget.Spinner.SpinnerPopup#setAdapter(android.widget.ListAdapter)
		 */
		@Override
		public void setAdapter(ListAdapter adapter) {
			mListAdapter = adapter;
		}

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.widget.Spinner.SpinnerPopup#setBackgroundDrawable(android.graphics.drawable.Drawable)
		 */
		@Override
		public void setBackgroundDrawable(Drawable bg) {
			Log.e(TAG, "Cannot set popup background for MODE_DIALOG, ignoring");
		}

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.widget.Spinner.SpinnerPopup#setHorizontalOffset(int)
		 */
		@Override
		public void setHorizontalOffset(int px) {
			Log.e(TAG, "Cannot set horizontal offset for MODE_DIALOG, ignoring");
		}

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.widget.Spinner.SpinnerPopup#setPromptText(java.lang.CharSequence)
		 */
		@Override
		public void setPromptText(CharSequence hintText) {
			mPrompt = hintText;
		}

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.widget.Spinner.SpinnerPopup#setVerticalOffset(int)
		 */
		@Override
		public void setVerticalOffset(int px) {
			Log.e(TAG, "Cannot set vertical offset for MODE_DIALOG, ignoring");
		}

		/* (non-Javadoc)
		 * @see com.WazaBe.HoloEverywhere.widget.Spinner.SpinnerPopup#show()
		 */
		@Override
		public void show() {
			AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
			if (mPrompt != null) {
				builder.setTitle(mPrompt);
			}
			mPopup = builder.setSingleChoiceItems(mListAdapter,
					getSelectedItemPosition(), this).show();
		}
	}

	/*
	 * private class DropdownPopup extends ListPopupWindow implements
	 * SpinnerPopup { private CharSequence mHintText; private ListAdapter
	 * mAdapter;
	 * 
	 * public DropdownPopup(Context context, AttributeSet attrs, int
	 * defStyleRes) { super(context, attrs, 0, defStyleRes);
	 * 
	 * setAnchorView(Spinner.this); setModal(true);
	 * setPromptPosition(POSITION_PROMPT_ABOVE); setOnItemClickListener(new
	 * AdapterView.OnItemClickListener() { public void
	 * onItemClick(AdapterView<?> parent, View v, int position, long id) {
	 * Spinner.this.setSelection(position); if (mOnItemClickListener != null) {
	 * Spinner.this.performItemClick(v, position, mAdapter.getItemId(position));
	 * } dismiss(); } }); }
	 * 
	 * @Override public void setAdapter(ListAdapter adapter) {
	 * super.setAdapter(adapter); mAdapter = adapter; }
	 * 
	 * public CharSequence getHintText() { return mHintText; }
	 * 
	 * public void setPromptText(CharSequence hintText) { mHintText = hintText;
	 * }
	 * 
	 * @Override public void show() { final Drawable background =
	 * getBackground(); int bgOffset = 0; if (background != null) {
	 * background.getPadding(mTempRect); bgOffset = -mTempRect.left; } else {
	 * mTempRect.left = mTempRect.right = 0; }
	 * 
	 * final int spinnerPaddingLeft = Spinner.this.getPaddingLeft(); if
	 * (mDropDownWidth == WRAP_CONTENT) { final int spinnerWidth =
	 * Spinner.this.getWidth(); final int spinnerPaddingRight =
	 * Spinner.this.getPaddingRight();
	 * 
	 * int contentWidth = measureContentWidth( (SpinnerAdapter) mAdapter,
	 * getBackground()); final int contentWidthLimit =
	 * getContext().getResources() .getDisplayMetrics().widthPixels -
	 * mTempRect.left - mTempRect.right; if (contentWidth > contentWidthLimit) {
	 * contentWidth = contentWidthLimit; }
	 * 
	 * setContentWidth(Math.max(contentWidth, spinnerWidth - spinnerPaddingLeft
	 * - spinnerPaddingRight)); } else if (mDropDownWidth == MATCH_PARENT) {
	 * final int spinnerWidth = Spinner.this.getWidth(); final int
	 * spinnerPaddingRight = Spinner.this.getPaddingRight();
	 * setContentWidth(spinnerWidth - spinnerPaddingLeft - spinnerPaddingRight);
	 * } else { setContentWidth(mDropDownWidth); } setHorizontalOffset(bgOffset
	 * + spinnerPaddingLeft);
	 * setInputMethodMode(ListPopupWindow.INPUT_METHOD_NOT_NEEDED);
	 * super.show(); getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
	 * setSelection(Spinner.this.getSelectedItemPosition()); } }
	 */
	/**
	 * The Class DropDownAdapter.
	 */
	private static class DropDownAdapter implements ListAdapter, SpinnerAdapter {
		
		/** The m adapter. */
		private SpinnerAdapter mAdapter;
		
		/** The m list adapter. */
		private ListAdapter mListAdapter;

		/**
		 * Instantiates a new drop down adapter.
		 *
		 * @param adapter the adapter
		 */
		public DropDownAdapter(SpinnerAdapter adapter) {
			mAdapter = adapter;
			if (adapter instanceof ListAdapter) {
				mListAdapter = (ListAdapter) adapter;
			}
		}

		/* (non-Javadoc)
		 * @see android.widget.ListAdapter#areAllItemsEnabled()
		 */
		@Override
		public boolean areAllItemsEnabled() {
			final ListAdapter adapter = mListAdapter;
			if (adapter != null) {
				return adapter.areAllItemsEnabled();
			} else {
				return true;
			}
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getCount()
		 */
		@Override
		public int getCount() {
			return mAdapter == null ? 0 : mAdapter.getCount();
		}

		/* (non-Javadoc)
		 * @see android.widget.SpinnerAdapter#getDropDownView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			return mAdapter == null ? null : mAdapter.getDropDownView(position,
					convertView, parent);
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItem(int)
		 */
		@Override
		public Object getItem(int position) {
			return mAdapter == null ? null : mAdapter.getItem(position);
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItemId(int)
		 */
		@Override
		public long getItemId(int position) {
			return mAdapter == null ? -1 : mAdapter.getItemId(position);
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItemViewType(int)
		 */
		@Override
		public int getItemViewType(int position) {
			return 0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return getDropDownView(position, convertView, parent);
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getViewTypeCount()
		 */
		@Override
		public int getViewTypeCount() {
			return 1;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#hasStableIds()
		 */
		@Override
		public boolean hasStableIds() {
			return mAdapter != null && mAdapter.hasStableIds();
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#isEmpty()
		 */
		@Override
		public boolean isEmpty() {
			return getCount() == 0;
		}

		/* (non-Javadoc)
		 * @see android.widget.ListAdapter#isEnabled(int)
		 */
		@Override
		public boolean isEnabled(int position) {
			final ListAdapter adapter = mListAdapter;
			if (adapter != null) {
				return adapter.isEnabled(position);
			} else {
				return true;
			}
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#registerDataSetObserver(android.database.DataSetObserver)
		 */
		@Override
		public void registerDataSetObserver(DataSetObserver observer) {
			if (mAdapter != null) {
				mAdapter.registerDataSetObserver(observer);
			}
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#unregisterDataSetObserver(android.database.DataSetObserver)
		 */
		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {
			if (mAdapter != null) {
				mAdapter.unregisterDataSetObserver(observer);
			}
		}
	}

	/**
	 * The Interface SpinnerPopup.
	 */
	private interface SpinnerPopup {
		
		/**
		 * Dismiss.
		 */
		public void dismiss();

		/**
		 * Gets the background.
		 *
		 * @return the background
		 */
		public Drawable getBackground();

		/**
		 * Gets the hint text.
		 *
		 * @return the hint text
		 */
		public CharSequence getHintText();

		/**
		 * Gets the horizontal offset.
		 *
		 * @return the horizontal offset
		 */
		public int getHorizontalOffset();

		/**
		 * Gets the vertical offset.
		 *
		 * @return the vertical offset
		 */
		public int getVerticalOffset();

		/**
		 * Checks if is showing.
		 *
		 * @return true, if is showing
		 */
		public boolean isShowing();

		/**
		 * Sets the adapter.
		 *
		 * @param adapter the new adapter
		 */
		public void setAdapter(ListAdapter adapter);

		/**
		 * Sets the background drawable.
		 *
		 * @param bg the new background drawable
		 */
		public void setBackgroundDrawable(Drawable bg);

		/**
		 * Sets the horizontal offset.
		 *
		 * @param px the new horizontal offset
		 */
		public void setHorizontalOffset(int px);

		/**
		 * Sets the prompt text.
		 *
		 * @param hintText the new prompt text
		 */
		public void setPromptText(CharSequence hintText);

		/**
		 * Sets the vertical offset.
		 *
		 * @param px the new vertical offset
		 */
		public void setVerticalOffset(int px);

		/**
		 * Show.
		 */
		public void show();
	}

	/** The Constant MAX_ITEMS_MEASURED. */
	private static final int MAX_ITEMS_MEASURED = 15;
	
	/** The Constant MODE_DIALOG. */
	public static final int MODE_DIALOG = 0;
	
	/** The Constant MODE_DROPDOWN. */
	public static final int MODE_DROPDOWN = 1;
	
	/** The Constant MODE_THEME. */
	private static final int MODE_THEME = -1;
	
	/** The Constant TAG. */
	private static final String TAG = "Spinner";
	
	/** The m disable children when disabled. */
	private boolean mDisableChildrenWhenDisabled;
	
	/** The m drop down width. */
	int mDropDownWidth;
	
	/** The m gravity. */
	private int mGravity;

	/** The m popup. */
	private SpinnerPopup mPopup;

	/** The m temp adapter. */
	private DropDownAdapter mTempAdapter;

	/** The m temp rect. */
	private Rect mTempRect = new Rect();

	/**
	 * Instantiates a new spinner.
	 *
	 * @param context the context
	 */
	public Spinner(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new spinner.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public Spinner(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.spinnerStyle);
	}

	/**
	 * Instantiates a new spinner.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public Spinner(Context context, AttributeSet attrs, int defStyle) {
		this(context, attrs, defStyle, MODE_THEME);
	}

	/**
	 * Instantiates a new spinner.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 * @param mode the mode
	 */
	public Spinner(Context context, AttributeSet attrs, int defStyle, int mode) {
		super(context, attrs, defStyle);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.Spinner, defStyle, R.style.Holo_Spinner);
		if (mode == MODE_THEME) {
			mode = a.getInt(R.styleable.Spinner_spinnerMode, MODE_DIALOG);
		}
		switch (mode) {
		case MODE_DIALOG: {
			mPopup = new DialogPopup();
			break;
		}
		case MODE_DROPDOWN: {
			throw new RuntimeException("Mode dropdown don't supported");
			/*
			 * DropdownPopup popup = new DropdownPopup(context, attrs,
			 * defStyle); mDropDownWidth = a.getLayoutDimension(
			 * R.styleable.Spinner_dropDownWidth,
			 * ViewGroup.LayoutParams.WRAP_CONTENT);
			 * popup.setBackgroundDrawable(a
			 * .getDrawable(R.styleable.Spinner_popupBackground)); final int
			 * verticalOffset = a.getDimensionPixelOffset(
			 * R.styleable.Spinner_dropDownVerticalOffset, 0); if
			 * (verticalOffset != 0) { popup.setVerticalOffset(verticalOffset);
			 * } final int horizontalOffset = a.getDimensionPixelOffset(
			 * R.styleable.Spinner_dropDownHorizontalOffset, 0); if
			 * (horizontalOffset != 0) {
			 * popup.setHorizontalOffset(horizontalOffset); } mPopup = popup;
			 * break;
			 */
		}
		}
		mGravity = a.getInt(R.styleable.Spinner_gravity, Gravity.CENTER);
		mPopup.setPromptText(a.getString(R.styleable.Spinner_prompt));
		mDisableChildrenWhenDisabled = a.getBoolean(
				R.styleable.Spinner_disableChildrenWhenDisabled, false);
		a.recycle();
		if (mTempAdapter != null) {
			mPopup.setAdapter(mTempAdapter);
			mTempAdapter = null;
		}
	}

	/**
	 * Instantiates a new spinner.
	 *
	 * @param context the context
	 * @param mode the mode
	 */
	public Spinner(Context context, int mode) {
		this(context, null, R.attr.spinnerStyle, mode);
	}

	/* (non-Javadoc)
	 * @see android.view.View#getBaseline()
	 */
	@Override
	public int getBaseline() {
		View child = null;
		if (getChildCount() > 0) {
			child = getChildAt(0);
		} else if (getAdapter() != null && getAdapter().getCount() > 0) {
			child = makeAndAddView(0);
			mRecycler.put(0, child);
			removeAllViewsInLayout();
		}
		if (child != null) {
			final int childBaseline = child.getBaseline();
			return childBaseline >= 0 ? child.getTop() + childBaseline : -1;
		} else {
			return -1;
		}
	}

	/**
	 * Gets the drop down horizontal offset.
	 *
	 * @return the drop down horizontal offset
	 */
	public int getDropDownHorizontalOffset() {
		return mPopup.getHorizontalOffset();
	}

	/**
	 * Gets the drop down vertical offset.
	 *
	 * @return the drop down vertical offset
	 */
	public int getDropDownVerticalOffset() {
		return mPopup.getVerticalOffset();
	}

	/**
	 * Gets the drop down width.
	 *
	 * @return the drop down width
	 */
	public int getDropDownWidth() {
		return mDropDownWidth;
	}

	/**
	 * Gets the gravity.
	 *
	 * @return the gravity
	 */
	public int getGravity() {
		return mGravity;
	}

	/**
	 * Gets the popup background.
	 *
	 * @return the popup background
	 */
	public Drawable getPopupBackground() {
		return mPopup.getBackground();
	}

	/**
	 * Gets the prompt.
	 *
	 * @return the prompt
	 */
	public CharSequence getPrompt() {
		return mPopup.getHintText();
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.widget.AbsSpinner#layout(int, boolean)
	 */
	@Override
	public void layout(int delta, boolean animate) {
		int childrenLeft = mSpinnerPadding.left;
		int childrenWidth = getRight() - getLeft() - mSpinnerPadding.left
				- mSpinnerPadding.right;
		if (mDataChanged) {
			handleDataChanged();
		}
		if (mItemCount == 0) {
			resetList();
			return;
		}
		if (mNextSelectedPosition >= 0) {
			setSelectedPositionInt(mNextSelectedPosition);
		}
		recycleAllViews();
		removeAllViewsInLayout();
		mFirstPosition = mSelectedPosition;
		View sel = makeAndAddView(mSelectedPosition);
		int width = sel.getMeasuredWidth();
		int selectedOffset = childrenLeft;
		switch (mGravity & Gravity.HORIZONTAL_GRAVITY_MASK) {
		case Gravity.CENTER_HORIZONTAL:
			selectedOffset = childrenLeft + childrenWidth / 2 - width / 2;
			break;
		case Gravity.RIGHT:
			selectedOffset = childrenLeft + childrenWidth - width;
			break;
		}
		sel.offsetLeftAndRight(selectedOffset);
		mRecycler.clear();
		invalidate();
		checkSelectionChanged();
		mDataChanged = false;
		mNeedSync = false;
		setNextSelectedPositionInt(mSelectedPosition);
	}

	/**
	 * Make and add view.
	 *
	 * @param position the position
	 * @return the view
	 */
	private View makeAndAddView(int position) {
		View child;
		if (!mDataChanged) {
			child = mRecycler.get(position);
			if (child != null) {
				setUpChild(child);
				return child;
			}
		}
		child = mAdapter.getView(position, null, this);
		setUpChild(child);
		return child;
	}

	/**
	 * Measure content width.
	 *
	 * @param adapter the adapter
	 * @param background the background
	 * @return the int
	 */
	int measureContentWidth(SpinnerAdapter adapter, Drawable background) {
		if (adapter == null) {
			return 0;
		}
		int width = 0;
		View itemView = null;
		int itemType = 0;
		final int widthMeasureSpec = MeasureSpec.makeMeasureSpec(0,
				MeasureSpec.UNSPECIFIED);
		final int heightMeasureSpec = MeasureSpec.makeMeasureSpec(0,
				MeasureSpec.UNSPECIFIED);
		int start = Math.max(0, getSelectedItemPosition());
		final int end = Math
				.min(adapter.getCount(), start + MAX_ITEMS_MEASURED);
		final int count = end - start;
		start = Math.max(0, start - (MAX_ITEMS_MEASURED - count));
		for (int i = start; i < end; i++) {
			final int positionType = adapter.getItemViewType(i);
			if (positionType != itemType) {
				itemType = positionType;
				itemView = null;
			}
			itemView = adapter.getView(i, itemView, this);
			if (itemView.getLayoutParams() == null) {
				itemView.setLayoutParams(new ViewGroup.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT));
			}
			itemView.measure(widthMeasureSpec, heightMeasureSpec);
			width = Math.max(width, itemView.getMeasuredWidth());
		}
		if (background != null) {
			background.getPadding(mTempRect);
			width += mTempRect.left + mTempRect.right;
		}
		return width;
	}

	/* (non-Javadoc)
	 * @see android.content.DialogInterface.OnClickListener#onClick(android.content.DialogInterface, int)
	 */
	@Override
	public void onClick(DialogInterface dialog, int which) {
		setSelection(which);
		dialog.dismiss();
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.widget.AdapterView#onDetachedFromWindow()
	 */
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if (mPopup != null && mPopup.isShowing()) {
			mPopup.dismiss();
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.widget.AbsSpinner#onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent)
	 */
	@Override
	public void onInitializeAccessibilityEvent(AccessibilityEvent event) {
		super.onInitializeAccessibilityEvent(event);
		event.setClassName(Spinner.class.getName());
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.widget.AbsSpinner#onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo)
	 */
	@Override
	@SuppressLint("NewApi")
	public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo info) {
		super.onInitializeAccessibilityNodeInfo(info);
		info.setClassName(Spinner.class.getName());
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.widget.AdapterView#onLayout(boolean, int, int, int, int)
	 */
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		layout(0, false);
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.widget.AbsSpinner#onMeasure(int, int)
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if (mPopup != null
				&& MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.AT_MOST) {
			final int measuredWidth = getMeasuredWidth();
			setMeasuredDimension(
					Math.min(
							Math.max(
									measuredWidth,
									measureContentWidth(getAdapter(),
											getBackground())), MeasureSpec
									.getSize(widthMeasureSpec)),
					getMeasuredHeight());
		}
	}

	/* (non-Javadoc)
	 * @see android.view.View#performClick()
	 */
	@Override
	public boolean performClick() {
		boolean handled = super.performClick();
		if (!handled) {
			handled = true;
			if (!mPopup.isShowing()) {
				mPopup.show();
			}
		}
		return handled;
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.widget.AbsSpinner#setAdapter(android.widget.SpinnerAdapter)
	 */
	@Override
	public void setAdapter(SpinnerAdapter adapter) {
		super.setAdapter(adapter);
		if (mPopup != null) {
			mPopup.setAdapter(new DropDownAdapter(adapter));
		} else {
			mTempAdapter = new DropDownAdapter(adapter);
		}
	}

	/**
	 * Sets the drop down horizontal offset.
	 *
	 * @param pixels the new drop down horizontal offset
	 */
	public void setDropDownHorizontalOffset(int pixels) {
		mPopup.setHorizontalOffset(pixels);
	}

	/**
	 * Sets the drop down vertical offset.
	 *
	 * @param pixels the new drop down vertical offset
	 */
	public void setDropDownVerticalOffset(int pixels) {
		mPopup.setVerticalOffset(pixels);
	}

	/**
	 * Sets the drop down width.
	 *
	 * @param pixels the new drop down width
	 */
	public void setDropDownWidth(int pixels) {
		/*
		 * if (!(mPopup instanceof DropdownPopup)) { Log.e(TAG,
		 * "Cannot set dropdown width for MODE_DIALOG, ignoring"); return; }
		 * mDropDownWidth = pixels;
		 */
	}

	/* (non-Javadoc)
	 * @see android.view.View#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if (mDisableChildrenWhenDisabled) {
			final int count = getChildCount();
			for (int i = 0; i < count; i++) {
				getChildAt(i).setEnabled(enabled);
			}
		}
	}

	/**
	 * Sets the gravity.
	 *
	 * @param gravity the new gravity
	 */
	public void setGravity(int gravity) {
		if (mGravity != gravity) {
			if ((gravity & Gravity.HORIZONTAL_GRAVITY_MASK) == 0) {
				gravity |= Gravity.LEFT;
			}
			mGravity = gravity;
			requestLayout();
		}
	}

	/* (non-Javadoc)
	 * @see com.WazaBe.HoloEverywhere.widget.AdapterView#setOnItemClickListener(com.WazaBe.HoloEverywhere.widget.AdapterView.OnItemClickListener)
	 */
	@Override
	public void setOnItemClickListener(OnItemClickListener l) {
		throw new RuntimeException(
				"setOnItemClickListener cannot be used with a spinner.");
	}

	/**
	 * Sets the on item click listener int.
	 *
	 * @param l the new on item click listener int
	 */
	public void setOnItemClickListenerInt(OnItemClickListener l) {
		super.setOnItemClickListener(l);
	}

	/**
	 * Sets the popup background drawable.
	 *
	 * @param background the new popup background drawable
	 */
	public void setPopupBackgroundDrawable(Drawable background) {
		/*
		 * if (!(mPopup instanceof DropdownPopup)) { Log.e(TAG,
		 * "setPopupBackgroundDrawable: incompatible spinner mode; ignoring..."
		 * ); return; } ((DropdownPopup)
		 * mPopup).setBackgroundDrawable(background);
		 */
	}

	/**
	 * Sets the popup background resource.
	 *
	 * @param resId the new popup background resource
	 */
	public void setPopupBackgroundResource(int resId) {
		setPopupBackgroundDrawable(getContext().getResources().getDrawable(
				resId));
	}

	/**
	 * Sets the prompt.
	 *
	 * @param prompt the new prompt
	 */
	public void setPrompt(CharSequence prompt) {
		mPopup.setPromptText(prompt);
	}

	/**
	 * Sets the prompt id.
	 *
	 * @param promptId the new prompt id
	 */
	public void setPromptId(int promptId) {
		setPrompt(getContext().getText(promptId));
	}

	/**
	 * Sets the up child.
	 *
	 * @param child the new up child
	 */
	private void setUpChild(View child) {
		ViewGroup.LayoutParams lp = child.getLayoutParams();
		if (lp == null) {
			lp = generateDefaultLayoutParams();
		}
		addViewInLayout(child, 0, lp);
		child.setSelected(hasFocus());
		if (mDisableChildrenWhenDisabled) {
			child.setEnabled(isEnabled());
		}
		int childHeightSpec = ViewGroup.getChildMeasureSpec(mHeightMeasureSpec,
				mSpinnerPadding.top + mSpinnerPadding.bottom, lp.height);
		int childWidthSpec = ViewGroup.getChildMeasureSpec(mWidthMeasureSpec,
				mSpinnerPadding.left + mSpinnerPadding.right, lp.width);
		child.measure(childWidthSpec, childHeightSpec);
		int childLeft = 0;
		int childTop = mSpinnerPadding.top
				+ (getMeasuredHeight() - mSpinnerPadding.bottom
						- mSpinnerPadding.top - child.getMeasuredHeight()) / 2;
		int childBottom = childTop + child.getMeasuredHeight();
		int width = child.getMeasuredWidth();
		int childRight = childLeft + width;
		child.layout(childLeft, childTop, childRight, childBottom);
	}
}