package com.WazaBe.HoloEverywhere.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityEvent;
import android.widget.CompoundButton;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class Switch.
 */
public class Switch extends CompoundButton {
	
	/** The Constant CHECKED_STATE_SET. */
	private static final int[] CHECKED_STATE_SET = { android.R.attr.state_checked };
	
	/** The Constant MONOSPACE. */
	private static final int MONOSPACE = 3;
	
	/** The Constant SANS. */
	private static final int SANS = 1;
	
	/** The Constant SERIF. */
	private static final int SERIF = 2;
	
	/** The Constant TOUCH_MODE_DOWN. */
	private static final int TOUCH_MODE_DOWN = 1;
	
	/** The Constant TOUCH_MODE_DRAGGING. */
	private static final int TOUCH_MODE_DRAGGING = 2;
	
	/** The Constant TOUCH_MODE_IDLE. */
	private static final int TOUCH_MODE_IDLE = 0;
	
	/** The m min fling velocity. */
	private int mMinFlingVelocity;
	
	/** The m off layout. */
	private Layout mOffLayout;
	
	/** The m on layout. */
	private Layout mOnLayout;
	
	/** The m switch bottom. */
	private int mSwitchBottom;
	
	/** The m switch height. */
	private int mSwitchHeight;
	
	/** The m switch left. */
	private int mSwitchLeft;
	
	/** The m switch min width. */
	private int mSwitchMinWidth;
	
	/** The m switch padding. */
	private int mSwitchPadding;
	
	/** The m switch right. */
	private int mSwitchRight;
	
	/** The m switch top. */
	private int mSwitchTop;
	
	/** The m switch width. */
	private int mSwitchWidth;
	
	/** The m temp rect. */
	private final Rect mTempRect = new Rect();
	
	/** The m text colors. */
	private ColorStateList mTextColors;
	
	/** The m text off. */
	private CharSequence mTextOff;
	
	/** The m text on. */
	private CharSequence mTextOn;
	
	/** The m text paint. */
	private TextPaint mTextPaint;
	
	/** The m thumb drawable. */
	private Drawable mThumbDrawable;
	
	/** The m thumb position. */
	private float mThumbPosition;
	
	/** The m thumb text padding. */
	private int mThumbTextPadding;
	
	/** The m thumb width. */
	private int mThumbWidth;
	
	/** The m touch mode. */
	private int mTouchMode;
	
	/** The m touch slop. */
	private int mTouchSlop;
	
	/** The m touch x. */
	private float mTouchX;
	
	/** The m touch y. */
	private float mTouchY;
	
	/** The m track drawable. */
	private Drawable mTrackDrawable;
	
	/** The m velocity tracker. */
	private VelocityTracker mVelocityTracker = VelocityTracker.obtain();

	/**
	 * Instantiates a new switch.
	 *
	 * @param context the context
	 */
	public Switch(Context context) {
		this(context, null);
	}

	/**
	 * Instantiates a new switch.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public Switch(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.switchStyle);
	}

	/**
	 * Instantiates a new switch.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	@SuppressLint("NewApi")
	public Switch(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
		Resources res = getResources();
		if (VERSION.SDK_INT >= 5) {
			mTextPaint.density = res.getDisplayMetrics().density;
		} else {
			mTextPaint.setTextSize(res.getDisplayMetrics().density * 16);
		}
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.Switch, defStyle, 0);
		mThumbDrawable = a.getDrawable(R.styleable.Switch_thumb);
		mTrackDrawable = a.getDrawable(R.styleable.Switch_track);
		mTextOn = a.getText(R.styleable.Switch_textOn);
		mTextOff = a.getText(R.styleable.Switch_textOff);
		mThumbTextPadding = a.getDimensionPixelSize(
				R.styleable.Switch_thumbTextPadding, 0);
		mSwitchMinWidth = a.getDimensionPixelSize(
				R.styleable.Switch_switchMinWidth, 0);
		mSwitchPadding = a.getDimensionPixelSize(
				R.styleable.Switch_switchPadding, 0);
		int appearance = a.getResourceId(
				R.styleable.Switch_switchTextAppearance, 0);
		if (appearance != 0) {
			setSwitchTextAppearance(context, appearance);
		}
		a.recycle();
		ViewConfiguration config = ViewConfiguration.get(context);
		mTouchSlop = config.getScaledTouchSlop();
		mMinFlingVelocity = config.getScaledMinimumFlingVelocity();
		refreshDrawableState();
		setChecked(isChecked());
	}

	/**
	 * Animate thumb to checked state.
	 *
	 * @param newCheckedState the new checked state
	 */
	private void animateThumbToCheckedState(boolean newCheckedState) {
		setChecked(newCheckedState);
	}

	/**
	 * Cancel super touch.
	 *
	 * @param ev the ev
	 */
	private void cancelSuperTouch(MotionEvent ev) {
		MotionEvent cancel = MotionEvent.obtain(ev);
		cancel.setAction(MotionEvent.ACTION_CANCEL);
		super.onTouchEvent(cancel);
		cancel.recycle();
	}

	/* (non-Javadoc)
	 * @see android.widget.CompoundButton#drawableStateChanged()
	 */
	@Override
	protected void drawableStateChanged() {
		super.drawableStateChanged();
		int[] myDrawableState = getDrawableState();
		if (mThumbDrawable != null) {
			mThumbDrawable.setState(myDrawableState);
		}
		if (mTrackDrawable != null) {
			mTrackDrawable.setState(myDrawableState);
		}
		invalidate();
	}

	/* (non-Javadoc)
	 * @see android.widget.TextView#getCompoundPaddingRight()
	 */
	@Override
	public int getCompoundPaddingRight() {
		int padding = super.getCompoundPaddingRight() + mSwitchWidth;
		if (!TextUtils.isEmpty(getText())) {
			padding += mSwitchPadding;
		}
		return padding;
	}

	/**
	 * Gets the target checked state.
	 *
	 * @return the target checked state
	 */
	private boolean getTargetCheckedState() {
		return mThumbPosition >= getThumbScrollRange() / 2;
	}

	/**
	 * Gets the text off.
	 *
	 * @return the text off
	 */
	public CharSequence getTextOff() {
		return mTextOff;
	}

	/**
	 * Gets the text on.
	 *
	 * @return the text on
	 */
	public CharSequence getTextOn() {
		return mTextOn;
	}

	/**
	 * Gets the thumb scroll range.
	 *
	 * @return the thumb scroll range
	 */
	private int getThumbScrollRange() {
		if (mTrackDrawable == null) {
			return 0;
		}
		mTrackDrawable.getPadding(mTempRect);
		return mSwitchWidth - mThumbWidth - mTempRect.left - mTempRect.right;
	}

	/**
	 * Hit thumb.
	 *
	 * @param x the x
	 * @param y the y
	 * @return true, if successful
	 */
	private boolean hitThumb(float x, float y) {
		mThumbDrawable.getPadding(mTempRect);
		final int thumbTop = mSwitchTop - mTouchSlop;
		final int thumbLeft = mSwitchLeft + (int) (mThumbPosition + 0.5f)
				- mTouchSlop;
		final int thumbRight = thumbLeft + mThumbWidth + mTempRect.left
				+ mTempRect.right + mTouchSlop;
		final int thumbBottom = mSwitchBottom + mTouchSlop;
		return x > thumbLeft && x < thumbRight && y > thumbTop
				&& y < thumbBottom;
	}

	/* (non-Javadoc)
	 * @see android.widget.CompoundButton#jumpDrawablesToCurrentState()
	 */
	@SuppressLint("NewApi")
	@Override
	public void jumpDrawablesToCurrentState() {
		super.jumpDrawablesToCurrentState();
		mThumbDrawable.jumpToCurrentState();
		mTrackDrawable.jumpToCurrentState();
	}

	/**
	 * Make layout.
	 *
	 * @param text the text
	 * @return the layout
	 */
	private Layout makeLayout(CharSequence text) {
		return new StaticLayout(text, mTextPaint, (int) FloatMath.ceil(Layout
				.getDesiredWidth(text, mTextPaint)),
				Layout.Alignment.ALIGN_NORMAL, 1.f, 0, true);
	}

	/* (non-Javadoc)
	 * @see android.widget.CompoundButton#onCreateDrawableState(int)
	 */
	@Override
	protected int[] onCreateDrawableState(int extraSpace) {
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
		if (isChecked()) {
			mergeDrawableStates(drawableState, CHECKED_STATE_SET);
		}
		return drawableState;
	}

	/* (non-Javadoc)
	 * @see android.widget.CompoundButton#onDraw(android.graphics.Canvas)
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int switchLeft = mSwitchLeft;
		int switchTop = mSwitchTop;
		int switchRight = mSwitchRight;
		int switchBottom = mSwitchBottom;
		mTrackDrawable.setBounds(switchLeft, switchTop, switchRight,
				switchBottom);
		mTrackDrawable.draw(canvas);
		canvas.save();
		mTrackDrawable.getPadding(mTempRect);
		int switchInnerLeft = switchLeft + mTempRect.left;
		int switchInnerTop = switchTop + mTempRect.top;
		int switchInnerRight = switchRight - mTempRect.right;
		int switchInnerBottom = switchBottom - mTempRect.bottom;
		canvas.clipRect(switchInnerLeft, switchTop, switchInnerRight,
				switchBottom);
		mThumbDrawable.getPadding(mTempRect);
		final int thumbPos = (int) (mThumbPosition + 0.5f);
		int thumbLeft = switchInnerLeft - mTempRect.left + thumbPos;
		int thumbRight = switchInnerLeft + thumbPos + mThumbWidth
				+ mTempRect.right;
		mThumbDrawable
				.setBounds(thumbLeft, switchTop, thumbRight, switchBottom);
		mThumbDrawable.draw(canvas);
		if (mTextColors != null) {
			mTextPaint.setColor(mTextColors.getColorForState(
					getDrawableState(), mTextColors.getDefaultColor()));
		}
		mTextPaint.drawableState = getDrawableState();
		Layout switchText = getTargetCheckedState() ? mOnLayout : mOffLayout;
		canvas.translate(
				(thumbLeft + thumbRight) / 2 - switchText.getWidth() / 2,
				(switchInnerTop + switchInnerBottom) / 2
						- switchText.getHeight() / 2);
		switchText.draw(canvas);
		canvas.restore();
	}

	/* (non-Javadoc)
	 * @see android.widget.TextView#onLayout(boolean, int, int, int, int)
	 */
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		mThumbPosition = isChecked() ? getThumbScrollRange() : 0;
		int switchRight = getWidth() - getPaddingRight();
		int switchLeft = switchRight - mSwitchWidth;
		int switchTop = 0;
		int switchBottom = 0;
		switch (getGravity() & Gravity.VERTICAL_GRAVITY_MASK) {
		default:
		case Gravity.TOP:
			switchTop = getPaddingTop();
			switchBottom = switchTop + mSwitchHeight;
			break;
		case Gravity.CENTER_VERTICAL:
			switchTop = (getPaddingTop() + getHeight() - getPaddingBottom())
					/ 2 - mSwitchHeight / 2;
			switchBottom = switchTop + mSwitchHeight;
			break;
		case Gravity.BOTTOM:
			switchBottom = getHeight() - getPaddingBottom();
			switchTop = switchBottom - mSwitchHeight;
			break;
		}
		mSwitchLeft = switchLeft;
		mSwitchTop = switchTop;
		mSwitchBottom = switchBottom;
		mSwitchRight = switchRight;
	}

	/* (non-Javadoc)
	 * @see android.widget.TextView#onMeasure(int, int)
	 */
	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);
		if (mOnLayout == null) {
			mOnLayout = makeLayout(mTextOn);
		}
		if (mOffLayout == null) {
			mOffLayout = makeLayout(mTextOff);
		}
		mTrackDrawable.getPadding(mTempRect);
		final int maxTextWidth = Math.max(mOnLayout.getWidth(),
				mOffLayout.getWidth());
		final int switchWidth = Math.max(mSwitchMinWidth, maxTextWidth * 2
				+ mThumbTextPadding * 4 + mTempRect.left + mTempRect.right);
		final int switchHeight = mTrackDrawable.getIntrinsicHeight();
		mThumbWidth = maxTextWidth + mThumbTextPadding * 2;
		switch (widthMode) {
		case MeasureSpec.AT_MOST:
			widthSize = Math.min(widthSize, switchWidth);
			break;
		case MeasureSpec.UNSPECIFIED:
			widthSize = switchWidth;
			break;
		case MeasureSpec.EXACTLY:
			break;
		}
		switch (heightMode) {
		case MeasureSpec.AT_MOST:
			heightSize = Math.min(heightSize, switchHeight);
			break;
		case MeasureSpec.UNSPECIFIED:
			heightSize = switchHeight;
			break;
		case MeasureSpec.EXACTLY:
			break;
		}
		mSwitchWidth = switchWidth;
		mSwitchHeight = switchHeight;
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		final int measuredHeight = getMeasuredHeight();
		if (measuredHeight < switchHeight) {
			setMeasuredDimension(MeasureSpec.makeMeasureSpec(
					getMeasuredWidth(), MeasureSpec.UNSPECIFIED), switchHeight);
		}
	}

	/* (non-Javadoc)
	 * @see android.widget.TextView#onPopulateAccessibilityEvent(android.view.accessibility.AccessibilityEvent)
	 */
	@SuppressLint("NewApi")
	@Override
	public void onPopulateAccessibilityEvent(AccessibilityEvent event) {
		super.onPopulateAccessibilityEvent(event);
		if (isChecked()) {
			CharSequence text = mOnLayout.getText();
			if (TextUtils.isEmpty(text)) {
				text = getContext().getString(R.string.switch_on);
			}
			event.getText().add(text);
		} else {
			CharSequence text = mOffLayout.getText();
			if (TextUtils.isEmpty(text)) {
				text = getContext().getString(R.string.switch_off);
			}
			event.getText().add(text);
		}
	}

	/* (non-Javadoc)
	 * @see android.widget.TextView#onTouchEvent(android.view.MotionEvent)
	 */
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		mVelocityTracker.addMovement(ev);
		switch (ev.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN: {
			final float x = ev.getX();
			final float y = ev.getY();
			if (isEnabled() && hitThumb(x, y)) {
				mTouchMode = TOUCH_MODE_DOWN;
				mTouchX = x;
				mTouchY = y;
			}
			return true;
		}

		case MotionEvent.ACTION_MOVE: {
			switch (mTouchMode) {
			case TOUCH_MODE_IDLE:
				break;
			case TOUCH_MODE_DOWN: {
				final float x = ev.getX();
				final float y = ev.getY();
				if (Math.abs(x - mTouchX) > mTouchSlop
						|| Math.abs(y - mTouchY) > mTouchSlop) {
					mTouchMode = TOUCH_MODE_DRAGGING;
					getParent().requestDisallowInterceptTouchEvent(true);
					mTouchX = x;
					mTouchY = y;
					return true;
				}
				break;
			}
			case TOUCH_MODE_DRAGGING: {
				final float x = ev.getX();
				final float dx = x - mTouchX;
				float newPos = Math.max(0,
						Math.min(mThumbPosition + dx, getThumbScrollRange()));
				if (newPos != mThumbPosition) {
					mThumbPosition = newPos;
					mTouchX = x;
					invalidate();
				}
				return true;
			}
			}
			break;
		}
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_CANCEL: {
			if (mTouchMode == TOUCH_MODE_DRAGGING) {
				stopDrag(ev);
				return true;
			}
			mTouchMode = TOUCH_MODE_IDLE;
			mVelocityTracker.clear();
			break;
		}
		}
		return super.onTouchEvent(ev);
	}

	/* (non-Javadoc)
	 * @see android.widget.CompoundButton#setChecked(boolean)
	 */
	@Override
	public void setChecked(boolean checked) {
		super.setChecked(checked);
		mThumbPosition = checked ? getThumbScrollRange() : 0;
		invalidate();
	}

	/**
	 * Sets the switch text appearance.
	 *
	 * @param context the context
	 * @param resid the resid
	 */
	public void setSwitchTextAppearance(Context context, int resid) {
		TypedArray appearance = context.obtainStyledAttributes(resid,
				R.styleable.TextAppearance);
		ColorStateList colors;
		int ts;
		colors = appearance
				.getColorStateList(R.styleable.TextAppearance_textColor);
		if (colors != null) {
			mTextColors = colors;
		} else {
			mTextColors = getTextColors();
		}
		ts = appearance.getDimensionPixelSize(
				R.styleable.TextAppearance_textSize, 0);
		if (ts != 0) {
			if (ts != mTextPaint.getTextSize()) {
				mTextPaint.setTextSize(ts);
				requestLayout();
			}
		}
		int typefaceIndex, styleIndex;
		typefaceIndex = appearance.getInt(R.styleable.TextAppearance_typeface,
				-1);
		styleIndex = appearance
				.getInt(R.styleable.TextAppearance_textStyle, -1);
		setSwitchTypefaceByIndex(typefaceIndex, styleIndex);
		appearance.recycle();
	}

	/**
	 * Sets the switch typeface.
	 *
	 * @param tf the new switch typeface
	 */
	public void setSwitchTypeface(Typeface tf) {
		if (mTextPaint.getTypeface() != tf) {
			mTextPaint.setTypeface(tf);

			requestLayout();
			invalidate();
		}
	}

	/**
	 * Sets the switch typeface.
	 *
	 * @param tf the tf
	 * @param style the style
	 */
	public void setSwitchTypeface(Typeface tf, int style) {
		if (style > 0) {
			if (tf == null) {
				tf = Typeface.defaultFromStyle(style);
			} else {
				tf = Typeface.create(tf, style);
			}
			setSwitchTypeface(tf);
			int typefaceStyle = tf != null ? tf.getStyle() : 0;
			int need = style & ~typefaceStyle;
			mTextPaint.setFakeBoldText((need & Typeface.BOLD) != 0);
			mTextPaint.setTextSkewX((need & Typeface.ITALIC) != 0 ? -0.25f : 0);
		} else {
			mTextPaint.setFakeBoldText(false);
			mTextPaint.setTextSkewX(0);
			setSwitchTypeface(tf);
		}
	}

	/**
	 * Sets the switch typeface by index.
	 *
	 * @param typefaceIndex the typeface index
	 * @param styleIndex the style index
	 */
	private void setSwitchTypefaceByIndex(int typefaceIndex, int styleIndex) {
		Typeface tf = null;
		switch (typefaceIndex) {
		case SANS:
			tf = Typeface.SANS_SERIF;
			break;
		case SERIF:
			tf = Typeface.SERIF;
			break;
		case MONOSPACE:
			tf = Typeface.MONOSPACE;
			break;
		}
		setSwitchTypeface(tf, styleIndex);
	}

	/**
	 * Sets the text off.
	 *
	 * @param textOff the new text off
	 */
	public void setTextOff(CharSequence textOff) {
		mTextOff = textOff;
		requestLayout();
	}

	/**
	 * Sets the text on.
	 *
	 * @param textOn the new text on
	 */
	public void setTextOn(CharSequence textOn) {
		mTextOn = textOn;
		requestLayout();
	}

	/**
	 * Stop drag.
	 *
	 * @param ev the ev
	 */
	private void stopDrag(MotionEvent ev) {
		mTouchMode = TOUCH_MODE_IDLE;
		boolean commitChange = ev.getAction() == MotionEvent.ACTION_UP
				&& isEnabled();
		cancelSuperTouch(ev);
		if (commitChange) {
			boolean newState;
			mVelocityTracker.computeCurrentVelocity(1000);
			float xvel = mVelocityTracker.getXVelocity();
			if (Math.abs(xvel) > mMinFlingVelocity) {
				newState = xvel > 0;
			} else {
				newState = getTargetCheckedState();
			}
			animateThumbToCheckedState(newState);
		} else {
			animateThumbToCheckedState(isChecked());
		}
	}

	/* (non-Javadoc)
	 * @see android.widget.CompoundButton#verifyDrawable(android.graphics.drawable.Drawable)
	 */
	@Override
	protected boolean verifyDrawable(Drawable who) {
		return super.verifyDrawable(who) || who == mThumbDrawable
				|| who == mTrackDrawable;
	}
}