package com.WazaBe.HoloEverywhere.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.SpinnerAdapter;

import com.WazaBe.HoloEverywhere.ArrayAdapter;
import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class AbsSpinner.
 */
public abstract class AbsSpinner extends AdapterView<SpinnerAdapter> {
	
	/**
	 * The Class RecycleBin.
	 */
	class RecycleBin {
		
		/** The m scrap heap. */
		private final SparseArray<View> mScrapHeap = new SparseArray<View>();

		/**
		 * Clear.
		 */
		void clear() {
			final SparseArray<View> scrapHeap = mScrapHeap;
			final int count = scrapHeap.size();
			for (int i = 0; i < count; i++) {
				final View view = scrapHeap.valueAt(i);
				if (view != null) {
					removeDetachedView(view, true);
				}
			}
			scrapHeap.clear();
		}

		/**
		 * Gets the.
		 *
		 * @param position the position
		 * @return the view
		 */
		View get(int position) {
			View result = mScrapHeap.get(position);
			if (result != null) {
				mScrapHeap.delete(position);
			}
			return result;
		}

		/**
		 * Put.
		 *
		 * @param position the position
		 * @param v the v
		 */
		public void put(int position, View v) {
			mScrapHeap.put(position, v);
		}
	}

	/**
	 * The Class SavedState.
	 */
	static class SavedState extends BaseSavedState {
		
		/** The Constant CREATOR. */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			@Override
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			@Override
			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};
		
		/** The position. */
		int position;

		/** The selected id. */
		long selectedId;

		/**
		 * Instantiates a new saved state.
		 *
		 * @param in the in
		 */
		private SavedState(Parcel in) {
			super(in);
			selectedId = in.readLong();
			position = in.readInt();
		}

		/**
		 * Instantiates a new saved state.
		 *
		 * @param superState the super state
		 */
		SavedState(Parcelable superState) {
			super(superState);
		}

		/**
		 * To string.
		 *
		 * @return the string
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "AbsSpinner.SavedState{"
					+ Integer.toHexString(System.identityHashCode(this))
					+ " selectedId=" + selectedId + " position=" + position
					+ "}";
		}

		/**
		 * Write to parcel.
		 *
		 * @param out the out
		 * @param flags the flags
		 * @see android.view.AbsSavedState#writeToParcel(android.os.Parcel, int)
		 */
		@Override
		public void writeToParcel(Parcel out, int flags) {
			super.writeToParcel(out, flags);
			out.writeLong(selectedId);
			out.writeInt(position);
		}
	}

	/** The m adapter. */
	SpinnerAdapter mAdapter;
	
	/** The m data set observer. */
	private DataSetObserver mDataSetObserver;
	
	/** The m height measure spec. */
	int mHeightMeasureSpec;
	
	/** The m recycler. */
	final RecycleBin mRecycler = new RecycleBin();
	
	/** The m selection bottom padding. */
	int mSelectionBottomPadding = 0;
	
	/** The m selection left padding. */
	int mSelectionLeftPadding = 0;
	
	/** The m selection right padding. */
	int mSelectionRightPadding = 0;
	
	/** The m selection top padding. */
	int mSelectionTopPadding = 0;
	
	/** The m spinner padding. */
	final Rect mSpinnerPadding = new Rect();

	/** The m touch frame. */
	private Rect mTouchFrame;

	/** The m width measure spec. */
	int mWidthMeasureSpec;

	/**
	 * Instantiates a new abs spinner.
	 *
	 * @param context the context
	 */
	public AbsSpinner(Context context) {
		super(context);
		initAbsSpinner();
	}

	/**
	 * Instantiates a new abs spinner.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public AbsSpinner(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	/**
	 * Instantiates a new abs spinner.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public AbsSpinner(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initAbsSpinner();
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.AbsSpinner, defStyle, 0);
		CharSequence[] entries = a.getTextArray(R.styleable.AbsSpinner_entries);
		if (entries != null) {
			ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
					context, R.layout.simple_spinner_item, entries);
			adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
			setAdapter(adapter);
		}
		a.recycle();
	}

	/**
	 * Generate default layout params.
	 *
	 * @return the view group. layout params
	 * @see android.view.ViewGroup#generateDefaultLayoutParams()
	 */
	@Override
	protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
		return new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	/**
	 * Gets the adapter.
	 *
	 * @return the adapter
	 * @see com.WazaBe.HoloEverywhere.widget.AdapterView#getAdapter()
	 */
	@Override
	public SpinnerAdapter getAdapter() {
		return mAdapter;
	}

	/**
	 * Gets the child height.
	 *
	 * @param child the child
	 * @return the child height
	 */
	int getChildHeight(View child) {
		return child.getMeasuredHeight();
	}

	/**
	 * Gets the child width.
	 *
	 * @param child the child
	 * @return the child width
	 */
	int getChildWidth(View child) {
		return child.getMeasuredWidth();
	}

	/**
	 * Gets the count.
	 *
	 * @return the count
	 * @see com.WazaBe.HoloEverywhere.widget.AdapterView#getCount()
	 */
	@Override
	public int getCount() {
		return mItemCount;
	}

	/**
	 * Gets the selected view.
	 *
	 * @return the selected view
	 * @see com.WazaBe.HoloEverywhere.widget.AdapterView#getSelectedView()
	 */
	@Override
	public View getSelectedView() {
		if (mItemCount > 0 && mSelectedPosition >= 0) {
			return getChildAt(mSelectedPosition - mFirstPosition);
		} else {
			return null;
		}
	}

	/**
	 * Inits the abs spinner.
	 */
	private void initAbsSpinner() {
		setFocusable(true);
		setWillNotDraw(false);
	}

	/**
	 * Layout.
	 *
	 * @param delta the delta
	 * @param animate the animate
	 */
	abstract void layout(int delta, boolean animate);

	/**
	 * On initialize accessibility event.
	 *
	 * @param event the event
	 * @see com.WazaBe.HoloEverywhere.widget.AdapterView#onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent)
	 */
	@Override
	@SuppressLint("NewApi")
	public void onInitializeAccessibilityEvent(AccessibilityEvent event) {
		super.onInitializeAccessibilityEvent(event);
		event.setClassName(AbsSpinner.class.getName());
	}

	/**
	 * On initialize accessibility node info.
	 *
	 * @param info the info
	 * @see com.WazaBe.HoloEverywhere.widget.AdapterView#onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo)
	 */
	@Override
	@SuppressLint("NewApi")
	public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo info) {
		super.onInitializeAccessibilityNodeInfo(info);
		info.setClassName(AbsSpinner.class.getName());
	}

	/**
	 * On measure.
	 *
	 * @param widthMeasureSpec the width measure spec
	 * @param heightMeasureSpec the height measure spec
	 * @see android.view.View#onMeasure(int, int)
	 */
	@SuppressLint("NewApi")
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize;
		int heightSize;
		mSpinnerPadding.left = getPaddingLeft() > mSelectionLeftPadding ? getPaddingLeft()
				: mSelectionLeftPadding;
		mSpinnerPadding.top = getPaddingTop() > mSelectionTopPadding ? getPaddingTop()
				: mSelectionTopPadding;
		mSpinnerPadding.right = getPaddingRight() > mSelectionRightPadding ? getPaddingRight()
				: mSelectionRightPadding;
		mSpinnerPadding.bottom = getPaddingBottom() > mSelectionBottomPadding ? getPaddingBottom()
				: mSelectionBottomPadding;
		if (mDataChanged) {
			handleDataChanged();
		}
		int preferredHeight = 0;
		int preferredWidth = 0;
		boolean needsMeasuring = true;
		int selectedPosition = getSelectedItemPosition();
		if (selectedPosition >= 0 && mAdapter != null
				&& selectedPosition < mAdapter.getCount()) {
			View view = mRecycler.get(selectedPosition);
			if (view == null) {
				view = mAdapter.getView(selectedPosition, null, this);
				if (VERSION.SDK_INT >= 16
						&& view.getImportantForAccessibility() == IMPORTANT_FOR_ACCESSIBILITY_AUTO) {
					view.setImportantForAccessibility(IMPORTANT_FOR_ACCESSIBILITY_YES);
				}
			}
			if (view != null) {
				mRecycler.put(selectedPosition, view);
			}
			if (view != null) {
				if (view.getLayoutParams() == null) {
					mBlockLayoutRequests = true;
					view.setLayoutParams(generateDefaultLayoutParams());
					mBlockLayoutRequests = false;
				}
				measureChild(view, widthMeasureSpec, heightMeasureSpec);
				preferredHeight = getChildHeight(view) + mSpinnerPadding.top
						+ mSpinnerPadding.bottom;
				preferredWidth = getChildWidth(view) + mSpinnerPadding.left
						+ mSpinnerPadding.right;
				needsMeasuring = false;
			}
		}
		if (needsMeasuring) {
			preferredHeight = mSpinnerPadding.top + mSpinnerPadding.bottom;
			if (widthMode == MeasureSpec.UNSPECIFIED) {
				preferredWidth = mSpinnerPadding.left + mSpinnerPadding.right;
			}
		}
		preferredHeight = Math
				.max(preferredHeight, getSuggestedMinimumHeight());
		preferredWidth = Math.max(preferredWidth, getSuggestedMinimumWidth());
		heightSize = com.WazaBe.HoloEverywhere.widget.View
				.supportResolveSizeAndState(preferredHeight, heightMeasureSpec,
						0);
		widthSize = com.WazaBe.HoloEverywhere.widget.View
				.supportResolveSizeAndState(preferredWidth, widthMeasureSpec, 0);
		setMeasuredDimension(widthSize, heightSize);
		mHeightMeasureSpec = heightMeasureSpec;
		mWidthMeasureSpec = widthMeasureSpec;
	}

	/**
	 * On restore instance state.
	 *
	 * @param state the state
	 * @see android.view.View#onRestoreInstanceState(android.os.Parcelable)
	 */
	@Override
	public void onRestoreInstanceState(Parcelable state) {
		SavedState ss = (SavedState) state;

		super.onRestoreInstanceState(ss.getSuperState());

		if (ss.selectedId >= 0) {
			mDataChanged = true;
			mNeedSync = true;
			mSyncRowId = ss.selectedId;
			mSyncPosition = ss.position;
			mSyncMode = SYNC_SELECTED_POSITION;
			requestLayout();
		}
	}

	/**
	 * On save instance state.
	 *
	 * @return the parcelable
	 * @see android.view.View#onSaveInstanceState()
	 */
	@Override
	public Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();
		SavedState ss = new SavedState(superState);
		ss.selectedId = getSelectedItemId();
		if (ss.selectedId >= 0) {
			ss.position = getSelectedItemPosition();
		} else {
			ss.position = INVALID_POSITION;
		}
		return ss;
	}

	/**
	 * Point to position.
	 *
	 * @param x the x
	 * @param y the y
	 * @return the int
	 */
	public int pointToPosition(int x, int y) {
		Rect frame = mTouchFrame;
		if (frame == null) {
			mTouchFrame = new Rect();
			frame = mTouchFrame;
		}
		final int count = getChildCount();
		for (int i = count - 1; i >= 0; i--) {
			View child = getChildAt(i);
			if (child.getVisibility() == View.VISIBLE) {
				child.getHitRect(frame);
				if (frame.contains(x, y)) {
					return mFirstPosition + i;
				}
			}
		}
		return INVALID_POSITION;
	}

	/**
	 * Recycle all views.
	 */
	void recycleAllViews() {
		final int childCount = getChildCount();
		final AbsSpinner.RecycleBin recycleBin = mRecycler;
		final int position = mFirstPosition;
		for (int i = 0; i < childCount; i++) {
			View v = getChildAt(i);
			int index = position + i;
			recycleBin.put(index, v);
		}
	}

	/**
	 * Request layout.
	 *
	 * @see android.view.View#requestLayout()
	 */
	@Override
	public void requestLayout() {
		if (!mBlockLayoutRequests) {
			super.requestLayout();
		}
	}

	/**
	 * Reset list.
	 */
	void resetList() {
		mDataChanged = false;
		mNeedSync = false;
		removeAllViewsInLayout();
		mOldSelectedPosition = INVALID_POSITION;
		mOldSelectedRowId = INVALID_ROW_ID;
		setSelectedPositionInt(INVALID_POSITION);
		setNextSelectedPositionInt(INVALID_POSITION);
		invalidate();
	}

	/**
	 * Sets the adapter.
	 *
	 * @param adapter the new adapter
	 * @see com.WazaBe.HoloEverywhere.widget.AdapterView#setAdapter(android.widget.Adapter)
	 */
	@Override
	public void setAdapter(SpinnerAdapter adapter) {
		if (null != mAdapter) {
			mAdapter.unregisterDataSetObserver(mDataSetObserver);
			resetList();
		}
		mAdapter = adapter;
		mOldSelectedPosition = INVALID_POSITION;
		mOldSelectedRowId = INVALID_ROW_ID;
		if (mAdapter != null) {
			mOldItemCount = mItemCount;
			mItemCount = mAdapter.getCount();
			checkFocus();
			mDataSetObserver = new AdapterDataSetObserver();
			mAdapter.registerDataSetObserver(mDataSetObserver);
			int position = mItemCount > 0 ? 0 : INVALID_POSITION;
			setSelectedPositionInt(position);
			setNextSelectedPositionInt(position);
			if (mItemCount == 0) {
				checkSelectionChanged();
			}
		} else {
			checkFocus();
			resetList();
			checkSelectionChanged();
		}
		requestLayout();
	}

	/**
	 * Sets the selection.
	 *
	 * @param position the new selection
	 * @see com.WazaBe.HoloEverywhere.widget.AdapterView#setSelection(int)
	 */
	@Override
	public void setSelection(int position) {
		setNextSelectedPositionInt(position);
		requestLayout();
		invalidate();
	}

	/**
	 * Sets the selection.
	 *
	 * @param position the position
	 * @param animate the animate
	 */
	public void setSelection(int position, boolean animate) {
		boolean shouldAnimate = animate && mFirstPosition <= position
				&& position <= mFirstPosition + getChildCount() - 1;
		setSelectionInt(position, shouldAnimate);
	}

	/**
	 * Sets the selection int.
	 *
	 * @param position the position
	 * @param animate the animate
	 */
	void setSelectionInt(int position, boolean animate) {
		if (position != mOldSelectedPosition) {
			mBlockLayoutRequests = true;
			int delta = position - mSelectedPosition;
			setNextSelectedPositionInt(position);
			layout(delta, animate);
			mBlockLayoutRequests = false;
		}
	}
}