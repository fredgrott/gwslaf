package com.WazaBe.HoloEverywhere.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class TextView.
 */
public class TextView extends android.widget.TextView {

	/** The all caps. */
	private boolean allCaps = false;

	/**
	 * Instantiates a new text view.
	 *
	 * @param context the context
	 */
	public TextView(Context context) {
		super(context);
		init(null, 0);
	}

	/**
	 * Instantiates a new text view.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public TextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs, 0);
	}

	/**
	 * Instantiates a new text view.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public TextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs, defStyle);
	}

	/**
	 * Inits the.
	 *
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	protected void init(AttributeSet attrs, int defStyle) {
		TypedArray a = getContext().obtainStyledAttributes(attrs,
				R.styleable.TextView, defStyle, 0);
		allCaps = a.getBoolean(R.styleable.TextView_textAllCaps, false);
		a.recycle();
	}

	/**
	 * Checks if is all caps.
	 *
	 * @return true, if is all caps
	 */
	public boolean isAllCaps() {
		return allCaps;
	}

	/* (non-Javadoc)
	 * @see android.widget.TextView#setAllCaps(boolean)
	 */
	@Override
	public void setAllCaps(boolean allCaps) {
		this.allCaps = allCaps;
	}

	/* (non-Javadoc)
	 * @see android.widget.TextView#setText(java.lang.CharSequence, android.widget.TextView.BufferType)
	 */
	@Override
	public void setText(CharSequence text, BufferType type) {
		super.setText(allCaps ? text.toString().toUpperCase() : text, type);
	}
}
