package com.WazaBe.HoloEverywhere.widget;

import org.bitbucket.fredgrott.gwslaf.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;

// TODO: Auto-generated Javadoc
/**
 * The Class Divider.
 */
public class Divider extends View {

	/**
	 * Instantiates a new divider.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public Divider(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);

	}

	/**
	 * Instantiates a new divider.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public Divider(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	/**
	 * Instantiates a new divider.
	 *
	 * @param context the context
	 */
	public Divider(Context context) {
		super(context);
		init(context, null, 0);
	}

	/**
	 * Inits the.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	protected void init(Context context, AttributeSet attrs, int defStyle) {
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.Divider, defStyle, 0);
		boolean vertical = a.getInt(R.styleable.Divider_android_orientation,
				LinearLayout.VERTICAL) == LinearLayout.VERTICAL;
		a.recycle();
		TypedValue value = new TypedValue();
		context.getTheme().resolveAttribute(
				vertical ? R.attr.dividerVertical : R.attr.dividerHorizontal,
				value, true);
		if (value.resourceId > 0) {
			setBackgroundResource(value.resourceId);
		}
	}

}
