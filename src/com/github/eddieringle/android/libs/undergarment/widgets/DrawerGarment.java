/*
 * Copyright (c) 2012 Eddie Ringle <eddie@eringle.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions
 * and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions
 * and the following disclaimer in the documentation and/or other materials provided with the
 * distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.github.eddieringle.android.libs.undergarment.widgets;

import org.bitbucket.fredgrott.gwslaf.R;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Scroller;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

// TODO: Auto-generated Javadoc
/**
 * DrawerGarment <p/> An implementation of the slide-out navigation pattern.
 */
public class DrawerGarment extends FrameLayout {

    /** The Constant SLIDE_TARGET_CONTENT. */
    public static final int SLIDE_TARGET_CONTENT = 0;

    /** The Constant SLIDE_TARGET_WINDOW. */
    public static final int SLIDE_TARGET_WINDOW = 1;

    /** The Constant SCROLL_DURATION. */
    private static final int SCROLL_DURATION = 400;

    /** The Constant TOUCH_TARGET_WIDTH_DIP. */
    private static final float TOUCH_TARGET_WIDTH_DIP = 48.0f;

    /** The m added. */
    private boolean mAdded = false;

    /** The m drawer enabled. */
    private boolean mDrawerEnabled = true;

    /** The m drawer opened. */
    private boolean mDrawerOpened = false;

    /** The m drawer moving. */
    private boolean mDrawerMoving = false;

    /** The m gesture started. */
    private boolean mGestureStarted = false;

    /** The m decor offset x. */
    private int mDecorOffsetX = 0;

    /** The m gesture start x. */
    private int mGestureStartX;

    /** The m gesture current x. */
    private int mGestureCurrentX;

    /** The m gesture start y. */
    private int mGestureStartY;

    /** The m gesture current y. */
    private int mGestureCurrentY;

    /** The m slide target. */
    private int mSlideTarget;

    /** The m touch target width. */
    private int mTouchTargetWidth;

    /** The m shadow drawable. */
    private Drawable mShadowDrawable;

    /** The m scroller handler. */
    private Handler mScrollerHandler;

    /** The m scroller. */
    private Scroller mScroller;

    /** The m decor view. */
    private ViewGroup mDecorView;

    /** The m content target. */
    private ViewGroup mContentTarget;

    /** The m content target parent. */
    private ViewGroup mContentTargetParent;

    /** The m window target. */
    private ViewGroup mWindowTarget;

    /** The m window target parent. */
    private ViewGroup mWindowTargetParent;

    /** The m decor content. */
    private ViewGroup mDecorContent;

    /** The m decor content parent. */
    private ViewGroup mDecorContentParent;

    /** The m drawer content. */
    private ViewGroup mDrawerContent;

    /** The m velocity tracker. */
    private VelocityTracker mVelocityTracker;

    /** The m drawer callbacks. */
    private IDrawerCallbacks mDrawerCallbacks;

    /**
     * The Interface IDrawerCallbacks.
     */
    public static interface IDrawerCallbacks {

        /**
         * On drawer opened.
         */
        public void onDrawerOpened();

        /**
         * On drawer closed.
         */
        public void onDrawerClosed();
    }

    /**
     * The Class SmoothInterpolator.
     */
    public static class SmoothInterpolator implements Interpolator {

        /**
         * Gets the interpolation.
         *
         * @param v the v
         * @return the interpolation
         * @see android.animation.TimeInterpolator#getInterpolation(float)
         */
        @Override
        public float getInterpolation(float v) {
            return (float) (Math.pow((double) v - 1.0, 5.0) + 1.0f);
        }
    }

    /**
     * Reconfigure view hierarchy.
     */
    public void reconfigureViewHierarchy() {
        if (mDecorView == null) {
            return;
        }
        if (mDrawerContent != null) {
            removeView(mDrawerContent);
        }
        if (mDecorContent != null) {
            /*
             * Add the window/content (whatever it is at the time) back to its original parent.
             */
            removeView(mDecorContent);
            mDecorContentParent.addView(mDecorContent);

            /*
             * Reset the window/content's OnClickListener/background color to default values as well
             */
            mDecorContent.setOnClickListener(null);
            mDecorContent.setBackgroundColor(Color.TRANSPARENT);
        }
        if (mAdded) {
            mDecorContentParent.removeView(this);
        }
        if (mSlideTarget == SLIDE_TARGET_CONTENT) {
            mDecorContent = mContentTarget;
            mDecorContentParent = mContentTargetParent;
        } else if (mSlideTarget == SLIDE_TARGET_WINDOW) {
            mDecorContent = mWindowTarget;
            mDecorContentParent = mWindowTargetParent;
        } else {
            throw new IllegalArgumentException(
                    "Slide target must be one of SLIDE_TARGET_CONTENT or SLIDE_TARGET_WINDOW.");
        }
        ((ViewGroup) mDecorContent.getParent()).removeView(mDecorContent);
        addView(mDrawerContent);
        addView(mDecorContent, new LayoutParams(MATCH_PARENT, MATCH_PARENT));
        mDecorContentParent.addView(this);
        mAdded = true;

        /* TODO: Make this a configurable attribute */
        mDecorContent.setBackgroundColor(Color.WHITE);

        /*
         * Set an empty onClickListener on the Decor content parent to prevent any touch events
         * from escaping and passing through to the drawer even while it's closed.
         */
        mDecorContent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    /**
     * Instantiates a new drawer garment.
     *
     * @param activity the activity
     * @param drawerLayout the drawer layout
     */
    public DrawerGarment(Activity activity, int drawerLayout) {
        super(activity);

        final DisplayMetrics dm = activity.getResources().getDisplayMetrics();

        mTouchTargetWidth = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                TOUCH_TARGET_WIDTH_DIP, dm));

        mShadowDrawable = getResources().getDrawable(R.drawable.decor_shadow);
        mShadowDrawable.setBounds(-mTouchTargetWidth / 6, 0, 0, dm.heightPixels);

        mScrollerHandler = new Handler();
        mScroller = new Scroller(activity, new SmoothInterpolator());
        
        /* Default to targeting the entire window (i.e., including the Action Bar) */
        mSlideTarget = SLIDE_TARGET_WINDOW;

        mDecorView = (ViewGroup) activity.getWindow().getDecorView();
        mWindowTarget = (ViewGroup) mDecorView.getChildAt(0);
        mWindowTargetParent = (ViewGroup) mWindowTarget.getParent();
        mContentTarget = (ViewGroup) mDecorView.findViewById(android.R.id.content);
        mContentTargetParent = (ViewGroup) mContentTarget.getParent();
        mDrawerContent = (ViewGroup) LayoutInflater.from(activity).inflate(drawerLayout, null);

        /*
         * Mutilate the view hierarchy and re-appropriate the slide target,
         * be it the entire window or just android.R.id.content, under
         * this DrawerGarment.
         */
        reconfigureViewHierarchy();

        mDrawerContent.setPadding(0, 0, mTouchTargetWidth, 0);

        /*
         * This currently causes lock-ups on 10" tablets (e.g., Xoom & Transformer),
         * should probably look into why this is happening.
         *
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setLayerType(LAYER_TYPE_HARDWARE, null);
        }
         */
    }

    /**
     * On layout.
     *
     * @param changed the changed
     * @param left the left
     * @param top the top
     * @param right the right
     * @param bottom the bottom
     * @see android.widget.FrameLayout#onLayout(boolean, int, int, int, int)
     */
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        Rect windowRect = new Rect();
        mDecorView.getWindowVisibleDisplayFrame(windowRect);

        if (mSlideTarget == SLIDE_TARGET_WINDOW) {
            mDrawerContent.layout(left, top + windowRect.top, right, bottom);
        } else {
            mDrawerContent.layout(left, mDecorContent.getTop(), right, bottom);
        }
        mDecorContent.layout(mDecorContent.getLeft(), mDecorContent.getTop(),
                mDecorContent.getLeft() + right, bottom);
    }

    /**
     * On intercept touch event.
     *
     * @param ev the ev
     * @return true, if successful
     * @see android.view.ViewGroup#onInterceptTouchEvent(android.view.MotionEvent)
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final ViewConfiguration vc = ViewConfiguration.get(getContext());
        final float touchThreshold = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30.0f,
                getResources().getDisplayMetrics());
        final int widthPixels = getResources().getDisplayMetrics().widthPixels;

        final double hypo;
        final boolean overcameSlop;

        /* Immediately bomb out if the drawer is disabled */
        if (!mDrawerEnabled) {
            return false;
        }

        /*
         * ...otherwise, handle the various types of input events.
         */
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                /*
                * Record the starting X and Y positions for the possible gesture.
                */
                mGestureStartX = mGestureCurrentX = (int) (ev.getX() + 0.5f);
                mGestureStartY = mGestureCurrentY = (int) (ev.getY() + 0.5f);

                /*
                * If the starting X position is within the touch threshold of 30dp inside the
                * screen's
                * left edge, set mGestureStared to true so that future ACTION_MOVE events will
                * continue being handled here.
                */

                if (mGestureStartX < touchThreshold && !mDrawerOpened) {
                    mGestureStarted = true;
                }

                if (mGestureStartX > widthPixels - mTouchTargetWidth && mDrawerOpened) {
                    mGestureStarted = true;
                }

                /*
                * We still want to return false here since we aren't positive we've got a gesture
                * we want just yet.
                */
                return false;
            case MotionEvent.ACTION_MOVE:

                /* Make sure the gesture was started within 30dp of the screen's left edge. */
                if (!mGestureStarted) {
                    return false;
                }

                /* Make sure we're not going backwards, but only if the drawer isn't open yet */
                if (!mDrawerOpened && (ev.getX() < mGestureCurrentX || ev
                        .getX() < mGestureStartX)) {
                    return (mGestureStarted = false);
                }

                /*
                * Update the current X and Y positions for the gesture.
                */
                mGestureCurrentX = (int) (ev.getX() + 0.5f);
                mGestureCurrentY = (int) (ev.getY() + 0.5f);

                /*
                * Decide whether there is enough movement to do anything real.
                */
                hypo = Math.hypot(mGestureCurrentX - mGestureStartX,
                        mGestureCurrentY - mGestureStartY);
                overcameSlop = hypo > vc.getScaledTouchSlop();

                /*
                * If the last check is true, we'll start handling events in DrawerGarment's
                * onTouchEvent(MotionEvent) method from now on.
                */
                return overcameSlop;
            case MotionEvent.ACTION_UP:
                /*
                * If we just tapped the right edge with the drawer open, close the drawer.
                */
                if (mGestureStartX > widthPixels - mTouchTargetWidth && mDrawerOpened) {
                    closeDrawer();
                }

                mGestureStarted = false;
                mGestureStartX = mGestureCurrentX = -1;
                mGestureStartY = mGestureCurrentY = -1;
                return false;
        }

        return false;
    }

    /**
     * On touch event.
     *
     * @param event the event
     * @return true, if successful
     * @see android.view.View#onTouchEvent(android.view.MotionEvent)
     */
    @SuppressWarnings("unused")
	@Override
    public boolean onTouchEvent(MotionEvent event) {

        final ViewConfiguration vc = ViewConfiguration.get(getContext());
        final int widthPixels = getResources().getDisplayMetrics().widthPixels;

        final int deltaX = (int) (event.getX() + 0.5f) - mGestureCurrentX;
        final int deltaY = (int) (event.getY() + 0.5f) - mGestureCurrentY;

        /*
         * Obtain a new VelocityTracker if we don't already have one. Also add this MotionEvent
         * to the new/existing VelocityTracker so we can determine flings later on.
         */
        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);

        /*
         * Update the current X and Y positions for the ongoing gesture.
         */
        mGestureCurrentX = (int) (event.getX() + 0.5f);
        mGestureCurrentY = (int) (event.getY() + 0.5f);

        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                mDrawerMoving = true;

                if (mDecorOffsetX + deltaX > widthPixels - mTouchTargetWidth) {
                    if (mDecorOffsetX != widthPixels - mTouchTargetWidth) {
                        mDrawerOpened = true;
                        mDecorContent.offsetLeftAndRight(
                                widthPixels - mTouchTargetWidth - mDecorOffsetX);
                        mDecorOffsetX = widthPixels - mTouchTargetWidth;
                        invalidate();
                    }
                } else if (mDecorOffsetX + deltaX < 0) {
                    if (mDecorOffsetX != 0) {
                        mDrawerOpened = false;
                        mDecorContent.offsetLeftAndRight(0 - mDecorContent.getLeft());
                        mDecorOffsetX = 0;
                        invalidate();
                    }
                } else {
                    mDecorContent.offsetLeftAndRight(deltaX);
                    mDecorOffsetX += deltaX;
                    invalidate();
                }

                return true;
            case MotionEvent.ACTION_UP:
                mGestureStarted = false;
                mDrawerMoving = false;

                /*
                * Determine if the user performed a fling based on the final velocity of the
                * gesture.
                */
                mVelocityTracker.computeCurrentVelocity(1000);
                if (Math.abs(mVelocityTracker.getXVelocity()) > vc
                        .getScaledMinimumFlingVelocity()) {
                    /*
                    * Okay, the user did a fling, so determine the direction in which
                    * the fling was flung so we know which way to toggle the drawer state.
                    */
                    if (mVelocityTracker.getXVelocity() > 0) {
                        mDrawerOpened = false;
                        openDrawer();
                    } else {
                        mDrawerOpened = true;
                        closeDrawer();
                    }
                } else {
                    /*
                    * No sizable fling has been flung, so fling the drawer towards whichever side
                    * we're closest to being flung at.
                    */
                    if (mDecorOffsetX > (widthPixels / 2.0)) {
                        mDrawerOpened = false;
                        openDrawer();
                    } else {
                        mDrawerOpened = true;
                        closeDrawer();
                    }
                }
                return true;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see android.view.ViewGroup#dispatchDraw(android.graphics.Canvas)
     */
    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        if (mDrawerOpened || mDrawerMoving) {
            canvas.save();
            canvas.translate(mDecorOffsetX, 0);
            mShadowDrawable.draw(canvas);
            canvas.restore();
        }
    }

    /**
     * Sets the drawer enabled.
     *
     * @param enabled the new drawer enabled
     */
    public void setDrawerEnabled(final boolean enabled) {
        mDrawerEnabled = enabled;
    }

    /**
     * Checks if is drawer enabled.
     *
     * @return true, if is drawer enabled
     */
    public boolean isDrawerEnabled() {
        return mDrawerEnabled;
    }

    /**
     * Toggle drawer.
     *
     * @param animate the animate
     */
    public void toggleDrawer(final boolean animate) {
        if (!mDrawerOpened) {
            openDrawer(animate);
        } else {
            closeDrawer(animate);
        }
    }

    /**
     * Toggle drawer.
     */
    public void toggleDrawer() {
        toggleDrawer(true);
    }

    /**
     * Open drawer.
     *
     * @param animate the animate
     */
    public void openDrawer(final boolean animate) {
        if (mDrawerOpened || mDrawerMoving) {
            return;
        }

        mDrawerMoving = true;

        final int widthPixels = getResources().getDisplayMetrics().widthPixels;
        mScroller
                .startScroll(mDecorOffsetX, 0, (widthPixels - mTouchTargetWidth) - mDecorOffsetX, 0,
                        animate ? SCROLL_DURATION : 0);

        mScrollerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final boolean scrolling = mScroller.computeScrollOffset();
                mDecorContent.offsetLeftAndRight(mScroller.getCurrX() - mDecorOffsetX);
                mDecorOffsetX = mScroller.getCurrX();
                postInvalidate();

                if (!scrolling) {
                    mDrawerMoving = false;
                    mDrawerOpened = true;
                    if (mDrawerCallbacks != null) {
                        mScrollerHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                enableDisableViewGroup(mDecorContent, false);
                                mDrawerCallbacks.onDrawerOpened();
                            }
                        });
                    }
                } else {
                    mScrollerHandler.postDelayed(this, 16);
                }
            }
        }, 16);
    }

    /**
     * Open drawer.
     */
    public void openDrawer() {
        openDrawer(true);
    }

    /**
     * Close drawer.
     *
     * @param animate the animate
     */
    @SuppressWarnings("unused")
	public void closeDrawer(final boolean animate) {
        if (!mDrawerOpened || mDrawerMoving) {
            return;
        }

        mDrawerMoving = true;

        final int widthPixels = getResources().getDisplayMetrics().widthPixels;
        mScroller.startScroll(mDecorOffsetX, 0, -mDecorOffsetX, 0,
                animate ? SCROLL_DURATION : 0);

        mScrollerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final boolean scrolling = mScroller.computeScrollOffset();
                mDecorContent.offsetLeftAndRight(mScroller.getCurrX() - mDecorOffsetX);
                mDecorOffsetX = mScroller.getCurrX();
                postInvalidate();

                if (!scrolling) {
                    mDrawerMoving = false;
                    mDrawerOpened = false;
                    if (mDrawerCallbacks != null) {
                        mScrollerHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                enableDisableViewGroup(mDecorContent, true);
                                mDrawerCallbacks.onDrawerClosed();
                            }
                        });
                    }
                } else {
                    mScrollerHandler.postDelayed(this, 16);
                }
            }
        }, 16);
    }

    /**
     * Close drawer.
     */
    public void closeDrawer() {
        closeDrawer(true);
    }

    /**
     * Checks if is drawer opened.
     *
     * @return true, if is drawer opened
     */
    public boolean isDrawerOpened() {
        return mDrawerOpened;
    }

    /**
     * Checks if is drawer moving.
     *
     * @return true, if is drawer moving
     */
    public boolean isDrawerMoving() {
        return mDrawerMoving;
    }

    /**
     * Sets the drawer callbacks.
     *
     * @param callbacks the new drawer callbacks
     */
    public void setDrawerCallbacks(final IDrawerCallbacks callbacks) {
        mDrawerCallbacks = callbacks;
    }

    /**
     * Gets the drawer callbacks.
     *
     * @return the drawer callbacks
     */
    public IDrawerCallbacks getDrawerCallbacks() {
        return mDrawerCallbacks;
    }

    /**
     * Gets the slide target.
     *
     * @return the slide target
     */
    public int getSlideTarget() {
        return mSlideTarget;
    }

    /**
     * Sets the slide target.
     *
     * @param slideTarget the new slide target
     */
    public void setSlideTarget(final int slideTarget) {
        if (mSlideTarget != slideTarget) {
            mSlideTarget = slideTarget;
            reconfigureViewHierarchy();
        }

    }

    /**
     * Enable disable view group.
     *
     * @param viewGroup the view group
     * @param enabled the enabled
     */
    public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            if (view.isFocusable()) {
                view.setEnabled(enabled);
            }
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            } else if (view instanceof ListView) {
                if (view.isFocusable()) {
                    view.setEnabled(enabled);
                }
                ListView listView = (ListView) view;
                int listChildCount = listView.getChildCount();
                for (int j = 0; j < listChildCount; j++) {
                    if (view.isFocusable()) {
                        listView.getChildAt(j).setEnabled(false);
                    }
                }
            }
        }
    }
}