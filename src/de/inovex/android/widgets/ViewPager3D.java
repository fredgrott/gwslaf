package de.inovex.android.widgets;

import org.bitbucket.fredgrott.gwslaf.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Camera;

import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.ObjectAnimator;

// TODO: Auto-generated Javadoc
/**
 * The Class ViewPager3D.
 */
public class ViewPager3D extends ViewPager {

	/** maximum overscroll rotation of the children is 90 divided by this value. */
	final static float DEFAULT_OVERSCROLL_ROTATION = 2f;

	/** maximum z distance to translate child view. */
	final static int DEFAULT_OVERSCROLL_TRANSLATION = 150;

	/** maximum z distanze during swipe. */
	final static int DEFAULT_SWIPE_TRANSLATION = 100;

	/** maximum rotation during swipe is 90 divided by this value. */
	final static float DEFAULT_SWIPE_ROTATION = 3;

	/** duration of overscroll animation in ms. */
	final private static int DEFAULT_OVERSCROLL_ANIMATION_DURATION = 400;

	/** if true alpha of children gets animated during swipe and overscroll. */
	final private static boolean DEFAULT_ANIMATE_ALPHA = true;

	/** The Constant DEBUG_TAG. */
	@SuppressWarnings("unused")
	private final static String DEBUG_TAG = ViewPager.class.getSimpleName();
	
	/** The Constant INVALID_POINTER_ID. */
	private final static int INVALID_POINTER_ID = -1;
	
	/** The Constant RADIANS. */
	private final static double RADIANS = 180f / Math.PI;

	/**
	 * The Class OverscrollEffect.
	 *
	 * @author renard
	 */
	private class OverscrollEffect {
		
		/** The m overscroll. */
		private float mOverscroll;
		
		/** The m animator. */
		private Animator mAnimator;

		/**
		 * Sets the pull.
		 *
		 * @param deltaDistance [0..1] 0->no overscroll, 1>full overscroll
		 */
		public void setPull(final float deltaDistance) {
			mOverscroll = deltaDistance;
			invalidate();
		}

		/**
		 * called when finger is released. starts to animate back to default
		 * position
		 */
		private void onRelease() {
			if (mAnimator != null && mAnimator.isRunning()) {
				mAnimator.addListener(new AnimatorListener() {

					@Override
					public void onAnimationStart(Animator animation) {
					}

					@Override
					public void onAnimationRepeat(Animator animation) {
					}

					@Override
					public void onAnimationEnd(Animator animation) {
						startAnimation(0);
					}

					@Override
					public void onAnimationCancel(Animator animation) {
					}
				});
				mAnimator.cancel();
			} else {
				startAnimation(0);
			}
		}

		/**
		 * Start animation.
		 *
		 * @param target the target
		 */
		private void startAnimation(final float target) {
			mAnimator = ObjectAnimator.ofFloat(this, "pull", mOverscroll, target);
			mAnimator.setInterpolator(new DecelerateInterpolator());
			final float scale = Math.abs(target - mOverscroll);
			mAnimator.setDuration((long) (DEFAULT_OVERSCROLL_ANIMATION_DURATION * scale));
			mAnimator.start();
		}

		/**
		 * Checks if is overscrolling.
		 *
		 * @return true, if is overscrolling
		 */
		private boolean isOverscrolling() {
			if (mScrollPosition == 0 && mOverscroll < 0) {
				return true;
			}
			final boolean isLast = (getAdapter().getCount() - 1) == mScrollPosition;
			if (isLast && mOverscroll > 0) {
				return true;
			}
			return false;
		}

	}

	/** The m overscroll effect. */
	final private OverscrollEffect mOverscrollEffect = new OverscrollEffect();
	
	/** The m camera. */
	final private Camera mCamera = new Camera();

	/** The m scroll listener. */
	private OnPageChangeListener mScrollListener;
	
	/** The m last motion x. */
	private float mLastMotionX;
	
	/** The m active pointer id. */
	private int mActivePointerId;
	
	/** The m scroll position. */
	private int mScrollPosition;
	
	/** The m scroll position offset. */
	private float mScrollPositionOffset;
	
	/** The m touch slop. */
	final private int mTouchSlop;

	/** The m overscroll rotation. */
	private float mOverscrollRotation;
	
	/** The m swipe rotation. */
	private float mSwipeRotation;
	
	/** The m overscroll translation. */
	private int mOverscrollTranslation;
	
	/** The m swipe translation. */
	private int mSwipeTranslation;
	
	/** The m overscroll animation duration. */
	private int mOverscrollAnimationDuration;
	
	/** The m animate alpha. */
	private boolean mAnimateAlpha;

	/**
	 * Instantiates a new view pager3 d.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public ViewPager3D(Context context, AttributeSet attrs) {
		super(context, attrs);
		setStaticTransformationsEnabled(true);
		final ViewConfiguration configuration = ViewConfiguration.get(context);
		mTouchSlop = ViewConfigurationCompat.getScaledPagingTouchSlop(configuration);
		super.setOnPageChangeListener(new MyOnPageChangeListener());
		init(attrs);
	}

	/**
	 * Inits the.
	 *
	 * @param attrs the attrs
	 */
	private void init(AttributeSet attrs) {
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.ViewPager3D);
		mOverscrollRotation = a.getFloat(R.styleable.ViewPager3D_overscroll_rotation, DEFAULT_OVERSCROLL_ROTATION);
		mSwipeRotation = a.getFloat(R.styleable.ViewPager3D_swipe_rotation, DEFAULT_SWIPE_ROTATION);
		mSwipeTranslation = a.getInt(R.styleable.ViewPager3D_swipe_translation, DEFAULT_SWIPE_TRANSLATION);
		mOverscrollTranslation = a.getInt(R.styleable.ViewPager3D_overscroll_translation, DEFAULT_OVERSCROLL_TRANSLATION);
		mOverscrollAnimationDuration = a.getInt(R.styleable.ViewPager3D_overscroll_animation_duration, DEFAULT_OVERSCROLL_ANIMATION_DURATION);
		mAnimateAlpha = a.getBoolean(R.styleable.ViewPager3D_animate_alpha, DEFAULT_ANIMATE_ALPHA);
		a.recycle();
	}

	/**
	 * Checks if is animate alpha.
	 *
	 * @return true, if is animate alpha
	 */
	public boolean isAnimateAlpha() {
		return mAnimateAlpha;
	}

	/**
	 * Sets the animate alpha.
	 *
	 * @param mAnimateAlpha the new animate alpha
	 */
	public void setAnimateAlpha(boolean mAnimateAlpha) {
		this.mAnimateAlpha = mAnimateAlpha;
	}

	/**
	 * Gets the overscroll animation duration.
	 *
	 * @return the overscroll animation duration
	 */
	public int getOverscrollAnimationDuration() {
		return mOverscrollAnimationDuration;
	}

	/**
	 * Sets the overscroll animation duration.
	 *
	 * @param mOverscrollAnimationDuration the new overscroll animation duration
	 */
	public void setOverscrollAnimationDuration(int mOverscrollAnimationDuration) {
		this.mOverscrollAnimationDuration = mOverscrollAnimationDuration;
	}

	/**
	 * Gets the swipe translation.
	 *
	 * @return the swipe translation
	 */
	public int getSwipeTranslation() {
		return mSwipeTranslation;
	}

	/**
	 * Sets the swipe translation.
	 *
	 * @param mSwipeTranslation the new swipe translation
	 */
	public void setSwipeTranslation(int mSwipeTranslation) {
		this.mSwipeTranslation = mSwipeTranslation;
	}

	/**
	 * Gets the overscroll translation.
	 *
	 * @return the overscroll translation
	 */
	public int getOverscrollTranslation() {
		return mOverscrollTranslation;
	}

	/**
	 * Sets the overscroll translation.
	 *
	 * @param mOverscrollTranslation the new overscroll translation
	 */
	public void setOverscrollTranslation(int mOverscrollTranslation) {
		this.mOverscrollTranslation = mOverscrollTranslation;
	}

	/**
	 * Gets the swipe rotation.
	 *
	 * @return the swipe rotation
	 */
	public float getSwipeRotation() {
		return mSwipeRotation;
	}

	/**
	 * Sets the swipe rotation.
	 *
	 * @param mSwipeRotation the new swipe rotation
	 */
	public void setSwipeRotation(float mSwipeRotation) {
		this.mSwipeRotation = mSwipeRotation;
	}

	/**
	 * Gets the overscroll rotation.
	 *
	 * @return the overscroll rotation
	 */
	public float getOverscrollRotation() {
		return mOverscrollRotation;
	}

	/**
	 * Sets the overscroll rotation.
	 *
	 * @param mOverscrollRotation the new overscroll rotation
	 */
	public void setOverscrollRotation(float mOverscrollRotation) {
		this.mOverscrollRotation = mOverscrollRotation;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.ViewPager#setOnPageChangeListener(android.support.v4.view.ViewPager.OnPageChangeListener)
	 */
	@Override
	public void setOnPageChangeListener(OnPageChangeListener listener) {
		mScrollListener = listener;
	};

	/**
	 * The listener interface for receiving myOnPageChange events.
	 * The class that is interested in processing a myOnPageChange
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addMyOnPageChangeListener<code> method. When
	 * the myOnPageChange event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see MyOnPageChangeEvent
	 */
	private class MyOnPageChangeListener implements OnPageChangeListener {

		/* (non-Javadoc)
		 * @see android.support.v4.view.ViewPager.OnPageChangeListener#onPageScrolled(int, float, int)
		 */
		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			if (mScrollListener != null) {
				mScrollListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
			}
			mScrollPosition = position;
			mScrollPositionOffset = positionOffset;
		}

		/* (non-Javadoc)
		 * @see android.support.v4.view.ViewPager.OnPageChangeListener#onPageSelected(int)
		 */
		@Override
		public void onPageSelected(int position) {
			if (mScrollListener != null) {
				mScrollListener.onPageSelected(position);
			}
		}

		/* (non-Javadoc)
		 * @see android.support.v4.view.ViewPager.OnPageChangeListener#onPageScrollStateChanged(int)
		 */
		@Override
		public void onPageScrollStateChanged(final int state) {
			if (mScrollListener != null) {
				mScrollListener.onPageScrollStateChanged(state);
			}
			if (state == SCROLL_STATE_IDLE) {
				mScrollPositionOffset = 0;
			}
		}
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.ViewPager#onInterceptTouchEvent(android.view.MotionEvent)
	 */
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		final int action = ev.getAction() & MotionEventCompat.ACTION_MASK;
		switch (action) {
		case MotionEvent.ACTION_DOWN: {
			mLastMotionX = ev.getX();
			mActivePointerId = MotionEventCompat.getPointerId(ev, 0);
			break;
		}
		case MotionEventCompat.ACTION_POINTER_DOWN: {
			final int index = MotionEventCompat.getActionIndex(ev);
			final float x = MotionEventCompat.getX(ev, index);
			mLastMotionX = x;
			mActivePointerId = MotionEventCompat.getPointerId(ev, index);
			break;
		}
		}
		return super.onInterceptTouchEvent(ev);
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.ViewPager#onTouchEvent(android.view.MotionEvent)
	 */
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		boolean callSuper = false;

		final int action = ev.getAction();
		switch (action) {
		case MotionEvent.ACTION_DOWN: {
			mLastMotionX = ev.getX();
			mActivePointerId = MotionEventCompat.getPointerId(ev, 0);
			break;
		}
		case MotionEventCompat.ACTION_POINTER_DOWN: {
			callSuper = true;
			final int index = MotionEventCompat.getActionIndex(ev);
			final float x = MotionEventCompat.getX(ev, index);
			mLastMotionX = x;
			mActivePointerId = MotionEventCompat.getPointerId(ev, index);
			break;
		}
		case MotionEvent.ACTION_MOVE: {
			if (mActivePointerId != INVALID_POINTER_ID) {
				// Scroll to follow the motion event
				final int activePointerIndex = MotionEventCompat.findPointerIndex(ev, mActivePointerId);
				final float x = MotionEventCompat.getX(ev, activePointerIndex);
				final float deltaX = mLastMotionX - x;
				final float oldScrollX = getScrollX();
				final int width = getWidth();
				final int widthWithMargin = width + getPageMargin();
				final int lastItemIndex = getAdapter().getCount() - 1;
				final int currentItemIndex = getCurrentItem();
				final float leftBound = Math.max(0, (currentItemIndex - 1) * widthWithMargin);
				final float rightBound = Math.min(currentItemIndex + 1, lastItemIndex) * widthWithMargin;
				final float scrollX = oldScrollX + deltaX;
				if (mScrollPositionOffset == 0) {
					if (scrollX < leftBound) {
						if (leftBound == 0) {
							final float over = deltaX + mTouchSlop;
							mOverscrollEffect.setPull(over / width);
						}
					} else if (scrollX > rightBound) {
						if (rightBound == lastItemIndex * widthWithMargin) {
							final float over = scrollX - rightBound - mTouchSlop;
							mOverscrollEffect.setPull(over / width);
						}
					}
				} else {
					mLastMotionX = x;
				}
			} else {
				mOverscrollEffect.onRelease();
			}
			break;
		}
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_CANCEL: {
			mActivePointerId = INVALID_POINTER_ID;
			mOverscrollEffect.onRelease();
			break;
		}
		case MotionEvent.ACTION_POINTER_UP: {
			final int pointerIndex = (ev.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
			final int pointerId = MotionEventCompat.getPointerId(ev, pointerIndex);
			if (pointerId == mActivePointerId) {
				// This was our active pointer going up. Choose a new
				// active pointer and adjust accordingly.
				final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
				mLastMotionX = ev.getX(newPointerIndex);
				mActivePointerId = MotionEventCompat.getPointerId(ev, newPointerIndex);
				callSuper = true;
			}
			break;
		}
		}

		if (mOverscrollEffect.isOverscrolling() && !callSuper) {
			return true;
		} else {
			return super.onTouchEvent(ev);
		}
	}

	/* (non-Javadoc)
	 * @see android.view.ViewGroup#getChildStaticTransformation(android.view.View, android.view.animation.Transformation)
	 */
	@Override
	protected boolean getChildStaticTransformation(View child, Transformation t) {
		if (child.getWidth() == 0) {
			return false;
		}
		final int position = child.getLeft() / child.getWidth();
		final boolean isFirstOrLast = position == 0 || (position == getAdapter().getCount() - 1);
		if (mOverscrollEffect.isOverscrolling() && isFirstOrLast) {
			final float dx = getWidth() / 2;
			final int dy = getHeight() / 2;
			t.getMatrix().reset();
			final float translateZ = (float) (mOverscrollTranslation * Math.sin(Math.PI * Math.abs(mOverscrollEffect.mOverscroll)));
			final float degrees = 90 / mOverscrollRotation - (float) ((RADIANS * Math.acos(mOverscrollEffect.mOverscroll)) / mOverscrollRotation);

			mCamera.save();
			mCamera.rotateY(degrees);
			mCamera.translate(0, 0, translateZ);
			mCamera.getMatrix(t.getMatrix());
			mCamera.restore();
			t.getMatrix().preTranslate(-dx, -dy);
			t.getMatrix().postTranslate(dx, dy);

			if (mAnimateAlpha) {
				t.setTransformationType(Transformation.TYPE_BOTH);
				t.setAlpha((float) (Math.sin((1 - Math.abs(mOverscrollEffect.mOverscroll)) * Math.PI / 2)));
			}

			return true;
		} else if (mScrollPositionOffset > 0) {

			float dx = getWidth() / 2;
			final int dy = getHeight() / 2;

			final double degrees;
			if (position > mScrollPosition) {
				if (mAnimateAlpha) {
					t.setTransformationType(Transformation.TYPE_BOTH);
					t.setAlpha((float) (Math.sin(mScrollPositionOffset * Math.PI / 2)));
				}
				// right side
				degrees = -(90 / mSwipeRotation) + (RADIANS * Math.acos(1 - mScrollPositionOffset)) / mSwipeRotation;
			} else {
				if (mAnimateAlpha) {
					t.setTransformationType(Transformation.TYPE_BOTH);
					t.setAlpha((float) (Math.sin(mScrollPositionOffset * Math.PI / 2 + Math.PI / 2)));
				}
				// left side
				degrees = (90 / mSwipeRotation) - (RADIANS * Math.acos(mScrollPositionOffset)) / mSwipeRotation;
			}
			final float translateZ = (float) (mSwipeTranslation * Math.sin((Math.PI) * mScrollPositionOffset));

			t.getMatrix().reset();
			mCamera.save();
			mCamera.rotateY((float) degrees);
			mCamera.translate(0, 0, translateZ);
			mCamera.getMatrix(t.getMatrix());
			mCamera.restore();
			// pivot point is center of child
			t.getMatrix().preTranslate(-dx, -dy);
			t.getMatrix().postTranslate(dx, dy);
			return true;
		}
		return false;
	}
}
