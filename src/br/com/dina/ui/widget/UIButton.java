package br.com.dina.ui.widget;

import org.bitbucket.fredgrott.gwslaf.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

// TODO: Auto-generated Javadoc
/**
 * The Class UIButton.
 */
public class UIButton extends LinearLayout {

	/** The m inflater. */
	private LayoutInflater mInflater;
	
	/** The m button container. */
	private LinearLayout mButtonContainer;
	
	/** The m click listener. */
	private ClickListener mClickListener;
	
	/** The m title. */
	private CharSequence mTitle;
	
	/** The m subtitle. */
	private CharSequence mSubtitle;
	
	/** The m image. */
	private int mImage;
	
	/**
	 * Instantiates a new uI button.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	@SuppressWarnings("deprecation")
	public UIButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setClickable(true);
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mButtonContainer = (LinearLayout) mInflater.inflate(R.layout.list_item_single, null);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
		
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.UIButton, 0, 0);
		mTitle = a.getString(R.styleable.UIButton_titleme);
		mSubtitle = a.getString(R.styleable.UIButton_subtitleme);
		mImage = a.getResourceId(R.styleable.UIButton_image, -1);
		
		if(mTitle != null) {
			((TextView) mButtonContainer.findViewById(R.id.title)).setText(mTitle.toString());
		} else {
			((TextView) mButtonContainer.findViewById(R.id.title)).setText("subtitle");
		}
		
		if(mSubtitle != null) {
			((TextView) mButtonContainer.findViewById(R.id.subtitle)).setText(mSubtitle.toString());
		} else {
			((TextView) mButtonContainer.findViewById(R.id.subtitle)).setVisibility(View.GONE);
		}
		
		if(mImage > -1) {
			((ImageView) mButtonContainer.findViewById(R.id.image)).setImageResource(mImage);
		}
		
		mButtonContainer.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				if(mClickListener != null)
					mClickListener.onClick(UIButton.this);
			}
			
		});
		
		addView(mButtonContainer, params);
	}	
	
	/**
	 * The listener interface for receiving click events.
	 * The class that is interested in processing a click
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addClickListener<code> method. When
	 * the click event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ClickEvent
	 */
	public interface ClickListener {		
		
		/**
		 * On click.
		 *
		 * @param view the view
		 */
		void onClick(View view);		
	}
	
	/**
	 * Adds the click listener.
	 *
	 * @param listener the listener
	 */
	public void addClickListener(ClickListener listener) {
		this.mClickListener = listener;
	}
	
	/**
	 * Removes the click listener.
	 */
	public void removeClickListener() {
		this.mClickListener = null;
	}

}
