package br.com.dina.ui.widget;


import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.bitbucket.fredgrott.gwslaf.R;
import br.com.dina.ui.model.BasicItem;
import br.com.dina.ui.model.IListItem;
import br.com.dina.ui.model.ViewItem;

// TODO: Auto-generated Javadoc
/**
 * The Class UITableView.
 */
public class UITableView extends LinearLayout {
	
	/** The m index controller. */
	private int mIndexController = 0;
	
	/** The m inflater. */
	private LayoutInflater mInflater;
	
	/** The m main container. */
	private LinearLayout mMainContainer;
	
	/** The m list container. */
	private LinearLayout mListContainer;
	
	/** The m item list. */
	private List<IListItem> mItemList;
	
	/** The m click listener. */
	private ClickListener mClickListener;
	
	/**
	 * Instantiates a new uI table view.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public UITableView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mItemList = new ArrayList<IListItem>();
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mMainContainer = (LinearLayout)  mInflater.inflate(R.layout.list_container, null);
		@SuppressWarnings("deprecation")
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
		addView(mMainContainer, params);				
		mListContainer = (LinearLayout) mMainContainer.findViewById(R.id.buttonsContainer);		
	}
	
	/**
	 * Adds the basic item.
	 *
	 * @param title the title
	 */
	public void addBasicItem(String title) {
		mItemList.add(new BasicItem(title));
	}
	
	/**
	 * Adds the basic item.
	 *
	 * @param title the title
	 * @param summary the summary
	 */
	public void addBasicItem(String title, String summary) {
		mItemList.add(new BasicItem(title, summary));
	}
	
	/**
	 * Adds the basic item.
	 *
	 * @param title the title
	 * @param summary the summary
	 * @param color the color
	 */
	public void addBasicItem(String title, String summary, int color) {
		mItemList.add(new BasicItem(title, summary, color));
	}
	
	/**
	 * Adds the basic item.
	 *
	 * @param drawable the drawable
	 * @param title the title
	 * @param summary the summary
	 */
	public void addBasicItem(int drawable, String title, String summary) {
		mItemList.add(new BasicItem(drawable, title, summary));
	}
	
	/**
	 * Adds the basic item.
	 *
	 * @param drawable the drawable
	 * @param title the title
	 * @param summary the summary
	 * @param color the color
	 */
	public void addBasicItem(int drawable, String title, String summary, int color) {
		mItemList.add(new BasicItem(drawable, title, summary, color));
	}
	
	/**
	 * Adds the basic item.
	 *
	 * @param item the item
	 */
	public void addBasicItem(BasicItem item) {
		mItemList.add(item);
	}
	
	/**
	 * Adds the view item.
	 *
	 * @param itemView the item view
	 */
	public void addViewItem(ViewItem itemView) {
		mItemList.add(itemView);
	}
	
	/**
	 * Commit.
	 */
	public void commit() {
		mIndexController = 0;
		
		if(mItemList.size() > 1) {
			//when the list has more than one item
			for(IListItem obj : mItemList) {
				View tempItemView;
				if(mIndexController == 0) {
					tempItemView = mInflater.inflate(R.layout.list_item_top, null);
				}
				else if(mIndexController == mItemList.size()-1) {
					tempItemView = mInflater.inflate(R.layout.list_item_bottom, null);
				}
				else {
					tempItemView = mInflater.inflate(R.layout.list_item_middle, null);
				}	
				setupItem(tempItemView, obj, mIndexController);
				tempItemView.setClickable(obj.isClickable());
				mListContainer.addView(tempItemView);
				mIndexController++;
			}
		}
		else if(mItemList.size() == 1) {
			//when the list has only one item
			View tempItemView = mInflater.inflate(R.layout.list_item_single, null);
			IListItem obj = mItemList.get(0);
			setupItem(tempItemView, obj, mIndexController);
			tempItemView.setClickable(obj.isClickable());
			mListContainer.addView(tempItemView);
		}
	}
	
	/**
	 * Setup item.
	 *
	 * @param view the view
	 * @param item the item
	 * @param index the index
	 */
	private void setupItem(View view, IListItem item, int index) {
		if(item instanceof BasicItem) {
			BasicItem tempItem = (BasicItem) item;
			setupBasicItem(view, tempItem, mIndexController);
		}
		else if(item instanceof ViewItem) {
			ViewItem tempItem = (ViewItem) item;
			setupViewItem(view, tempItem, mIndexController);
		}
	}
	
	/**
	 * Setup basic item.
	 *
	 * @param view the view
	 * @param item the item
	 * @param index the index
	 */
	private void setupBasicItem(View view, BasicItem item, int index) {
		if(item.getDrawable() > -1) {
			((ImageView) view.findViewById(R.id.image)).setBackgroundResource(item.getDrawable());
		}
		if(item.getSubtitle() != null) {
			((TextView) view.findViewById(R.id.subtitle)).setText(item.getSubtitle());
		}
		else {
			((TextView) view.findViewById(R.id.subtitle)).setVisibility(View.GONE);
		}		
		((TextView) view.findViewById(R.id.title)).setText(item.getTitle());
		if(item.getColor() > -1) {
			((TextView) view.findViewById(R.id.title)).setTextColor(item.getColor());
		}
		view.setTag(index);
		if(item.isClickable()) {
			view.setOnClickListener( new View.OnClickListener() {
	
				@Override
				public void onClick(View view) {
					if(mClickListener != null)
						mClickListener.onClick((Integer) view.getTag());
				}
				
			});	
		}
		else {
			((ImageView) view.findViewById(R.id.chevron)).setVisibility(View.GONE);
		}
	}
	
	/**
	 * Setup view item.
	 *
	 * @param view the view
	 * @param itemView the item view
	 * @param index the index
	 */
	private void setupViewItem(View view, ViewItem itemView, int index) {
		if(itemView.getView() != null) {
			LinearLayout itemContainer = (LinearLayout) view.findViewById(R.id.itemContainer);
			itemContainer.removeAllViews();
			//itemContainer.removeAllViewsInLayout();
			itemContainer.addView(itemView.getView());
		}
	}
	
	/**
	 * The listener interface for receiving click events.
	 * The class that is interested in processing a click
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addClickListener<code> method. When
	 * the click event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ClickEvent
	 */
	public interface ClickListener {		
		
		/**
		 * On click.
		 *
		 * @param index the index
		 */
		void onClick(int index);		
	}
	
	/**
	 * Gets the count.
	 *
	 * @return the count
	 */
	public int getCount() {
		return mItemList.size();
	}
	
	/**
	 * Clear.
	 */
	public void clear() {
		mItemList.clear();
		mListContainer.removeAllViews();
	}
	
	/**
	 * Sets the click listener.
	 *
	 * @param listener the new click listener
	 */
	public void setClickListener(ClickListener listener) {
		this.mClickListener = listener;
	}
	
	/**
	 * Removes the click listener.
	 */
	public void removeClickListener() {
		this.mClickListener = null;
	}

}
