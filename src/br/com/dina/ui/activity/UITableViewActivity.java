package br.com.dina.ui.activity;


import android.app.Activity;
import android.os.Bundle;
import org.bitbucket.fredgrott.gwslaf.R;
import br.com.dina.ui.widget.UITableView;

// TODO: Auto-generated Javadoc
/**
 * The Class UITableViewActivity.
 */
public abstract class UITableViewActivity extends Activity {

	/** The m table view. */
	private UITableView mTableView;
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
    public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
	     setContentView(R.layout.uitableview_activity);
	     mTableView = (UITableView) findViewById(R.id.tableView);
	     populateList();
	     mTableView.commit();
	}
	
	/**
	 * Gets the uI table view.
	 *
	 * @return the uI table view
	 */
	protected UITableView getUITableView() {
		return mTableView;
	}
	
	/**
	 * Populate list.
	 */
	protected abstract void populateList();
	
}
