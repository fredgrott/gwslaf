package android.view.ext;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bitbucket.fredgrott.gwslaf.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

// TODO: Auto-generated Javadoc
/**
 * Provides a "Path" like menu for android. ??
 * 
 * TODO: tell about usage
 * 
 * @author Siyamed SINIR
 * 
 */
public class SatelliteMenu extends FrameLayout {

	/** The Constant DEFAULT_SATELLITE_DISTANCE. */
	private static final int DEFAULT_SATELLITE_DISTANCE = 200;
	
	/** The Constant DEFAULT_TOTAL_SPACING_DEGREES. */
	private static final float DEFAULT_TOTAL_SPACING_DEGREES = 90f;
	
	/** The Constant DEFAULT_CLOSE_ON_CLICK. */
	private static final boolean DEFAULT_CLOSE_ON_CLICK = true;
	
	/** The Constant DEFAULT_EXPAND_DURATION. */
	private static final int DEFAULT_EXPAND_DURATION = 400;

	/** The main rotate right. */
	private Animation mainRotateRight;
	
	/** The main rotate left. */
	private Animation mainRotateLeft;

	/** The img main. */
	private ImageView imgMain;
	
	/** The item clicked listener. */
	private SateliteClickedListener itemClickedListener;
	
	/** The internal item click listener. */
	private InternalSatelliteOnClickListener internalItemClickListener;

	/** The menu items. */
	private List<SatelliteMenuItem> menuItems = new ArrayList<SatelliteMenuItem>();
	
	/** The view to item map. */
	private Map<View, SatelliteMenuItem> viewToItemMap = new HashMap<View, SatelliteMenuItem>();

	/** The plus animation active. */
	private AtomicBoolean plusAnimationActive = new AtomicBoolean(false);

	// ?? how to save/restore?
	/** The gap degrees provider. */
	private IDegreeProvider gapDegreesProvider = new DefaultDegreeProvider();

	//States of these variables are saved
	/** The rotated. */
	private boolean rotated = false;
	
	/** The measure diff. */
	private int measureDiff = 0;
	//States of these variables are saved - Also configured from XML 
	/** The total spacing degree. */
	private float totalSpacingDegree = DEFAULT_TOTAL_SPACING_DEGREES;
	
	/** The satellite distance. */
	private int satelliteDistance = DEFAULT_SATELLITE_DISTANCE;	
	
	/** The expand duration. */
	private int expandDuration = DEFAULT_EXPAND_DURATION;
	
	/** The close items on click. */
	private boolean closeItemsOnClick = DEFAULT_CLOSE_ON_CLICK;

	/**
	 * Instantiates a new satellite menu.
	 *
	 * @param context the context
	 */
	public SatelliteMenu(Context context) {
		super(context);
		init(context, null, 0);
	}

	/**
	 * Instantiates a new satellite menu.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public SatelliteMenu(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, 0);
	}

	/**
	 * Instantiates a new satellite menu.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	public SatelliteMenu(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	/**
	 * Inits the.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 * @param defStyle the def style
	 */
	private void init(Context context, AttributeSet attrs, int defStyle) {
		LayoutInflater.from(context).inflate(R.layout.sat_main, this, true);		
		imgMain = (ImageView) findViewById(R.id.sat_main);

		if(attrs != null){			
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SatelliteMenu, defStyle, 0);					
			satelliteDistance = typedArray.getDimensionPixelSize(R.styleable.SatelliteMenu_satelliteDistance, DEFAULT_SATELLITE_DISTANCE);
			totalSpacingDegree = typedArray.getFloat(R.styleable.SatelliteMenu_totalSpacingDegree, DEFAULT_TOTAL_SPACING_DEGREES);
			closeItemsOnClick = typedArray.getBoolean(R.styleable.SatelliteMenu_closeOnClick, DEFAULT_CLOSE_ON_CLICK);
			expandDuration = typedArray.getInt(R.styleable.SatelliteMenu_expandDuration, DEFAULT_EXPAND_DURATION);
			//float satelliteDistance = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 170, getResources().getDisplayMetrics());
			typedArray.recycle();
		}
		
		
		mainRotateLeft = SatelliteAnimationCreator.createMainButtonAnimation(context);
		mainRotateRight = SatelliteAnimationCreator.createMainButtonInverseAnimation(context);

		Animation.AnimationListener plusAnimationListener = new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				plusAnimationActive.set(false);
			}
		};

		mainRotateLeft.setAnimationListener(plusAnimationListener);
		mainRotateRight.setAnimationListener(plusAnimationListener);

		imgMain.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SatelliteMenu.this.onClick();
			}
		});

		internalItemClickListener = new InternalSatelliteOnClickListener(this);
	}

	/**
	 * On click.
	 */
	private void onClick() {
		if (plusAnimationActive.compareAndSet(false, true)) {
			if (!rotated) {
				imgMain.startAnimation(mainRotateLeft);
				for (SatelliteMenuItem item : menuItems) {
					item.getView().startAnimation(item.getOutAnimation());
				}
			} else {
				imgMain.startAnimation(mainRotateRight);
				for (SatelliteMenuItem item : menuItems) {
					item.getView().startAnimation(item.getInAnimation());
				}
			}
			rotated = !rotated;
		}
	}

	/**
	 * Open items.
	 */
	private void openItems() {
		if (plusAnimationActive.compareAndSet(false, true)) {
			if (!rotated) {
				imgMain.startAnimation(mainRotateLeft);
				for (SatelliteMenuItem item : menuItems) {
					item.getView().startAnimation(item.getOutAnimation());
				}
			}
			rotated = !rotated;
		}
	}

	/**
	 * Close items.
	 */
	private void closeItems() {
		if (plusAnimationActive.compareAndSet(false, true)) {
			if (rotated) {
				imgMain.startAnimation(mainRotateRight);
				for (SatelliteMenuItem item : menuItems) {
					item.getView().startAnimation(item.getInAnimation());
				}
			}
			rotated = !rotated;
		}
	}

	/**
	 * Adds the items.
	 *
	 * @param items the items
	 */
	public void addItems(List<SatelliteMenuItem> items) {

		menuItems.addAll(items);
		this.removeView(imgMain);
		TextView tmpView = new TextView(getContext());
		tmpView.setLayoutParams(new FrameLayout.LayoutParams(0, 0));

		float[] degrees = getDegrees(menuItems.size());
		int index = 0;
		for (SatelliteMenuItem menuItem : menuItems) {
			int finalX = SatelliteAnimationCreator.getTranslateX(
					degrees[index], satelliteDistance);
			int finalY = SatelliteAnimationCreator.getTranslateY(
					degrees[index], satelliteDistance);

			ImageView itemView = (ImageView) LayoutInflater.from(getContext())
					.inflate(R.layout.sat_item_cr, this, false);
			ImageView cloneView = (ImageView) LayoutInflater.from(getContext())
					.inflate(R.layout.sat_item_cr, this, false);
			itemView.setTag(menuItem.getId());
			cloneView.setVisibility(View.GONE);
			itemView.setVisibility(View.GONE);

			cloneView.setOnClickListener(internalItemClickListener);
			cloneView.setTag(Integer.valueOf(menuItem.getId()));
			FrameLayout.LayoutParams layoutParams = getLayoutParams(cloneView);
			layoutParams.bottomMargin = Math.abs(finalY);
			layoutParams.leftMargin = Math.abs(finalX);
			cloneView.setLayoutParams(layoutParams);

			if (menuItem.getImgResourceId() > 0) {
				itemView.setImageResource(menuItem.getImgResourceId());
				cloneView.setImageResource(menuItem.getImgResourceId());
			} else if (menuItem.getImgDrawable() != null) {
				itemView.setImageDrawable(menuItem.getImgDrawable());
				cloneView.setImageDrawable(menuItem.getImgDrawable());
			}

			Animation itemOut = SatelliteAnimationCreator.createItemOutAnimation(getContext(), index,expandDuration, finalX, finalY);
			Animation itemIn = SatelliteAnimationCreator.createItemInAnimation(getContext(), index, expandDuration, finalX, finalY);
			Animation itemClick = SatelliteAnimationCreator.createItemClickAnimation(getContext());

			menuItem.setView(itemView);
			menuItem.setCloneView(cloneView);
			menuItem.setInAnimation(itemIn);
			menuItem.setOutAnimation(itemOut);
			menuItem.setClickAnimation(itemClick);
			menuItem.setFinalX(finalX);
			menuItem.setFinalY(finalY);

			itemIn.setAnimationListener(new SatelliteAnimationListener(itemView, true, viewToItemMap));
			itemOut.setAnimationListener(new SatelliteAnimationListener(itemView, false, viewToItemMap));
			itemClick.setAnimationListener(new SatelliteItemClickAnimationListener(this, menuItem.getId()));
			
			this.addView(itemView);
			this.addView(cloneView);
			viewToItemMap.put(itemView, menuItem);
			viewToItemMap.put(cloneView, menuItem);
			index++;
		}

		this.addView(imgMain);
	}

	/**
	 * Gets the degrees.
	 *
	 * @param count the count
	 * @return the degrees
	 */
	private float[] getDegrees(int count) {
		return gapDegreesProvider.getDegrees(count, totalSpacingDegree);
	}

	/**
	 * Recalculate measure diff.
	 */
	private void recalculateMeasureDiff() {
		int itemWidth = 0;
		if (menuItems.size() > 0) {
			itemWidth = menuItems.get(0).getView().getWidth();
		}
		measureDiff = Float.valueOf(satelliteDistance * 0.2f).intValue()
				+ itemWidth;
	}

	/**
	 * Reset items.
	 */
	private void resetItems() {
		if (menuItems.size() > 0) {
			List<SatelliteMenuItem> items = new ArrayList<SatelliteMenuItem>(
					menuItems);
			menuItems.clear();
			this.removeAllViews();
			addItems(items);
		}
	}

	/* (non-Javadoc)
	 * @see android.widget.FrameLayout#onMeasure(int, int)
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		recalculateMeasureDiff();

		int totalHeight = imgMain.getHeight() + satelliteDistance + measureDiff;
		int totalWidth = imgMain.getWidth() + satelliteDistance + measureDiff;
		setMeasuredDimension(totalWidth, totalHeight);
	}

	/**
	 * The listener interface for receiving satelliteItemClickAnimation events.
	 * The class that is interested in processing a satelliteItemClickAnimation
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addSatelliteItemClickAnimationListener<code> method. When
	 * the satelliteItemClickAnimation event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see SatelliteItemClickAnimationEvent
	 */
	private static class SatelliteItemClickAnimationListener implements Animation.AnimationListener {
		
		/** The menu ref. */
		private WeakReference<SatelliteMenu> menuRef;
		
		/** The tag. */
		private int tag;
		
		/**
		 * Instantiates a new satellite item click animation listener.
		 *
		 * @param menu the menu
		 * @param tag the tag
		 */
		public SatelliteItemClickAnimationListener(SatelliteMenu menu, int tag) {
			this.menuRef = new WeakReference<SatelliteMenu>(menu);
			this.tag = tag;
		}
		
		/* (non-Javadoc)
		 * @see android.view.animation.Animation.AnimationListener#onAnimationEnd(android.view.animation.Animation)
		 */
		@Override
		public void onAnimationEnd(Animation animation) {
		}
		
		/* (non-Javadoc)
		 * @see android.view.animation.Animation.AnimationListener#onAnimationRepeat(android.view.animation.Animation)
		 */
		@Override
		public void onAnimationRepeat(Animation animation) {
		}
		
		/* (non-Javadoc)
		 * @see android.view.animation.Animation.AnimationListener#onAnimationStart(android.view.animation.Animation)
		 */
		@Override
		public void onAnimationStart(Animation animation) {
			SatelliteMenu menu = menuRef.get();
			if(menu != null && menu.closeItemsOnClick){
				menu.close();
				if(menu.itemClickedListener != null){
					menu.itemClickedListener.eventOccured(tag);
				}
			}
		}		
	}
	
	/**
	 * The listener interface for receiving satelliteAnimation events.
	 * The class that is interested in processing a satelliteAnimation
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addSatelliteAnimationListener<code> method. When
	 * the satelliteAnimation event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see SatelliteAnimationEvent
	 */
	private static class SatelliteAnimationListener implements Animation.AnimationListener {
		
		/** The view ref. */
		private WeakReference<View> viewRef;
		
		/** The is in animation. */
		private boolean isInAnimation;
		
		/** The view to item map. */
		private Map<View, SatelliteMenuItem> viewToItemMap;

		/**
		 * Instantiates a new satellite animation listener.
		 *
		 * @param view the view
		 * @param isIn the is in
		 * @param viewToItemMap the view to item map
		 */
		public SatelliteAnimationListener(View view, boolean isIn, Map<View, SatelliteMenuItem> viewToItemMap) {
			this.viewRef = new WeakReference<View>(view);
			this.isInAnimation = isIn;
			this.viewToItemMap = viewToItemMap;
		}

		/* (non-Javadoc)
		 * @see android.view.animation.Animation.AnimationListener#onAnimationStart(android.view.animation.Animation)
		 */
		@Override
		public void onAnimationStart(Animation animation) {
			if (viewRef != null) {
				View view = viewRef.get();
				if (view != null) {
					SatelliteMenuItem menuItem = viewToItemMap.get(view);
					if (isInAnimation) {
						menuItem.getView().setVisibility(View.VISIBLE);
						menuItem.getCloneView().setVisibility(View.GONE);
					} else {
						menuItem.getCloneView().setVisibility(View.GONE);
						menuItem.getView().setVisibility(View.VISIBLE);
					}
				}
			}
		}

		/* (non-Javadoc)
		 * @see android.view.animation.Animation.AnimationListener#onAnimationRepeat(android.view.animation.Animation)
		 */
		@Override
		public void onAnimationRepeat(Animation animation) {
		}

		/* (non-Javadoc)
		 * @see android.view.animation.Animation.AnimationListener#onAnimationEnd(android.view.animation.Animation)
		 */
		@Override
		public void onAnimationEnd(Animation animation) {
			if (viewRef != null) {
				View view = viewRef.get();
				if (view != null) {
					SatelliteMenuItem menuItem = viewToItemMap.get(view);

					if (isInAnimation) {
						menuItem.getView().setVisibility(View.GONE);
						menuItem.getCloneView().setVisibility(View.GONE);
					} else {
						menuItem.getCloneView().setVisibility(View.VISIBLE);
						menuItem.getView().setVisibility(View.GONE);
					}
				}
			}
		}
	}

	/**
	 * Gets the view to item map.
	 *
	 * @return the view to item map
	 */
	public Map<View, SatelliteMenuItem> getViewToItemMap() {
		return viewToItemMap;
	}
	
	/**
	 * Gets the layout params.
	 *
	 * @param view the view
	 * @return the layout params
	 */
	private static FrameLayout.LayoutParams getLayoutParams(View view) {
		return (FrameLayout.LayoutParams) view.getLayoutParams();
	}

	/**
	 * The listener interface for receiving internalSatelliteOnClick events.
	 * The class that is interested in processing a internalSatelliteOnClick
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addInternalSatelliteOnClickListener<code> method. When
	 * the internalSatelliteOnClick event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see InternalSatelliteOnClickEvent
	 */
	private static class InternalSatelliteOnClickListener implements View.OnClickListener {
		
		/** The menu ref. */
		private WeakReference<SatelliteMenu> menuRef;
		
		/**
		 * Instantiates a new internal satellite on click listener.
		 *
		 * @param menu the menu
		 */
		public InternalSatelliteOnClickListener(SatelliteMenu menu) {
			this.menuRef = new WeakReference<SatelliteMenu>(menu);
		}

		/* (non-Javadoc)
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			SatelliteMenu menu = menuRef.get();
			if(menu != null){
				SatelliteMenuItem menuItem = menu.getViewToItemMap().get(v);
				v.startAnimation(menuItem.getClickAnimation());	
			}
		}
	}
	
	/**
	 * Sets the click listener for satellite items.
	 *
	 * @param itemClickedListener the new on item clicked listener
	 */
	public void setOnItemClickedListener(SateliteClickedListener itemClickedListener) {
		this.itemClickedListener = itemClickedListener;
	}

	
	/**
	 * Defines the algorithm to define the gap between each item.
	 * Note: Calling before adding items is strongly recommended.
	 *
	 * @param gapDegreeProvider the new gap degree provider
	 */
	public void setGapDegreeProvider(IDegreeProvider gapDegreeProvider) {
		this.gapDegreesProvider = gapDegreeProvider;
		resetItems();
	}

	/**
	 * Defines the total space between the initial and final item in degrees.
	 * Note: Calling before adding items is strongly recommended.
	 * 
	 * @param totalSpacingDegree The degree between initial and final items. 
	 */
	public void setTotalSpacingDegree(float totalSpacingDegree) {
		this.totalSpacingDegree = totalSpacingDegree;
		resetItems();
	}

	/**
	 * Sets the distance of items from the center in pixels.
	 * Note: Calling before adding items is strongly recommended.
	 * 
	 * @param distance the distance of items to center in pixels.
	 */
	public void setSatelliteDistance(int distance) {
		this.satelliteDistance = distance;
		resetItems();
	}

	/**
	 * Sets the duration for expanding and collapsing the items in miliseconds. 
	 * Note: Calling before adding items is strongly recommended.
	 * 
	 * @param expandDuration the duration for expanding and collapsing the items in miliseconds.
	 */
	public void setExpandDuration(int expandDuration) {
		this.expandDuration = expandDuration;
		resetItems();
	}
	
	/**
	 * Sets the image resource for the center button.
	 * 
	 * @param resource The image resource.
	 */
	public void setMainImage(int resource) {
		this.imgMain.setImageResource(resource);
	}

	/**
	 * Sets the image drawable for the center button.
	 *
	 * @param drawable the new main image
	 */
	public void setMainImage(Drawable drawable) {
		this.imgMain.setImageDrawable(drawable);
	}

	/**
	 * Defines if the menu shall collapse the items when an item is clicked. Default value is true.
	 *
	 * @param closeItemsOnClick the new close items on click
	 */
	public void setCloseItemsOnClick(boolean closeItemsOnClick) {
		this.closeItemsOnClick = closeItemsOnClick;
	}
	
	/**
	 * The listener class for item click event. 
	 * @author Siyamed SINIR
	 */
	public interface SateliteClickedListener {
		/**
		 * When an item is clicked, informs with the id of the item, which is given while adding the items. 
		 * 
		 * @param id The id of the item. 
		 */
		public void eventOccured(int id);
	}

	/**
	 * Expand the menu items. 
	 */
	public void expand() {
		openItems();
	}

	/**
	 * Collapse the menu items.
	 */
	public void close() {
		closeItems();
	}

	/* (non-Javadoc)
	 * @see android.view.View#onSaveInstanceState()
	 */
	@Override
	protected Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();
		SavedState ss = new SavedState(superState);
		ss.rotated = rotated;
		ss.totalSpacingDegree = totalSpacingDegree;
		ss.satelliteDistance = satelliteDistance;
		ss.measureDiff = measureDiff;
		ss.expandDuration = expandDuration;
		ss.closeItemsOnClick = closeItemsOnClick;
		return ss;
	}

	/* (non-Javadoc)
	 * @see android.view.View#onRestoreInstanceState(android.os.Parcelable)
	 */
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		SavedState ss = (SavedState) state;
		rotated = ss.rotated;
		totalSpacingDegree = ss.totalSpacingDegree;
		satelliteDistance = ss.satelliteDistance;
		measureDiff = ss.measureDiff;
		expandDuration = ss.expandDuration;
		closeItemsOnClick = ss.closeItemsOnClick;

		super.onRestoreInstanceState(ss.getSuperState());
	}

	/**
	 * The Class SavedState.
	 */
	static class SavedState extends BaseSavedState {
		
		/** The rotated. */
		boolean rotated;
		
		/** The total spacing degree. */
		private float totalSpacingDegree;
		
		/** The satellite distance. */
		private int satelliteDistance;
		
		/** The measure diff. */
		private int measureDiff;
		
		/** The expand duration. */
		private int expandDuration;
		
		/** The close items on click. */
		private boolean closeItemsOnClick;
		
		/**
		 * Instantiates a new saved state.
		 *
		 * @param superState the super state
		 */
		SavedState(Parcelable superState) {
			super(superState);
		}

		/**
		 * Instantiates a new saved state.
		 *
		 * @param in the in
		 */
		public SavedState(Parcel in) {
			super(in);
			rotated = Boolean.valueOf(in.readString());
			totalSpacingDegree = in.readFloat();
			satelliteDistance = in.readInt();
			measureDiff = in.readInt();
			expandDuration = in.readInt();
			closeItemsOnClick = Boolean.valueOf(in.readString());
		}

		/* (non-Javadoc)
		 * @see android.view.AbsSavedState#describeContents()
		 */
		@Override
		public int describeContents() {
			return 0;
		}

		/* (non-Javadoc)
		 * @see android.view.AbsSavedState#writeToParcel(android.os.Parcel, int)
		 */
		@Override
		public void writeToParcel(Parcel out, int flags) {
			out.writeString(Boolean.toString(rotated));
			out.writeFloat(totalSpacingDegree);
			out.writeInt(satelliteDistance);
			out.writeInt(measureDiff);
			out.writeInt(expandDuration);
			out.writeString(Boolean.toString(closeItemsOnClick));
		}

		/** The Constant CREATOR. */
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};
	}
}
