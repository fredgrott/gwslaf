package android.support.v4.app;

import android.util.Log;
import android.view.View;
import android.view.Window;
import com.actionbarsherlock.ActionBarSherlock.OnCreatePanelMenuListener;
import com.actionbarsherlock.ActionBarSherlock.OnMenuItemSelectedListener;
import com.actionbarsherlock.ActionBarSherlock.OnPreparePanelListener;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/** I'm in ur package. Stealing ur variables. */
public abstract class _ActionBarSherlockTrojanHorse extends FragmentActivity implements OnCreatePanelMenuListener, OnPreparePanelListener, OnMenuItemSelectedListener {
    
    /** The Constant TAG. */
    private static final String TAG = "_ActionBarSherlockTrojanHorse";

    /**
     * Fragment interface for menu creation callback.
     *
     * @see OnCreateOptionsMenuEvent
     */
    public interface OnCreateOptionsMenuListener {
    	
        /**
         * On create options menu.
         *
         * @param menu the menu
         * @param inflater the inflater
         */
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater);
    }
    
    /**
     * Fragment interface for menu preparation callback.
     *
     * @see OnPrepareOptionsMenuEvent
     */
    public interface OnPrepareOptionsMenuListener {
        
        /**
         * On prepare options menu.
         *
         * @param menu the menu
         */
        public void onPrepareOptionsMenu(Menu menu);
    }
    
    /**
     * Fragment interface for menu item selection callback.
     *
     * @see OnOptionsItemSelectedEvent
     */
    public interface OnOptionsItemSelectedListener {
      
      /**
       * On options item selected.
       *
       * @param item the item
       * @return true, if successful
       */
      public boolean onOptionsItemSelected(MenuItem item);
    }

    /** The m created menus. */
    private ArrayList<Fragment> mCreatedMenus;


    ///////////////////////////////////////////////////////////////////////////
    // Sherlock menu handling
    ///////////////////////////////////////////////////////////////////////////

    /**
     * On create panel menu.
     *
     * @param featureId the feature id
     * @param menu the menu
     * @return true, if successful
     * @see android.support.v4.app.FragmentActivity#onCreatePanelMenu(int, android.view.Menu)
     */
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        Log.d(TAG, "[onCreatePanelMenu] featureId: " + featureId + ", menu: " + menu);

        if (featureId == Window.FEATURE_OPTIONS_PANEL) {
            boolean result = onCreateOptionsMenu(menu);
            Log.d(TAG, "[onCreatePanelMenu] activity create result: " + result);

            MenuInflater inflater = getSupportMenuInflater();
            boolean show = false;
            ArrayList<Fragment> newMenus = null;
            if (mFragments.mActive != null) {
                for (int i = 0; i < mFragments.mAdded.size(); i++) {
                    Fragment f = mFragments.mAdded.get(i);
                    if (f != null && !f.mHidden && f.mHasMenu && f.mMenuVisible && f instanceof OnCreateOptionsMenuListener) {
                        show = true;
                        ((OnCreateOptionsMenuListener)f).onCreateOptionsMenu(menu, inflater);
                        if (newMenus == null) {
                            newMenus = new ArrayList<Fragment>();
                        }
                        newMenus.add(f);
                    }
                }
            }

            if (mCreatedMenus != null) {
                for (int i = 0; i < mCreatedMenus.size(); i++) {
                    Fragment f = mCreatedMenus.get(i);
                    if (newMenus == null || !newMenus.contains(f)) {
                        f.onDestroyOptionsMenu();
                    }
                }
            }

            mCreatedMenus = newMenus;

            Log.d(TAG, "[onCreatePanelMenu] fragments create result: " + show);
            result |= show;

            Log.d(TAG, "[onCreatePanelMenu] returning " + result);
            return result;
        }
        return false;
    }

    /**
     * On prepare panel.
     *
     * @param featureId the feature id
     * @param view the view
     * @param menu the menu
     * @return true, if successful
     * @see android.support.v4.app.FragmentActivity#onPreparePanel(int, android.view.View, android.view.Menu)
     */
    public boolean onPreparePanel(int featureId, View view, Menu menu) {
        Log.d(TAG, "[onPreparePanel] featureId: " + featureId + ", view: " + view + " menu: " + menu);

        if (featureId == Window.FEATURE_OPTIONS_PANEL) {
            boolean result = onPrepareOptionsMenu(menu);
            Log.d(TAG, "[onPreparePanel] activity prepare result: " + result);

            boolean show = false;
            if (mFragments.mActive != null) {
                for (int i = 0; i < mFragments.mAdded.size(); i++) {
                    Fragment f = mFragments.mAdded.get(i);
                    if (f != null && !f.mHidden && f.mHasMenu && f.mMenuVisible && f instanceof OnPrepareOptionsMenuListener) {
                        show = true;
                        ((OnPrepareOptionsMenuListener)f).onPrepareOptionsMenu(menu);
                    }
                }
            }

            Log.d(TAG, "[onPreparePanel] fragments prepare result: " + show);
            result |= show;

            result &= menu.hasVisibleItems();
            Log.d(TAG, "[onPreparePanel] returning " + result);
            return result;
        }
        return false;
    }

    /**
     * On menu item selected.
     *
     * @param featureId the feature id
     * @param item the item
     * @return true, if successful
     * @see android.support.v4.app.FragmentActivity#onMenuItemSelected(int, android.view.MenuItem)
     */
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        Log.d(TAG, "[onMenuItemSelected] featureId: " + featureId + ", item: " + item);

        if (featureId == Window.FEATURE_OPTIONS_PANEL) {
            if (onOptionsItemSelected(item)) {
                return true;
            }

            if (mFragments.mActive != null) {
                for (int i = 0; i < mFragments.mAdded.size(); i++) {
                    Fragment f = mFragments.mAdded.get(i);
                    if (f != null && !f.mHidden && f.mHasMenu && f.mMenuVisible && f instanceof OnOptionsItemSelectedListener) {
                        if (((OnOptionsItemSelectedListener)f).onOptionsItemSelected(item)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * On create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    public abstract boolean onCreateOptionsMenu(Menu menu);

    /**
     * On prepare options menu.
     *
     * @param menu the menu
     * @return true, if successful
     * @see android.app.Activity#onPrepareOptionsMenu(android.view.Menu)
     */
    public abstract boolean onPrepareOptionsMenu(Menu menu);

    /**
     * On options item selected.
     *
     * @param item the item
     * @return true, if successful
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    public abstract boolean onOptionsItemSelected(MenuItem item);

    /**
     * Gets the support menu inflater.
     *
     * @return the support menu inflater
     */
    public abstract MenuInflater getSupportMenuInflater();
}
