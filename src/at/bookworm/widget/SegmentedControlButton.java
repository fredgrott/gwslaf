/*
 * Copyright 2011 Benjamin Ferrari
 * http://bookworm.at
 * https://github.com/bookwormat/segcontrol
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.bookworm.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.RadioButton;
import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class SegmentedControlButton.
 *
 * @author benjamin ferrari
 * @author Grantland Chew <grantlandchew@gmail.com>
 */
public class SegmentedControlButton extends RadioButton {

    /** The m text all caps. */
    private boolean mTextAllCaps;

    /** The m line height selected. */
    private int mLineHeightSelected;
    
    /** The m line height unselected. */
    private int mLineHeightUnselected;

    /** The m line paint. */
    private Paint mLinePaint;

    /**
     * Instantiates a new segmented control button.
     *
     * @param context the context
     * @param attrs the attrs
     */
    public SegmentedControlButton(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.segmentedControlButtonStyle);
    }

    /**
     * Instantiates a new segmented control button.
     *
     * @param context the context
     * @param attrs the attrs
     * @param defStyle the def style
     */
    public SegmentedControlButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    /**
     * Inits the.
     *
     * @param attrs the attrs
     * @param defStyle the def style
     */
    public void init(AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray a = this.getContext().obtainStyledAttributes(attrs, R.styleable.SegmentedControlButton, defStyle, R.style.Widget_Holo_SegmentedControl);

            mTextAllCaps = a.getBoolean(R.styleable.SegmentedControlButton_textAllCaps, false);
            setTextCompat(getText());

            int lineColor = a.getColor(R.styleable.SegmentedControlButton_lineColor, 0);
            mLineHeightUnselected = a.getDimensionPixelSize(R.styleable.SegmentedControlButton_lineHeightUnselected, 0);
            mLineHeightSelected = a.getDimensionPixelSize(R.styleable.SegmentedControlButton_lineHeightSelected, 0);

            mLinePaint = new Paint();
            mLinePaint.setColor(lineColor);
            mLinePaint.setStyle(Style.FILL);

            a.recycle();
        }
    }

    /* (non-Javadoc)
     * @see android.widget.CompoundButton#onDraw(android.graphics.Canvas)
     */
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw the line
        if (mLinePaint.getColor() != 0 && (mLineHeightSelected > 0 || mLineHeightUnselected > 0)) {
            int lineHeight;
            if (isChecked()) {
                lineHeight = mLineHeightSelected;
            } else {
                lineHeight = mLineHeightUnselected;
            }

            if (lineHeight > 0) {
                Rect rect = new Rect(0, this.getHeight() - lineHeight, getWidth(), this.getHeight());
                canvas.drawRect(rect, mLinePaint);
            }
        }
    }

    // Used for android:textAllCaps
    /**
     * Sets the text compat.
     *
     * @param text the new text compat
     */
    public void setTextCompat(CharSequence text) {
        if (mTextAllCaps) {
            setText(text.toString().toUpperCase());
        }
        else {
            setText(text);
        }
    }

    /**
     * Gets the line color.
     *
     * @return the line color
     */
    public int getLineColor() {
        return mLinePaint.getColor();
    }

    /**
     * Gets the line height unselected.
     *
     * @return the line height unselected
     */
    public int getLineHeightUnselected() {
        return mLineHeightUnselected;
    }

    /**
     * Sets the line color.
     *
     * @param lineColor the new line color
     */
    public void setLineColor(int lineColor) {
        mLinePaint.setColor(lineColor);
    }
}
