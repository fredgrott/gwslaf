package at.bookworm.widget;

import android.content.Context;
import android.util.AttributeSet;
import org.bitbucket.fredgrott.gwslaf.R;

// TODO: Auto-generated Javadoc
/**
 * The Class TabControlButton.
 *
 * @author Grantland Chew <grantlandchew@gmail.com>
 */
public class TabControlButton extends SegmentedControlButton {

    /**
     * Instantiates a new tab control button.
     *
     * @param context the context
     * @param attrs the attrs
     */
    public TabControlButton(Context context, AttributeSet attrs) {
        super(context, attrs, R.attr.tabControlButtonStyle);
    }
}
