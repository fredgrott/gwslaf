package fr.xgouchet.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import org.bitbucket.fredgrott.gwslaf.R;

/**
 * 
 * A RangeSlider is a slider bar which has two draggable thumbs. The user can
 * touch either of the thmb and drag it left or right to set the extremities of
 * a range. Placing focusable widgets to the left or right of a RangeSlider is
 * discouraged.
 * 
 * Clients of the RangeSlider can attach a
 * RangeSlider.OnRangeSliderChangeListener to be notified of the user's actions.
 * 
 * 
 * 
 * @author Xavier Gouchet
 * 
 */
public class RangeSlider extends View implements OnTouchListener {

	/**
	 * A callback that notifies clients when the range value has been changed.
	 * This includes changes that were initiated by the user through a touch
	 * gesture as well as changes that were initiated programmatically.
	 */
	public interface OnRangeSliderChangeListener {

		/**
		 * Called when the user moves the left thumb (ie lower bound of the
		 * range)
		 * 
		 * @param slider
		 *            the range slider on which the thumb was moved
		 */
		public void onLeftThumbMove(RangeSlider slider);

		/**
		 * Called when the user moves the right thumb (ie higher bound of the
		 * range)
		 * 
		 * @param slider
		 *            the range slider on which the thumb was moved
		 */
		public void onRightThumbMove(RangeSlider slider);
	}

	/**
	 * Simple constructor to use when creating a view from code.
	 * 
	 * 
	 * @param context
	 *            The Context the view is running in, through which it can
	 *            access the current theme, resources, etc.
	 */
	public RangeSlider(Context context) {
		this(context, null);
	}

	/**
	 * Constructor that is called when inflating a view from XML. This is called
	 * when a view is being constructed from an XML file, supplying attributes
	 * that were specified in the XML file. This version uses a default style of
	 * 0, so the only attribute values applied are those in the Context's Theme
	 * and the given AttributeSet.
	 * 
	 * The method onFinishInflate() will be called after all children have been
	 * added.
	 * 
	 * @param context
	 *            The Context the view is running in, through which it can
	 *            access the current theme, resources, etc. The attributes of
	 *            the XML tag that is inflating the view.
	 * @param attrs
	 *            The attributes of the XML tag that is inflating the view.
	 */
	public RangeSlider(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	/**
	 * Perform inflation from XML and apply a class-specific base style. This
	 * constructor of View allows subclasses to use their own base style when
	 * they are inflating. For example, a Button class's constructor would call
	 * this version of the super class constructor and supply R.attr.buttonStyle
	 * for defStyle; this allows the theme's button style to modify all of the
	 * base view attributes (in particular its background) as well as the Button
	 * class's attributes.
	 * 
	 * @param context
	 *            The Context the view is running in, through which it can
	 *            access the current theme, resources, etc.
	 * @param attrs
	 *            The attributes of the XML tag that is inflating the view.
	 * @param defStyle
	 *            The default style to apply to this view. If 0, no style will
	 *            be applied (beyond what is included in the theme). This may
	 *            either be an attribute resource, whose value will be retrieved
	 *            from the current theme, or an explicit style resource.
	 */
	public RangeSlider(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		initRangeSlider(context, attrs);

		setOnTouchListener(this);
	}

	/**
	 * @see android.view.View.OnTouchListener#onTouch(android.view.View,
	 *      android.view.MotionEvent)
	 */
	public boolean onTouch(View v, MotionEvent e) {

		float width, boundLeft, boundRight, range, touchx, value;

		width = (mViewWidth - mPadLeft - mPadRight);
		range = (mRangeMaxValue - mRangeMinValue);

		boundLeft = ((mRangeLeftValue - mRangeMinValue) * width) / range;
		boundRight = ((mRangeRightValue - mRangeMinValue) * width) / range;

		touchx = e.getX();

		switch (e.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (touchx < boundLeft)
				mDragging = 1;
			else if (touchx > boundRight)
				mDragging = 2;
			else if ((touchx - boundLeft) < ((boundRight - boundLeft) / 3))
				mDragging = 1;
			else if ((boundRight - touchx) < ((boundRight - boundLeft) / 3))
				mDragging = 2;
		case MotionEvent.ACTION_MOVE:
			value = Math.round((touchx * range) / width) + mRangeMinValue;
			if ((mDragging == 1) && (value < mRangeRightValue)) {
				mRangeLeftValue = (int) Math.max(value, mRangeMinValue);
				if (mListener != null)
					mListener.onLeftThumbMove(this);
			}

			if ((mDragging == 2) && (value > mRangeLeftValue)) {
				mRangeRightValue = (int) Math.min(value, mRangeMaxValue);
				if (mListener != null)
					mListener.onRightThumbMove(this);
			}
			break;
		case MotionEvent.ACTION_UP:
		default:
			mDragging = 0;
			break;
		}

		invalidate();
		return true;
	}

	/**
	 * Sets a listener to receive notifications of changes to the RangeSlider's
	 * thumbs.
	 * 
	 * @param l
	 *            The seek bar notification listener
	 */
	public void setOnRangeSliderChangeListener(OnRangeSliderChangeListener l) {
		mListener = l;
	}

	/**
	 * @return the value of the left thumb on the slider
	 */
	public int getLeftValue() {
		return mRangeLeftValue;
	}

	/**
	 * @return the value of the right thumb on the slider
	 */
	public int getRightValue() {
		return mRangeRightValue;
	}

	/**
	 * @return the minimum value for the range
	 */
	public int getMinValue() {
		return mRangeMinValue;
	}

	/**
	 * @return the maximum value for the range
	 */
	public int getMaxValue() {
		return mRangeMaxValue;
	}

	/**
	 * @return the offset for the left thumb
	 */
	public int getLeftThumbOffset() {
		return mLeftThumbOffset;
	}

	/**
	 * @return the offset for the right thumb
	 */
	public int getRightThumbOffset() {
		return mRightThumbOffset;
	}

	/**
	 * @param value
	 *            the value of the left thumb
	 */
	public void setLeftValue(int value) {
		mRangeLeftValue = value;
	}

	/**
	 * @param value
	 *            the value of the right thumb
	 */
	public void setRightValue(int value) {
		mRangeRightValue = value;
	}

	/**
	 * @param minValue
	 *            the min value of the slider
	 */
	public void setMinValue(int minValue) {
		mRangeMinValue = minValue;
	}

	/**
	 * @param maxValue
	 *            the max value of the slider
	 */
	public void setMaxValue(int maxValue) {
		mRangeMaxValue = maxValue;
	}

	/**
	 * @param drawable
	 *            the drawable to use for the slider's background
	 */
	public void setBackgroundDrawable(Drawable drawable) {
		mBackgroundDrawable = drawable;
	}

	/**
	 * @param drawable
	 *            the drawable to use for the slider's foreground
	 */
	public void setForegroundDrawable(Drawable drawable) {
		mForegroundDrawable = drawable;
	}

	/**
	 * @param drawable
	 *            the drawable for the left thumb (when not pressed)
	 */
	public void setLeftDrawable(Drawable drawable) {
		mLeftDrawable = drawable;
	}

	/**
	 * @param drawable
	 *            the drawable for the right thumb (when not pressed)
	 */
	public void setRightDrawable(Drawable drawable) {
		mRightDrawable = drawable;
	}

	/**
	 * @param drawable
	 *            the drawable for the left thumb (when pressed)
	 */
	public void setLeftSelectedDrawable(Drawable drawable) {
		mLeftSelectedDrawable = drawable;
	}

	/**
	 * @param drawable
	 *            the drawable for the right thumb (when pressed)
	 */
	public void setRightSelectedDrawable(Drawable drawable) {
		mRightSelectedDrawable = drawable;
	}

	/**
	 * Sets the thumb offset that allows the left thumb to extend out of the
	 * range of the track.
	 * 
	 * @param offset
	 *            the offset amount in pixels
	 */
	public void setLeftThumbOffset(int offset) {
		mLeftThumbOffset = offset;
	}

	/**
	 * Sets the thumb offset that allows the left thumb to extend out of the
	 * range of the track.
	 * 
	 * @param offset
	 *            the offset amount in pixels
	 */
	public void setRightThumbOffset(int offset) {
		mRightThumbOffset = offset;
	}

	/**
	 * @see android.view.View#onDraw(android.graphics.Canvas)
	 */
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		int width, boundLeft, boundRight, range;

		width = (mViewWidth - mPadLeft - mPadRight);
		range = (mRangeMaxValue - mRangeMinValue);

		boundLeft = ((mRangeLeftValue - mRangeMinValue) * width) / range;
		boundRight = ((mRangeRightValue - mRangeMinValue) * width) / range;

		mForegroundDrawable.setBounds(mPadLeft + boundLeft, mPadTop + 4,
				mPadLeft + boundRight, mViewHeight - mPadBottom - 4);

		mLeftDrawable.setBounds(mPadLeft + boundLeft + mLeftThumbOffset,
				mPadTop, mPadLeft + boundLeft + mLeftThumbOffset
						+ mLeftThumbWidth, mViewHeight - mPadBottom);

		mRightDrawable.setBounds(mPadLeft + boundRight + mRightThumbOffset
				- mRightThumbWidth, mPadTop, mPadLeft + boundRight
				+ mRightThumbOffset, mViewHeight - mPadBottom);

		mBackgroundDrawable.draw(canvas);
		mForegroundDrawable.draw(canvas);
		mLeftDrawable.draw(canvas);
		mRightDrawable.draw(canvas);

		if (mDragging == 1) {
			mLeftSelectedDrawable.setBounds(mLeftDrawable.getBounds());
			mLeftSelectedDrawable.draw(canvas);
		} else if (mDragging == 2) {
			mRightSelectedDrawable.setBounds(mRightDrawable.getBounds());
			mRightSelectedDrawable.draw(canvas);
		}

	}

	/**
	 * @see android.view.View#onMeasure(int, int)
	 */
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int dw, dh;

		dw = dh = 0;
		if (mBackgroundDrawable != null) {
			dw = Math.max(dw, mBackgroundDrawable.getIntrinsicWidth());
			dh = Math.max(dh, mBackgroundDrawable.getIntrinsicHeight());
		}
		if ((mLeftDrawable != null) && (mRightDrawable != null)) {
			mLeftThumbWidth = mLeftDrawable.getIntrinsicWidth();
			mRightThumbWidth = mRightDrawable.getIntrinsicWidth();
			dw = Math.max(dw, mLeftThumbWidth + mRightThumbWidth);
			dh = Math.max(dh, Math.max(mLeftDrawable.getIntrinsicHeight(),
					mRightDrawable.getIntrinsicHeight()));
		}

		dh = Math.max(mViewMinHeight, Math.min(mViewMaxHeight, dh));

		dw += mPadLeft + mPadRight;
		dh += mPadTop + mPadBottom;

		setMeasuredDimension(resolveSize(dw, widthMeasureSpec), resolveSize(dh,
				heightMeasureSpec));
	}

	/**
	 * @see android.view.View#onSizeChanged(int, int, int, int)
	 */
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		mBackgroundDrawable.setBounds(mPadLeft, mPadTop + 4, w - mPadRight, h
				- mPadBottom - 4);

		mViewWidth = w;
		mViewHeight = h;

	}

	/**
	 * Init all values from any xml attributes or from default values
	 * 
	 * @param ctx
	 *            The Context the view is running in, through which it can
	 *            access the current theme, resources, etc.
	 * @param attrs
	 *            The attributes of the XML tag that is inflating the view.
	 */
	private void initRangeSlider(Context ctx, AttributeSet attrs) {
		int id;

		// Styleable attributes
		TypedArray a = ctx.obtainStyledAttributes(attrs,
				R.styleable.RangeSlider);

		mLeftThumbOffset = a.getDimensionPixelOffset(
				R.styleable.RangeSlider_left_thumb_offset, 0);
		mRightThumbOffset = a.getDimensionPixelOffset(
				R.styleable.RangeSlider_right_thumb_offset, 0);
		mRangeMinValue = a.getInt(R.styleable.RangeSlider_range_min_value, 0);
		mRangeMaxValue = a.getInt(R.styleable.RangeSlider_range_max_value, 100);

		id = a.getResourceId(R.styleable.RangeSlider_slider_background,
				R.drawable.slider_background);
		mBackgroundDrawable = ctx.getResources().getDrawable(id);
		id = a.getResourceId(R.styleable.RangeSlider_slider_foreground,
				R.drawable.slider_foreground);
		mForegroundDrawable = ctx.getResources().getDrawable(id);
		id = a.getResourceId(R.styleable.RangeSlider_left_thumb,
				R.drawable.seek_thumb_normal);
		mLeftDrawable = ctx.getResources().getDrawable(id);
		id = a.getResourceId(R.styleable.RangeSlider_right_thumb,
				R.drawable.seek_thumb_normal);
		mRightDrawable = ctx.getResources().getDrawable(id);
		id = a.getResourceId(R.styleable.RangeSlider_left_thumb_pressed,
				R.drawable.seek_thumb_pressed);
		mLeftSelectedDrawable = ctx.getResources().getDrawable(id);
		id = a.getResourceId(R.styleable.RangeSlider_right_thumb_pressed,
				R.drawable.seek_thumb_pressed);
		mRightSelectedDrawable = ctx.getResources().getDrawable(id);

		a.recycle();

		// private members
		mDragging = 0;
		mRangeLeftValue = mRangeMinValue;
		mRangeRightValue = mRangeMaxValue;

		// TODO read attributes for width / height and Padding
		mViewMinWidth = 24;
		mViewMaxWidth = 48;
		mViewMinHeight = 24;
		mViewMaxHeight = 48;
	}

	private OnRangeSliderChangeListener mListener;

	private int mRangeLeftValue;
	private int mRangeRightValue;

	private int mRangeMinValue, mRangeMaxValue;

	private int mViewMinWidth, mViewMaxWidth;
	private int mViewMinHeight, mViewMaxHeight;
	private int mViewWidth, mViewHeight;

	private int mLeftThumbWidth, mRightThumbWidth;

	private int mLeftThumbOffset, mRightThumbOffset;

	private int mPadTop, mPadLeft, mPadBottom, mPadRight;

	private Drawable mBackgroundDrawable, mForegroundDrawable;
	private Drawable mLeftDrawable, mRightDrawable;
	private Drawable mLeftSelectedDrawable, mRightSelectedDrawable;

	private int mDragging;
}
