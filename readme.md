GWSLAF
---

GWSLAF, an android project library to make it easier to keep an android 
application's look and feel consistent across multiple android OS 
versions and OEM skins.

# Requirements

Compiler set to 1.6 and target api set to the highest, at this time 15.
Right click and set project android lint properties to ignore all.

# License

[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

# Credits

I have incoroprated these projects into this one:

[ActionbarSherlock]()

[HanselAndGredel]()



[HoloEveryWhere]()
